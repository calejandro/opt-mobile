package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.usescases;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.repository.ObserverModeRepository;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.presenter.IObserverModeRequestHandler;

import java.util.List;

public class ObserverModeInteractor implements IObserverModeRequestHandler {

    private ObserverModeRepository repository;

    public ObserverModeInteractor() {
        this.repository = new ObserverModeRepository();
    }

    @Override
    public boolean isNecesaryShowDialog() {
        return repository.isNecesaryShowDialog();
    }

    @Override
    public void setNecesaryShowDialog(boolean isNecesary) {
        repository.setIsNecessaryShowDialog(!isNecesary);
    }

    @Override
    public void doSelectLines(@NonNull List<LineasProduccionDto> listLines) {
        repository.setLocalLines(listLines);
        repository.setUserModeSelected();
        //repository.setPlantSelected(listLines.get(0).getProductionPlant());
    }
}
