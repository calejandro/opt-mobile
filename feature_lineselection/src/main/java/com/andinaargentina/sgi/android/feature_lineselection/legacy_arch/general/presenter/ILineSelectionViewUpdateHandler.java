package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter;

import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.ui_sdk.components.base.connection.IConnectionStatusViewUpdateHandler;

import java.util.List;

/**
 * Esta interfaz define la comunicación
 * entre el presenter y la vista, es decir la actualización
 * de la vista.
 * Esta interfaz la debe implementar la vista
 */
public interface ILineSelectionViewUpdateHandler extends IConnectionStatusViewUpdateHandler{

    void hideSelectSiteDialog();

    void showSelectSiteDialog(@NonNull List<String> lisNames, int positionSelected);

    void changeToolbarTitle(@NonNull String name);

    void resetView(boolean isObserverMode);

    void finishView();

    Context getContext();

    void updateDataOnView(@NonNull List<LineasProduccionDto> listLines);

    void createObserverMode();

    void createSupervisorMode();
}
