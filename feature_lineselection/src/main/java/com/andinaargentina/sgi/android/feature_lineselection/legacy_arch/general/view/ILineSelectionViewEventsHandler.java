package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view;

public interface ILineSelectionViewEventsHandler {

    /**
     * La vista se debe mostrar sin datos
     * locales, ya que es la primera vez
     * que el usuario la va a ver
     */
    void onNewSesionView();

    /**
     * La vista debe recargar
     * datos cacheados, ya que en esta
     * instancia el usuario posee una
     * selección realizada
     */
    void onChangeSesionView();

    /**
     * Intentar traer datos desde internet
     */
    void onTryGetLinesRequest();

    /**
     * Se abrió el dialogo de selección
     * de sites
     */
    void onSiteDialogOppened();

    /**
     * Se seleccionó un site
     */
    void onSiteSelected(int positionSelected);


}
