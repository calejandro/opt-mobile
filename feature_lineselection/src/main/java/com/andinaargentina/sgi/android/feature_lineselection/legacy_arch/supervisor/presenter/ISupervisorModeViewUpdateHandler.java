package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.presenter;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.ui_sdk.components.dialog.ObserverModeDialog;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.LineWithOrderDialogModel;

import java.util.List;

/**
 * Operaciones de vista para el modo observacion
 */
public interface ISupervisorModeViewUpdateHandler {

    //provisorio
    void onInitPostView(@NonNull List<LineasProduccionDto> listLines);

    void setToggleOnSupervisorMode();

    void showDialogSupervisorMode(ObserverModeDialog.DialogClickHandler listener,
                                  List<LineWithOrderDialogModel> listLines);

    void showDialogObserverMode(ObserverModeDialog.DialogClickHandler listener);

    void hideDialogObserverMode();

    void setTextBtnLineSelectedOnSupervisorMode();

    void showLines(@NonNull List<LineasProduccionDto> listData, List<LineasProduccionUsuarioDto> lineasProduccionUsuarioDtos);

    void showEmptyState();

    void hideEmptyState();

    void enableBtnSelectLines(boolean enable);

}
