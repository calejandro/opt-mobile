package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.viewmodel;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class LineSelectionViewModel implements IBaseViewModel {

    public List<LineasProduccionDto> listLines;
    public boolean isObserverMode;
    public List<PlantasDto> listPlantV2;
    public PlantasDto plantSelected;

    public int getPositionOfPlantSelected(){
        int position = 0;
        if (listPlantV2!=null && plantSelected!=null){
            for (PlantasDto plantDto: listPlantV2) {
                if (plantSelected.getIdPlanta() != plantDto.getIdPlanta()) {
                    position++;
                }else{
                    break;
                }
            }
        }
        return position;
    }

    public List<String> getPlantNames(){
        List<String> listNames = new ArrayList<>();
        if (listPlantV2!=null){
            for (PlantasDto plantDto: listPlantV2) {
                listNames.add(plantDto.getDescPlanta());
            }
        }
        return listNames;
    }

    @NotNull
    @Override
    public String name() {
        return "LineSelectionViewModel";
    }
}
