package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.usescases;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.ProductionPlant;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

import java.util.List;

public interface IObserverModeTransactionHandler {

    void setLocalLines(@NonNull List<LineasProduccionDto> listLines);

    @NonNull
    List<LineasProduccionDto> getLocalLines();

    void setIsNecessaryShowDialog(boolean remember);

    boolean isNecesaryShowDialog();

    void setUserModeSelected();

    void setPlantSelected(@NonNull ProductionPlant plantDto);

}
