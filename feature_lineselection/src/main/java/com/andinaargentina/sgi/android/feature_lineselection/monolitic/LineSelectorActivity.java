package com.andinaargentina.sgi.android.feature_lineselection.monolitic;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.events.lineSelector.LineSelectorSuccessEvent;
import com.andinaargentina.core_services.events.plants.GetPlantsSuccessEventV2;
import com.andinaargentina.core_services.providers.api.clients.lineselection.LineSelectionService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.delete.DeleteLineSelectedService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.modify.LineModifyService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.clients.logout.LogoutService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.api.core.util.NetworkUtils;
import com.andinaargentina.core_services.providers.apiv2.clients.PlantListServiceV2;
import com.andinaargentina.core_services.providers.apiv2.events.LineTakenListEvent;
import com.andinaargentina.core_services.providers.apiv2.events.ShiftsListSuccessEvent;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.sgi.android.feature_lineselection.R;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.repository.LineSelectionRepository;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.adapter.LineSelectionAdapter;
import com.andinaargentina.sgi.android.feature_lineselection.utils.Utils;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.andinaargentina.sgi.core_sdk.base.times.UtilTimes;
import com.andinaargentina.sgi.core_sdk.base.workers.GoToLoginEvent;
import com.andinaargentina.ui_sdk.components.base.connection.ConnectionViewDelegate;
import com.andinaargentina.ui_sdk.components.dialog.ChangeSiteDialog;
import com.andinaargentina.ui_sdk.components.dialog.ObserverModeDialog;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.LineWithOrderDialogModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.TakeLineConfirmDialog;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import io.paperdb.Paper;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.andinaargentina.RepositoryConst.IS_OBSERVER_MODE;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.adapter.LineSelectionHolder.KEY_CHEKED;
import static com.andinaargentina.sgi.core_sdk.base.times.UtilTimes.MANANA;
import static com.andinaargentina.sgi.core_sdk.base.times.UtilTimes.NOCHE;
import static com.andinaargentina.sgi.core_sdk.base.times.UtilTimes.TARDE;

@Route(path = ViewsNames.ROUTE_VIEW_NAME_LINE_SELECTOR)
@RequiresApi(api = Build.VERSION_CODES.N)
public class LineSelectorActivity extends AppCompatActivity implements
        BaseAdapter.IHolderItemClick {

    public static final String WITH_BACK_BUTTON = "withBackButton";
    public static final String IS_SUPERVISOR_MODE = "isSupervisorMode";

    //<editor-fold desc="Declaracion de views">

    //Toolbar
    private ToggleButton toggleButton;
    private ImageView btnBack;
    private View imgChangeSite;
    private TextView titleBar;
    private TextView txtTitleTop;

    //BtnSelectLines
    private TextView txtBtnVisualizarLines;
    private View btnVisualizarLineas;
    private ImageView imgBtnVisualizarLines;

    //List
    private LineSelectionAdapter adapter;
    private RecyclerView recyclerView;


    //Dialog
    private ChangeSiteDialog changeSiteDialog = null;
    private TakeLineConfirmDialog takeLineConfirmDialog = null;
    private ObserverModeDialog observerDialog = null;

    //Others
    private LineSelectionMonoliticViewModel viewModel = null;
    private ConnectionViewDelegate connectionViewDelegate = null;

    private LineSelectionRepository repository;

    //EmpyState
    private View viewEmptyLabel;
    private View bntChangeModeEmptyState;

    private Button btnSelectAll;

    private LogoutService logoutService;

    private CountDownLatch finishDelete = new CountDownLatch(1);
    private CountDownLatch finishModify = new CountDownLatch(1);

    //</editor-fold>

    //<editor-fold desc="Base actions"

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_selector);
        if(UtilSesion.checkSession(this)){
            return;
        }
        registerObserver();

        //Init dependencies
        initDependencies();
        initViewMode();
        initViews();
    }

    public void onClickLogout(View view){
        this.logoutService = new LogoutService(ApiRestModule.providesApiService(false));
        (new UtilSesion()).logout(this, false);
        UtilSesion.clearSession();
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(this);
        finish();
    }

    private void initDependencies() {
        this.viewModel = new LineSelectionMonoliticViewModel();
        this.repository = new LineSelectionRepository(
                new LineSelectionService(),
                new PlantListServiceV2(),
                new LineSelectedService(ApiRestModule.providesApiService(false)),
                new LineModifyService(ApiRestModule.providesApiService(false)),
                new DeleteLineSelectedService(ApiRestModule.providesApiService(false)),
                new CompositeDisposable());
        this.connectionViewDelegate = new ConnectionViewDelegate(this);
        this.connectionViewDelegate.onCreatedView();
    }

    private void initViewMode() {
        Intent intent = getIntent();
        boolean showBackButton = intent.getBooleanExtra(WITH_BACK_BUTTON,false);
        viewModel.setShowBackButton(showBackButton);
        boolean isSupervisorMode = Prefs.getBoolean(IS_OBSERVER_MODE, true);
        viewModel.setSupervisorMode(isSupervisorMode);
        this.popupViewModel();
    }

    private void popupViewModel() {
        if (repository.getPlantSelectedV2() != null){
            this.viewModel.setPlantSelectedV2(repository.getPlantSelectedV2());
            this.viewModel.setListLines(repository.getLocalLines());
            this.viewModel.setListPlantV2(repository.getLocalPlantsV2());
            this.viewModel.setSupervisorMode(repository.isSupervisorMode());
            int countSelected = 0;
            for (LineasProduccionDto lineDto: repository.getLocalLines()) {
                if (lineDto.isItemChecked())
                    countSelected++;
            }
            this.viewModel.setSelectedCount(countSelected);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void initViews(){
        //Toolbar
        toggleButton = findViewById(R.id.line_selector_toggle);
        imgChangeSite = findViewById(R.id.toolbar_img);
        btnBack = findViewById(R.id.toolbar_menu);
        titleBar = findViewById(R.id.toolbar_menu_title);
        txtTitleTop = findViewById(R.id.line_selector_title);

        //EmpyState
        viewEmptyLabel = findViewById(R.id.line_selector_empty_state);
        bntChangeModeEmptyState = findViewById(R.id.empty_state_btn_visualize_lines);

        //List
        this.recyclerView = findViewById(R.id.line_selector_recycler);

        //BtnSelectLines
        this.txtBtnVisualizarLines = findViewById(R.id.line_selector_txt_btn_select);
        this.btnVisualizarLineas = findViewById(R.id.line_selector_container_select);
        this.imgBtnVisualizarLines = findViewById(R.id.line_selector_img_btn_select);

        this.btnSelectAll = findViewById(R.id.line_selector_btn_select_all);

        //Dialogs
        setOnClicksListeners();
        setBackButton();
        setToolbarName();
        if (!this.repository.isUserSupervisor()){
            setMode(false);
            this.toggleButton.setVisibility(View.GONE);
            this.viewModel.setSupervisorMode(false);
            txtTitleTop.setText("Usuario no supervisor, solo puede visualizar");
        }else{
            setMode(this.viewModel.isSupervisorMode());
        }
        //LLamo a las plantas
        this.callPlantsWebService();
    }

    private void registerObserver() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterObserver();
    }

    private void unregisterObserver() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    private void setToolbarName() {
        if (viewModel.getPlantSelectedV2() != null){
            changeToolbarTitle(viewModel.getPlantSelectedV2().getDescPlanta());
        }
    }

    private void callPlantsWebService() {
        this.connectionViewDelegate.showProgressDialog();
        this.repository.getTurnos(this);
    }

    private void callGetLinesWebService(){
        this.connectionViewDelegate.showProgressDialog();
        repository.getLines(String.valueOf(this.viewModel.getPlantSelectedV2().getIdPlanta()), this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setMode(boolean isSupervisorMode) {
        setToggleButton(isSupervisorMode);
        setTextBtnLineSelect(isSupervisorMode);
        setBtnSelectAll(isSupervisorMode);
        if (this.viewModel.getListLines() == null ||
            this.viewModel.getListLines().isEmpty()){
            showEmptyState();
        }else {
            hideEmptyState();
            this.showLines(this.viewModel.getListLines(), this.viewModel.getLinesTaken(), this.viewModel.isObserverMode());
        }
        this.enableBtnSelectLines((this.viewModel.getSelectedCount())!=0);
        //recargar adapter según corresponda
    }


    public void setToggleButton(boolean isSupervisorMode){
        this.toggleButton.setChecked(!isSupervisorMode);
    }

    public void setTextBtnLineSelect(boolean isSupervisorMode) {
        if (isSupervisorMode){
            Picasso.with(this).load(R.drawable.i_line_taked).into(this.imgBtnVisualizarLines);
            this.txtBtnVisualizarLines.setText("Tomar líneas");
        }else{
            Picasso.with(this).load(R.drawable.i_eye).into(this.imgBtnVisualizarLines);
            this.txtBtnVisualizarLines.setText("Visualizar lineas");
        }
    }

    public void setBtnSelectAll(boolean isSupervisor){
        if (isSupervisor){
            this.btnSelectAll.setVisibility(View.GONE);
            this.txtTitleTop.setText("Lineas para supervisar");
        }else{
            this.btnSelectAll.setVisibility(View.VISIBLE);
            this.txtTitleTop.setText("Lineas para visualizar");
        }
    }

    public void enableBtnSelectLines(boolean enable) {
        int colorSelected = this.getResources().getColor(R.color.color_txt_red_disable);
        if (enable) colorSelected = this.getResources().getColor(R.color.colorWhite);
        ImageViewCompat.setImageTintList(this.imgBtnVisualizarLines, ColorStateList.valueOf(colorSelected));
        this.txtBtnVisualizarLines.setTextColor(colorSelected);
        this.btnVisualizarLineas.setEnabled(enable);
    }

    public void showSelectSiteDialog(@NonNull List<String> lisNames, int positionSelected) {
        String message = getString(R.string.line_select_site_dialog_description);
        String title = getString(R.string.line_select_site_dialog_title);
        String ok = getString(R.string.line_select_dialog_btn_ok);
        changeSiteDialog = new ChangeSiteDialog(this, title, message,"Cancelame",ok,
                lisNames, positionSelected);
        changeSiteDialog.setClickHandler(new ChangeSiteDialog.ChangeSiteDialogClickHandler() {
            @Override
            public void positiveClick(int positionSelected) {
                LineSelectorActivity.this.viewModel.setPlantSelectedV2(
                        LineSelectorActivity.this.viewModel.getListPlantV2().get(positionSelected));
                LineSelectorActivity.this.callGetLinesWebService();
                LineSelectorActivity.this.changeToolbarTitle(
                        LineSelectorActivity.this.viewModel.getPlantSelectedV2().getDescPlanta());
                Utils.setCurrentPlant(LineSelectorActivity.this.getApplicationContext(), LineSelectorActivity.this.viewModel.getPlantSelectedV2().getIdPlanta());
                changeSiteDialog.dismiss();
                changeSiteDialog.setClickHandler(null);
                changeSiteDialog = null;
            }

            @Override
            public void closeClick() {
                changeSiteDialog.dismiss();
                changeSiteDialog.setClickHandler(null);
                changeSiteDialog = null;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        UtilSesion.checkSession(this);
    }

    public void changeToolbarTitle(@NonNull String name) {
        titleBar.setText(name);
    }

    public void showLines(@NonNull List<LineasProduccionDto> listData, List<LineasProduccionUsuarioDto> listLineTaken, boolean isObserverMode) {
        this.recyclerView.setVisibility(View.VISIBLE);
        adapter = new LineSelectionAdapter(listData,listLineTaken, this,isObserverMode);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                RecyclerView.VERTICAL, false);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
    }

    public void showEmptyState() {
        recyclerView.setVisibility(View.INVISIBLE);
        viewEmptyLabel.setAlpha(1.0f);
        viewEmptyLabel.setVisibility(View.VISIBLE);
    }

    public void hideEmptyState() {
        viewEmptyLabel.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                viewEmptyLabel.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onItemClick(View caller, int position, @Nullable Bundle bundle) {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED, null);
        if (bundle!=null && !bundle.isEmpty()){
            LineasProduccionDto lineasProduccionDto = viewModel.getListLines().get(position);
            boolean checked = bundle.getBoolean(KEY_CHEKED);
            lineasProduccionDto.setItemChecked(checked);
            if (this.viewModel.isSupervisorMode() && this.viewModel.getSelectedCount() == 3 && checked){
                lineasProduccionDto.setItemChecked(false);
            } else {
                if (checked) {
                    this.viewModel.upCountSelected();
                    LineasProduccionUsuarioDto lineaUsuario = new LineasProduccionUsuarioDto(user.getUserId(), lineasProduccionDto.getIdLinea(), lineasProduccionDto, viewModel.isSupervisorMode());
                    lineaUsuario.setSysUsers(user);
                    viewModel.getLinesTaken().add(lineaUsuario);
                } else {
                    this.viewModel.downCountSelected();
                    Optional<LineasProduccionUsuarioDto> lineaUsuario = viewModel.getLinesTaken().stream().filter(f -> f.getIdLinea() == lineasProduccionDto.getIdLinea()).findAny();
                    lineasProduccionDto.setLineasProduccionUsuarioDto(null);
                    if(lineaUsuario.isPresent()) {
                        viewModel.getLinesTaken().remove(lineaUsuario.get());
                    }
                }
            }


            if (this.viewModel.isSupervisorMode()){
                checkLinesEnabledSupervisorMode();
            }
            this.enableBtnSelectLines(this.viewModel.getSelectedCount() > 0);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void checkLinesEnabledSupervisorMode() {
        if (viewModel.getSelectedCount()==3){
            for (LineasProduccionDto lineDto: viewModel.getListLines()) {
                if (!lineDto.isItemChecked()){
                    lineDto.setItemEnabled(false);
                }
            }
            this.adapter.notifyDataSetChanged();
        } else {
            for (LineasProduccionDto lineDto: viewModel.getListLines()) {
                lineDto.setItemEnabled(true);
            }
            this.adapter.notifyDataSetChanged();
        }
    }

    public void showTakeLineConfirmDialog(List<LineWithOrderDialogModel> listNames) {
        String message = this.getString(R.string.line_select_dialog_description);
        String title = this.getString(R.string.line_select_dialog_title);
        String ok = this.getString(R.string.line_select_dialog_btn_ok);

        if (takeLineConfirmDialog==null)
            takeLineConfirmDialog = new TakeLineConfirmDialog(this, this.viewModel.getLinesTakenMe(), null);
        takeLineConfirmDialog.setClickHandler(new TakeLineConfirmDialog.DialogTakeLineClickHandler() {
            @Override
            public void positiveClick(@Nullable TakeLineConfirmDialog.LineAndOrderSelected line1Selected,
                                      @Nullable TakeLineConfirmDialog.LineAndOrderSelected line2Selected,
                                      @Nullable TakeLineConfirmDialog.LineAndOrderSelected line3Selected) {

                if(line1Selected != null) {
                    viewModel.getLinesTakenMe().stream().filter(f -> f.getIdLinea() == Integer.valueOf(line1Selected.getLineId())).findFirst().get()
                            .setIdOrdenSap(Integer.valueOf(line1Selected.getOrderId()));
                }

                if(line2Selected != null) {
                    viewModel.getLinesTakenMe().stream().filter(f -> f.getIdLinea() == Integer.valueOf(line2Selected.getLineId())).findFirst().get()
                            .setIdOrdenSap(Integer.valueOf(line2Selected.getOrderId()));
                }

                if(line3Selected != null) {
                    viewModel.getLinesTakenMe().stream().filter(f -> f.getIdLinea() == Integer.valueOf(line3Selected.getLineId())).findFirst().get()
                            .setIdOrdenSap(Integer.valueOf(line3Selected.getOrderId()));
                }

                takeLineConfirmDialog.dismiss();
                takeLineConfirmDialog = null;
                repository.setNecesaryShowConfirmTakeDialog(true);
                showSelectHourDialog();
            }

            @Override
            public void negativeClick() {
                takeLineConfirmDialog.dismiss();
                takeLineConfirmDialog = null;
            }
        });
        takeLineConfirmDialog.show();
    }

    public void showDialogObserverMode() {
        String message = this.getString(R.string.line_select_dialog_description);
        String title = this.getString(R.string.line_select_dialog_title);
        String ok = this.getString(R.string.line_select_dialog_btn_ok);
        observerDialog = new ObserverModeDialog(this, title, message,"Cancelame",ok);
        observerDialog.setClickHandler(new ObserverModeDialog.DialogClickHandler() {
            @Override
            public void positiveClick(boolean isChecked) {
                observerDialog.dismiss();
                observerDialog = null;
                repository.setNecessaryShowObserverModeDialog(!isChecked);
                LineSelectorActivity.this.viewModel.changeMode();
                LineSelectorActivity.this
                        .setMode(LineSelectorActivity.this.viewModel.isSupervisorMode());
                LineSelectorActivity.this.connectionViewDelegate.showProgressDialog();
                LineSelectorActivity.this.repository.getLines(
                        String.valueOf(LineSelectorActivity.this.viewModel.getPlantSelectedV2().getIdPlanta()),
                        LineSelectorActivity.this);
            }

            @Override
            public void negativeClick() {
                observerDialog.dismiss();
                observerDialog = null;
            }
        });
        observerDialog.show();
    }

    private void setBackButton() {
        if (viewModel.isShowBackButton()){
            btnBack.setImageResource(R.drawable.ic_back);
            this.btnBack.setOnClickListener(v -> finish());
        }
        else{
            btnBack.setImageResource(R.drawable.ic_power_settings_new_black_24dp);
            btnBack.setOnClickListener(view -> onClickLogout(view));
        }
    }


    private void setOnClicksListeners() {
        this.bntChangeModeEmptyState.setOnClickListener(v -> onChangeModeButton());
        this.toggleButton.setOnClickListener(v -> {
            if (!this.repository.isUserSupervisor()){
                Toast.makeText(this,"El usuario no es un usuario supervisor",Toast.LENGTH_LONG).show();
            }else{
                onChangeModeButton();
            }
        });

        this.imgChangeSite.setOnClickListener((v) -> showSelectSiteDialog(this.viewModel.getPlantNames(), this.viewModel.getPositionOfPlantSelected()));

        this.btnVisualizarLineas.setOnClickListener(v -> {
            if (this.viewModel.isSupervisorMode() && repository.isNecesaryShowConfirmTakeDialog()){
                this.showTakeLineConfirmDialog(this.viewModel.getListLinesSelectedName());
            }else{
                this.acceptSelection(null);
            }
        });

        this.btnSelectAll.setOnClickListener(v -> {
            this.btnSelectAll.setActivated(!this.btnSelectAll.isActivated());
            if (this.btnSelectAll.isActivated()){
                selectAllLines();
                this.btnSelectAll.setText("Deseleccionar todas las líneas");
            }else{
                unselectAllLines();
                this.btnSelectAll.setText("Seleccionar toda la planta");
            }
        });
    }

    private void selectAllLines() {
        int cantItemsChecked = this.adapter.selectAllLines();
        for (int i = 0; i < cantItemsChecked; i++) {
            viewModel.upCountSelected();
        }
        this.enableBtnSelectLines(viewModel.getSelectedCount()>0);
    }

    private void unselectAllLines(){
        this.adapter.unSelectAllLines();
        viewModel.setSelectedCount(0);
        this.enableBtnSelectLines(viewModel.getSelectedCount()>0);
    }

    private void acceptSelection(Date shiftConfirmationDate) {
        checkAndExecuteEndpoints(shiftConfirmationDate);
    }

    private boolean hasPendingRequest = false;
    private Date shiftConfirmationDate;

    private void checkAndExecuteEndpoints(Date shiftConfirmationDate) {
        hasPendingRequest = false;
        this.shiftConfirmationDate = shiftConfirmationDate;
        int userId = this.repository.getUserId();
        LineSelectionAdapter adapter1 = (LineSelectionAdapter) this.recyclerView.getAdapter();
        selectionLinesForSupervisorMode(userId, adapter1, shiftConfirmationDate);
    }

    private void unselectAllLinesForVisualize(int userId, LineSelectionAdapter adapter1,Date shiftConfirmationDate){
        List<SelectedDto> listLinesForUnselect = new ArrayList<>();
        for (LineasProduccionDto lineDto: this.viewModel.getListLines()) {
            /*if (lineDto.getVisualizedBy() !=null){
                for (UserDto userVisualization: lineDto.getVisualizedBy()) {
                    if (userVisualization.getId() == userId){
                        listLinesForUnselect.add(new SelectedDto(lineDto.getId(), userId,
                                lineDto.getProductionOrderIdAssigned(), shiftConfirmationDate,
                                lineDto.getIdTakenLine()));
                    }
                }
            }*/
        }
        LineSelectedDto requestDtoForUnselect;
        if (!listLinesForUnselect.isEmpty()){
            requestDtoForUnselect = new LineSelectedDto(this.viewModel.isSupervisorMode(), listLinesForUnselect);
            postDeleteLineSelected(requestDtoForUnselect);
        }
    }

    private void unselectAllLinesForSupervisor(int userId, LineSelectionAdapter adapter1,Date shiftConfirmationDate){
        List<SelectedDto> listLinesForUnselect = new ArrayList<>();
        /*for (LineasProduccionDto lineDto: adapter1.getListData()) {
            if (lineDto.getTakenBy() !=null && (lineDto.getTakenBy().getId() == userId)){
                listLinesForUnselect.add(new SelectedDto(lineDto.getId(), userId,
                        lineDto.getProductionOrderIdAssigned(), shiftConfirmationDate,
                        lineDto.getIdTakenLine()));
            }
        }*/
        LineSelectedDto requestDtoForUnselect;
        if (!listLinesForUnselect.isEmpty()){
            requestDtoForUnselect = new LineSelectedDto(this.viewModel.isSupervisorMode(), listLinesForUnselect);
            postDeleteLineSelected(requestDtoForUnselect);
        }
    }

    private void selectionLinesForSupervisorMode(int userId, LineSelectionAdapter adapter1,Date shiftConfirmationDate) {
        List<Observable<LineasProduccionUsuarioDto>> observables = new ArrayList<>();
        List<LineasProduccionUsuarioDto> linesTakenMe = new ArrayList<>(this.viewModel.getLinesTakenMe());
        List<Observable<LineasProduccionUsuarioDto>> observableList = new ArrayList<>();
        for (LineasProduccionUsuarioDto lineaUsuario:
             linesTakenMe) {
            lineaUsuario.setLineaProduccion(null);
            lineaUsuario.setSapOrdenesProduccion(null);
            lineaUsuario.setFechaToma(shiftConfirmationDate);
            lineaUsuario.setUserId(userId);
            lineaUsuario.setSupervisada(this.viewModel.isSupervisorMode());
            lineaUsuario.setOrders(null);
            if(!viewModel.isSupervisorMode()) {
                lineaUsuario.setFechaToma(new Date());
                Integer ordenSap = viewModel.getLinesTaken().stream().filter(f -> f.getIdLinea() == lineaUsuario.getIdLinea() && f.getSupervisada() == true)
                        .findAny().get().getIdOrdenSap();
                lineaUsuario.setIdOrdenSap(ordenSap);
            }
        }

        OptimusServices.getInstance().getLineasProduccionUsuarioService().take(linesTakenMe).enqueue(new Callback<List<LineasProduccionUsuarioDto>>() {
            @Override
            public void onResponse(Call<List<LineasProduccionUsuarioDto>> call, Response<List<LineasProduccionUsuarioDto>> response) {
                List<LineasProduccionDto> lineasProduccionDtos = new ArrayList<>();
                for (LineasProduccionUsuarioDto lineasProduccionUsuarioDto:
                        viewModel.getLinesTakenMe()) {
                    LineasProduccionDto lineasProduccionDto = viewModel.getListLines().stream().filter(f -> f.getIdLinea() == lineasProduccionUsuarioDto.getIdLinea())
                            .findFirst().get();
                    lineasProduccionDtos.add(lineasProduccionDto);
                }
                repository.setPlantSelectedV2(viewModel.getPlantSelectedV2());
                repository.putSelectLines(viewModel.getListLines());
                repository.setSupervisorMode(viewModel.isSupervisorMode());
                repository.putSelectLines(lineasProduccionDtos);
                LineSelectorActivity.this.finish();
                ARouter.getInstance()
                        .build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                        .withFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .navigation(LineSelectorActivity.this);

            }

            @Override
            public void onFailure(Call<List<LineasProduccionUsuarioDto>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });


        /*for (LineasProduccionUsuarioDto lineasProduccionUsuarioDto:
             this.viewModel.getLinesTakenMe()) {
            if(lineasProduccionUsuarioDto.getId() == 0) {
                observables.add(OptimusServices.getInstance().getLineasProduccionUsuarioService().Create(lineasProduccionUsuarioDto));
            }
        }


        Observable.zipIterable(observables, new Function<Object[], List<LineasProduccionUsuarioDto>>() {
                    @Override
                    public List<LineasProduccionUsuarioDto> apply(@io.reactivex.annotations.NonNull Object[] objects) throws Exception {
                        return new ArrayList<>();
                    }
                }, false ,1).subscribe(new Consumer<List<LineasProduccionUsuarioDto>>() {
            @Override
            public void accept(List<LineasProduccionUsuarioDto> lineasProduccionUsuarioDtos) throws Exception {
                System.out.println("1");
            }
        });*/

                /*repository.putSelectLines(localseLines);
        this.connectionViewDelegate.hideProgressDialog();
        this.finish();
        ARouter.getInstance()
                .build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                .withFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .navigation(this);*/
    }

    private void modifyLinesForSupervisorMode(int userId, LineSelectionAdapter adapter1,Date shiftConfirmationDate) {
        //GetLines for unselect
        List<SelectedDto> listLinesForUnselect = new ArrayList<>();
        /*for (LineasProduccionDto lineDto: adapter1.getListData()) {
            if (lineDto.getTakenBy() !=null && (lineDto.getTakenBy().getId() == userId) && !lineDto.isItemChecked()){
                listLinesForUnselect.add(new SelectedDto(lineDto.getId(), userId,
                        lineDto.getProductionOrderIdAssigned(), shiftConfirmationDate,
                        lineDto.getIdTakenLine()));
            }
        }*/
        if (!listLinesForUnselect.isEmpty()){
            LineSelectedDto requestDtoForUnselect = new LineSelectedDto(this.viewModel.isSupervisorMode(), listLinesForUnselect);
            postDeleteLineSelected(requestDtoForUnselect);
        }
        //

        //GetLines for modify
        List<SelectedDto> listLinesForModify = new ArrayList<>();
        /*for (LineasProduccionDto lineDto: adapter1.getListData()) {
            if (lineDto.getTakenBy() !=null && (lineDto.getTakenBy().getId() == userId) && (lineDto.getProductionOrderIdAssigned() != lineDto.orderIdSelected)){
                listLinesForModify.add(new SelectedDto(lineDto.getId(), userId,
                        lineDto.orderIdSelected, shiftConfirmationDate,
                        lineDto.getIdTakenLine()));
            }
        }*/
        if (!listLinesForModify.isEmpty()){
            LineSelectedDto requestDtoForModify = new LineSelectedDto(this.viewModel.isSupervisorMode(), listLinesForModify);
            postChangeOrder(requestDtoForModify);
        }
        //
    }

    private void modifyLinesForVisualizationMode(int userId, LineSelectionAdapter adapter1,Date shiftConfirmationDate) {
        //

        //GetLines for unselect
        List<SelectedDto> listLinesForUnselect = new ArrayList<>();
        /*for (LineasProduccionDto lineDto: adapter1.getListData()) {
            if (!lineDto.isItemChecked() && lineDto.getVisualizedBy() !=null){
                for (UserDto userVisualization: lineDto.getVisualizedBy()) {
                    if (userVisualization.getId() == userId){
                        listLinesForUnselect.add(new SelectedDto(lineDto.getId(), userId,
                                lineDto.getProductionOrderIdAssigned(), shiftConfirmationDate,
                                lineDto.getIdTakenLine()));
                    }
                }
            }
        }*/
        if (!listLinesForUnselect.isEmpty()){
            LineSelectedDto requestDtoForUnselect = new LineSelectedDto(this.viewModel.isSupervisorMode(), listLinesForUnselect);
            postDeleteLineSelected(requestDtoForUnselect);
        }
    }

    private void postChangeOrder(LineSelectedDto dto){
        finishModify = new CountDownLatch(1);
        hasPendingRequest = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                LineSelectorActivity.this.repository.postModifyLine(dto,LineSelectorActivity.this);
            }
        }).start();
        try {
            finishModify.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void postSelectNewLine(LineSelectedDto dto){
        hasPendingRequest = true;
        this.repository.postSelection(dto, this);
    }

    private void getSelectNewLine(boolean isSpervicion){
        hasPendingRequest = true;
        this.repository.getSelection(isSpervicion, this);
    }

    private void postDeleteLineSelected(LineSelectedDto idLines){
        finishDelete = new CountDownLatch(1);
        hasPendingRequest = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                LineSelectorActivity.this.repository.postDeleteLine(idLines,LineSelectorActivity.this);
            }
        }).start();
        try {
            finishDelete.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void onChangeModeButton() {
        if (this.viewModel.isSupervisorMode()
                && repository.isNecessaryShowObserverModeDialog()){
            showDialogObserverMode();
        }else{
            this.viewModel.changeMode();
            setMode(this.viewModel.isSupervisorMode());LineSelectorActivity.this.connectionViewDelegate.showProgressDialog();
            this.repository.getLines(String.valueOf(this.viewModel.getPlantSelectedV2().getIdPlanta()), this);
        }
    }

    private void showToast(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }

    //Event bus

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getTurnosListSuccess(ShiftsListSuccessEvent response) {
        this.viewModel.setTurno(response.getResponse());
        this.repository.getPlants(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getPlantsSuccessV2(GetPlantsSuccessEventV2 event) {
        if (event.getResponse()!= null && !event.getResponse().isEmpty()){
            repository.setLocalPlantsV2(event.getResponse());
            long currentPlant = Utils.getCurrentPlant(LineSelectorActivity.this.getApplicationContext());
            PlantasDto plantSelected = event.getResponse().get(0);
            if(currentPlant != 0){
                for(PlantasDto plantDto: event.getResponse()){
                    if(plantDto.getIdPlanta() == currentPlant){
                        plantSelected = plantDto;
                    }
                }
            }
            repository.setPlantSelectedV2(plantSelected);
            this.viewModel.setListPlantV2(event.getResponse());
            this.viewModel.setPlantSelectedV2(plantSelected);
            changeToolbarTitle(this.viewModel.getPlantSelectedV2().getDescPlanta());
            this.repository.getLines(String.valueOf(this.viewModel.getPlantSelectedV2().getIdPlanta()),
                    this);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getLineTakenSuccess(LineTakenListEvent event) {
        this.viewModel.setLinesTaken(event.getResponse());
            if (this.viewModel.getListLines() == null ||
                this.viewModel.getListLines().isEmpty()){
            this.showEmptyState();
        }else{
            this.hideEmptyState();
            this.showLines(this.viewModel.getListLines(), this.viewModel.getLinesTaken(), this.viewModel.isObserverMode());
        }
        if (repository.isSupervisorMode()){
            if (this.viewModel.getSelectedCount() > 0 && this.viewModel.getSelectedCount() <= 3)
                this.enableBtnSelectLines(true);
            else
                this.enableBtnSelectLines(false);

            checkLinesEnabledSupervisorMode();

        }else{
            this.enableBtnSelectLines(this.viewModel.getSelectedCount() > 0);
        }
        this.connectionViewDelegate.hideProgressDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getLinesSuccess(LineSelectorSuccessEvent event) {
        this.repository.getLinesTaken(String.valueOf(this.viewModel.getPlantSelectedV2().getIdPlanta()),
                this.viewModel.getTurno().get(0).getInicio(),
                this.viewModel.getTurno().get(2).getFin(),
                this);
        this.viewModel.setListLines(event.getResponse());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGoToLogin(GoToLoginEvent event){
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(this);
        this.finish();
    }

    private String[] getValuesForNightTurn(int currentHour){
        int totalHours = currentHour + 3;
        String mValues[] = new String[totalHours];
        for (int i = 0; i < totalHours; i++) {
            switch (i){
                case 0 : {
                    mValues[i]="22";
                    break;
                }
                case 1 : {
                    mValues[i]="23";
                    break;
                }
                case 2 : {
                    mValues[i]="0";
                    break;
                }
                case 3 : {
                    mValues[i]="1";
                    break;
                }
                case 4 : {
                    mValues[i]="2";
                    break;
                }
                case 5 : {
                    mValues[i]="3";
                    break;
                }
                case 6 : {
                    mValues[i]="4";
                    break;
                }
                case 7 : {
                    mValues[i]="5";
                    break;
                }
            }
        }
        return mValues;
    }

    public void showSelectHourDialog()
    {
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        final int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int minValue = currentHour;
        int currentMinute = calendar.get(Calendar.MINUTE);

        final Dialog d = new Dialog(this);
        d.setContentView(R.layout.select_hour_dialog);
        d.setCancelable(false);
        Button b1 = d.findViewById(R.id.button1);
        Button b2 = d.findViewById(R.id.button2);
        final NumberPicker npHour = d.findViewById(R.id.numberPicker1);
        final NumberPicker npMinute = d.findViewById(R.id.numberPicker2);
        setNumberPickerTextColor(npHour, Color.WHITE);
        setNumberPickerTextColor(npMinute, Color.WHITE);
        String turnCalculated = UtilTimes.getTurnByTime();
        switch (turnCalculated){
            case TARDE:
                minValue = 14;
                break;
            case MANANA:
                minValue = 6;
                break;
            case NOCHE:
                minValue = 22;
                break;
            default: minValue = 0;
        }
        if (turnCalculated.equals(NOCHE)){
            //Si ya es despues de medianoche
            if (currentHour<=5){
                //hasta las 5 am
                String[] mValues = getValuesForNightTurn(currentHour);

                npHour.setMaxValue(mValues.length-1);
                npHour.setMinValue(0);
                npHour.setWrapSelectorWheel(true);
                npHour.setDisplayedValues(mValues);
            }else {
                npHour.setMaxValue(currentHour);
                npHour.setMinValue(minValue);
                npHour.setWrapSelectorWheel(false);
                npHour.setValue(currentHour);
            }
        }else{
            npHour.setMaxValue(currentHour);
            npHour.setMinValue(minValue);
            npHour.setWrapSelectorWheel(false);
            npHour.setValue(currentHour);
        }

        npMinute.setMaxValue(59);
        npMinute.setMinValue(0);
        npMinute.setWrapSelectorWheel(false);
        npMinute.setValue(currentMinute);

        b1.setOnClickListener(v -> {
            Date dateSelected = null;
            if (turnCalculated.equals(NOCHE)){
                if ((currentHour<=5) && (npHour.getValue() == 0 || npHour.getValue() == 1)){
                    final Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, -1);
                    dateSelected = cal.getTime();
                    String[] valuesForNightTurn = getValuesForNightTurn(currentHour);
                    int realhour = Integer.parseInt(valuesForNightTurn[npHour.getValue()]);
                    dateSelected.setHours(realhour);
                }else{
                    dateSelected = new Date();
                    if (npHour.getDisplayedValues()!=null){
                        int realhour = Integer.parseInt(npHour.getDisplayedValues()[npHour.getValue()]);
                        dateSelected.setHours(realhour);
                    }else{
                        dateSelected.setHours(npHour.getValue());
                    }
                }
                dateSelected.setMinutes(npMinute.getValue());
            }else{
                dateSelected = new Date();
                dateSelected.setHours(npHour.getValue());
                dateSelected.setMinutes(npMinute.getValue());
            }
            LineSelectorActivity.this.acceptSelection(dateSelected);
            d.dismiss();
        });
        b2.setOnClickListener(v -> d.dismiss());
        d.show();
    }

    @SuppressLint("LongLogTag")
    public static void setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {
        try{
            Field selectorWheelPaintField = numberPicker.getClass()
                    .getDeclaredField("mSelectorWheelPaint");
            selectorWheelPaintField.setAccessible(true);
            ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
        }
        catch(NoSuchFieldException e){
            Log.w("setNumberPickerTextColor", e);
        }
        catch(IllegalAccessException e){
            Log.w("setNumberPickerTextColor", e);
        }
        catch(IllegalArgumentException e){
            Log.w("setNumberPickerTextColor", e);
        }

        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText)
                ((EditText)child).setTextColor(color);
        }
        numberPicker.invalidate();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectionSuccess(LineSelectionRepository.SelectionSuccessEvent event){
        hasPendingRequest = event != null;
        new Thread(new Runnable() {
            @Override
            public void run() {
                LineSelectorActivity.this.continueSelectionLines();
            }
        }).start();
    }

    public void continueSelectionLines(){
        int userId = this.repository.getUserId();
        LineSelectionAdapter adapter1 = (LineSelectionAdapter) this.recyclerView.getAdapter();
        if (this.viewModel.isSupervisorMode()){
            modifyLinesForSupervisorMode(userId, adapter1, shiftConfirmationDate);
        } else{
            modifyLinesForVisualizationMode(userId, adapter1, shiftConfirmationDate);
        }
        Utils.setCurrentPlant(LineSelectorActivity.this.getApplicationContext(), this.viewModel.getPlantSelectedV2().getIdPlanta());
        this.repository.setPlantSelectedV2(this.viewModel.getPlantSelectedV2());
        this.repository.putSelectLines(this.viewModel.getListLines());
        this.repository.setSupervisorMode(this.viewModel.isSupervisorMode());
        if (hasPendingRequest){
            getSelectNewLine(this.viewModel.isSupervisorMode());
        }else{
            this.connectionViewDelegate.hideProgressDialog();
            ARouter.getInstance()
                    .build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                    .withFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .navigation(this);
            this.finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectionError(LineSelectionRepository.SelectionErrorEvent event){
        Log.e("Selection Line error", event.getMessageError());
        this.connectionViewDelegate.hideProgressDialog();
        if(NetworkUtils.isConnected(LineSelectorActivity.this)){
            this.showToast("Se actualizó el listado de lineas disponibles");
            callPlantsWebService();
        }else{
            this.showToast("Error en el servidor, al intentar seleccionar líneas");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeleteFinish(LineSelectionRepository.FinishDeleteEvent event){
        finishDelete.countDown();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onModifyFinish(LineSelectionRepository.FinishModifyEvent event){
        finishModify.countDown();
    }
}
