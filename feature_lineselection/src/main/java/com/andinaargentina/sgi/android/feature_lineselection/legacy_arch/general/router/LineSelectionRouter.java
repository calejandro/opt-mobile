package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.router;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.router.ViewsNames;

public class LineSelectionRouter implements ILineSelectionNavigationHandler {

    @Override
    public void goToHomePage() {
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                .navigation();
    }

    @Override
    public void goToBack() {
    }
}
