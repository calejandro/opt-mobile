package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.usescases;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.presenter.ISupervisorModeRequestHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.repository.SupervisorModeRepository;

import java.util.List;

public class SupervisorModeInteractor implements ISupervisorModeRequestHandler {

    private SupervisorModeRepository repository;

    public SupervisorModeInteractor() {
        this.repository = new SupervisorModeRepository();
    }

    @Override
    public boolean isNecesaryShowDialog() {
        return repository.isNecesaryShowDialog();
    }

    @Override
    public void setNecesaryShowDialog(boolean isNecesary) {
        repository.setIsNecessaryShowDialog(isNecesary);
    }

    @Override
    public void doSelectLines(@NonNull List<LineasProduccionDto> listLines) {
        repository.setLocalLines(listLines);
        repository.setUserModeSelected();
        repository.setPlantSelected(listLines.get(0).getPlanta());
    }
}
