package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.usescases;

import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;

import java.util.List;

/**
 * Esta interfaz define el contrato
 * entre las respuestas que vienen desde
 * el interactor hacia el repository.
 *
 * La debe implementar el repository
 */
public interface ISelectionTransactionHandler {


    /**
     * Se seleccionó recordar no mostrar
     * el popup del modo observación
     */
    boolean isNecessaryShowObserverModeDialog();

    /**
     * Se seleccionó recordar no mostrar
     * el popup del confiramcion para tomar lineas
     */
    boolean isNecesaryShowConfirmTakeDialog();


    void setNecessaryShowObserverModeDialog(boolean remember);

    void setNecesaryShowConfirmTakeDialog(boolean remember);

    /**
     * Se seleccionaron las lineas a visualizar
     * se debe hacer el request
     */
    void putSelectLines(List<LineasProduccionDto> listLines);

    /**
     * Se deben traer las lineas a visualizar
     * se debe hacer el request
     */
    void getLines(String plantId, Context context);

    void postSelection(LineSelectedDto lineSelectedDto, Context context);

    void postModifyLine(LineSelectedDto lineSelectedDto, Context context);

    void postDeleteLine(LineSelectedDto lineSelectedDto, Context context);

    void getSelection(boolean isSupervision, Context context);

    /**
     * Se deben traer las lineas seleccionadas
     */
    List<LineasProduccionDto> getLocalLines();

    /**
     * Pregunto si existen
     * lineas almacenadas
     * @return true si hay lineas guardads, false en caso contrario
     */
    boolean existLocalLines();

    boolean isSupervisorMode();

    /**
     * Get plants
     */
    void getPlants(Context context);

    void getTurnos(Context context);

    void setLocalPlantsV2(@NonNull List<PlantasDto> plantDtoList);

    void setPlantSelectedV2(@NonNull PlantasDto plantSelected);

    List<PlantasDto> getLocalPlantsV2();

    PlantasDto getPlantSelectedV2();

    int getUserId();

    String getUserTurn();

    boolean isUserSupervisor();

}
