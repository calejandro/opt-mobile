package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.view;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

import java.util.List;

public interface ISupervisorModeViewEventHandler {

    void onCreatedView(@NonNull List<LineasProduccionDto> listLines);

    void onSelectedLine(int position, boolean checked);

    void onLineSelectionButtonPress();

    void onTogglePressed();

    /**
     * Ocultar dialog de visualización
     */
    void onObserverModeDialogHide(boolean rememberDecision);

}
