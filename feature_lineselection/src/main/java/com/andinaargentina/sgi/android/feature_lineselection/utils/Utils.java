package com.andinaargentina.sgi.android.feature_lineselection.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Utils {
    public static final String PREFS_FILENAME = "CONFIG_PREFS";
    private static final String CURRENT_PLAN ="CURRENT_PLAN";

    public static void setCurrentPlant(Context context,long id){
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putLong(CURRENT_PLAN, id); //3

        editor.commit(); //4
    }
    public static long getCurrentPlant(Context context) {
        SharedPreferences settings;
        settings = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        return settings.getLong(CURRENT_PLAN,0);
    }
}
