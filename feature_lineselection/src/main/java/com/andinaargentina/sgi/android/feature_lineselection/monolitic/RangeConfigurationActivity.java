package com.andinaargentina.sgi.android.feature_lineselection.monolitic;

import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.sgi.android.feature_lineselection.R;

@Route(path = "/selection/rangeConfigurations")
public class RangeConfigurationActivity extends AppCompatActivity {

    private NumberPicker numberPickerPerformanceAccumulatedWhite;
    private NumberPicker numberPickerPerformanceAccumulatedYellow;
    private NumberPicker numberPickerPerformanceAccumulatedRed;


    private NumberPicker numberPickerPerformanceMinuteToMinuteWhite;
    private NumberPicker numberPickerPerformanceMinuteToMinuteYellow;
    private NumberPicker numberPickerPerformanceMinuteToMinuteRed;


    private NumberPicker numberPickerMermasWhite;
    private NumberPicker numberPickerMermasYellow;
    private NumberPicker numberPickerMermasRed;


    private NumberPicker numberPickerAlertsOk;
    private NumberPicker numberPickerAlertsWarning;
    private NumberPicker numberPickerAlertsRed;

    private NumberPicker numberPickerMermasAlertsOk;
    private NumberPicker numberPickerMermasAlertsWarning;
    private NumberPicker numberPickerMermasAlertsRed;

    private View btnBack;
    private View btnSaveConfigurations;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_umbral_configuration);
        setButtons();
        setNumberPickersPerformanceAccumulated();
        setNumberPickersPerformanceMinuteToMinute();
        setNumberPickerMermas();
        setNumberPickerAlerts();
        setNumberPickerMermasAlerts();
    }

    private void setButtons() {
        btnBack = findViewById(R.id.toolbar_btn_back);
        btnSaveConfigurations = findViewById(R.id.umbral_configuration_container_btn_select);
        btnSaveConfigurations.setOnClickListener(v -> saveConfigurations());
    }

    private void saveConfigurations() {
        Toast.makeText(this, "Guardo las configuraciones", Toast.LENGTH_LONG).show();
    }

    private void setNumberPickerMermasAlerts() {
        numberPickerMermasAlertsOk = findViewById(R.id.numberPickerMermasAlertsOk);
        numberPickerMermasAlertsWarning = findViewById(R.id.numberPickerMermasAlertsWarning);
        numberPickerMermasAlertsRed = findViewById(R.id.numberPickerMermasAlertsRed);

        numberPickerMermasAlertsOk.setMinValue(0);
        numberPickerMermasAlertsOk.setMaxValue(100);

        numberPickerMermasAlertsWarning.setMinValue(0);
        numberPickerMermasAlertsWarning.setMaxValue(100);

        numberPickerMermasAlertsRed.setMinValue(0);
        numberPickerMermasAlertsRed.setMaxValue(100);
    }

    private void setNumberPickerAlerts() {
        numberPickerAlertsOk = findViewById(R.id.numberPickerAlertsOk);
        numberPickerAlertsWarning = findViewById(R.id.numberPickerAlertsWarning);
        numberPickerAlertsRed = findViewById(R.id.numberPickerAlertsRed);

        numberPickerAlertsOk.setMinValue(0);
        numberPickerAlertsOk.setMaxValue(60);

        numberPickerAlertsWarning.setMinValue(0);
        numberPickerAlertsWarning.setMaxValue(60);

        numberPickerAlertsRed.setMinValue(0);
        numberPickerAlertsRed.setMaxValue(60);
    }

    private void setNumberPickerMermas() {
        numberPickerMermasWhite = findViewById(R.id.numberPickerMermasWhite);
        numberPickerMermasYellow = findViewById(R.id.numberPickerMermasYellow);
        numberPickerMermasRed = findViewById(R.id.numberPickerMermasRed);

        numberPickerMermasWhite.setMinValue(0);
        numberPickerMermasWhite.setMaxValue(100);

        numberPickerMermasYellow.setMinValue(0);
        numberPickerMermasYellow.setMaxValue(100);

        numberPickerMermasRed.setMinValue(0);
        numberPickerMermasRed.setMaxValue(100);
    }

    private void setNumberPickersPerformanceMinuteToMinute() {
        numberPickerPerformanceMinuteToMinuteWhite = findViewById(R.id.numberPickerPerformanceMinuteToMinuteWhite);
        numberPickerPerformanceMinuteToMinuteYellow = findViewById(R.id.numberPickerPerformanceMinuteToMinuteYellow);
        numberPickerPerformanceMinuteToMinuteRed = findViewById(R.id.numberPickerPerformanceMinuteToMinuteRed);

        numberPickerPerformanceMinuteToMinuteWhite.setMinValue(0);
        numberPickerPerformanceMinuteToMinuteWhite.setMaxValue(60);

        numberPickerPerformanceMinuteToMinuteYellow.setMinValue(0);
        numberPickerPerformanceMinuteToMinuteYellow.setMaxValue(60);

        numberPickerPerformanceMinuteToMinuteRed.setMinValue(0);
        numberPickerPerformanceMinuteToMinuteRed.setMaxValue(60);
    }

    private void setNumberPickersPerformanceAccumulated() {
        numberPickerPerformanceAccumulatedWhite = findViewById(R.id.numberPickerPerformanceAccumulatedWhite);
        numberPickerPerformanceAccumulatedYellow = findViewById(R.id.numberPickerPerformanceAccumulatedYellow);
        numberPickerPerformanceAccumulatedRed = findViewById(R.id.numberPickerPerformanceAccumulatedRed);

        numberPickerPerformanceAccumulatedWhite.setMinValue(0);
        numberPickerPerformanceAccumulatedWhite.setMaxValue(100);

        numberPickerPerformanceAccumulatedYellow.setMinValue(0);
        numberPickerPerformanceAccumulatedYellow.setMaxValue(100);

        numberPickerPerformanceAccumulatedRed.setMinValue(0);
        numberPickerPerformanceAccumulatedRed.setMaxValue(100);
    }

}
