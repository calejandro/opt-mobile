package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.router;

public interface ILineSelectionNavigationHandler {

    /**
     * Vamos a la página principal, tener en cuenta
     * el modo o perfil de usuario con el que iniciar
     */
    void goToHomePage();

    /**
     * Volvemos a la página anterior
     * de selección de líneas
     */
    void goToBack();
}
