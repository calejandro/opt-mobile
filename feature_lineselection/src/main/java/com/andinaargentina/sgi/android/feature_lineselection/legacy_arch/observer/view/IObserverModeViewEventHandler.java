package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.view;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

import java.util.List;

public interface IObserverModeViewEventHandler {

    void onCreatedView(@NonNull List<LineasProduccionDto> listLines);

    void onSelectedLine(int position, boolean checked);

    void onLineSelectionButtonPress();

    void onTogglePressed();

    /**
     * Ocultar dialog de visualización
     */
    void onObserverModeDialogHide(boolean rememberDecision);

}
