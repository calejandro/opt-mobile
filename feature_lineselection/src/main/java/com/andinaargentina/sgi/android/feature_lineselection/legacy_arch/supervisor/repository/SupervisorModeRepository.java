package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.repository;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.ProductionPlant;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.local.base.CrudLocalProvider;
import com.andinaargentina.core_services.providers.local.linesselected.LinesSelectedProvider;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.usescases.ISupervisorModeTransactionHandler;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

public class SupervisorModeRepository implements ISupervisorModeTransactionHandler {

    private CrudLocalProvider<List<LineasProduccionDto>> linesSelectedProvider;

    public SupervisorModeRepository(){
        this.linesSelectedProvider = new LinesSelectedProvider();
    }

    @Override
    public void setLocalLines(@NonNull List<LineasProduccionDto> listLines) {
        linesSelectedProvider.writeData("Lines_selected",listLines);
    }

    @NonNull
    @Override
    public List<LineasProduccionDto> getLocalLines() {
        return linesSelectedProvider.readData("Lines_selected");
    }

    @Override
    public void setIsNecessaryShowDialog(boolean remember) {
        Prefs.putBoolean("remember_confirm_take_dialog", remember);
    }

    @Override
    public boolean isNecesaryShowDialog() {
        return Prefs.getBoolean("remember_confirm_take_dialog",true);
    }

    @Override
    public void setUserModeSelected() {
        Prefs.putBoolean("is_observer_mode", true);
    }

    @Override
    public void setPlantSelected(@NonNull PlantasDto plantDto) {
        Prefs.putString("plant_name",plantDto.getDescPlanta());
        Prefs.putLong("plant_id",plantDto.getIdPais());
    }
}
