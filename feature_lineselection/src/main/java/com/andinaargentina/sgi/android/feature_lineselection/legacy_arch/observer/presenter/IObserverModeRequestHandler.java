package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.presenter;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

import java.util.List;

/**
 * Esta interfaz define la comunicación
 * entre el presenter y el interactor
 * Esta interfaz debe ser implementada por el
 * interactor
 */
public interface IObserverModeRequestHandler {

    //Es necesario mostrar el dialogo de aviso
    boolean isNecesaryShowDialog();

    //Setear flag
    void setNecesaryShowDialog(boolean isNecesary);

    //Notificar que la selección está lista
    void doSelectLines(@NonNull List<LineasProduccionDto> listLines);

}
