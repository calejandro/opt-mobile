package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter;

import com.andinaargentina.core_services.events.lineSelector.LineSelectorErrorEvent;
import com.andinaargentina.core_services.events.lineSelector.LineSelectorSuccessEvent;
import com.andinaargentina.core_services.events.plants.GetPlantsSuccessEvent;
import com.andinaargentina.core_services.events.plants.GetPlantsSuccessEventV2;
import com.andinaargentina.core_services.providers.api.core.util.NetworkUtils;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.ILineSelectionViewEventsHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.router.ILineSelectionNavigationHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.usescases.ILineSelectionResponseHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.viewmodel.LineSelectionViewModel;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter;
import com.andinaargentina.ui_sdk.ViewFinish;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class LineSelectionPresenter extends BasePresenter<LineSelectionViewModel> implements
        ILineSelectionViewEventsHandler, ILineSelectionResponseHandler {

    private ILineSelectionRequestHandler requestHandler;
    private ILineSelectionViewUpdateHandler viewUpdateHandler;
    private ILineSelectionNavigationHandler router;

    private LineSelectionViewModel viewModel;

    public LineSelectionPresenter(ILineSelectionRequestHandler requestHandler,
                                  ILineSelectionViewUpdateHandler viewUpdateHandler,
                                  ILineSelectionNavigationHandler router) {
        EventBus.getDefault().register(this);
        this.viewModel = new LineSelectionViewModel();
        this.requestHandler = requestHandler;
        this.viewUpdateHandler = viewUpdateHandler;
        this.router = router;
    }

    @Override
    public void loadViewModelState(@Nullable LineSelectionViewModel viewModelStateSaved) {
        super.loadViewModelState(viewModelStateSaved);
        if (viewModelStateSaved!=null){
            //restauramos estado
            //TODO falta validar como sigue esto
            //con la componetizacion
            this.viewModel = viewModelStateSaved;
        }
    }

    @Override
    public void onNewSesionView() {
        this.requestHandler.fetchPlants(viewUpdateHandler.getContext());
    }

    @Override
    public void onChangeSesionView() {
        this.viewModel.listLines = this.requestHandler.fetchLocalLines();
        this.viewModel.plantSelected = this.requestHandler.getPlantSelected();
        this.viewModel.listPlantV2 = this.requestHandler.getLocalPlants();
        this.viewUpdateHandler.changeToolbarTitle(this.viewModel.plantSelected.getDescPlanta());
        this.viewUpdateHandler.updateDataOnView(this.viewModel.listLines);
    }

    @Override
    public void onTryGetLinesRequest() {
        if (NetworkUtils.isConnected(viewUpdateHandler.getContext())){
            viewUpdateHandler.hideNotificationConnection();
            viewUpdateHandler.showProgressDialog();
            requestHandler.fetchInternetLines(String.valueOf(viewModel.plantSelected.getIdPlanta()),
                    viewUpdateHandler.getContext());
        }else {
            viewUpdateHandler.hideProgressDialog();
            viewUpdateHandler.showNotificationInternetIsGone(v -> onTryGetLinesRequest());
        }
    }

    @Override
    public void onSiteDialogOppened() {
        this.viewUpdateHandler.showSelectSiteDialog(viewModel.getPlantNames(), viewModel.getPositionOfPlantSelected());
    }

    @Override
    public void onSiteSelected(int positionSelected) {
        if (this.viewModel.getPositionOfPlantSelected() != positionSelected){
            this.viewModel.plantSelected = this.viewModel.listPlantV2.get(positionSelected);
            this.viewUpdateHandler.changeToolbarTitle(this.viewModel.plantSelected.getDescPlanta());
            this.requestHandler.fetchInternetLines(String.valueOf(this.viewModel.plantSelected.getIdPlanta()),
                    this.viewUpdateHandler.getContext());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getPlantsSuccess(GetPlantsSuccessEventV2 event) {
        viewUpdateHandler.hideProgressDialog();
        this.viewModel.listPlantV2 = event.getResponse();
        this.viewModel.plantSelected = this.viewModel.listPlantV2.get(0);
        this.viewUpdateHandler.changeToolbarTitle(this.viewModel.plantSelected.getDescPlanta());
        this.requestHandler.fetchInternetLines(String.valueOf(this.viewModel.plantSelected.getIdPlanta()),
                this.viewUpdateHandler.getContext());
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getLinesSuccess(LineSelectorSuccessEvent event) {
        viewUpdateHandler.hideProgressDialog();
        //this.viewModel.listLines = event.getResponse();
        this.viewUpdateHandler.updateDataOnView(this.viewModel.listLines);
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getLinesError(LineSelectorErrorEvent event) {
        viewUpdateHandler.hideProgressDialog();
        viewUpdateHandler.showNotificationServerError(v -> onTryGetLinesRequest());
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void selectedLinesSuccess(LineSelectorSuccessEvent event) {

    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void selectedLinesError(LineSelectorErrorEvent event) {

    }

    @NotNull
    @Override
    public LineSelectionViewModel getViewModel() {
        return viewModel;
    }

    @NotNull
    @Override
    public String getViewModelName() {
        return "LineSelectionViewModel";
    }

    @Override
    protected void doOnDestroy() {
        super.doOnDestroy();
        EventBus.getDefault().unregister(this);
        this.viewModel = null;
        this.viewUpdateHandler = null;
        this.router = null;
        this.requestHandler = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFinishView(ViewFinish event){
        this.viewUpdateHandler.finishView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSavePlants(SavePlants event){
        this.requestHandler.savePlants(viewModel.listPlantV2);
        this.requestHandler.savePlantSelected(viewModel.plantSelected);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangeMode(ChangeUserMode event) {
        this.viewUpdateHandler.resetView(event.isObserverMode);
    }

    public static class SavePlants {
    }

    public static class ChangeUserMode{
        public boolean isObserverMode;
    }
}
