package com.andinaargentina.sgi.android.feature_lineselection.monolitic;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.ProductionOrders;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.UserDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.LineWithOrderDialogModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

@RequiresApi(api = Build.VERSION_CODES.N)
public class LineSelectionMonoliticViewModel implements IBaseViewModel {

    private List<LineasProduccionDto> listLines= new ArrayList<>();
    private boolean isSupervisorMode = true;
    private boolean showBackButton = false;
    private List<PlantasDto> listPlantV2;
    private PlantasDto plantSelectedV2;
    private int selectedCount;
    private List<LineasProduccionUsuarioDto> linesTaken;
    private List<ShiftDto> turno;

    public void changeMode(){
        reset();
        this.isSupervisorMode = ! this.isSupervisorMode;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<LineasProduccionDto> getListLines() {
        List<LineasProduccionDto> lineas = new ArrayList<LineasProduccionDto>();
        this.selectedCount = 0;
        if(this.linesTaken != null) {
            SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
            for (LineasProduccionDto lineDto : listLines) {
                Optional<LineasProduccionUsuarioDto> lineTakeStream = linesTaken.stream().filter(f ->
                        f.getIdLinea() == lineDto.getIdLinea() && f.getSupervisada() == true).findFirst();
                if(isSupervisorMode) {
                    if(((lineTakeStream.isPresent() && lineTakeStream.get().getUserId() == user.getUserId()))) {
                        this.selectedCount++;
                        lineDto.setItemChecked(true);
                        lineas.add(lineDto);
                        lineDto.setLineasProduccionUsuarioDto(lineTakeStream.get());
                    } else if(!lineTakeStream.isPresent()) {
                        lineas.add(lineDto);
                    }
                    lineDto.setItemEnabled(true);
                } else {
                    if(lineTakeStream.isPresent()) {
                        lineDto.setLineasProduccionUsuarioDto(lineTakeStream.get());
                        Optional<LineasProduccionUsuarioDto> lineTakeMeStream = linesTaken.stream().filter(f ->
                                f.getIdLinea() == lineDto.getIdLinea() && f.getSupervisada() == false && f.getUserId() == user.getUserId()).findFirst();
                        if(lineTakeMeStream.isPresent()) {
                            lineDto.setItemChecked(true);
                            this.selectedCount++;
                        }
                        lineDto.setItemEnabled(true);
                    }

                    lineas.add(lineDto);
                }

            }
        }
        return lineas;
    }

    public void setLinesTaken(List<LineasProduccionUsuarioDto> linesTaken) {
        this.linesTaken = linesTaken;
    }

    public List<LineasProduccionUsuarioDto> getLinesTaken() {
        return linesTaken;
    }

    public List<LineasProduccionUsuarioDto> getLinesTakenMe() {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        return linesTaken.stream().filter(f -> f.getUserId() == user.getUserId() && f.getFechaLiberacion() == null).collect(Collectors.toList());
    }

    public List<LineasProduccionDto> getFilteredLines() {
        return this.listLines;
    }

    public List<ShiftDto> getTurno() {
        return turno;
    }

    public void setTurno(List<ShiftDto> turno) {
        this.turno = turno;
    }

    public void upCountSelected(){
        this.selectedCount ++;
    }

    public void downCountSelected(){
        this.selectedCount --;
    }

    public void setListLines(List<LineasProduccionDto> listLines) {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        this.selectedCount = 0;
        this.listLines = listLines;

    }

    public boolean isSupervisorMode() {
        return isSupervisorMode;
    }

    public boolean isObserverMode(){
        return !isSupervisorMode;
    }

    public void setSupervisorMode(boolean supervisorMode) {
        isSupervisorMode = supervisorMode;
    }

    public boolean isShowBackButton() {
        return showBackButton;
    }

    public void setShowBackButton(boolean showBackButton) {
        this.showBackButton = showBackButton;
    }

    public List<PlantasDto> getListPlantV2() {
        return listPlantV2;
    }
    public void setListPlantV2(List<PlantasDto> listPlant) {
        this.listPlantV2 = listPlant;
    }

    public void setPlantSelectedV2(PlantasDto plantSelected) {
        this.plantSelectedV2 = plantSelected;
    }

    public PlantasDto getPlantSelectedV2() {
        return plantSelectedV2;
    }

    public int getPositionOfPlantSelected(){
        int position = 0;
        if (listPlantV2!=null && plantSelectedV2!=null){
            for (PlantasDto plantDto: listPlantV2) {
                if (plantSelectedV2.getIdPlanta() != plantDto.getIdPlanta()) {
                    position++;
                }else{
                    break;
                }
            }
        }
        return position;
    }

    public List<String> getPlantNames(){
        List<String> listNames = new ArrayList<>();
        if (listPlantV2!=null){
            for (PlantasDto plantDto: listPlantV2) {
                listNames.add(plantDto.getDescPlanta());
            }
        }
        return listNames;
    }

    @NotNull
    @Override
    public String name() {
        return "LineSelectionViewModel";
    }

    public int getSelectedCount() {
        return selectedCount;
    }

    public void setSelectedCount(int selectedCount) {
        this.selectedCount = selectedCount;
    }

    public void reset() {
        this.linesTaken = new ArrayList<>();
        this.listLines = new ArrayList<>();
        this.selectedCount = 0;
    }

    public List<LineWithOrderDialogModel> getListLinesSelectedName() {
        List<LineWithOrderDialogModel> listLinesWithOrders = new ArrayList<>();
        /*if (this.listLines!=null){
            for (LineasProduccionDto lineDto: listLines) {
                if (lineDto.isItemChecked()){
                    List<OrderDialogModel> orderList = new ArrayList<>(lineDto.getProductionOrders().size());
                    for (ProductionOrders order: lineDto.getProductionOrders()) {
                        boolean isOrderAssigned = false;
                        if (order.getId() == lineDto.getProductionOrderIdAssigned())
                            isOrderAssigned = true;
                        orderList.add(new OrderDialogModel(order.getShortName(),String.valueOf(order.getId()), isOrderAssigned));
                    }
                    listLinesWithOrders.add(new LineWithOrderDialogModel(lineDto.getDescription(),String.valueOf(lineDto.getId()),orderList));
                }
            }
        }*/
        return listLinesWithOrders;
    }
}
