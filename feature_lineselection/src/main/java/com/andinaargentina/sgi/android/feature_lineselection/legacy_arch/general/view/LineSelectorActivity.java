package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.feature_lineselection.R;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter.ILineSelectionViewUpdateHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter.LineSelectionPresenter;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.router.LineSelectionRouter;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.usescases.LineSelectionInteractor;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.view.ObserverModeViewDelegate;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.view.SupervisorModeViewDelegate;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter;
import com.andinaargentina.sgi.core_sdk.base.components.view.BaseActivity;
import com.andinaargentina.ui_sdk.components.base.connection.ConnectionViewDelegate;
import com.andinaargentina.ui_sdk.components.dialog.ChangeSiteDialog;

import org.jetbrains.annotations.NotNull;

import java.util.List;

//@Route(path = ViewsNames.ROUTE_VIEW_NAME_LINE_SELECTOR)
public class LineSelectorActivity extends BaseActivity
        implements ILineSelectionViewUpdateHandler {

    //<editor-fold desc="Declaracion de views">

    //Toolbar
    private ToggleButton toggleButtonObservableMode;
    private View btnBack;
    private View imgChangeSite;
    private TextView titleBar;

    //Dialogs
    private ChangeSiteDialog changeSiteDialog = null;

    //Others
    private LineSelectionPresenter eventsHandler;
    private boolean showBackButton;

    // Delegates
    private ConnectionViewDelegate connectionViewDelegate;
    private ObserverModeViewDelegate observerModeViewDelegate;
    private SupervisorModeViewDelegate supervisorModeViewDelegate;

    //</editor-fold>

    //<editor-fold desc="Base actions"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        eventsHandler = new LineSelectionPresenter(new LineSelectionInteractor(),
                this, new LineSelectionRouter());
        Intent intent = getIntent();
        showBackButton = intent.getBooleanExtra("withBackButton",false);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initViews(){
        //Toolbar
        toggleButtonObservableMode = findViewById(R.id.line_selector_toggle);
        imgChangeSite = findViewById(R.id.toolbar_img);
        btnBack = findViewById(R.id.toolbar_menu);
        titleBar = findViewById(R.id.toolbar_menu_title);

        //Dialogs
        setOnClicksListeners();
        createDelegates();
        setBackButton();
        resetView(false);
    }

    public void resetView(boolean isObserverMode) {
        //crear presenter deacuerdo
        if (isObserverMode){
            createObserverMode();
        }else{
            createSupervisorMode();
        }
        if (showBackButton){
            btnBack.setVisibility(View.VISIBLE);
            btnBack.setOnClickListener(v -> LineSelectorActivity.this.finishView());
            this.eventsHandler.onChangeSesionView();
        } else {
            btnBack.setVisibility(View.GONE);
            this.eventsHandler.onNewSesionView();
        }
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_line_selector;
    }

    @NotNull
    @Override
    public BasePresenter getBasePresenter() {
        return eventsHandler;
    }

    //</editor-fold>

    //<editor-fold desc="Container actions">

    @Override
    public void hideSelectSiteDialog() {
        if (changeSiteDialog!=null){
            changeSiteDialog.dismiss();
            changeSiteDialog.setClickHandler(null);
            changeSiteDialog = null;
        }
    }

    @Override
    public void showSelectSiteDialog(@NonNull List<String> lisNames, int positionSelected) {
        String message = getString(R.string.line_select_site_dialog_description);
        String title = getString(R.string.line_select_site_dialog_title);
        String ok = getString(R.string.line_select_dialog_btn_ok);
        changeSiteDialog = new ChangeSiteDialog(this, title, message,"Cancelame",ok,
                lisNames, positionSelected);
        changeSiteDialog.setClickHandler(new ChangeSiteDialog.ChangeSiteDialogClickHandler() {
            @Override
            public void positiveClick(int positionSelected) {
                LineSelectorActivity.this.eventsHandler.onSiteSelected(positionSelected);
                changeSiteDialog.dismiss();
                changeSiteDialog.setClickHandler(null);
                changeSiteDialog = null;
            }

            @Override
            public void closeClick() {
                changeSiteDialog.dismiss();
                changeSiteDialog.setClickHandler(null);
                changeSiteDialog = null;
            }
        });
    }

    @Override
    public void changeToolbarTitle(@NonNull String name) {
        titleBar.setText(name);
    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.imgChangeSite != null){
            this.imgChangeSite.setOnClickListener(null);
            this.imgChangeSite = null;
        }
        if (this.eventsHandler!=null){
            this.eventsHandler.onDestroy();
            this.eventsHandler = null;
        }
        if (this.connectionViewDelegate!=null){
            this.connectionViewDelegate.onDestroy();
            this.connectionViewDelegate = null;
        }
    }

    @Override
    public void updateDataOnView(@NonNull List<LineasProduccionDto> listLines) {
        if (this.supervisorModeViewDelegate!=null){
            this.supervisorModeViewDelegate.onInitPostView(listLines);
        }
        else if (this.observerModeViewDelegate!=null){
            this.observerModeViewDelegate.onInitPostView(listLines);
        }
    }

    //</editor-fold>

    //<editor-fold desc="Connection view actions">

    @Override
    public void showNotificationInternetIsGone(View.OnClickListener listener) {
        connectionViewDelegate.showNotificationInternetIsGone(listener);
    }

    @Override
    public void showNotificationServerError(View.OnClickListener listener) {
        connectionViewDelegate.showNotificationServerError(listener);
    }

    @Override
    public void showNotificationConectionLost(View.OnClickListener listener) {
        connectionViewDelegate.showNotificationConectionLost(listener);
    }

    @Override
    public void showProgressDialog() {
        connectionViewDelegate.showProgressDialog();
    }

    @Override
    public void hideProgressDialog() {
        connectionViewDelegate.hideProgressDialog();
    }

    @Override
    public void hideNotificationConnection() {
        connectionViewDelegate.hideNotificationConnection();
    }

    //</editor-fold>

    //<editor-fold desc="Support methods">
    private void setBackButton() {
        if (showBackButton)
            btnBack.setVisibility(View.VISIBLE);
        else
            btnBack.setVisibility(View.GONE);
    }

    private void createDelegates() {
        connectionViewDelegate = new ConnectionViewDelegate(this);
        connectionViewDelegate.onCreatedView();
    }

    @Override
    public void createObserverMode() {
        supervisorModeViewDelegate = null;
        observerModeViewDelegate = new ObserverModeViewDelegate(this);
        observerModeViewDelegate.onCreatedView();
    }

    @Override
    public void createSupervisorMode() {
        observerModeViewDelegate = null;
        supervisorModeViewDelegate = new SupervisorModeViewDelegate(this);
        supervisorModeViewDelegate.onCreatedView();
    }

    private void setOnClicksListeners() {
        imgChangeSite.setOnClickListener((v) -> eventsHandler.onSiteDialogOppened());
    }
    //</editor-fold>

}
