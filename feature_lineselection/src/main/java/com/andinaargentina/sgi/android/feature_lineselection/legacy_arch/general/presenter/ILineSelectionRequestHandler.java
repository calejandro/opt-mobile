package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter;

import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;

import java.util.List;

/**
 * Esta interfaz define la comunicación
 * entre el presenter y el interactor
 * Esta interfaz debe ser implementada por el
 * interactor
 */
public interface ILineSelectionRequestHandler {

    boolean isNecesaryShowConfirmTakeDialog();

    void setNecesaryShowConfirmTakeDialog(boolean isNecesary);


    boolean isSupervisorMode();

    boolean existLocalLines();

    /**
     * Obtengo las lineas de producción
     */
    List<LineasProduccionDto> fetchLocalLines();

    /**
     * Obtengo las lineas de producción
     */
    void fetchInternetLines(String plantId, Context context);

    void fetchPlants(@NonNull Context context);

    void savePlants(@NonNull List<PlantasDto> plantDtoList);

    void savePlantSelected(@NonNull PlantasDto plantSelected);

    List<PlantasDto> getLocalPlants();

    PlantasDto getPlantSelected();
}
