package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.adapter;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.UserDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_lineselection.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;
import com.anychart.core.annotations.Line;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

public class LineSelectionAdapter extends BaseAdapter<LineasProduccionDto> {

    private boolean isObserverMode;
    private IHolderItemClick clickListener;

    public LineSelectionAdapter(@NonNull List<LineasProduccionDto> dataList,
                                @NonNull List<LineasProduccionUsuarioDto> lineTaken,
                                @NonNull IHolderItemClick clickListener,
                                boolean isObserverMode){
        super(getDataFiltered(dataList, lineTaken, isObserverMode), clickListener);
        this.clickListener = clickListener;
        this.isObserverMode = isObserverMode;
    }

    private static List<LineasProduccionDto> getDataFiltered(List<LineasProduccionDto> dataList, List<LineasProduccionUsuarioDto> linetaken, boolean isObserverMode) {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        List<LineasProduccionDto> list = new ArrayList<>();
        for (LineasProduccionDto lineDto: dataList) {
            list.add(lineDto);
        }
        return list;
    }

    public int selectAllLines(){
        int cantItemsChedked = 0;
        if (isObserverMode){
            for (LineasProduccionDto lineDto: getListData()) {
                if (lineDto.isItemEnabled()){
                    lineDto.setItemChecked(true);
                    cantItemsChedked++;
                }else{
                    lineDto.setItemChecked(false);
                }
            }
        }
        notifyDataSetChanged();
        return cantItemsChedked;
    }

    public void unSelectAllLines(){
        if (isObserverMode){
            for (LineasProduccionDto lineDto: getListData()) {
                lineDto.setItemChecked(false);
            }
        }
        notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public LineSelectionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selector,parent,false);
        return new LineSelectionHolder(v,isObserverMode, clickListener);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull BaseHolder<LineasProduccionDto> holder) {
        holder.viewDetached();
        super.onViewDetachedFromWindow(holder);
    }
}
