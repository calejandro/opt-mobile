package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.presenter;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter.LineSelectionPresenter;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.usescases.IObserverModeResponseHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.view.IObserverModeViewEventHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.viewmodel.ObserverModeViewModel;
import com.andinaargentina.ui_sdk.ViewFinish;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class ObserverModePresenter implements IObserverModeViewEventHandler,
        IObserverModeResponseHandler {

    private IObserverModeViewUpdateHandler viewUpdateHandler;
    private IObserverModeRequestHandler requestHandler;
    private ObserverModeViewModel viewModel;

    public ObserverModePresenter(@NonNull IObserverModeViewUpdateHandler viewUpdateHandler,
                                 @NonNull IObserverModeRequestHandler requestHandler,
                                 @NonNull ObserverModeViewModel viewModel) {
        this.viewUpdateHandler = viewUpdateHandler;
        this.requestHandler = requestHandler;
        this.viewModel = viewModel;
    }

    @Override
    public void onCreatedView(@NonNull List<LineasProduccionDto> listLines) {
        setViewOnOberverMode();
        showStatusLines();
        resetViewModelData(listLines);
        this.showLines();
        this.viewUpdateHandler.enableBtnSelectLines(viewModel.existLinesSelected());
    }

    private void setViewOnOberverMode() {
        viewUpdateHandler.setToggleOnObserverMode();
        viewUpdateHandler.setTextBtnLineSelectedOnObserverMode();
    }

    private void showStatusLines() {
        //cargar datos
        if (viewModel.existLines()){
            showLines();
        }else{
            showEmptyState();
        }
    }

    private void showLines() {
        this.viewUpdateHandler.hideEmptyState();
        this.viewUpdateHandler.showLines(this.viewModel.listLines, this.viewModel.listLineTaken);
    }

    private void showEmptyState() {
        viewUpdateHandler.showEmptyState();
    }

    @Override
    public void onSelectedLine(int position, boolean checked) {
        if (checked)
            viewModel.selectedCount ++;
        else
            viewModel.selectedCount --;
        this.viewUpdateHandler.enableBtnSelectLines((viewModel.selectedCount>0));
        viewModel.listLines.get(position).setItemChecked(checked);
    }

    @Override
    public void onLineSelectionButtonPress() {
        requestHandler.doSelectLines(viewModel.listLines);
        EventBus.getDefault().post(new LineSelectionPresenter.SavePlants());
        EventBus.getDefault().post(new ViewFinish());
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                .navigation();
    }

    @Override
    public void onTogglePressed() {
        LineSelectionPresenter.ChangeUserMode event = new LineSelectionPresenter.ChangeUserMode();
        event.isObserverMode = false;
        EventBus.getDefault().post(event);
    }

    @Override
    public void onObserverModeDialogHide(boolean rememberDecision) {
        if (rememberDecision) requestHandler.setNecesaryShowDialog(false);
    }

    private void resetViewModelData(List<LineasProduccionDto> dataResponse) {
        this.viewModel.listLines = dataResponse;
        this.viewModel.selectedCount = this.viewModel.getSelectedLinesOnly().size();
        this.viewModel.isNecessaryShowDialog = this.requestHandler.isNecesaryShowDialog();
        this.viewUpdateHandler.enableBtnSelectLines(false);
    }

}
