package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.repository;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.events.lineSelector.LineSelectorErrorEvent;
import com.andinaargentina.core_services.events.lineSelector.LineSelectorSuccessEvent;
import com.andinaargentina.core_services.events.plants.GetPlantsErrorEvent;
import com.andinaargentina.core_services.events.plants.GetPlantsSuccessEventV2;
import com.andinaargentina.core_services.providers.api.clients.lineselection.LineSelectionService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.delete.DeleteLineSelectedService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.modify.LineModifyService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;
import com.andinaargentina.core_services.providers.apiv2.clients.LineTakenService;
import com.andinaargentina.core_services.providers.apiv2.clients.PlantListServiceV2;
import com.andinaargentina.core_services.providers.apiv2.clients.ShiftService;
import com.andinaargentina.core_services.providers.apiv2.events.LineTakenListEvent;
import com.andinaargentina.core_services.providers.apiv2.events.ShiftsListSuccessEvent;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.services.TurnosService;
import com.andinaargentina.core_services.providers.local.base.CrudLocalProvider;
import com.andinaargentina.core_services.providers.local.linesselected.LinesSelectedProvider;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.usescases.ISelectionTransactionHandler;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.andinaargentina.RepositoryConst.*;
@RequiresApi(api = Build.VERSION_CODES.N)
public class LineSelectionRepository implements ISelectionTransactionHandler{
    private LineSelectionService selectionService;
    private PlantListServiceV2 getPlantListServiceV2;
    private LineSelectedService selectedService;
    private LineModifyService modifyLineService;
    private DeleteLineSelectedService deleteLineSelectedService;
    private CompositeDisposable compositeDisposable;
    private CrudLocalProvider<List<LineasProduccionDto>> linesSelectedProvider;
    private LineTakenService lineTakenService = new LineTakenService();
    private ShiftService shiftService = new ShiftService();

    public LineSelectionRepository(LineSelectionService selectionService,
                                   PlantListServiceV2 getPlantListServiceV2,
                                   LineSelectedService selectedService,
                                   LineModifyService modifyLineService,
                                   DeleteLineSelectedService deleteLineSelectedService,
                                   CompositeDisposable compositeDisposable) {
        this.selectionService = selectionService;
        this.compositeDisposable = compositeDisposable;
        this.deleteLineSelectedService = deleteLineSelectedService;
        this.selectedService = selectedService;
        this.modifyLineService = modifyLineService;
        this.getPlantListServiceV2 = getPlantListServiceV2;
        this.linesSelectedProvider = new LinesSelectedProvider();
    }

    @Override
    public boolean isNecessaryShowObserverModeDialog() {
        return Prefs.getBoolean(REMEMBER_DIALOG_OBSERVER_MODE,true);
    }

    @Override
    public boolean isNecesaryShowConfirmTakeDialog() {
        //return Prefs.getBoolean(REMEMBER_DIALOG_TAKE_COMFIRM,true);
        return true; // por cambio de requerimiento, esto siempre va a ser necesario
    }

    @Override
    public void setNecessaryShowObserverModeDialog(boolean isNecessary) {
        Prefs.putBoolean(REMEMBER_DIALOG_OBSERVER_MODE, isNecessary);
    }

    @Override
    public void setNecesaryShowConfirmTakeDialog(boolean isNecesary) {
        Prefs.putBoolean(REMEMBER_DIALOG_TAKE_COMFIRM, isNecesary);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void putSelectLines(List<LineasProduccionDto> listLines) {
        Prefs.putBoolean(LINES_SELECTED, true);
        //Escribo datos en local
        List<LineasProduccionDto> lineSelected = new ArrayList<LineasProduccionDto>();
        for (LineasProduccionDto linea:
             listLines) {
            if(linea.isItemChecked()) {
                lineSelected.add(linea);
            }
        }
        linesSelectedProvider.writeData(LINES_SELECTED, lineSelected);
    }

    @Override
    public void getLines(String plantId, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = selectionService
                    .getLinesByPlant(plantId,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getLineSelectionsObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void getTurnos(Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = shiftService
                    .listShifts(context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getShiftsObservable());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }


    public void getLinesTaken(String plantId, Date dateFrom, Date dateTo, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = lineTakenService
                    .getLinesTakenByPlant(plantId, dateFrom, dateTo,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getLineTakenObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void postSelection(LineSelectedDto lineSelectedDto, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = selectedService
                    .postLineSelected(lineSelectedDto,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(lineSelectedObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void getSelection(boolean isSupervision, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = selectedService
                    .getLineSelected(isSupervision,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getLineSelectedObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void postModifyLine(LineSelectedDto lineSelectedDto, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = modifyLineService
                    .postLineModify(lineSelectedDto,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(postModifyObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    /*public void listShift(Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = shiftService
                    .listShifts(context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(postModifyObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }*/

    @Override
    public void postDeleteLine(LineSelectedDto lineSelectedDto, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = deleteLineSelectedService
                    .postDeleteSelections(lineSelectedDto,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getPostDeleteObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void getPlants(Context context) {
        Logger.t("Exequiel").d("debuggg entro a getPlants");
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetPlantListDisposableV2 = getPlantListServiceV2
                    .getPlantsList(context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getPlantListObserverV2());
            compositeDisposable.add(requestGetPlantListDisposableV2);
        }
    }

    @Override
    public void setLocalPlantsV2(@NonNull List<PlantasDto> plantDtoList) {
        Paper.book("SGI-BOOK").write(PLANTS_SAVED, plantDtoList);
    }

    @Override
    public void setPlantSelectedV2(@NonNull PlantasDto plantSelected) {
        Paper.book("SGI-BOOK").write(PLANT_SELECTED, plantSelected);
    }

    @Override
    public List<PlantasDto> getLocalPlantsV2() {
        return Paper.book("SGI-BOOK").read(PLANTS_SAVED, new ArrayList<>());
    }

    @Override
    public PlantasDto getPlantSelectedV2() {
        PlantasDto dto = Paper.book("SGI-BOOK").read(PLANT_SELECTED);
        return dto;
    }

    @Override
    public int getUserId() {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        return user.getUserId();
    }

    @Override
    public String getUserTurn() {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        return "";
    }

    @Override
    public boolean isUserSupervisor() {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        return user.ismEsSupervisor();
    }

    @Override
    public List<LineasProduccionDto> getLocalLines() {
        List<LineasProduccionDto> lines = Paper.book("SGI-BOOK").read(LINES_SELECTED, new ArrayList<>());
        return lines;
    }

    @Override
    public boolean existLocalLines() {
        return Prefs.getBoolean(LINES_SELECTED, false);
    }

    private DisposableSingleObserver<List<LineasProduccionUsuarioDto>> getLineTakenObserver(){
        return new DisposableSingleObserver<List<LineasProduccionUsuarioDto>>() {
            @Override
            public void onSuccess(List<LineasProduccionUsuarioDto> response) {
                EventBus.getDefault().post(new LineTakenListEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                //TODO completar onError para diferenciar
                //distintos tipos de errores
                EventBus.getDefault().post(new LineSelectorErrorEvent(new ServiceError()));
            }
        };
    }

    private DisposableSingleObserver<List<ShiftDto>> getShiftsObservable(){
        return new DisposableSingleObserver<List<ShiftDto>>() {
            @Override
            public void onSuccess(List<ShiftDto> response) {
                EventBus.getDefault().post(new ShiftsListSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                //TODO completar onError para diferenciar
                //distintos tipos de errores
                EventBus.getDefault().post(new LineSelectorErrorEvent(new ServiceError()));
            }
        };
    }

    private DisposableSingleObserver<List<LineasProduccionDto>> getLineSelectionsObserver(){
        return new DisposableSingleObserver<List<LineasProduccionDto>>() {
            @Override
            public void onSuccess(List<LineasProduccionDto> response) {
                EventBus.getDefault().post(new LineSelectorSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                //TODO completar onError para diferenciar
                //distintos tipos de errores
                EventBus.getDefault().post(new LineSelectorErrorEvent(new ServiceError()));
            }
        };
    }

    private DisposableSingleObserver<List<PlantasDto>> getPlantListObserverV2(){
        return new DisposableSingleObserver<List<PlantasDto>>() {
            @Override
            public void onSuccess(List<PlantasDto> response) {
                EventBus.getDefault().post(new GetPlantsSuccessEventV2(response));
            }

            @Override
            public void onError(Throwable e) {
                EventBus.getDefault().post(new GetPlantsErrorEvent(new ServiceError()));
            }
        };
    }


    private DisposableSingleObserver<List<SelectedDto>> lineSelectedObserver(){
        return new DisposableSingleObserver<List<SelectedDto>>() {
            @Override
            public void onSuccess(List<SelectedDto> response) {
                EventBus.getDefault().post(new SelectionSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                //distintos tipos de errores
                String errorMessage = "Undefined error";
                if (e!=null && e.getLocalizedMessage()!=null)
                    errorMessage = e.getLocalizedMessage();
                EventBus.getDefault().post(new SelectionErrorEvent(errorMessage));
            }
        };
    }

    private DisposableSingleObserver<List<SelectedDto>> getLineSelectedObserver(){
        return new DisposableSingleObserver<List<SelectedDto>>() {
            @Override
            public void onSuccess(List<SelectedDto> response) {
                EventBus.getDefault().post(new GetSelectionSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                //distintos tipos de errores
                String errorMessage = "Undefined error";
                if (e!=null && e.getLocalizedMessage()!=null)
                    errorMessage = e.getLocalizedMessage();
                EventBus.getDefault().post(new SelectionErrorEvent(errorMessage));
            }
        };
    }

    private DisposableSingleObserver<List<SelectedDto>> postModifyObserver(){
        return new DisposableSingleObserver<List<SelectedDto>>() {
            @Override
            public void onSuccess(List<SelectedDto> response) {
                Log.d("Select Line repository", "postModify onSuccess");
                EventBus.getDefault().post(new FinishModifyEvent());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Select Line repository", "postModify onError");
                EventBus.getDefault().post(new FinishModifyEvent());
            }
        };
    }

    private DisposableSingleObserver<List<SelectedDto>> getPostDeleteObserver(){
        return new DisposableSingleObserver<List<SelectedDto>>() {
            @Override
            public void onSuccess(List<SelectedDto> response) {
                Log.d("Select Line repository", "PostDelete onSuccess");
                EventBus.getDefault().post(new FinishDeleteEvent());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("Select Line repository", "PostDelete onError");
                EventBus.getDefault().post(new FinishDeleteEvent());
            }
        };
    }

    @Override
    public boolean isSupervisorMode() {
        return Prefs.getBoolean(IS_OBSERVER_MODE, true);
    }

    public void setSupervisorMode(boolean isSupervisorMode){
        Prefs.putBoolean(IS_OBSERVER_MODE, isSupervisorMode);
    }

    public class SelectionSuccessEvent{
        private List<SelectedDto> lineSelectedDto;

        public SelectionSuccessEvent(List<SelectedDto> lineSelectedDto) {
            this.lineSelectedDto = lineSelectedDto;
        }

        public List<SelectedDto> getLineSelectedDto() {
            return lineSelectedDto;
        }

        public void setLineSelectedDto(List<SelectedDto> lineSelectedDto) {
            this.lineSelectedDto = lineSelectedDto;
        }
    }

    public class GetSelectionSuccessEvent{
        private List<SelectedDto> lineSelectedDto;

        public GetSelectionSuccessEvent(List<SelectedDto> lineSelectedDto) {
            this.lineSelectedDto = lineSelectedDto;
        }

        public List<SelectedDto> getLineSelectedDto() {
            return lineSelectedDto;
        }

        public void setLineSelectedDto(List<SelectedDto> lineSelectedDto) {
            this.lineSelectedDto = lineSelectedDto;
        }
    }

    public class FinishDeleteEvent{}

    public class FinishModifyEvent{}

    public class SelectionErrorEvent{
        private String messageError;

        public SelectionErrorEvent(String messageError) {
            this.messageError = messageError;
        }

        public String getMessageError() {
            return messageError;
        }

        public void setMessageError(String messageError) {
            this.messageError = messageError;
        }
    }
}
