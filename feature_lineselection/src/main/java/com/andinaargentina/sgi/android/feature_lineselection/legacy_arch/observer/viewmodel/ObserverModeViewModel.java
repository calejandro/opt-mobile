package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.viewmodel;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class ObserverModeViewModel implements IBaseViewModel {

    public int selectedCount;
    public List<LineasProduccionDto> listLines;
    public List<LineasProduccionUsuarioDto> listLineTaken;
    public boolean isLocalLinesReaded;
    public boolean isNecessaryShowDialog = true;

    public boolean existLines(){
        return !(listLines==null || listLines.isEmpty());
    }

    public boolean existLinesSelected(){
        return (this.selectedCount>0);
    }

    public List<LineasProduccionDto> getSelectedLinesOnly(){
        List<LineasProduccionDto> list = new ArrayList<>();
        for (LineasProduccionDto dto: listLines) {
            if (dto.isItemChecked()){
                list.add(dto);
            }
        }
        return list;
    }

    @NotNull
    @Override
    public String name() {
        return "SupervisorModeViewModel";
    }
}
