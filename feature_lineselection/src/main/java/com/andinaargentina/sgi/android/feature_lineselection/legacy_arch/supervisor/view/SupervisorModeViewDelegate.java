package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.sgi.android.feature_lineselection.R;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.adapter.LineSelectionAdapter;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.presenter.ISupervisorModeViewUpdateHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.presenter.SupervisorModePresenter;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.usescases.SupervisorModeInteractor;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.viewmodel.SupervisorModeViewModel;
import com.andinaargentina.ui_sdk.components.base.connection.ICompositeView;
import com.andinaargentina.ui_sdk.components.dialog.ObserverModeDialog;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.LineWithOrderDialogModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.TakeLineConfirmDialog;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.adapter.LineSelectionHolder.KEY_CHEKED;

@RequiresApi(api = Build.VERSION_CODES.N)
public class SupervisorModeViewDelegate implements ISupervisorModeViewUpdateHandler, ICompositeView, BaseAdapter.IHolderItemClick {

    private WeakReference<Activity> rootView;
    private SupervisorModePresenter eventsHandler;

    //List
    private WeakReference<LineSelectionAdapter> adapter;
    private WeakReference<RecyclerView> recyclerView;

    //EmpyState
    private WeakReference<View> viewEmptyLabel;

    //Toolbar
    private WeakReference<ToggleButton> toggleButton;

    //Dialog
    private TakeLineConfirmDialog dialog = null;

    //BtnSelectLines
    private WeakReference<TextView> txtBtnVisualizarLines;
    private WeakReference<View> btnVisualizarLineas;
    private WeakReference<ImageView> imgBtnVisualizarLines;

    public SupervisorModeViewDelegate(Activity rootView){
        this.rootView = new WeakReference<>(rootView);
        this.eventsHandler = new SupervisorModePresenter(this,
                new SupervisorModeInteractor(), getViewModel());
        this.initView();
    }

    private SupervisorModeViewModel getViewModel() {
        SupervisorModeViewModel viewModel = new SupervisorModeViewModel();
        return viewModel;
    }

    private void initView(){
        //List
        this.recyclerView = new WeakReference<>(rootView.get().findViewById(R.id.line_selector_recycler));

        //EmpyState
        this.viewEmptyLabel = new WeakReference<>(rootView.get().findViewById(R.id.line_selector_empty_state));

        //Toolbar
        this.toggleButton = new WeakReference<>(rootView.get().findViewById(R.id.line_selector_toggle));

        this.toggleButton.get().setOnClickListener(v -> SupervisorModeViewDelegate.this.eventsHandler.onTogglePressed());

        //BtnSelectLines
        this.txtBtnVisualizarLines = new WeakReference<>(rootView.get().findViewById(R.id.line_selector_txt_btn_select));
        this.btnVisualizarLineas = new WeakReference<>(rootView.get().findViewById(R.id.line_selector_container_select));
        this.btnVisualizarLineas.get().setOnClickListener(v -> SupervisorModeViewDelegate.this.eventsHandler.onLineSelectionButtonPress());
        this.imgBtnVisualizarLines = new WeakReference<>(rootView.get().findViewById(R.id.line_selector_img_btn_select));
    }

    @Override
    public void onInitPostView(@NonNull List<LineasProduccionDto> listLines) {
        this.eventsHandler.onCreatedView(listLines);
    }

    @Override
    public void setToggleOnSupervisorMode() {
        this.toggleButton.get().setChecked(false);
    }

    @Override
    public void showDialogSupervisorMode(ObserverModeDialog.DialogClickHandler listener,
                                         List<LineWithOrderDialogModel> listLines) {
        String message = this.rootView.get().getString(R.string.line_select_dialog_description);
        String title = this.rootView.get().getString(R.string.line_select_dialog_title);
        String ok = this.rootView.get().getString(R.string.line_select_dialog_btn_ok);

        dialog = new TakeLineConfirmDialog(this.rootView.get().getApplicationContext(),null, null);
        dialog.setClickHandler(new TakeLineConfirmDialog.DialogTakeLineClickHandler() {
            @Override
            public void positiveClick(@Nullable TakeLineConfirmDialog.LineAndOrderSelected line1Selected,
                                      @Nullable TakeLineConfirmDialog.LineAndOrderSelected line2Selected,
                                      @Nullable TakeLineConfirmDialog.LineAndOrderSelected line3Selected) {
                listener.positiveClick(false);
                dialog.dismiss();
                dialog = null;
            }

            @Override
            public void negativeClick() {
                listener.negativeClick();
                dialog.dismiss();
                dialog = null;
            }
        });
        dialog.show();
    }

    @Override
    public void hideDialogObserverMode() {
        dialog.dismiss();
    }

    @Override
    public void showDialogObserverMode(ObserverModeDialog.DialogClickHandler listener) {
        String message = this.rootView.get().getString(R.string.line_select_dialog_description);
        String title = this.rootView.get().getString(R.string.line_select_dialog_title);
        String ok = this.rootView.get().getString(R.string.line_select_dialog_btn_ok);
        ObserverModeDialog observerDialog = new ObserverModeDialog(this.rootView.get(), title, message,"Cancelame",ok);
        observerDialog.setClickHandler(new ObserverModeDialog.DialogClickHandler() {
            @Override
            public void positiveClick(boolean isChecked) {
                observerDialog.dismiss();
                listener.positiveClick(isChecked);
            }

            @Override
            public void negativeClick() {
                observerDialog.dismiss();
                listener.negativeClick();
            }
        });
        observerDialog.show();
    }


    @Override
    public void setTextBtnLineSelectedOnSupervisorMode() {
        Picasso.with(this.rootView.get()).load(R.drawable.i_line_taked).into(this.imgBtnVisualizarLines.get());
        this.txtBtnVisualizarLines.get().setText("Tomar líneas");
    }

    @Override
    public void showLines(@NonNull List<LineasProduccionDto> listData, List<LineasProduccionUsuarioDto> lineasProduccionUsuarioDtos) {
        this.recyclerView.get().setVisibility(View.VISIBLE);
        adapter = new WeakReference<>(new LineSelectionAdapter(listData,lineasProduccionUsuarioDtos, this,false));
        final LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.get(), RecyclerView.VERTICAL, false);
        recyclerView.get().setAdapter(adapter.get());
        recyclerView.get().setNestedScrollingEnabled(false);
        recyclerView.get().setLayoutManager(layoutManager);
    }

    @Override
    public void showEmptyState() {
        viewEmptyLabel.get().setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyState() {
        viewEmptyLabel.get().animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                viewEmptyLabel.get().setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void enableBtnSelectLines(boolean enable) {
        int colorSelected = rootView.get().getResources().getColor(R.color.color_txt_red_disable);
        if (enable) colorSelected = rootView.get().getResources().getColor(R.color.colorWhite);
        ImageViewCompat.setImageTintList(this.imgBtnVisualizarLines.get(), ColorStateList.valueOf(colorSelected));
        this.txtBtnVisualizarLines.get().setTextColor(colorSelected);
        this.btnVisualizarLineas.get().setEnabled(enable);
    }


    @Override
    public void onCreatedView() {
    }

    @Override
    public void onDestroy() {
        this.toggleButton.get().setOnClickListener(null);
        this.toggleButton.clear();
        this.toggleButton = null;

        this.dialog.dismiss();
        this.dialog.setClickHandler(null);
        this.dialog = null;

        this.rootView.clear();
        this.rootView = null;
    }

    @Override
    public void onItemClick(View caller, int position, @Nullable Bundle bundle) {
        if (bundle!=null && !bundle.isEmpty())
            eventsHandler.onSelectedLine(position,bundle.getBoolean(KEY_CHEKED));
    }
}
