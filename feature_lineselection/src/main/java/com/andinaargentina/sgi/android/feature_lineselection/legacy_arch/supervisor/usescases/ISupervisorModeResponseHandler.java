package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.usescases;

/**
 * Esta interfaz define el contrato
 * entre las respuestas que vienen desde
 * el interactor hacia el presenter
 * La debe implementar el presenter
 */
public interface ISupervisorModeResponseHandler {

}
