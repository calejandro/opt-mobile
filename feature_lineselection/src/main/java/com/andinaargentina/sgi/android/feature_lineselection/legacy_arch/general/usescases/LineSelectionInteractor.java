package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.usescases;

import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.LineSelectionService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.delete.DeleteLineSelectedService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.modify.LineModifyService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedService;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantListService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.clients.PlantListServiceV2;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.repository.LineSelectionRepository;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter.ILineSelectionRequestHandler;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class LineSelectionInteractor implements ILineSelectionRequestHandler {

    private LineSelectionRepository repository;

    public LineSelectionInteractor() {
        this.repository = new LineSelectionRepository(
                        new LineSelectionService(),
                        new PlantListServiceV2(),
                        new LineSelectedService(ApiRestModule.providesApiService(false)),
                        new LineModifyService(ApiRestModule.providesApiService(false)),
                        new DeleteLineSelectedService(ApiRestModule.providesApiService(false)),
                        new CompositeDisposable());
    }

    @Override
    public boolean existLocalLines() {
        return repository.existLocalLines();
    }

    @Override
    public List<LineasProduccionDto> fetchLocalLines() {
        return repository.getLocalLines();
    }

    @Override
    public void fetchInternetLines(String plantId, Context context) {
        repository.getLines(plantId,context);
    }

    @Override
    public void fetchPlants(@NonNull Context context) {
        repository.getPlants(context);
    }

    @Override
    public void savePlants(@NonNull List<PlantasDto> plantDtoList) {
        repository.setLocalPlantsV2(plantDtoList);
    }

    @Override
    public void savePlantSelected(@NonNull PlantasDto plantSelected) {
        repository.setPlantSelectedV2(plantSelected);
    }

    @Override
    public List<PlantasDto> getLocalPlants() {
        return repository.getLocalPlantsV2();
    }

    @Override
    public PlantasDto getPlantSelected() {
        return repository.getPlantSelectedV2();
    }

    @Override
    public boolean isSupervisorMode() {
        return repository.isSupervisorMode();
    }


    @Override
    public boolean isNecesaryShowConfirmTakeDialog() {
        return repository.isNecesaryShowConfirmTakeDialog();
    }

    @Override
    public void setNecesaryShowConfirmTakeDialog(boolean isNecesary) {
        repository.setNecesaryShowConfirmTakeDialog(isNecesary);
    }
}
