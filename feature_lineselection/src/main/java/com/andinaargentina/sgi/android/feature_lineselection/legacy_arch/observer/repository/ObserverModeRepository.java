package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.repository;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.ProductionPlant;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.local.base.CrudLocalProvider;
import com.andinaargentina.core_services.providers.local.linesselected.LinesSelectedProvider;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.usescases.IObserverModeTransactionHandler;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

public class ObserverModeRepository implements IObserverModeTransactionHandler {

    private CrudLocalProvider<List<LineasProduccionDto>> linesSelectedProvider;

    public ObserverModeRepository(){
        this.linesSelectedProvider = new LinesSelectedProvider();
    }

    @Override
    public void setLocalLines(@NonNull List<LineasProduccionDto> listLines) {
        linesSelectedProvider.writeData("Lines_selected",listLines);
    }

    @NonNull
    @Override
    public List<LineasProduccionDto> getLocalLines() {
        return linesSelectedProvider.readData("Lines_selected");
    }

    @Override
    public void setIsNecessaryShowDialog(boolean remember) {
        Prefs.putBoolean("remember_dialog_observer_mode", remember);
    }

    @Override
    public boolean isNecesaryShowDialog() {
        return Prefs.getBoolean("remember_dialog_observer_mode",true);
    }

    @Override
    public void setUserModeSelected() {
        Prefs.putBoolean("is_observer_mode", true);
    }

    @Override
    public void setPlantSelected(@NonNull ProductionPlant plantDto) {
        Prefs.putString("plant_name",plantDto.getDescription());
        Prefs.putLong("plant_id",plantDto.getId());
    }
}
