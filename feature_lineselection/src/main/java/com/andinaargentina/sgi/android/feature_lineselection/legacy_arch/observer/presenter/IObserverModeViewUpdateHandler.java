package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.observer.presenter;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;

import java.util.List;

/**
 * Operaciones de vista para el modo observacion
 */
public interface IObserverModeViewUpdateHandler {

    //provisorio
    void onInitPostView(@NonNull List<LineasProduccionDto> listLines);

    void setToggleOnObserverMode();

    void hideDialogObserverMode();

    void setTextBtnLineSelectedOnObserverMode();

    void showLines(@NonNull List<LineasProduccionDto> listData, List<LineasProduccionUsuarioDto> lineTaken);

    void showEmptyState();

    void hideEmptyState();

    void enableBtnSelectLines(boolean enable);

}
