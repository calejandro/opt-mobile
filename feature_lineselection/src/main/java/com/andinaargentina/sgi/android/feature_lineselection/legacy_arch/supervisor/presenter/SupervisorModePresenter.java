package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.presenter;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.presenter.LineSelectionPresenter;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.usescases.ISupervisorModeResponseHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.view.ISupervisorModeViewEventHandler;
import com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.viewmodel.SupervisorModeViewModel;
import com.andinaargentina.ui_sdk.ViewFinish;
import com.andinaargentina.ui_sdk.components.dialog.ObserverModeDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class SupervisorModePresenter implements ISupervisorModeViewEventHandler,
        ISupervisorModeResponseHandler {

    private ISupervisorModeViewUpdateHandler viewUpdateHandler;
    private ISupervisorModeRequestHandler requestHandler;
    private SupervisorModeViewModel viewModel;

    public SupervisorModePresenter(@NonNull ISupervisorModeViewUpdateHandler viewUpdateHandler,
                                   @NonNull ISupervisorModeRequestHandler requestHandler,
                                   @NonNull SupervisorModeViewModel viewModel) {
        this.viewUpdateHandler = viewUpdateHandler;
        this.requestHandler = requestHandler;
        this.viewModel = viewModel;
    }

    @Override
    public void onCreatedView(@NonNull List<LineasProduccionDto> listLines) {
        setViewOnOberverMode();
        showStatusLines();
        resetViewModelData(listLines);
        this.showLines();
        this.viewUpdateHandler.enableBtnSelectLines(viewModel.existLinesSelected());
    }

    private void setViewOnOberverMode() {
        viewUpdateHandler.setToggleOnSupervisorMode();
        viewUpdateHandler.setTextBtnLineSelectedOnSupervisorMode();
    }

    private void showStatusLines() {
        //cargar datos
        if (viewModel.existLines()){
            showLines();
        }else{
            showEmptyState();
        }
    }

    private void showLines() {
        this.viewUpdateHandler.hideEmptyState();
        this.viewUpdateHandler.showLines(this.viewModel.listLines, this.viewModel.listLineTaken);
    }

    private void showEmptyState() {
        viewUpdateHandler.showEmptyState();
    }

    @Override
    public void onSelectedLine(int position, boolean checked) {
        if (checked)
            viewModel.selectedCount ++;
        else
            viewModel.selectedCount --;
        viewModel.listLines.get(position).setItemChecked(checked);
        /*if (viewModel.selectedCount==3){
            for (LineasProduccionDto lineDto: viewModel.listLines) {
                if (!lineDto.isProductionLineTaken() && !lineDto.isItemChecked()){
                    lineDto.itemEnabled=false;
                }
            }
        }else{
            for (LineasProduccionDto lineDto: viewModel.listLines) {
                if (!lineDto.isProductionLineTaken()){
                    lineDto.itemEnabled=true;
                }
            }
        }*/
        this.viewUpdateHandler.showLines(viewModel.listLines, this.viewModel.listLineTaken);
        this.viewUpdateHandler.enableBtnSelectLines((viewModel.selectedCount>0));
        viewModel.listLines.get(position).setItemChecked(checked);
    }

    @Override
    public void onLineSelectionButtonPress() {
        if (requestHandler.isNecesaryShowDialog()){
            viewUpdateHandler.showDialogSupervisorMode(new ObserverModeDialog.DialogClickHandler() {
                @Override
                public void positiveClick(boolean isChecked) {
                    onObserverModeDialogHide(isChecked);
                }

                @Override
                public void negativeClick() {
                    //No  hacer nada
                }
            },this.viewModel.getNameSelectedLinesOnly());
        }else {
            goToNextPage();
        }
    }

    @Override
    public void onTogglePressed() {
        if (requestHandler.isNecesaryShowDialog()){
            viewUpdateHandler.showDialogObserverMode(new ObserverModeDialog.DialogClickHandler() {
                @Override
                public void positiveClick(boolean isChecked) {
                    onObserverModeDialogHide(isChecked);
                    LineSelectionPresenter.ChangeUserMode event = new LineSelectionPresenter.ChangeUserMode();
                    event.isObserverMode = true;
                    EventBus.getDefault().post(event);
                }

                @Override
                public void negativeClick() {
                    //No  hacer nada
                }
            });
        }else{
            onObserverModeDialogHide(false);
            LineSelectionPresenter.ChangeUserMode event = new LineSelectionPresenter.ChangeUserMode();
            event.isObserverMode = true;
            EventBus.getDefault().post(event);
        }
    }

    @Override
    public void onObserverModeDialogHide(boolean rememberDecision) {
        requestHandler.setNecesaryShowDialog(rememberDecision);
        goToNextPage();
    }

    private void goToNextPage() {
        requestHandler.doSelectLines(viewModel.listLines);
        EventBus.getDefault().post(new LineSelectionPresenter.SavePlants());
        EventBus.getDefault().post(new ViewFinish());
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                .navigation();
    }

    private void resetViewModelData(List<LineasProduccionDto> dataResponse) {
        List<LineasProduccionDto> provisory = new ArrayList<>();
        /*for (LineasProduccionDto lineDto: dataResponse) {
            if (!lineDto.isProductionLineTaken()){
                provisory.add(lineDto);
            }
        }*/
        this.viewModel.listLines = provisory;
        this.viewModel.selectedCount = this.viewModel.getSelectedLinesOnly().size();
        this.viewModel.isNecessaryShowDialog = this.requestHandler.isNecesaryShowDialog();
        this.viewUpdateHandler.enableBtnSelectLines(false);
    }

}
