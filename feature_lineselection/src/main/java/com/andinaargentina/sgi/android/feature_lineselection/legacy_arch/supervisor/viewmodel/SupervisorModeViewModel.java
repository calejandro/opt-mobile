package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.supervisor.viewmodel;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.ProductionOrders;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.LineWithOrderDialogModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class SupervisorModeViewModel implements IBaseViewModel {

    public int selectedCount;
    public List<LineasProduccionDto> listLines;
    public List<LineasProduccionUsuarioDto> listLineTaken;
    public boolean isLocalLinesReaded;
    public boolean isNecessaryShowDialog = true;

    public boolean existLines(){
        return !(listLines==null || listLines.isEmpty());
    }

    public boolean existLinesSelected(){
        return (this.selectedCount>0);
    }

    public List<LineasProduccionDto> getSelectedLinesOnly(){
        List<LineasProduccionDto> list = new ArrayList<>();
        for (LineasProduccionDto dto: listLines) {
            if (dto.isItemChecked()){
                list.add(dto);
            }
        }
        return list;
    }

    public List<LineWithOrderDialogModel> getNameSelectedLinesOnly(){
        List<LineasProduccionDto> lisLinesSelected = getSelectedLinesOnly();
        List<LineWithOrderDialogModel> listNames = new ArrayList<>(lisLinesSelected.size());
        int i = 0;
        /*for (LineasProduccionDto lineDto: lisLinesSelected) {
            List<OrderDialogModel> orderList = new ArrayList<>(lineDto.getProductionOrders().size());
            for (ProductionOrders order: lineDto.getProductionOrders()) {
                orderList.add(new OrderDialogModel(order.getShortName(),String.valueOf(order.getId()), false));
            }
            listNames.add(new LineWithOrderDialogModel(lineDto.getDescription(),String.valueOf(lineDto.getId()), orderList));
        }*/
        return listNames;
    }

    @NotNull
    @Override
    public String name() {
        return "SupervisorModeViewModel";
    }
}
