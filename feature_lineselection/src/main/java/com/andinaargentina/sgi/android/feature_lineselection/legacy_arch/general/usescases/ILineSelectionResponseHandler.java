package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.usescases;

import com.andinaargentina.core_services.events.lineSelector.LineSelectorErrorEvent;
import com.andinaargentina.core_services.events.lineSelector.LineSelectorSuccessEvent;

/**
 * Esta interfaz define el contrato
 * entre las respuestas que vienen desde
 * el interactor hacia el presenter
 * La debe implementar el presenter
 */
public interface ILineSelectionResponseHandler {

    /**
     * Obtengo las lineas que se pueden
     * visualizar
     * @param event
     */
    void getLinesSuccess(LineSelectorSuccessEvent event);

    /**
     * Ocurrio un error en la comuncación
     * @param event
     */
    void getLinesError(LineSelectorErrorEvent event);

    /**
     * Notifico que se seleccionaron lineas
     * @param event
     */
    void selectedLinesSuccess(LineSelectorSuccessEvent event);

    /**
     * Ocurrio un error en la comuncación
     * @param event
     */
    void selectedLinesError(LineSelectorErrorEvent event);
}
