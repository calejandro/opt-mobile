package com.andinaargentina.sgi.android.feature_lineselection.legacy_arch.general.view.adapter;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_lineselection.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

@RequiresApi(api = Build.VERSION_CODES.N)
public class LineSelectionHolder extends BaseHolder<LineasProduccionDto> {

    //Android Views
    private View viewRootContainer;
    private TextView txtName;
    private TextView txtLeyend;
    private ImageView imgIcon;
    private CheckBox chkBox;

    //Holder Data
    private boolean isObserverMode;
    private LineasProduccionDto data;

    public static final String KEY_CHEKED = "isChecked";

    public LineSelectionHolder(View itemView, boolean isObserverMode,
                               BaseAdapter.IHolderItemClick clickListener) {
        super(itemView);
        this.isObserverMode = isObserverMode;
        viewRootContainer = itemView.findViewById(R.id.item_selector_root_container);
        txtName = itemView.findViewById(R.id.item_selector_txt_name);
        txtLeyend = itemView.findViewById(R.id.item_selector_txt_description);
        imgIcon = itemView.findViewById(R.id.item_selector_img);
        chkBox = itemView.findViewById(R.id.item_selector_checkbox);
        chkBox.setOnClickListener(v -> {
            if (data.isItemEnabled()){
                data.setItemChecked(chkBox.isChecked());
                Bundle bundle = new Bundle();
                bundle.putBoolean(KEY_CHEKED, chkBox.isChecked());
                int position = getAdapterPosition();
                clickListener.onItemClick(getThisView(),position, bundle);
            }
        });
    }


    @Override
    public void setDataInHolder(LineasProduccionDto data) {
        this.data = data;
        if (isObserverMode){
            viewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_blue);
        }else{
            viewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_green);
            if (!data.isItemEnabled()){
                viewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_green_disabled);
            }
        }

        txtName.setText(data.getDescLinea());
        chkBox.setEnabled(data.isItemEnabled());
        if (data.getLineasProduccionUsuarioDto() != null){
            txtLeyend.setText(itemView.getContext().getString(R.string.item_selector_separator_bar) + data.getLineasProduccionUsuarioDto().getSysUsers().getLastName() + ", " +
                    data.getLineasProduccionUsuarioDto().getSysUsers().getFullName());
            Picasso.with(itemView.getContext()).load(R.drawable.ic_line_occuped).into(imgIcon);
            if (isObserverMode){
                SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                if (data.getLineasProduccionUsuarioDto().getUserId() == user.getUserId()){
                    viewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_gray);
                    data.setItemEnabled(false);
                    chkBox.setEnabled(false);
                }
            }

        } else {
            txtLeyend.setText(itemView.getContext().getString(R.string.item_selector_sin_supervision));
            Picasso.with(itemView.getContext()).load(R.drawable.ic_free_line).into(imgIcon);
            if (isObserverMode){
                viewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_gray);
                data.setItemEnabled(false);
                chkBox.setEnabled(false);
            }
        }
        chkBox.setChecked(data.isItemChecked());

        if (!data.isHasProductionOrder()){
            txtLeyend.setText(itemView.getContext().getString(R.string.item_selector_sin_orden));
            viewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_gray);
            data.setItemEnabled(false);
            chkBox.setEnabled(false);
        }
    }

    @Override
    public void viewDetached() {
        super.viewDetached();
    }
}
