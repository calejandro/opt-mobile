package com.samples.uitesting

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kheera.kheerasample.R

/**
 * A screen that is shown after the user logs in
 */
class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
    }

}
