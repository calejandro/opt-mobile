
package com.andinaargentina.legacy.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andina on 05/06/18.
 */

public class User {

    @SerializedName("mUserId")
    @Expose
    private Integer mUserId;
    @SerializedName("mUserName")
    @Expose
    private String mUserName;
    @SerializedName("mPassword")
    @Expose
    private String mPassword;
    @SerializedName("mEsSupervisor")
    @Expose
    private boolean mEsSupervisor;
    @SerializedName("mFullName")
    @Expose
    private String mFullName;
    @SerializedName("mLastName")
    @Expose
    private String mLastName;
    @SerializedName("mEmail")
    @Expose
    private String mEmail;
    @SerializedName("mInitURL")
    @Expose
    private String mInitURL;
    @SerializedName("mIsActive")
    @Expose
    private String mIsActive;
    @SerializedName("mAgreedTerms")
    @Expose
    private String mAgreedTerms;
    @SerializedName("mLanguageId")
    @Expose
    private Integer mLanguageId;
    @SerializedName("mNroCliente")
    @Expose
    private String mNroCliente;
    @SerializedName("mFoto")
    @Expose
    private String mFoto;
    @SerializedName("mTelefono")
    @Expose
    private String mTelefono;
    @SerializedName("mCodAreaTelefono")
    @Expose
    private String mCodAreaTelefono;
    @SerializedName("mUserHorarioContacto")
    @Expose
    private String mUserHorarioContacto;
    @SerializedName("mEsAdministrador")
    @Expose
    private Boolean mEsAdministrador;
    @SerializedName("mNumClienteAnteriorBasis")
    @Expose
    private Integer mNumClienteAnteriorBasis;
    @SerializedName("mNumClienteBasis")
    @Expose
    private Integer mNumClienteBasis;
    @SerializedName("mFechaAlta")
    @Expose
    private Object mFechaAlta;
    @SerializedName("mCUIT")
    @Expose
    private String mCUIT;
    @SerializedName("mRetrieveDistincts")
    @Expose
    private Boolean mRetrieveDistincts;
    @SerializedName("mMensajeLogin")
    @Expose
    private String mMensajeLogin;
    @SerializedName("mToken")
    @Expose
    private String mToken;
    @SerializedName("mChangePass")
    @Expose
    private String mChangePass;
    @SerializedName("mVendor")
    @Expose
    private String mVendor;
    @SerializedName("mArea")
    @Expose
    private String mArea;

    @SerializedName("mTurno")
    @Expose
    private String mTurno;

    @SerializedName("mFechaInicioTurno")
    @Expose
    private Date mFechaInicioTurno;

    @SerializedName("mFechaFinTurno")
    @Expose
    private Date mFechaFinTurno;

    @SerializedName("PermissionsItems")
    @Expose
    private List<PermissionsItem> permissionsItems = new ArrayList<PermissionsItem>();

    /**
     * @return The mUserId
     */
    public Integer getMUserId() {
        return mUserId;
    }

    /**
     * @param mUserId The mUserId
     */
    public void setMUserId(Integer mUserId) {
        this.mUserId = mUserId;
    }

    /**
     * @return The mUserName
     */
    public String getMUserName() {
        return mUserName;
    }

    /**
     * @param mUserName The mUserName
     */
    public void setMUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    /**
     * @return The mPassword
     */
    public String getMPassword() {
        return mPassword;
    }

    /**
     * @param mPassword The mPassword
     */
    public void setMPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    /**
     * @return The mFullName
     */
    public String getMFullName() {
        return mFullName;
    }

    /**
     * @param mFullName The mFullName
     */
    public void setMFullName(String mFullName) {
        this.mFullName = mFullName;
    }

    /**
     * @return The mLastName
     */
    public String getMLastName() {
        return mLastName;
    }

    /**
     * @param mLastName The mLastName
     */
    public void setMLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    /**
     * @return The mEmail
     */
    public String getMEmail() {
        return mEmail;
    }

    /**
     * @param mEmail The mEmail
     */
    public void setMEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    /**
     * @return The mInitURL
     */
    public String getMInitURL() {
        return mInitURL;
    }

    /**
     * @param mInitURL The mInitURL
     */
    public void setMInitURL(String mInitURL) {
        this.mInitURL = mInitURL;
    }

    /**
     * @return The mIsActive
     */
    public String getMIsActive() {
        return mIsActive;
    }

    /**
     * @param mIsActive The mIsActive
     */
    public void setMIsActive(String mIsActive) {
        this.mIsActive = mIsActive;
    }

    /**
     * @return The mAgreedTerms
     */
    public String getMAgreedTerms() {
        return mAgreedTerms;
    }

    /**
     * @param mAgreedTerms The mAgreedTerms
     */
    public void setMAgreedTerms(String mAgreedTerms) {
        this.mAgreedTerms = mAgreedTerms;
    }

    /**
     * @return The mLanguageId
     */
    public Integer getMLanguageId() {
        return mLanguageId;
    }

    /**
     * @param mLanguageId The mLanguageId
     */
    public void setMLanguageId(Integer mLanguageId) {
        this.mLanguageId = mLanguageId;
    }

    /**
     * @return The mNroCliente
     */
    public String getMNroCliente() {
        return mNroCliente;
    }

    /**
     * @param mNroCliente The mNroCliente
     */
    public void setMNroCliente(String mNroCliente) {
        this.mNroCliente = mNroCliente;
    }

    /**
     * @return The mFoto
     */
    public String getMFoto() {
        return mFoto;
    }

    /**
     * @param mFoto The mFoto
     */
    public void setMFoto(String mFoto) {
        this.mFoto = mFoto;
    }

    /**
     * @return The mTelefono
     */
    public String getMTelefono() {
        return mTelefono;
    }

    /**
     * @param mTelefono The mTelefono
     */
    public void setMTelefono(String mTelefono) {
        this.mTelefono = mTelefono;
    }

    /**
     * @return The mCodAreaTelefono
     */
    public String getMCodAreaTelefono() {
        return mCodAreaTelefono;
    }

    /**
     * @param mCodAreaTelefono The mCodAreaTelefono
     */
    public void setMCodAreaTelefono(String mCodAreaTelefono) {
        this.mCodAreaTelefono = mCodAreaTelefono;
    }

    /**
     * @return The mUserHorarioContacto
     */
    public String getMUserHorarioContacto() {
        return mUserHorarioContacto;
    }

    /**
     * @param mUserHorarioContacto The mUserHorarioContacto
     */
    public void setMUserHorarioContacto(String mUserHorarioContacto) {
        this.mUserHorarioContacto = mUserHorarioContacto;
    }

    /**
     * @return The mEsAdministrador
     */
    public Boolean getMEsAdministrador() {
        return mEsAdministrador;
    }

    /**
     * @param mEsAdministrador The mEsAdministrador
     */
    public void setMEsAdministrador(Boolean mEsAdministrador) {
        this.mEsAdministrador = mEsAdministrador;
    }

    /**
     * @return The mNumClienteAnteriorBasis
     */
    public Integer getMNumClienteAnteriorBasis() {
        return mNumClienteAnteriorBasis;
    }

    /**
     * @param mNumClienteAnteriorBasis The mNumClienteAnteriorBasis
     */
    public void setMNumClienteAnteriorBasis(Integer mNumClienteAnteriorBasis) {
        this.mNumClienteAnteriorBasis = mNumClienteAnteriorBasis;
    }

    /**
     * @return The mNumClienteBasis
     */
    public Integer getMNumClienteBasis() {
        return mNumClienteBasis;
    }

    /**
     * @param mNumClienteBasis The mNumClienteBasis
     */
    public void setMNumClienteBasis(Integer mNumClienteBasis) {
        this.mNumClienteBasis = mNumClienteBasis;
    }

    /**
     * @return The mFechaAlta
     */
    public Object getMFechaAlta() {
        return mFechaAlta;
    }

    /**
     * @param mFechaAlta The mFechaAlta
     */
    public void setMFechaAlta(Object mFechaAlta) {
        this.mFechaAlta = mFechaAlta;
    }

    /**
     * @return The mCUIT
     */
    public String getMCUIT() {
        return mCUIT;
    }

    /**
     * @param mCUIT The mCUIT
     */
    public void setMCUIT(String mCUIT) {
        this.mCUIT = mCUIT;
    }

    /**
     * @return The mRetrieveDistincts
     */
    public Boolean getMRetrieveDistincts() {
        return mRetrieveDistincts;
    }

    /**
     * @param mRetrieveDistincts The mRetrieveDistincts
     */
    public void setMRetrieveDistincts(Boolean mRetrieveDistincts) {
        this.mRetrieveDistincts = mRetrieveDistincts;
    }

    /**
     * @return The mMensajeLogin
     */
    public String getMMensajeLogin() {
        return mMensajeLogin;
    }

    /**
     * @param mMensajeLogin The mMensajeLogin
     */
    public void setMMensajeLogin(String mMensajeLogin) {
        this.mMensajeLogin = mMensajeLogin;
    }

    /**
     * @return The mToken
     */
    public String getMToken() {
        return mToken;
    }

    /**
     * @param mToken The mToken
     */
    public void setMToken(String mToken) {
        this.mToken = mToken;
    }

    /**
     * @return The mChangePass
     */
    public String getMChangePass() {
        return mChangePass;
    }

    /**
     * @param mChangePass The mChangePass
     */
    public void setMChangePass(String mChangePass) {
        this.mChangePass = mChangePass;
    }

    /**
     * @return The mVendor
     */
    public String getMVendor() {
        return mVendor;
    }

    /**
     * @param mVendor The mVendor
     */
    public void setMVendor(String mVendor) {
        this.mVendor = mVendor;
    }

    /**
     * @return The mArea
     */
    public String getMArea() {
        return mArea;
    }

    /**
     * @param mArea The mArea
     */
    public void setMArea(String mArea) {
        this.mArea = mArea;
    }

    /**
     * @return The permissionsItems
     */
    public List<PermissionsItem> getPermissionsItems() {
        return permissionsItems;
    }

    /**
     * @param permissionsItems The PermissionsItems
     */
    public void setPermissionsItems(List<PermissionsItem> permissionsItems) {
        this.permissionsItems = permissionsItems;
    }

    public boolean ismEsSupervisor() {
        return mEsSupervisor;
    }

    public void setmEsSupervisor(boolean mEsSupervisor) {
        this.mEsSupervisor = mEsSupervisor;
    }

    public String getmTurno() {
        return mTurno;
    }

    public void setmTurno(String mTurno) {
        this.mTurno = mTurno;
    }

    public Date getmFechaInicioTurno() {
        return mFechaInicioTurno;
    }

    public void setmFechaInicioTurno(Date mFechaInicioTurno) {
        this.mFechaInicioTurno = mFechaInicioTurno;
    }

    public Date getmFechaFinTurno() {
        return mFechaFinTurno;
    }

    public void setmFechaFinTurno(Date mFechaFinTurno) {
        this.mFechaFinTurno = mFechaFinTurno;
    }
}
