
package com.andinaargentina.legacy.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Alertas {

    @SerializedName("Alertas")
    @Expose
    private ArrayList<Alerta> alertas = new ArrayList<Alerta>();
    @SerializedName("CantidadSinLeer")
    @Expose
    private Integer cantidadSinLeer;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;

    /**
     * 
     * @return
     *     The alertas
     */
    public ArrayList<Alerta> getAlertas() {
        return alertas;
    }

    /**
     * 
     * @param alertas
     *     The Alertas
     */
    public void setAlertas(ArrayList<Alerta> alertas) {
        this.alertas = alertas;
    }

    /**
     * 
     * @return
     *     The cantidadSinLeer
     */
    public Integer getCantidadSinLeer() {
        return cantidadSinLeer;
    }

    /**
     * 
     * @param cantidadSinLeer
     *     The CantidadSinLeer
     */
    public void setCantidadSinLeer(Integer cantidadSinLeer) {
        this.cantidadSinLeer = cantidadSinLeer;
    }

    /**
     * 
     * @return
     *     The mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * 
     * @param mensaje
     *     The Mensaje
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
