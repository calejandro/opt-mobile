
package com.andinaargentina.legacy.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Alerta {

    @SerializedName("UserIdsParaGuardar")
    @Expose
    private List<Object> userIdsParaGuardar = new ArrayList<Object>();
    @SerializedName("<ClienteView>k__BackingField")
    @Expose
    private Object clienteView;
    @SerializedName("<FechaLeida>k__BackingField")
    @Expose
    private Object fechaLeida;
    @SerializedName("<FechaLeida_to>k__BackingField")
    @Expose
    private Object fechaLeidaTo;
    @SerializedName("<IdAlert>k__BackingField")
    @Expose
    private Integer idAlert;
    @SerializedName("<FromAddress>k__BackingField")
    @Expose
    private String fromAddress;
    @SerializedName("<ToAddress>k__BackingField")
    @Expose
    private String toAddress;
    @SerializedName("<CCAddress>k__BackingField")
    @Expose
    private Object cCAddress;
    @SerializedName("<Subject>k__BackingField")
    @Expose
    private String subject;
    @SerializedName("<Message>k__BackingField")
    @Expose
    private String message;
    @SerializedName("<FileAttachName>k__BackingField")
    @Expose
    private Object fileAttachName;
    @SerializedName("<TemplateFileName>k__BackingField")
    @Expose
    private String templateFileName;
    @SerializedName("<StartDate>k__BackingField")
    @Expose
    private String startDate;
    @SerializedName("<StartDate2>k__BackingField")
    @Expose
    private String startDate2;
    @SerializedName("<Status>k__BackingField")
    @Expose
    private String status;
    @SerializedName("<AlertType>k__BackingField")
    @Expose
    private String alertType;
    @SerializedName("<Suspended>k__BackingField")
    @Expose
    private Boolean suspended;
    @SerializedName("<Retries>k__BackingField")
    @Expose
    private Integer retries;
    @SerializedName("<Locked>k__BackingField")
    @Expose
    private Boolean locked;
    @SerializedName("<LastControl>k__BackingField")
    @Expose
    private String lastControl;
    @SerializedName("<LastControl2>k__BackingField")
    @Expose
    private String lastControl2;
    @SerializedName("<LastError>k__BackingField")
    @Expose
    private Object lastError;
    @SerializedName("<UserId>k__BackingField")
    @Expose
    private Integer userId;
    @SerializedName("<ObtenerAlertasYAvisos>k__BackingField")
    @Expose
    private Boolean obtenerAlertasYAvisos;
    @SerializedName("<ObtenerAlertasLeidasYNoLeidas>k__BackingField")
    @Expose
    private Boolean obtenerAlertasLeidasYNoLeidas;
    @SerializedName("<ObtenerFechaAntesOIgualHoy>k__BackingField")
    @Expose
    private Boolean obtenerFechaAntesOIgualHoy;
    @SerializedName("<DialogoVisto>k__BackingField")
    @Expose
    private Boolean dialogoVisto;
    @SerializedName("<MostrarEnAppAndroid>k__BackingField")
    @Expose
    private Boolean mostrarEnAppAndroid;
    @SerializedName("mIDbTransaction")
    @Expose
    private Object mIDbTransaction;
    @SerializedName("mIDbConn")
    @Expose
    private Object mIDbConn;

    /**
     * 
     * @return
     *     The userIdsParaGuardar
     */
    public List<Object> getUserIdsParaGuardar() {
        return userIdsParaGuardar;
    }

    /**
     * 
     * @param userIdsParaGuardar
     *     The UserIdsParaGuardar
     */
    public void setUserIdsParaGuardar(List<Object> userIdsParaGuardar) {
        this.userIdsParaGuardar = userIdsParaGuardar;
    }

    /**
     * 
     * @return
     *     The clienteView
     */
    public Object getClienteView() {
        return clienteView;
    }

    /**
     * 
     * @param clienteView
     *     The <ClienteView>k__BackingField
     */
    public void setClienteView(Object clienteView) {
        this.clienteView = clienteView;
    }

    /**
     * 
     * @return
     *     The fechaLeida
     */
    public Object getFechaLeida() {
        return fechaLeida;
    }

    /**
     * 
     * @param fechaLeida
     *     The <FechaLeida>k__BackingField
     */
    public void setFechaLeida(Object fechaLeida) {
        this.fechaLeida = fechaLeida;
    }

    /**
     * 
     * @return
     *     The fechaLeidaTo
     */
    public Object getFechaLeidaTo() {
        return fechaLeidaTo;
    }

    /**
     * 
     * @param fechaLeidaTo
     *     The <FechaLeida_to>k__BackingField
     */
    public void setFechaLeidaTo(Object fechaLeidaTo) {
        this.fechaLeidaTo = fechaLeidaTo;
    }

    /**
     * 
     * @return
     *     The idAlert
     */
    public Integer getIdAlert() {
        return idAlert;
    }

    /**
     * 
     * @param idAlert
     *     The <IdAlert>k__BackingField
     */
    public void setIdAlert(Integer idAlert) {
        this.idAlert = idAlert;
    }

    /**
     * 
     * @return
     *     The fromAddress
     */
    public String getFromAddress() {
        return fromAddress;
    }

    /**
     * 
     * @param fromAddress
     *     The <FromAddress>k__BackingField
     */
    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    /**
     * 
     * @return
     *     The toAddress
     */
    public String getToAddress() {
        return toAddress;
    }

    /**
     * 
     * @param toAddress
     *     The <ToAddress>k__BackingField
     */
    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    /**
     * 
     * @return
     *     The cCAddress
     */
    public Object getCCAddress() {
        return cCAddress;
    }

    /**
     * 
     * @param cCAddress
     *     The <CCAddress>k__BackingField
     */
    public void setCCAddress(Object cCAddress) {
        this.cCAddress = cCAddress;
    }

    /**
     * 
     * @return
     *     The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * 
     * @param subject
     *     The <Subject>k__BackingField
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The <Message>k__BackingField
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The fileAttachName
     */
    public Object getFileAttachName() {
        return fileAttachName;
    }

    /**
     * 
     * @param fileAttachName
     *     The <FileAttachName>k__BackingField
     */
    public void setFileAttachName(Object fileAttachName) {
        this.fileAttachName = fileAttachName;
    }

    /**
     * 
     * @return
     *     The templateFileName
     */
    public String getTemplateFileName() {
        return templateFileName;
    }

    /**
     * 
     * @param templateFileName
     *     The <TemplateFileName>k__BackingField
     */
    public void setTemplateFileName(String templateFileName) {
        this.templateFileName = templateFileName;
    }

    /**
     * 
     * @return
     *     The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * 
     * @param startDate
     *     The <StartDate>k__BackingField
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * 
     * @return
     *     The startDate2
     */
    public String getStartDate2() {
        return startDate2;
    }

    /**
     * 
     * @param startDate2
     *     The <StartDate2>k__BackingField
     */
    public void setStartDate2(String startDate2) {
        this.startDate2 = startDate2;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The <Status>k__BackingField
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The alertType
     */
    public String getAlertType() {
        return alertType;
    }

    /**
     * 
     * @param alertType
     *     The <AlertType>k__BackingField
     */
    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    /**
     * 
     * @return
     *     The suspended
     */
    public Boolean getSuspended() {
        return suspended;
    }

    /**
     * 
     * @param suspended
     *     The <Suspended>k__BackingField
     */
    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    /**
     * 
     * @return
     *     The retries
     */
    public Integer getRetries() {
        return retries;
    }

    /**
     * 
     * @param retries
     *     The <Retries>k__BackingField
     */
    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    /**
     * 
     * @return
     *     The locked
     */
    public Boolean getLocked() {
        return locked;
    }

    /**
     * 
     * @param locked
     *     The <Locked>k__BackingField
     */
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    /**
     * 
     * @return
     *     The lastControl
     */
    public String getLastControl() {
        return lastControl;
    }

    /**
     * 
     * @param lastControl
     *     The <LastControl>k__BackingField
     */
    public void setLastControl(String lastControl) {
        this.lastControl = lastControl;
    }

    /**
     * 
     * @return
     *     The lastControl2
     */
    public String getLastControl2() {
        return lastControl2;
    }

    /**
     * 
     * @param lastControl2
     *     The <LastControl2>k__BackingField
     */
    public void setLastControl2(String lastControl2) {
        this.lastControl2 = lastControl2;
    }

    /**
     * 
     * @return
     *     The lastError
     */
    public Object getLastError() {
        return lastError;
    }

    /**
     * 
     * @param lastError
     *     The <LastError>k__BackingField
     */
    public void setLastError(Object lastError) {
        this.lastError = lastError;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The <UserId>k__BackingField
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The obtenerAlertasYAvisos
     */
    public Boolean getObtenerAlertasYAvisos() {
        return obtenerAlertasYAvisos;
    }

    /**
     * 
     * @param obtenerAlertasYAvisos
     *     The <ObtenerAlertasYAvisos>k__BackingField
     */
    public void setObtenerAlertasYAvisos(Boolean obtenerAlertasYAvisos) {
        this.obtenerAlertasYAvisos = obtenerAlertasYAvisos;
    }

    /**
     * 
     * @return
     *     The obtenerAlertasLeidasYNoLeidas
     */
    public Boolean getObtenerAlertasLeidasYNoLeidas() {
        return obtenerAlertasLeidasYNoLeidas;
    }

    /**
     * 
     * @param obtenerAlertasLeidasYNoLeidas
     *     The <ObtenerAlertasLeidasYNoLeidas>k__BackingField
     */
    public void setObtenerAlertasLeidasYNoLeidas(Boolean obtenerAlertasLeidasYNoLeidas) {
        this.obtenerAlertasLeidasYNoLeidas = obtenerAlertasLeidasYNoLeidas;
    }

    /**
     * 
     * @return
     *     The obtenerFechaAntesOIgualHoy
     */
    public Boolean getObtenerFechaAntesOIgualHoy() {
        return obtenerFechaAntesOIgualHoy;
    }

    /**
     * 
     * @param obtenerFechaAntesOIgualHoy
     *     The <ObtenerFechaAntesOIgualHoy>k__BackingField
     */
    public void setObtenerFechaAntesOIgualHoy(Boolean obtenerFechaAntesOIgualHoy) {
        this.obtenerFechaAntesOIgualHoy = obtenerFechaAntesOIgualHoy;
    }

    /**
     * 
     * @return
     *     The dialogoVisto
     */
    public Boolean getDialogoVisto() {
        return dialogoVisto;
    }

    /**
     * 
     * @param dialogoVisto
     *     The <DialogoVisto>k__BackingField
     */
    public void setDialogoVisto(Boolean dialogoVisto) {
        this.dialogoVisto = dialogoVisto;
    }

    /**
     * 
     * @return
     *     The mostrarEnAppAndroid
     */
    public Boolean getMostrarEnAppAndroid() {
        return mostrarEnAppAndroid;
    }

    /**
     * 
     * @param mostrarEnAppAndroid
     *     The <MostrarEnAppAndroid>k__BackingField
     */
    public void setMostrarEnAppAndroid(Boolean mostrarEnAppAndroid) {
        this.mostrarEnAppAndroid = mostrarEnAppAndroid;
    }


    /**
     * 
     * @return
     *     The mIDbTransaction
     */
    public Object getMIDbTransaction() {
        return mIDbTransaction;
    }

    /**
     * 
     * @param mIDbTransaction
     *     The mIDbTransaction
     */
    public void setMIDbTransaction(Object mIDbTransaction) {
        this.mIDbTransaction = mIDbTransaction;
    }

    /**
     * 
     * @return
     *     The mIDbConn
     */
    public Object getMIDbConn() {
        return mIDbConn;
    }

    /**
     * 
     * @param mIDbConn
     *     The mIDbConn
     */
    public void setMIDbConn(Object mIDbConn) {
        this.mIDbConn = mIDbConn;
    }

    /**
     * Created by Andina on 05/06/18.
     */

    public static class PermissionsItem {

        @SerializedName("mPermissionItemId")
        @Expose
        private Integer mPermissionItemId;
        @SerializedName("mParentPermissionItemId")
        @Expose
        private Integer mParentPermissionItemId;
        @SerializedName("mResourceName")
        @Expose
        private String mResourceName;
        @SerializedName("mUrl")
        @Expose
        private String mUrl;
        @SerializedName("mUrlAppMobile")
        @Expose
        private String mUrlAppMobile;
        @SerializedName("mTarget")
        @Expose
        private String mTarget;
        @SerializedName("mMenuItemOrder")
        @Expose
        private Integer mMenuItemOrder;
        @SerializedName("mImage")
        @Expose
        private String mImage;
        @SerializedName("mImageHeight")
        @Expose
        private Integer mImageHeight;
        @SerializedName("mImageWidth")
        @Expose
        private Integer mImageWidth;
        @SerializedName("mIsAuthenticationRequired")
        @Expose
        private String mIsAuthenticationRequired;
        @SerializedName("mIsEnabled")
        @Expose
        private String mIsEnabled;
        @SerializedName("mIsVisible")
        @Expose
        private String mIsVisible;
        @SerializedName("mIsMenu")
        @Expose
        private String mIsMenu;
        @SerializedName("mMenuName")
        @Expose
        private String mMenuName;
        @SerializedName("mParentPermissionItem")
        @Expose
        private String mParentPermissionItem;
        @SerializedName("mSubsistemaId")
        @Expose
        private Integer mSubsistemaId;
        @SerializedName("<Children>k__BackingField")
        @Expose
        private List<Object> childrenKBackingField = new ArrayList<Object>();
        @SerializedName("mIDbTransaction")
        @Expose
        private Object mIDbTransaction;
        @SerializedName("mIDbConn")
        @Expose
        private Object mIDbConn;

        /**
         *
         * @return
         *     The mPermissionItemId
         */
        public Integer getMPermissionItemId() {
            return mPermissionItemId;
        }

        /**
         *
         * @param mPermissionItemId
         *     The mPermissionItemId
         */
        public void setMPermissionItemId(Integer mPermissionItemId) {
            this.mPermissionItemId = mPermissionItemId;
        }

        /**
         *
         * @return
         *     The mParentPermissionItemId
         */
        public Integer getMParentPermissionItemId() {
            return mParentPermissionItemId;
        }

        /**
         *
         * @param mParentPermissionItemId
         *     The mParentPermissionItemId
         */
        public void setMParentPermissionItemId(Integer mParentPermissionItemId) {
            this.mParentPermissionItemId = mParentPermissionItemId;
        }

        /**
         *
         * @return
         *     The mResourceName
         */
        public String getMResourceName() {
            return mResourceName;
        }

        /**
         *
         * @param mResourceName
         *     The mResourceName
         */
        public void setMResourceName(String mResourceName) {
            this.mResourceName = mResourceName;
        }

        /**
         *
         * @return
         *     The mUrl
         */
        public String getMUrl() {
            return mUrl;
        }

        /**
         *
         * @param mUrl
         *     The mUrl
         */
        public void setMUrl(String mUrl) {
            this.mUrl = mUrl;
        }

        /**
         *
         * @return
         *     The mUrlAppMobile
         */
        public String getMUrlAppMobile() {
            return mUrlAppMobile;
        }

        /**
         *
         * @param mUrlAppMobile
         *     The mUrlAppMobile
         */
        public void setMUrlAppMobile(String mUrlAppMobile) {
            this.mUrlAppMobile = mUrlAppMobile;
        }

        /**
         *
         * @return
         *     The mTarget
         */
        public String getMTarget() {
            return mTarget;
        }

        /**
         *
         * @param mTarget
         *     The mTarget
         */
        public void setMTarget(String mTarget) {
            this.mTarget = mTarget;
        }

        /**
         *
         * @return
         *     The mMenuItemOrder
         */
        public Integer getMMenuItemOrder() {
            return mMenuItemOrder;
        }

        /**
         *
         * @param mMenuItemOrder
         *     The mMenuItemOrder
         */
        public void setMMenuItemOrder(Integer mMenuItemOrder) {
            this.mMenuItemOrder = mMenuItemOrder;
        }

        /**
         *
         * @return
         *     The mImage
         */
        public String getMImage() {
            return mImage;
        }

        /**
         *
         * @param mImage
         *     The mImage
         */
        public void setMImage(String mImage) {
            this.mImage = mImage;
        }

        /**
         *
         * @return
         *     The mImageHeight
         */
        public Integer getMImageHeight() {
            return mImageHeight;
        }

        /**
         *
         * @param mImageHeight
         *     The mImageHeight
         */
        public void setMImageHeight(Integer mImageHeight) {
            this.mImageHeight = mImageHeight;
        }

        /**
         *
         * @return
         *     The mImageWidth
         */
        public Integer getMImageWidth() {
            return mImageWidth;
        }

        /**
         *
         * @param mImageWidth
         *     The mImageWidth
         */
        public void setMImageWidth(Integer mImageWidth) {
            this.mImageWidth = mImageWidth;
        }

        /**
         *
         * @return
         *     The mIsAuthenticationRequired
         */
        public String getMIsAuthenticationRequired() {
            return mIsAuthenticationRequired;
        }

        /**
         *
         * @param mIsAuthenticationRequired
         *     The mIsAuthenticationRequired
         */
        public void setMIsAuthenticationRequired(String mIsAuthenticationRequired) {
            this.mIsAuthenticationRequired = mIsAuthenticationRequired;
        }

        /**
         *
         * @return
         *     The mIsEnabled
         */
        public String getMIsEnabled() {
            return mIsEnabled;
        }

        /**
         *
         * @param mIsEnabled
         *     The mIsEnabled
         */
        public void setMIsEnabled(String mIsEnabled) {
            this.mIsEnabled = mIsEnabled;
        }

        /**
         *
         * @return
         *     The mIsVisible
         */
        public String getMIsVisible() {
            return mIsVisible;
        }

        /**
         *
         * @param mIsVisible
         *     The mIsVisible
         */
        public void setMIsVisible(String mIsVisible) {
            this.mIsVisible = mIsVisible;
        }

        /**
         *
         * @return
         *     The mIsMenu
         */
        public String getMIsMenu() {
            return mIsMenu;
        }

        /**
         *
         * @param mIsMenu
         *     The mIsMenu
         */
        public void setMIsMenu(String mIsMenu) {
            this.mIsMenu = mIsMenu;
        }

        /**
         *
         * @return
         *     The mMenuName
         */
        public String getMMenuName() {
            return mMenuName;
        }

        /**
         *
         * @param mMenuName
         *     The mMenuName
         */
        public void setMMenuName(String mMenuName) {
            this.mMenuName = mMenuName;
        }

        /**
         *
         * @return
         *     The mParentPermissionItem
         */
        public String getMParentPermissionItem() {
            return mParentPermissionItem;
        }

        /**
         *
         * @param mParentPermissionItem
         *     The mParentPermissionItem
         */
        public void setMParentPermissionItem(String mParentPermissionItem) {
            this.mParentPermissionItem = mParentPermissionItem;
        }

        /**
         *
         * @return
         *     The mSubsistemaId
         */
        public Integer getMSubsistemaId() {
            return mSubsistemaId;
        }

        /**
         *
         * @param mSubsistemaId
         *     The mSubsistemaId
         */
        public void setMSubsistemaId(Integer mSubsistemaId) {
            this.mSubsistemaId = mSubsistemaId;
        }

        /**
         *
         * @return
         *     The childrenKBackingField
         */
        public List<Object> getChildrenKBackingField() {
            return childrenKBackingField;
        }

        /**
         *
         * @param childrenKBackingField
         *     The <Children>k__BackingField
         */
        public void setChildrenKBackingField(List<Object> childrenKBackingField) {
            this.childrenKBackingField = childrenKBackingField;
        }

        /**
         *
         * @return
         *     The mIDbTransaction
         */
        public Object getMIDbTransaction() {
            return mIDbTransaction;
        }

        /**
         *
         * @param mIDbTransaction
         *     The mIDbTransaction
         */
        public void setMIDbTransaction(Object mIDbTransaction) {
            this.mIDbTransaction = mIDbTransaction;
        }

        /**
         *
         * @return
         *     The mIDbConn
         */
        public Object getMIDbConn() {
            return mIDbConn;
        }

        /**
         *
         * @param mIDbConn
         *     The mIDbConn
         */
        public void setMIDbConn(Object mIDbConn) {
            this.mIDbConn = mIDbConn;
        }

    }
}
