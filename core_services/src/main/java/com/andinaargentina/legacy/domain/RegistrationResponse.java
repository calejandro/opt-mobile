package com.andinaargentina.legacy.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andina on 05/06/18.
 */

public class RegistrationResponse {

    @SerializedName("mDeviceId")
    @Expose
    private Integer mDeviceId;
    @SerializedName("mUserId")
    @Expose
    private Integer mUserId;
    @SerializedName("mAppName")
    @Expose
    private String mAppName;
    @SerializedName("mRegistrationId")
    @Expose
    private String mRegistrationId;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;
    @SerializedName("mIDbTransaction")
    @Expose
    private Object mIDbTransaction;
    @SerializedName("mIDbConn")
    @Expose
    private Object mIDbConn;

    /**
     *
     * @return
     * The mDeviceId
     */
    public Integer getMDeviceId() {
        return mDeviceId;
    }

    /**
     *
     * @param mDeviceId
     * The mDeviceId
     */
    public void setMDeviceId(Integer mDeviceId) {
        this.mDeviceId = mDeviceId;
    }

    /**
     *
     * @return
     * The mUserId
     */
    public Integer getMUserId() {
        return mUserId;
    }

    /**
     *
     * @param mUserId
     * The mUserId
     */
    public void setMUserId(Integer mUserId) {
        this.mUserId = mUserId;
    }

    /**
     *
     * @return
     * The mAppName
     */
    public String getMAppName() {
        return mAppName;
    }

    /**
     *
     * @param mAppName
     * The mAppName
     */
    public void setMAppName(String mAppName) {
        this.mAppName = mAppName;
    }

    /**
     *
     * @return
     * The mRegistrationId
     */
    public String getMRegistrationId() {
        return mRegistrationId;
    }

    /**
     *
     * @param mRegistrationId
     * The mRegistrationId
     */
    public void setMRegistrationId(String mRegistrationId) {
        this.mRegistrationId = mRegistrationId;
    }

    /**
     *
     * @return
     * The mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     *
     * @param mensaje
     * The Mensaje
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     *
     * @return
     * The mIDbTransaction
     */
    public Object getMIDbTransaction() {
        return mIDbTransaction;
    }

    /**
     *
     * @param mIDbTransaction
     * The mIDbTransaction
     */
    public void setMIDbTransaction(Object mIDbTransaction) {
        this.mIDbTransaction = mIDbTransaction;
    }

    /**
     *
     * @return
     * The mIDbConn
     */
    public Object getMIDbConn() {
        return mIDbConn;
    }

    /**
     *
     * @param mIDbConn
     * The mIDbConn
     */
    public void setMIDbConn(Object mIDbConn) {
        this.mIDbConn = mIDbConn;
    }
}
