package com.andinaargentina.signalr;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

import io.reactivex.Completable;
import io.reactivex.observers.DisposableCompletableObserver;

public class SignalRService extends Service {
    private HubConnection mHubConnection;

    public SignalRService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        //startSignalR();
        return result;
    }

    @Override
    public void onDestroy() {
        if (mHubConnection!=null)
            mHubConnection.stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startSignalR() {
        String serverUrl = "http://181.142.96.26:81/mobileUpdates";
        mHubConnection = HubConnectionBuilder.create(serverUrl).build();
        Completable competable = mHubConnection.start();
        competable.subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                System.out.println("Completed!");
                // Do something after 5s = 5000ms
                Log.i("WebSocket", mHubConnection.getConnectionState().name());
                Log.i("WebSocket", String.valueOf(mHubConnection.getServerTimeout()));
                Log.i("WebSocket", mHubConnection.getConnectionState().toString());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                // Do something after 5s = 5000ms
                Log.i("WebSocket", mHubConnection.getConnectionState().name());
                Log.i("WebSocket", String.valueOf(mHubConnection.getServerTimeout()));
                Log.i("WebSocket", mHubConnection.getConnectionState().toString());
            }
        });

        //Hub --> "/alerts" -->  metodo que escucha -> sendAlertsList  , envio de alertas
        mHubConnection.on("/sendAlertsList", (message) -> {
            System.out.println("New Message: " + message);
        }, String.class);

        //Hub --> "/efficiencyReport" --> metodo que escucha sendReport, es para actualizar el reporte de eficiencia
        mHubConnection.on("/efficiencyGraph", (message) -> {
            System.out.println("New Message: " + message);
        }, String.class);

        //Hub --> "/pantalla de burbujitas" --> metodo que escucha sendReport, es para actualizar el reporte de eficiencia
        mHubConnection.on("/sendReport", (message) -> {
            System.out.println("New Message: " + message);
        }, String.class);

        //Hub --> "/machineActualStatusGraph" --> metodo que escucha machineActualStatusGraph, para actualizar los graficos de estado actual de maquinas
        mHubConnection.on("/machineActualStatusGraph", (message) -> {
            System.out.println("New Message: " + message);
        }, String.class);

        //Hub --> "/visualizationReport" -->   metodo que escucha -> visualizationReport, para los graficos de visualziacion
        mHubConnection.on("/visualizationReport", (message) -> {
            System.out.println("New Message: " + message);
        }, String.class);

    }
}
