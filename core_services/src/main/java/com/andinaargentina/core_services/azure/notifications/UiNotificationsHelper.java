package com.andinaargentina.core_services.azure.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.andinaargentina.core_services.R;
import com.andinaargentina.core_services.router.ViewsNames;


public class UiNotificationsHelper {

    public static void showNormalNotification(Context context, String title, String body){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "sgi_cocaandina_android_id";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Notificaciones de alertas");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("cocacola://coca.andinaargentina.com" + ViewsNames.ROUTE_VIEW_NAME_SPLASH));


        // This ensures that the back button follows the recommended
        // convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Adds the back stack for the Intent (but not the Intent itself)
        // stackBuilder.addParentStack(WebViewActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.coca_logo_provisorio);

        notificationBuilder.setAutoCancel(true)
                .addAction(new NotificationCompat.Action(R.drawable.coca_logo_provisorio,"Abrir alertas", resultPendingIntent))
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.coca_logo_provisorio)
                .setLargeIcon(bitmap)
                .setTicker("OPTIMUS")
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setContentText(body)
                .setContentInfo("Info");
        notificationManager.notify(1, notificationBuilder.build());
    }

    public static void showNormalNotificationEndTurn(Context context, Bundle params){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "sgi_cocaandina_android_id";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Cierre de sesión automático");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);


        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("cocacola://coca.andinaargentina.com" + ViewsNames.ROUTE_VIEW_NAME_SPLASH));


        // This ensures that the back button follows the recommended
        // convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Adds the back stack for the Intent (but not the Intent itself)
        // stackBuilder.addParentStack(WebViewActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.coca_logo_provisorio);

        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.coca_logo_provisorio)
                .setLargeIcon(bitmap)
                .setTicker("SGI")
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle("SGI Coca-Andina")
                .setContentText("Finalización de turno, cierre de sesión")
                .setContentInfo("Info");
        notificationManager.notify(1, notificationBuilder.build());
    }

    public static void showNormalNotificationFiveBefore(Context context, Bundle params){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "sgi_cocaandina_android_id";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Quedan 5 minutes para cierre de seión");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("cocacola://coca.andinaargentina.com" + ViewsNames.ROUTE_VIEW_NAME_HOME));


        // This ensures that the back button follows the recommended
        // convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Adds the back stack for the Intent (but not the Intent itself)
        // stackBuilder.addParentStack(WebViewActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.coca_logo_provisorio);

        notificationBuilder.setAutoCancel(true)
                .addAction(new NotificationCompat.Action(R.drawable.coca_logo_provisorio,"Abrir alertas", resultPendingIntent))
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.coca_logo_provisorio)
                .setLargeIcon(bitmap)
                .setTicker("SGI")
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle("SGI Coca-Andina")
                .setContentText("Quedan 5 minutos para cierre de sesión")
                .setContentInfo("Info");
        notificationManager.notify(1, notificationBuilder.build());
    }


}
