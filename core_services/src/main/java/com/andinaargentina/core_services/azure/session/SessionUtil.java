package com.andinaargentina.core_services.azure.session;

import android.content.Context;
import android.content.Intent;

import androidx.work.WorkManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.logout.LogoutService;
import com.andinaargentina.core_services.providers.api.clients.logout.UserLogoutDto;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.legacy.domain.User;
import com.pixplicity.easyprefs.library.Prefs;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

public class SessionUtil {
    private CompositeDisposable compositeDisposable;
    private LogoutService logoutService;

    public SessionUtil() {
        this.logoutService = new LogoutService(ApiRestModule.providesApiService(false));
        this.compositeDisposable = new CompositeDisposable();
    }

    public void logout(Context context) {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        Paper.book("SGI-BOOK").destroy();
        Prefs.clear();
        final WorkManager mWorkManager = WorkManager.getInstance();
        mWorkManager.cancelAllWork();
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(context);
        /*UserLogoutDto userLogoutDto = new UserLogoutDto(user.getMUserId(), user.getMToken(), user.ismEsSupervisor(), false);
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = logoutService
                    .postLogout(userLogoutDto, context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getLogoutObserver(context));
            compositeDisposable.add(requestGetProductsDisposable);
        }*/
    }

    private DisposableSingleObserver<UserLogoutDto> getLogoutObserver(Context ctx){
        return new DisposableSingleObserver<UserLogoutDto>() {
            @Override
            public void onSuccess(UserLogoutDto response) {
                Paper.book("SGI-BOOK").destroy();
                Prefs.clear();
                final WorkManager mWorkManager = WorkManager.getInstance();
                mWorkManager.cancelAllWork();
                ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .navigation(ctx);
            }

            @Override
            public void onError(Throwable e) {
                Paper.book("SGI-BOOK").destroy();
                Prefs.clear();
                final WorkManager mWorkManager = WorkManager.getInstance();
                mWorkManager.cancelAllWork();
            }
        };
    }
}