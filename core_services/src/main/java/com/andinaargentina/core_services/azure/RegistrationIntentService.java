package com.andinaargentina.core_services.azure;

import android.app.IntentService;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.BuildConfig;
import com.andinaargentina.core_services.providers.api.clients.device.AddDeviceRequestDto;
import com.andinaargentina.core_services.providers.api.clients.device.AddDeviceResponseDto;
import com.andinaargentina.core_services.providers.api.clients.device.AddDeviceService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.legacy.domain.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.windowsazure.messaging.NotificationHub;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.concurrent.TimeUnit;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.andinaargentina.RepositoryConst.FCM_REGISTRATION_ID;
import static com.andinaargentina.RepositoryConst.FCM_TOKEN;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;

@RequiresApi(api = Build.VERSION_CODES.N)
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    String FCM_token = null;

    private NotificationHub hub;

    private AddDeviceService addDeviceService;
    private CompositeDisposable compositeDisposable;

    public RegistrationIntentService() {
        super(TAG);
        addDeviceService = new AddDeviceService(ApiRestModule.providesApiService(false));
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String resultString = null;
        String regID = null;
        String storedToken = null;

        try {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    FCM_token = instanceIdResult.getToken();
                    Log.d(TAG, "FCM Registration Token: " + FCM_token);
                }
            });
            TimeUnit.SECONDS.sleep(1);

            // Storing the registration ID that indicates whether the generated token has been
            // sent to your server. If it is not stored, send the token to your server.
            // Otherwise, your server should have already received the token.
            if (((regID=Prefs.getString(FCM_REGISTRATION_ID,null)) == null)){
                NotificationHub hub = null;
                if (BuildConfig.FLAVOR == "prod"){
                    hub = new NotificationHub(NotificationSettings.HubNameProduction,
                            NotificationSettings.HubListenConnectionStringProduction, this);
                }else{
                    hub = new NotificationHub(NotificationSettings.HubNameDev,
                            NotificationSettings.HubListenConnectionStringDev, this);
                }
                Log.d(TAG, "Attempting a new registration with NH using FCM token : " + FCM_token);
                //regID = hub.register(FCM_token).getRegistrationId();

                // If you want to use tags...
                // Refer to : https://azure.microsoft.com/documentation/articles/notification-hubs-routing-tag-expressions/
                // regID = hub.register(token, "tag1,tag2").getRegistrationId();
                SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED, null);
                regID = hub.register(FCM_token, String.valueOf(user.getUserId())).getRegistrationId();

                resultString = "New NH Registration Successfully - RegId : " + regID;
                Log.d(TAG, resultString);

                Prefs.putString(FCM_REGISTRATION_ID,regID);
                Prefs.putString(FCM_TOKEN, FCM_token);
            }

            // Check to see if the token has been compromised and needs refreshing.
            else if ((storedToken=Prefs.getString(FCM_TOKEN, "")) != FCM_token) {
                if (BuildConfig.FLAVOR == "prod"){
                    hub = new NotificationHub(NotificationSettings.HubNameProduction,
                            NotificationSettings.HubListenConnectionStringProduction, this);
                }else{
                    hub = new NotificationHub(NotificationSettings.HubNameDev,
                            NotificationSettings.HubListenConnectionStringDev, this);
                }
                Log.d(TAG, "NH Registration refreshing with token : " + FCM_token);
                try{
                    // If you want to use tags...
                    // Refer to : https://azure.microsoft.com/documentation/articles/notification-hubs-routing-tag-expressions/
                    // regID = hub.register(token, "tag1,tag2").getRegistrationId();
                    SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED, null);
                    regID = hub.register(FCM_token, String.valueOf(user.getUserId())).getRegistrationId();
                    resultString = "New NH Registration Successfully - RegId : " + regID;
                    Log.d(TAG, resultString);
                }catch (Exception exception){
                    Log.d(TAG, "Error registrando el device en azure hub");
                }

                Prefs.putString(FCM_REGISTRATION_ID,regID);
                Prefs.putString(FCM_TOKEN, FCM_token);
            }

            else {
                resultString = "Previously Registered Successfully - RegId : " + regID;
            }
        } catch (Exception e) {
            Log.e(TAG, resultString="Failed to complete registration", e);
            // If an exception happens while fetching the new token or updating registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
        }

        // Notify UI that registration has completed.
        Log.d(TAG, "Notify UI that registration has completed.");
        this.addDevice(regID,FCM_token);
    }

    private void addDevice(String regID, String tockenId){
        try{
            /*User user = Paper.book("SGI-BOOK").read(USER_LOGGED, null);
            if (user != null){
                AddDeviceRequestDto addDeviceRequestDto = new AddDeviceRequestDto(user.getMUserId(),regID, tockenId);
                if (!compositeDisposable.isDisposed()) {
                    Disposable requestGetProductsDisposable = addDeviceService
                            .adddDevice(addDeviceRequestDto,getApplicationContext())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(getAddDeviceObserver());
                    compositeDisposable.add(requestGetProductsDisposable);
                }
            }*/
        }catch (Exception e){
            Crashes.trackError(e);
        }
    }

    private DisposableSingleObserver<AddDeviceResponseDto> getAddDeviceObserver() {
        return new DisposableSingleObserver<AddDeviceResponseDto>() {
            @Override
            public void onSuccess(AddDeviceResponseDto changeOrderRequestDto) {
                Log.i(TAG, "petición exitosa");
            }

            @Override
            public void onError(Throwable e) {
                Log.i(TAG, "petición con error");
            }
        };
    }
}