package com.andinaargentina.core_services.azure;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.andinaargentina.core_services.azure.notifications.UiNotificationsHelper;
import com.andinaargentina.core_services.azure.session.SessionUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.eventbus.EventBus;

public class FirebaseService extends FirebaseMessagingService
{
    private String TAG = "FirebaseService";

    public static final String NOTIFICATION_CHANNEL_ID = "sgi_cocaandina_android_id";
    public static final String NOTIFICATION_CHANNEL_NAME = "Notificaciones Optimus";
    public static final String NOTIFICATION_CHANNEL_DESCRIPTION = "Canal de notificaciones de alertas";

    //Data {notificationType=2}
    public static final int NOTIFICATION_TYPE_PP = 0;
    public static final int NOTIFICATION_TYPE_CIERRE_TURNO_EXTENDIDO = 1; //---> Al login
    public static final int NOTIFICATION_TYPE_CIERRE_TURNO_NORMAL = 2; //---> Al login
    public static final int NOTIFICATION_TYPE_MERMA = 3;
    public static final int NOTIFICATION_TYPE_CIERRE_TURNO_EXTENDIDO_CON_LINEA_EXISTENTE = 4; //---->Refresco la pantalla
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    static Context ctx;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Id: " + remoteMessage.getMessageId());
        Log.d(TAG, "Data: " + remoteMessage.getData());
        Log.d(TAG, "Notification: " + remoteMessage.getNotification());

        String nhMessage;
        String title;
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            nhMessage = remoteMessage.getNotification().getBody();
            title = remoteMessage.getNotification().getTitle();
        }
        else {
            nhMessage = "Error en la notificacion";//remoteMessage.getData().values().iterator().next();
            title = "Optimus CocaAndina";
        }
        UiNotificationsHelper.showNormalNotification(getApplicationContext(),title, nhMessage);
        sendNotification(nhMessage);
        MessageData messageData = new Gson().fromJson(remoteMessage.getData().toString(), MessageData.class);
        if (messageData.notificationType == NOTIFICATION_TYPE_CIERRE_TURNO_NORMAL){
            SessionUtil utilSesion = new SessionUtil();
            //utilSesion.logout(getApplicationContext());
        }
    }

    private void sendNotification(String msg) {
            Log.d("Notification", "La notificación llegó correctamente :) " + msg);
        //EventBus.getDefault().post(new OnMessageReceiver());
    }

    public static void createChannelAndHandleNotifications(Context context) {
        ctx = context;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(NOTIFICATION_CHANNEL_DESCRIPTION);
            channel.setShowBadge(true);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public class MessageData{
        @SerializedName("notificationType")
        public int notificationType;
    }

    public class OnMessageReceiver{
    }
}