package com.andinaargentina.core_services;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class UtilDate {

    /**
     * Obtiene un String con el formato "dd/MM/yyyy" desde un objeto del tipo
     * Date
     *
     * @param date
     *            El objeto Date con la fecha
     * @return El String con la fecha formateada
     */
    @SuppressLint("SimpleDateFormat")
    public static String fechaToString(Date date) {
        if (date != null) {
            SimpleDateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy");
            return fechaHora.format(date);
        }else
            return "";

    }

    /**
     * Obtiene un String con el formato "HH:mm" 0-23 Horas desde un objeto del
     * tipo Date
     *
     * @param date
     *            El objeto Date con la fecha
     * @return El String con la fecha formateada
     */
    @SuppressLint("SimpleDateFormat")
    public static String horaToString(Date date) {
        if (date != null)
        {
            SimpleDateFormat fechaHora = new SimpleDateFormat("HH:mm");
            return fechaHora.format(date);
        }else
            return "";
    }

    /**
     * Obtiene un String con el formato "dd/MM/yyyy HH:mm" 0-23 Horas desde un
     * objeto del tipo Date
     *
     * @param date
     *            El objeto Date con la fecha
     * @return El String con la fecha formateada
     */
    @SuppressLint("SimpleDateFormat")
    public static String fechaHoraToString(Date date) {
        if (date != null)
        {
            SimpleDateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return fechaHora.format(date);
        }else
            return "";
    }

    /**
     * Convierte una Cadena con el formato "dd/MM/yyyy" a un objeto del tipo
     * Date
     *
     * @param cadena
     *            La cadena que contiene la hora en el formato necesario
     * @return Un objeto Date que contiene la hora solicitada
     */
    @SuppressLint("SimpleDateFormat")
    public static Date fechaToDate(String cadena) {
        if (cadena == null || cadena.compareTo("")==0) {
            String a = new String("00/00/0000");
            cadena = a;
        }
        SimpleDateFormat formato;
        Date fecha = null;
        try {
            formato = new SimpleDateFormat("dd/MM/yyyy");
            fecha = formato.parse(cadena);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fecha;
    }

    /**
     * Convierte una Cadena con el formato HH:mm desde 0-23 Horas y m minutos a
     * un objeto del tipo Date
     *
     * @param cadena
     *            La cadena que contiene la hora en el formato necesario
     * @return Un objeto Date que contiene la hora solicitada
     */
    @SuppressLint("SimpleDateFormat")
    public static Date horaToDate(String cadena) {
        if (cadena == null || cadena.compareTo("")==0) {
            String a = new String("00:00");
            cadena = a;
        }
        SimpleDateFormat formato;
        Date fecha = null;
        try {
            formato = new SimpleDateFormat("HH:mm");
            fecha = formato.parse(cadena);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fecha;
    }

    /**
     * Convierte una Cadena con el formato "dd/MM/yyyy HH:mm" desde 0-23 Horas y
     * m minutos a un objeto del tipo Date
     *
     * @param cadena
     *            La cadena que contiene la hora en el formato necesario
     * @return Un objeto Date que contiene la hora solicitada
     */
    @SuppressLint("SimpleDateFormat")
    public static Date fechaHoraToDate(String cadena) {
        if (cadena == null || cadena.compareTo("")==0) {
            cadena = "00/00/00 00:00";
        }
        SimpleDateFormat formato;
        Date fecha = null;
        try {
            formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            fecha = formato.parse(cadena);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fecha;
    }

    public static Date fechaHoraToDate(String cadena,String formato) {
        SimpleDateFormat dateFormat;
        Date fecha = null;
        try {
            dateFormat = new SimpleDateFormat(formato);
            fecha = dateFormat.parse(cadena);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fecha;
    }



    /**
     * Luego agregar parametro segun corresponda
     * @return
     */
    public static Date newDate(){
        return new Date();
    }

    public static long calculateDifference(Date startDate, Date endDate, boolean inDays, boolean inHours, boolean inMinutes, boolean inSeconds) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long out = 0;

        if(inDays) {
            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;
            out = elapsedDays;
        }

        if(inHours) {
            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;
            out = elapsedHours;
        }

        if(inMinutes) {
            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;
            out = elapsedMinutes;
        }

        if(inSeconds) {
            long elapsedSeconds = different / secondsInMilli;
            out = elapsedSeconds;
        }

        return out;
    }

    public static int getYearOfDate(Date d){
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.YEAR);
    }

    public static int getMonthOfDate(Date d){
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.MONTH);
    }

    public static String getStringOfDate(String dateString, String formatString,String nuevoFormat){
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        Date newDate = null;
        String date="";
        try {
            newDate = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (newDate!=null){
            format = new SimpleDateFormat(nuevoFormat);
            date = format.format(newDate);
        }
        return date;
    }


}
