package com.andinaargentina.core_services.router;

public class ViewsNames {

    public static final String ROUTE_VIEW_NAME_SPLASH = "/splash/splash";
    public static final String ROUTE_VIEW_NAME_LOGIN = "/legacy/login";
    public static final String ROUTE_VIEW_NAME_CHANGE_PASSWORD = "";
    public static final String ROUTE_VIEW_NAME_UPDATE = "/sesion/update";

    public static final String ROUTE_VIEW_NAME_LINE_SELECTOR = "/seleccion/lineSelector";

    //HOME
    public static final String ROUTE_VIEW_NAME_HOME = "/home/activity";
    public static final String ROUTE_VIEW_NAME_ALERTS_INFORMED = "/home/alerts-informed";
    public static final String ROUTE_VIEW_NAME_HOME_LINES_CONTAINER = "/home/lines-container";

    //OBSERVER-MODE
    public static final String ROUTE_VIEW_NAME_VISUALIZATION_MODE = "/monitor/visualization-mode";

    //public static final String ROUTE_VIEW_NAME_LINE_SELECTOR = "/test/main";
    //ALERTS
    public static final String ROUTE_FRAGMENT_VIEW_NAME_ALERTS_READED = "/alerts/readed";
    public static final String ROUTE_FRAGMENT_VIEW_NAME_ALERTS_NEWS = "/alerts/news";
    public static final String ROUTE_FRAGMENT_VIEW_NAME_ALERTS_CONTAINER = "/alerts/container";

    //GRAPHICS
    public static final String ROUTE_FRAGMENT_VIEW_NAME_GRAPHICS_CONTAINER = "/graphics/container";
    public static final String ROUTE_FRAGMENT_VIEW_NAME_GRAPHICS_ITEM = "/graphics/item";
}
