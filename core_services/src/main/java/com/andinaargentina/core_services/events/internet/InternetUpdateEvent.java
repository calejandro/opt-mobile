package com.andinaargentina.core_services.events.internet;

public class InternetUpdateEvent {
    private boolean isConnected;

    public InternetUpdateEvent(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}

