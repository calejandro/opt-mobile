package com.andinaargentina.core_services.events.efficiency;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class EfficiencyErrorEvent extends BaseHttpErrorEvent {

    public EfficiencyErrorEvent(ServiceError error) {
        super(error);
    }

}
