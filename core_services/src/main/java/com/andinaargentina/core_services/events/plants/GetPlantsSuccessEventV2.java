package com.andinaargentina.core_services.events.plants;

import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;

import java.util.List;

public class GetPlantsSuccessEventV2 {
    private List<PlantasDto> response;

    public GetPlantsSuccessEventV2(List<PlantasDto> response) {
        this.response = response;
    }

    public List<PlantasDto> getResponse() {
        return response;
    }

    public void setResponse(List<PlantasDto> response) {
        this.response = response;
    }
}
