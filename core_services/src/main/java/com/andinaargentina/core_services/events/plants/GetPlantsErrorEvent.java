package com.andinaargentina.core_services.events.plants;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class GetPlantsErrorEvent extends BaseHttpErrorEvent {

    public GetPlantsErrorEvent(ServiceError error) {
        super(error);
    }

}
