package com.andinaargentina.core_services.events.login;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class LoginErrorEvent extends BaseHttpErrorEvent {

    public LoginErrorEvent(ServiceError error) {
        super(error);
    }

}
