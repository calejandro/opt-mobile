package com.andinaargentina.core_services.events.alerts;

import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;

import java.util.List;

public class AlertsSuccessEvent {

    private List<LineasProduccionAlertasDto> response;

    public AlertsSuccessEvent(List<LineasProduccionAlertasDto> response) {
        this.response = response;
    }

    public List<LineasProduccionAlertasDto> getResponse() {
        return response;
    }

    public void setResponse(List<LineasProduccionAlertasDto> response) {
        this.response = response;
    }
}
