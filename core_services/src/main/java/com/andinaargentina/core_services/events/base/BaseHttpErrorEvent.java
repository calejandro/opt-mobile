package com.andinaargentina.core_services.events.base;

import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class BaseHttpErrorEvent {

    private ServiceError error;

    public BaseHttpErrorEvent(ServiceError error) {
        this.error = error;
    }

    public ServiceError getError() {
        return error;
    }

    public void setError(ServiceError error) {
        this.error = error;
    }
}
