package com.andinaargentina.core_services.events.ValidateVersion;

public class ValidateVersionSuccessEvent {

    private String response;

    public ValidateVersionSuccessEvent(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
