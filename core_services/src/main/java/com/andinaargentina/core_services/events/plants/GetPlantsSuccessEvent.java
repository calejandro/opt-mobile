package com.andinaargentina.core_services.events.plants;

import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;

import java.util.List;

public class GetPlantsSuccessEvent {

    private List<PlantDto> response;

    public GetPlantsSuccessEvent(List<PlantDto> response) {
        this.response = response;
    }

    public List<PlantDto> getResponse() {
        return response;
    }

    public void setResponse(List<PlantDto> response) {
        this.response = response;
    }
}

