package com.andinaargentina.core_services.events.alerts;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class AlertsErrorEvent extends BaseHttpErrorEvent {

    public AlertsErrorEvent(ServiceError error) {
        super(error);
    }

}
