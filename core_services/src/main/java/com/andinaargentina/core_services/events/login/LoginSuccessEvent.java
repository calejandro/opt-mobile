package com.andinaargentina.core_services.events.login;

import com.andinaargentina.core_services.providers.api.clients.login.dto.User;

public class LoginSuccessEvent {

    private User userData;

    public LoginSuccessEvent(User userData) {
        this.userData = userData;
    }

    public User getUserData() {
        return userData;
    }

    public void setUserData(User userData) {
        this.userData = userData;
    }
}
