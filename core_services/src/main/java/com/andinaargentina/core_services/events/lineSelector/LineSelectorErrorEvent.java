package com.andinaargentina.core_services.events.lineSelector;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class LineSelectorErrorEvent extends BaseHttpErrorEvent {

    public LineSelectorErrorEvent(ServiceError error) {
        super(error);
    }

}
