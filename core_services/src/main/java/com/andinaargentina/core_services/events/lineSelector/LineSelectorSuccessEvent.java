package com.andinaargentina.core_services.events.lineSelector;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

import java.util.List;

public class LineSelectorSuccessEvent {

    private List<LineasProduccionDto> response;

    public LineSelectorSuccessEvent(List<LineasProduccionDto> response) {
        this.response = response;
    }

    public List<LineasProduccionDto> getResponse() {
        return response;
    }

    public void setResponse(List<LineasProduccionDto> response) {
        this.response = response;
    }
}
