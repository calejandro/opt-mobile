package com.andinaargentina.core_services.events.base.localproviders;

import androidx.annotation.NonNull;

public class BaseLocalProviderErrorEvent {

    @NonNull
    private String errorMessage;

    public BaseLocalProviderErrorEvent() {
        this.errorMessage = "Default error";
    }

    @NonNull
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@NonNull String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
