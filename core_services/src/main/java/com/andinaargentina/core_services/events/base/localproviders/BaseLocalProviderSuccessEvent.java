package com.andinaargentina.core_services.events.base.localproviders;

public abstract class BaseLocalProviderSuccessEvent<T> {

    private T dataResponse;

    public T getDataResponse() {
        return dataResponse;
    }

    public void setDataResponse(T dataResponse) {
        this.dataResponse = dataResponse;
    }
}
