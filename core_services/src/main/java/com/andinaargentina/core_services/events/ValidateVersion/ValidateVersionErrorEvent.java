package com.andinaargentina.core_services.events.ValidateVersion;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

public class ValidateVersionErrorEvent extends BaseHttpErrorEvent {

    public ValidateVersionErrorEvent(ServiceError error) {
        super(error);
    }

}
