package com.andinaargentina.core_services.events.internet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

public class InternetConnectionUpdateReceiver extends BroadcastReceiver {

    private final String TAG = "INTERNET_RECEIVER";

    @Override
    public void onReceive(Context context, Intent intent) {
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager
                    .getActiveNetworkInfo();
            boolean isConnected = false;
            if (networkInfo != null && networkInfo.isConnected()) {
                isConnected = true;
            }
            EventBus.getDefault().post(new InternetUpdateEvent(isConnected));
            Logger.t(TAG)
                    .d("El estado de internet cambio value is_connected: "+isConnected);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}