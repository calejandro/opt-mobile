package com.andinaargentina.core_services.events.efficiency;

import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyLineDto;

import java.util.List;

public class EfficiencySuccessEvent {

    private List<EfficiencyLineDto> response;

    public EfficiencySuccessEvent(List<EfficiencyLineDto> response) {
        this.response = response;
    }

    public List<EfficiencyLineDto> getResponse() {
        return response;
    }

    public void setResponse(List<EfficiencyLineDto> response) {
        this.response = response;
    }
}
