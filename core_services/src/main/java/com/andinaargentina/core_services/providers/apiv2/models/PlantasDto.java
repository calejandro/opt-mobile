package com.andinaargentina.core_services.providers.apiv2.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.List;

@Parcel
public class PlantasDto implements Serializable {
    @SerializedName("idPlanta")
    private  int idPlanta;
    @SerializedName("descPlanta")
    private String descPlanta;
    @SerializedName("fhUltModif")
    private String fhUltModif;
    @SerializedName("activo")
    private boolean activo;
    @SerializedName("idPais")
    private Integer idPais;
    @SerializedName("pais")
    private PaisesDto pais;
    @SerializedName("sysUsersPlantas")
    private List<SysUsersPlantsDto> sysUsersPlantas;

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public String getDescPlanta() {
        return descPlanta;
    }

    public void setDescPlanta(String descPlanta) {
        this.descPlanta = descPlanta;
    }

    public String getFhUltModif() {
        return fhUltModif;
    }

    public void setFhUltModif(String fhUltModif) {
        this.fhUltModif = fhUltModif;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public PaisesDto getPais() {
        return pais;
    }

    public void setPais(PaisesDto pais) {
        this.pais = pais;
    }

    public List<SysUsersPlantsDto> getSysUsersPlantas() {
        return sysUsersPlantas;
    }

    public void setSysUsersPlantas(List<SysUsersPlantsDto> sysUsersPlantas) {
        this.sysUsersPlantas = sysUsersPlantas;
    }
}
