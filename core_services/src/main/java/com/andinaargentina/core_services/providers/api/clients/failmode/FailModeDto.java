package com.andinaargentina.core_services.providers.api.clients.failmode;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class FailModeDto {

    @SerializedName("machineId")
    private int idMachine;

    @SerializedName("productionLineId")
    private int idLine;

    @SerializedName("failsModes")
    private List<DescriptionDto> failModesDto;

    @SerializedName("fails")
    private List<DescriptionDto> failsDto;

    @SerializedName("causes")
    private List<DescriptionDto> causesDto;

    public FailModeDto(int idMachine, int idLine, List<DescriptionDto> failModesDto, List<DescriptionDto> failsDto, List<DescriptionDto> causesDto) {
        this.idMachine = idMachine;
        this.idLine = idLine;
        this.failModesDto = failModesDto;
        this.failsDto = failsDto;
        this.causesDto = causesDto;
    }

    public FailModeDto() {
    }

    public List<DescriptionDto> getFailModesDto() {
        return failModesDto;
    }

    public void setFailModesDto(List<DescriptionDto> failModesDto) {
        this.failModesDto = failModesDto;
    }

    public List<DescriptionDto> getFailsDto() {
        return failsDto;
    }

    public void setFailsDto(List<DescriptionDto> failsDto) {
        this.failsDto = failsDto;
    }

    public List<DescriptionDto> getCausesDto() {
        return causesDto;
    }

    public void setCausesDto(List<DescriptionDto> causesDto) {
        this.causesDto = causesDto;
    }


    public int getIdMachine() {
        return idMachine;
    }

    public void setIdMachine(int idMachine) {
        this.idMachine = idMachine;
    }

    public int getIdLine() {
        return idLine;
    }

    public void setIdLine(int idLine) {
        this.idLine = idLine;
    }
}
