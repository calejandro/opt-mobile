
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Details implements Serializable
{

    @SerializedName("productionOrderId")
    @Expose
    private int productionOrderId;
    @SerializedName("skuId")
    @Expose
    private int skuId;
    @SerializedName("bottlesObjective")
    @Expose
    private int bottlesObjective;
    @SerializedName("bottlesProducedLastHour")
    @Expose
    private int bottlesProducedLastHour;
    @SerializedName("remainingBottlesToProduce")
    @Expose
    private int remainingBottlesToProduce;
    @SerializedName("remainingBottleToProduceInShift")
    @Expose
    private int remainingBottleToProduceInShift;
    private final static long serialVersionUID = 8849982527901082129L;

    public int getProductionOrderId() {
        return productionOrderId;
    }

    public void setProductionOrderId(int productionOrderId) {
        this.productionOrderId = productionOrderId;
    }

    public int getSkuId() {
        return skuId;
    }

    public void setSkuId(int skuId) {
        this.skuId = skuId;
    }

    public int getBottlesObjective() {
        return bottlesObjective;
    }

    public void setBottlesObjective(int bottlesObjective) {
        this.bottlesObjective = bottlesObjective;
    }

    public int getBottlesProducedLastHour() {
        return bottlesProducedLastHour;
    }

    public void setBottlesProducedLastHour(int bottlesProducedLastHour) {
        this.bottlesProducedLastHour = bottlesProducedLastHour;
    }

    public int getRemainingBottlesToProduce() {
        return remainingBottlesToProduce;
    }

    public void setRemainingBottlesToProduce(int remainingBottlesToProduce) {
        this.remainingBottlesToProduce = remainingBottlesToProduce;
    }

    public int getRemainingBottleToProduceInShift() {
        return remainingBottleToProduceInShift;
    }

    public void setRemainingBottleToProduceInShift(int remainingBottleToProduceInShift) {
        this.remainingBottleToProduceInShift = remainingBottleToProduceInShift;
    }

}
