
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CombinedChartDto implements Serializable
{

    @SerializedName("axisYName")
    @Expose
    private String axisYName;

    @SerializedName("axisXName")
    @Expose
    private String axisXName;

    @SerializedName("axisYMaximumEfficiencyValue")
    @Expose
    private double axisYMaximumEfficiencyValue;

    @SerializedName("axisYMaximumProductionValue")
    @Expose
    private int axisYMaximumProductionValue;

    @SerializedName("efficiencyLineChart")
    @Expose
    private List<EfficiencyLineChart> efficiencyLineChart = null;

    @SerializedName("productionBarChart")
    @Expose
    private List<ProductionBarChart> productionBarChart = null;


    private final static long serialVersionUID = -2379744866839295720L;

    public String getAxisYName() {
        return axisYName;
    }

    public void setAxisYName(String axisYName) {
        this.axisYName = axisYName;
    }

    public String getAxisXName() {
        return axisXName;
    }

    public void setAxisXName(String axisXName) {
        this.axisXName = axisXName;
    }

    public double getAxisYMaximumEfficiencyValue() {
        return axisYMaximumEfficiencyValue;
    }

    public void setAxisYMaximumEfficiencyValue(double axisYMaximumEfficiencyValue) {
        this.axisYMaximumEfficiencyValue = axisYMaximumEfficiencyValue;
    }

    public int getAxisYMaximumProductionValue() {
        return axisYMaximumProductionValue;
    }

    public void setAxisYMaximumProductionValue(int axisYMaximumProductionValue) {
        this.axisYMaximumProductionValue = axisYMaximumProductionValue;
    }

    public List<EfficiencyLineChart> getEfficiencyLineChart() {
        return efficiencyLineChart;
    }

    public void setEfficiencyLineChart(List<EfficiencyLineChart> efficiencyLineChart) {
        this.efficiencyLineChart = efficiencyLineChart;
    }

    public List<ProductionBarChart> getProductionBarChart() {
        return productionBarChart;
    }

    public void setProductionBarChart(List<ProductionBarChart> productionBarChart) {
        this.productionBarChart = productionBarChart;
    }

}
