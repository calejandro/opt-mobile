package com.andinaargentina.core_services.providers.apiv2.models;

public class ConfiguracionUmbralesTmefTmprDto {
    private int id;
    private int idLinea;
    private int idEquipo;
    private String tipo;
    private Double valorMinimo;
    private Double valorMaximo;
    private int idPlanta;
    private LineasProduccionDto lineaProduccion;
    private EquiposDto equipo;
    private PlantasDto planta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Double valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public Double getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(Double valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public EquiposDto getEquipo() {
        return equipo;
    }

    public void setEquipo(EquiposDto equipo) {
        this.equipo = equipo;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }
}
