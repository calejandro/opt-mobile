package com.andinaargentina.core_services.providers.api.clients.alerts;

import android.content.Context;

import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class InformAlertService extends BaseService<List<AlertLineDto>> {

    private ApiRestService apiRestService;

    @Inject
    public InformAlertService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<List<AlertLineDto>> informAlert(AlertDto alertToInform, String userID, Context context) {
        return super.getSingleObservable(apiRestService.informAlert(alertToInform, userID),context);
    }

    public Single<List<AlertLineDto>> informFailModeAlert(AlertFailModeInformRequestDto alertToInform, String userID, Context context) {
        return super.getSingleObservable(apiRestService.informFailModeAlert(alertToInform, userID),context);
    }
}
