
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Value implements Serializable
{

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("bottles")
    @Expose
    private int bottles;
    private final static long serialVersionUID = -4758032056138154042L;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getBottles() {
        return bottles;
    }

    public void setBottles(int bottles) {
        this.bottles = bottles;
    }

}
