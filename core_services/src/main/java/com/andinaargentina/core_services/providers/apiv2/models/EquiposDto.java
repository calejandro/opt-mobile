package com.andinaargentina.core_services.providers.apiv2.models;

public class EquiposDto {
    private int	integer;
    private String descEquipo;
    private int  idLinea;
    private Integer idPlanta;
    private Integer ordenVisualizacion;
    private Integer tipoEquipo;
    private Integer puedeRecuperarHistoricos;
    private Integer intervaloMuestreoMinutos;
    private Integer cantidadDecimales;
    private Double alLeerMultiplicarPor;
    private Integer alLeerMultiplicarPorPotenciaDiezALa;
    private String fhUltModif;
    private boolean activo;
    private Boolean prorratearLecturasPorMinuto;
    private Double ignorarValorMenorA;
    private Double ignorarValorMayorA;
    private LineasProduccionDto lineaProduccion;
    private PlantasDto planta;
    private AlertaMermaConfiguracionesDto alertaMermaConfiguraciones;

    public int getInteger() {
        return integer;
    }

    public void setInteger(int integer) {
        this.integer = integer;
    }

    public String getDescEquipo() {
        return descEquipo;
    }

    public void setDescEquipo(String descEquipo) {
        this.descEquipo = descEquipo;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public Integer getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(Integer idPlanta) {
        this.idPlanta = idPlanta;
    }

    public Integer getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(Integer ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public Integer getTipoEquipo() {
        return tipoEquipo;
    }

    public void setTipoEquipo(Integer tipoEquipo) {
        this.tipoEquipo = tipoEquipo;
    }

    public Integer getPuedeRecuperarHistoricos() {
        return puedeRecuperarHistoricos;
    }

    public void setPuedeRecuperarHistoricos(Integer puedeRecuperarHistoricos) {
        this.puedeRecuperarHistoricos = puedeRecuperarHistoricos;
    }

    public Integer getIntervaloMuestreoMinutos() {
        return intervaloMuestreoMinutos;
    }

    public void setIntervaloMuestreoMinutos(Integer intervaloMuestreoMinutos) {
        this.intervaloMuestreoMinutos = intervaloMuestreoMinutos;
    }

    public Integer getCantidadDecimales() {
        return cantidadDecimales;
    }

    public void setCantidadDecimales(Integer cantidadDecimales) {
        this.cantidadDecimales = cantidadDecimales;
    }

    public Double getAlLeerMultiplicarPor() {
        return alLeerMultiplicarPor;
    }

    public void setAlLeerMultiplicarPor(Double alLeerMultiplicarPor) {
        this.alLeerMultiplicarPor = alLeerMultiplicarPor;
    }

    public Integer getAlLeerMultiplicarPorPotenciaDiezALa() {
        return alLeerMultiplicarPorPotenciaDiezALa;
    }

    public void setAlLeerMultiplicarPorPotenciaDiezALa(Integer alLeerMultiplicarPorPotenciaDiezALa) {
        this.alLeerMultiplicarPorPotenciaDiezALa = alLeerMultiplicarPorPotenciaDiezALa;
    }

    public String getFhUltModif() {
        return fhUltModif;
    }

    public void setFhUltModif(String fhUltModif) {
        this.fhUltModif = fhUltModif;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Boolean getProrratearLecturasPorMinuto() {
        return prorratearLecturasPorMinuto;
    }

    public void setProrratearLecturasPorMinuto(Boolean prorratearLecturasPorMinuto) {
        this.prorratearLecturasPorMinuto = prorratearLecturasPorMinuto;
    }

    public Double getIgnorarValorMenorA() {
        return ignorarValorMenorA;
    }

    public void setIgnorarValorMenorA(Double ignorarValorMenorA) {
        this.ignorarValorMenorA = ignorarValorMenorA;
    }

    public Double getIgnorarValorMayorA() {
        return ignorarValorMayorA;
    }

    public void setIgnorarValorMayorA(Double ignorarValorMayorA) {
        this.ignorarValorMayorA = ignorarValorMayorA;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }

    public AlertaMermaConfiguracionesDto getAlertaMermaConfiguraciones() {
        return alertaMermaConfiguraciones;
    }

    public void setAlertaMermaConfiguraciones(AlertaMermaConfiguracionesDto alertaMermaConfiguraciones) {
        this.alertaMermaConfiguraciones = alertaMermaConfiguraciones;
    }
}
