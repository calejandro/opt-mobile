package com.andinaargentina.core_services.providers.local.user.read;

import com.andinaargentina.core_services.events.base.localproviders.BaseLocalProviderSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.login.dto.User;

public class UserLocalReadProviderSuccessEvent extends BaseLocalProviderSuccessEvent<User> {
}
