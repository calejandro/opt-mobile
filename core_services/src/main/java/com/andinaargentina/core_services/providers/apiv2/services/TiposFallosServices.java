package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.TiposFallosDto;

public interface TiposFallosServices extends BaseService<TiposFallosDto, BaseFilter> {
}
