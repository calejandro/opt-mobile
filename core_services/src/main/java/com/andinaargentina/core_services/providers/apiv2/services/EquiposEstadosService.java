package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.EquiposEstadosDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EquiposEstadosService {
    @GET("EquiposEstados")
    Call<List<EquiposEstadosDto>> list();
}
