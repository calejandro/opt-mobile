package com.andinaargentina.core_services.providers.apiv2.models;

public class MachineStatusProductionOrderDto {
    private int idLinea;
    private String descripcionLinea;
    private int idOrdenSAP;
    private int idSKU;
    private String descripcionSKU;
    private int idEquipo;
    private String descEquipo;
    private int ordenVisualizacion;
    private int idEstado;
    private String descripcionEstado;
    private int minutosAcumulados;
    private Double minutosAcumuladosPorcentaje;

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public int getIdOrdenSAP() {
        return idOrdenSAP;
    }

    public void setIdOrdenSAP(int idOrdenSAP) {
        this.idOrdenSAP = idOrdenSAP;
    }

    public int getIdSKU() {
        return idSKU;
    }

    public void setIdSKU(int idSKU) {
        this.idSKU = idSKU;
    }

    public String getDescripcionSKU() {
        return descripcionSKU;
    }

    public void setDescripcionSKU(String descripcionSKU) {
        this.descripcionSKU = descripcionSKU;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getDescEquipo() {
        return descEquipo;
    }

    public void setDescEquipo(String descEquipo) {
        this.descEquipo = descEquipo;
    }

    public int getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(int ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }

    public int getMinutosAcumulados() {
        return minutosAcumulados;
    }

    public void setMinutosAcumulados(int minutosAcumulados) {
        this.minutosAcumulados = minutosAcumulados;
    }

    public Double getMinutosAcumuladosPorcentaje() {
        return minutosAcumuladosPorcentaje;
    }

    public void setMinutosAcumuladosPorcentaje(Double minutosAcumuladosPorcentaje) {
        this.minutosAcumuladosPorcentaje = minutosAcumuladosPorcentaje;
    }
}
