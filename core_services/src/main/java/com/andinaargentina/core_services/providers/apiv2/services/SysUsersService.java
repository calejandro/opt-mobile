package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.LinkTemporalOrderDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SysUsersService{

    @GET("SysUsers/addSysUser")
    Call listAddSysUser();

    @GET("SysUsers/me")
    Call<SysUsersDto> me(@Query("includes") String[] includes);

    @GET("SysUsers/me/lineas")
    Call<List<LineasProduccionUsuarioDto>> meLineas(@Query("includes") String[] includes);
}