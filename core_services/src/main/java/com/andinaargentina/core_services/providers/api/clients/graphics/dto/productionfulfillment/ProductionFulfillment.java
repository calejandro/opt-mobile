
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductionFulfillment implements Serializable
{

    @SerializedName("productionLineId")
    @Expose
    private int productionLineId;
    @SerializedName("fulfillmentPercentaje")
    @Expose
    private double fulfillmentPercentaje;
    @SerializedName("details")
    @Expose
    private Details details;
    @SerializedName("combinedChart")
    @Expose
    private CombinedChart_ combinedChart;
    private final static long serialVersionUID = -2657912768980712825L;

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public double getFulfillmentPercentaje() {
        return fulfillmentPercentaje;
    }

    public void setFulfillmentPercentaje(double fulfillmentPercentaje) {
        this.fulfillmentPercentaje = fulfillmentPercentaje;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public CombinedChart_ getCombinedChart() {
        return combinedChart;
    }

    public void setCombinedChart(CombinedChart_ combinedChart) {
        this.combinedChart = combinedChart;
    }

}
