package com.andinaargentina.core_services.providers.local.linesselected;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.local.base.CrudLocalProvider;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;

import java.util.List;

public class LinesSelectedProvider extends CrudLocalProvider<List<LineasProduccionDto>> {
}
