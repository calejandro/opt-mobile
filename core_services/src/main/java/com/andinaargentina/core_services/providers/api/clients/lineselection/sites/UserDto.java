package com.andinaargentina.core_services.providers.api.clients.lineselection.sites;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class UserDto implements Serializable {

    @SerializedName("id")
    long id;

    @SerializedName("fullName")
    String fullname;

    public UserDto() {
    }

    public UserDto(long id, String fullname) {
        this.id = id;
        this.fullname = fullname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
