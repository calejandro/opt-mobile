package com.andinaargentina.core_services.providers.api.clients.plants;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class PlantListService extends BaseService<List<PlantDto>> {

    private ApiRestService apiRestService;

    @Inject
    public PlantListService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<List<PlantDto>> getPlantsList(Context context) {
        return super.getSingleObservable(apiRestService.getPlants(),context);
    }

}
