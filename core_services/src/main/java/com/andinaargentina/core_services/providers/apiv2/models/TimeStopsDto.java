package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class TimeStopsDto {
    private int idLinea;
    private String descripcionLinea;
    private Date hora;
    private int  idEquipo;
    private String descripcionEquipo;
    private int ordenVisualizacion;
    private int minutosAcumulados;
    private Integer idOrdenSAP;

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getDescripcionEquipo() {
        return descripcionEquipo;
    }

    public void setDescripcionEquipo(String descripcionEquipo) {
        this.descripcionEquipo = descripcionEquipo;
    }

    public int getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(int ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public int getMinutosAcumulados() {
        return minutosAcumulados;
    }

    public void setMinutosAcumulados(int minutosAcumulados) {
        this.minutosAcumulados = minutosAcumulados;
    }

    public Integer getIdOrdenSAP() {
        return idOrdenSAP;
    }

    public void setIdOrdenSAP(Integer idOrdenSAP) {
        this.idOrdenSAP = idOrdenSAP;
    }
}
