package com.andinaargentina.core_services.providers.apiv2.models;

public class UserSendResetDto {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

