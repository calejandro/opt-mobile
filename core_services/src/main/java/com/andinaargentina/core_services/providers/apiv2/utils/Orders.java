package com.andinaargentina.core_services.providers.apiv2.utils;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Orders {
    private List<Order> orders;

    public Orders() {
        this.orders = new ArrayList<Order>();
    }

    public List<Order> getOrders() {
        return orders;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this.orders);
    }
}
