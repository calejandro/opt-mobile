package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class ShiftDto {
    private String turno;
    private Date fecha;
    private Date inicio;
    private Date fin;
    private Boolean paso;

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Boolean getPaso() {
        return paso;
    }

    public void setPaso(Boolean paso) {
        this.paso = paso;
    }
}
