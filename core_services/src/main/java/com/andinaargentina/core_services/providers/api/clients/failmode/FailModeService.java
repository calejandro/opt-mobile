package com.andinaargentina.core_services.providers.api.clients.failmode;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import javax.inject.Inject;

import io.reactivex.Single;

public class FailModeService extends BaseService<FailModeDto> {

    private ApiRestService apiRestService;

    @Inject
    public FailModeService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<FailModeDto> getFailModes(String machineId, String lineId, Context context) {
        return super.getSingleObservable(apiRestService.getFailMode(machineId, lineId),context);
    }

    public Single<FailModeDto> getFailModesByAlertKind(String machineId, String lineId, int productionAlertKind, Context context) {
        return super.getSingleObservable(apiRestService.getFailModeByAlertKind(machineId, lineId, productionAlertKind),context);
    }

    public void cancel(){
        if (getCall()!=null)
            getCall().cancel();
    }
}
