package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class EvolucionParadaPropiaDto {
    private int idLinea;
    private String descripDescripcion_Linea;
    private Integer iD_Equipo;
    private String descripcion_Equipo;
    private Date banda_Horaria;
    private int minutos_Acumulados;

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescripDescripcion_Linea() {
        return descripDescripcion_Linea;
    }

    public void setDescripDescripcion_Linea(String descripDescripcion_Linea) {
        this.descripDescripcion_Linea = descripDescripcion_Linea;
    }

    public Integer getiD_Equipo() {
        return iD_Equipo;
    }

    public void setiD_Equipo(Integer iD_Equipo) {
        this.iD_Equipo = iD_Equipo;
    }

    public String getDescripcion_Equipo() {
        return descripcion_Equipo;
    }

    public void setDescripcion_Equipo(String descripcion_Equipo) {
        this.descripcion_Equipo = descripcion_Equipo;
    }

    public Date getBanda_Horaria() {
        return banda_Horaria;
    }

    public void setBanda_Horaria(Date banda_Horaria) {
        this.banda_Horaria = banda_Horaria;
    }

    public int getMinutos_Acumulados() {
        return minutos_Acumulados;
    }

    public void setMinutos_Acumulados(int minutos_Acumulados) {
        this.minutos_Acumulados = minutos_Acumulados;
    }
}
