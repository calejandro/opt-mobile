package com.andinaargentina.core_services.providers.api.core.base;

import com.andinaargentina.core_services.BuildConfig;
import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.gson.DoubleTypeAdapter;
import com.andinaargentina.core_services.providers.api.core.gson.IntTypeAdapter;
import com.andinaargentina.core_services.providers.api.core.gson.LongTypeAdapter;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.pixplicity.easyprefs.library.Prefs;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiRestModule {

    //Network constants
    private static final int TIMEOUT_CONNECT = 60;   //In seconds
    private static final int TIMEOUT_WRITE = 60;   //In seconds
    private static final int TIMEOUT_READ = 60;   //In seconds
    private static final String FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static List<OkHttpClient> listHttpClients;

    public static OkHttpClient getOkHttpClient(Interceptor interceptor) {
        return new OkHttpClient
                .Builder()
                .connectTimeout(TIMEOUT_CONNECT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_WRITE, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
                .addInterceptor(getRequestInterceptor())
                .addInterceptor(new StethoInterceptor())
                .addInterceptor(interceptor)
                .build();
    }

    private static HttpLoggingInterceptor getRequestInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(
                BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    private static Gson getGson(){
        return new GsonBuilder()
                .registerTypeAdapter(int.class, new IntTypeAdapter())
                .registerTypeAdapter(Integer.class, new IntTypeAdapter())
                .registerTypeAdapter(double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(Double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(long.class, new LongTypeAdapter())
                .registerTypeAdapter(Long.class, new LongTypeAdapter())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                //.setDateFormat(FORMAT_DATE)
                .create();
    }

    private static RxJava2CallAdapterFactory getRXAdapterFactory(){
        return RxJava2CallAdapterFactory
                .createWithScheduler(Schedulers.io());
    }

    @Provides
    //@Singleton
    public static ApiRestService providesApiService(boolean isOnlyApi) {
        String username = Prefs.getString("username", "");
        String password = Prefs.getString("password", "");
        String authToken = Credentials.basic(username, password);
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
        String baseUrl;
        if (BuildConfig.FLAVOR == "prod")
            baseUrl = (isOnlyApi) ?
                    "https://optimus.servicios-andina-ar.com/webapi/api/"
                    : "https://optimus.servicios-andina-ar.com/api/";
        else
            baseUrl = (isOnlyApi) ?
                "https://optimus-dev.servicios-andina-ar.com/webapi/api/"
                : "https://optimus-dev.servicios-andina-ar.com/api/";
        OkHttpClient httpClient = getOkHttpClient(interceptor);
        if (listHttpClients==null){
            listHttpClients = new ArrayList<>();
        }
        listHttpClients.add(httpClient);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(getRXAdapterFactory())
                .build();
        return retrofit.create(ApiRestService.class);
    }

    public static List<OkHttpClient> getListHttpClients(){
        return listHttpClients;
    }

    public static class DateDeserializer implements JsonDeserializer<Date> {

        private final String[] DATE_FORMATS = new String[] {
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                "yyyy-MM-dd'T'HH:mm:ss.SSS",
                "yyyy-MM-dd'T'HH:mm:ss.SS",
                "yyyy-MM-dd'T'HH:mm:ss",
        };

        @Override
        public Date deserialize(JsonElement jsonElement, Type typeOF,
                                JsonDeserializationContext context) throws JsonParseException {
            for (String format : DATE_FORMATS) {
                try {
                    return new SimpleDateFormat(format, Locale.US).parse(jsonElement.getAsString());
                } catch (ParseException e) {
                }
            }
            throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
                    + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));
        }
    }
}