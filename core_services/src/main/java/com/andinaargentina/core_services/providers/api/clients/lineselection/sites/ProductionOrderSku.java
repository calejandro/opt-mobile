package com.andinaargentina.core_services.providers.api.clients.lineselection.sites;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class ProductionOrderSku implements Serializable {

    @SerializedName("id")
    int id;

    @SerializedName("description")
    String description;

    public ProductionOrderSku() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
