package com.andinaargentina.core_services.providers.apiv2.clients;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.util.List;

import io.reactivex.Single;

public class PlantListServiceV2 extends BaseService<List<PlantasDto>> {

    public  PlantListServiceV2() {

    }
    
    public Single<List<PlantasDto>> getPlantsList(Context context) {
        return super.getSingleObservable(OptimusServices.getInstance().getPlantasService().list(),context);
    }
}
