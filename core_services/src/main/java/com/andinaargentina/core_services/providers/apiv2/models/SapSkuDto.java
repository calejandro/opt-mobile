package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class SapSkuDto implements Serializable {
    private int idSku;
    private String descSku;
    private String formato;
    private String marca;
    private String sabor;
    private String indicadorCarbono;
    private String materialEnvase;
    private String retornable;
    private String unidadMedida;
    private double conversionBotellas;
    private double conversionLitros;
    private int idCentro;

    public int getIdSku() {
        return idSku;
    }

    public void setIdSku(int idSku) {
        this.idSku = idSku;
    }

    public String getDescSku() {
        return descSku;
    }

    public void setDescSku(String descSku) {
        this.descSku = descSku;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    public String getIndicadorCarbono() {
        return indicadorCarbono;
    }

    public void setIndicadorCarbono(String indicadorCarbono) {
        this.indicadorCarbono = indicadorCarbono;
    }

    public String getMaterialEnvase() {
        return materialEnvase;
    }

    public void setMaterialEnvase(String materialEnvase) {
        this.materialEnvase = materialEnvase;
    }

    public String getRetornable() {
        return retornable;
    }

    public void setRetornable(String retornable) {
        this.retornable = retornable;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public double getConversionBotellas() {
        return conversionBotellas;
    }

    public void setConversionBotellas(int conversionBotellas) {
        this.conversionBotellas = conversionBotellas;
    }

    public double getConversionLitros() {
        return conversionLitros;
    }

    public void setConversionLitros(int conversionLitros) {
        this.conversionLitros = conversionLitros;
    }

    public int getIdCentro() {
        return idCentro;
    }

    public void setIdCentro(int idCentro) {
        this.idCentro = idCentro;
    }
}
