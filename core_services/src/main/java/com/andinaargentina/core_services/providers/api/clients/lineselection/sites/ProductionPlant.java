package com.andinaargentina.core_services.providers.api.clients.lineselection.sites;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class ProductionPlant implements Serializable {

    @SerializedName("description")
    String description;

    @SerializedName("id")
    int id;

    public ProductionPlant(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public ProductionPlant() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
