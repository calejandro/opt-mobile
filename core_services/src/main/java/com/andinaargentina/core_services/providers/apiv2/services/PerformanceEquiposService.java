package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.models.AverageTimeFailuresProductionDto;
import com.andinaargentina.core_services.providers.apiv2.models.EvolucionParadaPropiaDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineStatusProductionOrderDto;
import com.andinaargentina.core_services.providers.apiv2.models.TimeStopsDto;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PerformanceEquiposService {

    @GET("PerformanceEquipos/ActualStatus")
    Call<ArrayList<MachineActualStatusDto>> listActualStatus(@Query("idLinea") int idLinea);

    @GET("PerformanceEquipos/ProductionOrders")
    Call<ArrayList<MachineStatusProductionOrderDto>> listProductionOrders(@Query("idLinea") int idLinea,
                                                                          @Query("fechaHoraInicio") Date fechaHoraInicio,
                                                                          @Query("fechaHoraFin") Date fechaHoraFin);

    @GET("PerformanceEquipos/TimeStops")
    Call<ArrayList<TimeStopsDto>> listTimeStops(@Query("idLinea") int idLinea,
                                                @Query("fechaHoraInicio") String fechaHoraInicio,
                                                @Query("fechaHoraFin") String fechaHoraFin,
                                                @Query("idEstado") int idEstado);

    @GET("PerformanceEquipos/AverageTimeFailuresProduction")
    Call<ArrayList<AverageTimeFailuresProductionDto>> listAverageTimeFailuresProduction(@Query("idLinea") int idLinea,
                                                                                        @Query("fechaHoraInicio") String fechaHoraInicio,
                                                                                        @Query("fechaHoraFin") String fechaHoraFin);

    @GET("PerformanceEquipos/Evolucion-ParadaPropia")
    Call<ArrayList<EvolucionParadaPropiaDto>> listEvolucionParadaPropia(@Query("idLinea") int idLinea,
                                                                        @Query("idEquipo") int idEquipo);
}
