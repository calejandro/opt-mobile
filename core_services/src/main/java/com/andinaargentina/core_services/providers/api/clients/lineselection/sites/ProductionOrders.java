package com.andinaargentina.core_services.providers.api.clients.lineselection.sites;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class ProductionOrders implements Serializable {

    @SerializedName("id")
    int id;

    @SerializedName("productionOrderSku")
    ProductionOrderSku productionOrderSku;

    public ProductionOrders() {
    }

    public ProductionOrders(int id, ProductionOrderSku productionOrderSku) {
        this.id = id;
        this.productionOrderSku = productionOrderSku;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductionOrderSku getProductionOrderSku() {
        return productionOrderSku;
    }

    public void setProductionOrderSku(ProductionOrderSku productionOrderSku) {
        this.productionOrderSku = productionOrderSku;
    }

    public String getShortName(){
        return productionOrderSku.getDescription();
    }

}
