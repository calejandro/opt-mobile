package com.andinaargentina.core_services.providers.apiv2.models;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RequiresApi(api = Build.VERSION_CODES.N)
@Parcel
public class SysUsersDto implements Serializable {
    private int userId;
    private String userName;
    private String password;
    private String fullName;
    private String lastName;
    private String email;
    private String initUrl;
    private String isActive;
    private Integer languageId;
    private String changePass;
    private String agreedTerms;
    private String foto;
    private Date fechaAlta;
    private String userHorarioContacto;
    private String telefono;
    private String codAreaTelefono;
    private boolean esAdministrador;
    private String idUserIdentity;
    private UserDto userIdentity;
    private List<SysUsersPlantsDto> sysUsersPlantas;
    private List<MultipleGroupsDto> groups;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getName() {
        return this.fullName + ", " + this.getLastName();
    }

    public boolean ismEsSupervisor() {
        try{
            Optional<MultipleGroupsDto> usersGroupsDto = this.groups.stream().filter(f -> f.getUserGroupId() == 2).findAny();
            if(usersGroupsDto.isPresent()) {
                return true;
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return  false;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInitUrl() {
        return initUrl;
    }

    public void setInitUrl(String initUrl) {
        this.initUrl = initUrl;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public String getChangePass() {
        return changePass;
    }

    public void setChangePass(String changePass) {
        this.changePass = changePass;
    }

    public String getAgreedTerms() {
        return agreedTerms;
    }

    public void setAgreedTerms(String agreedTerms) {
        this.agreedTerms = agreedTerms;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUserHorarioContacto() {
        return userHorarioContacto;
    }

    public void setUserHorarioContacto(String userHorarioContacto) {
        this.userHorarioContacto = userHorarioContacto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCodAreaTelefono() {
        return codAreaTelefono;
    }

    public void setCodAreaTelefono(String codAreaTelefono) {
        this.codAreaTelefono = codAreaTelefono;
    }

    public boolean isEsAdministrador() {
        return esAdministrador;
    }

    public void setEsAdministrador(boolean esAdministrador) {
        this.esAdministrador = esAdministrador;
    }

    public String getIdUserIdentity() {
        return idUserIdentity;
    }

    public void setIdUserIdentity(String idUserIdentity) {
        this.idUserIdentity = idUserIdentity;
    }

    public UserDto getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(UserDto userIdentity) {
        this.userIdentity = userIdentity;
    }

    public List<SysUsersPlantsDto> getSysUsersPlantas() {
        return sysUsersPlantas;
    }

    public void setSysUsersPlantas(List<SysUsersPlantsDto> sysUsersPlantas) {
        this.sysUsersPlantas = sysUsersPlantas;
    }

    public List<MultipleGroupsDto> getGroups() {
        return groups;
    }

    public void setGroups(List<MultipleGroupsDto> groups) {
        this.groups = groups;
    }
}
