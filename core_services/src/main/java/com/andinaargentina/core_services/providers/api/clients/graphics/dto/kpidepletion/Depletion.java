
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Depletion implements Serializable
{

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("actualValue")
    @Expose
    private double actualValue;

    @SerializedName("accumulated")
    @Expose
    private double accumulated;

    @SerializedName("details")
    @Expose
    private Details__ details;

    @SerializedName("actualUmbralType")
    @Expose
    private String actualUmbralType;

    @SerializedName("accumulatedUmbralType")
    @Expose
    private String accumulatedUmbralType;


    private boolean isSelected;

    private final static long serialVersionUID = 1275410349498346782L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getActualValue() {
        return actualValue;
    }

    public void setActualValue(double actualValue) {
        this.actualValue = actualValue;
    }

    public double getAccumulated() {
        return accumulated;
    }

    public void setAccumulated(double accumulated) {
        this.accumulated = accumulated;
    }

    public Details__ getDetails() {
        return details;
    }

    public void setDetails(Details__ details) {
        this.details = details;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getActualUmbralType() {
        return actualUmbralType;
    }

    public void setActualUmbralType(String actualUmbralType) {
        this.actualUmbralType = actualUmbralType;
    }

    public String getAccumulatedUmbralType() {
        return accumulatedUmbralType;
    }

    public void setAccumulatedUmbralType(String accumulatedUmbralType) {
        this.accumulatedUmbralType = accumulatedUmbralType;
    }
}
