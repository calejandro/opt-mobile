package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.utils.Paginator;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PlantasService {

    @GET("Plantas")
    Call<List<PlantasDto>> list();
    @GET("Plantas")
    Call<List<PlantasDto>> list(@Query("includes") String[] includes, @Query("filter") BaseFilter filter, @Query("paginator") Paginator paginator, @Query("order") String orders);

    @GET("Plantas/{id}")
    Call<PlantasDto> get(@Path("id") String id);
    @GET("Plantas/{id}")
    Call<PlantasDto> get(@Path("id") String id, @Query("includes") String[] includes);

    @GET("Plantas/count")
    Call<Integer> count(@Query("filter") BaseFilter filter);

    @POST("Plantas")
    Call<PlantasDto> create(@Body() PlantasDto entity);

    @POST("Plantas")
    Call<PlantasDto> update(@Body() PlantasDto entity);

    @DELETE("Plantas/{id}")
    Call<PlantasDto> delete(String id);

    @DELETE("Plantas/exists/{id}")
    Call<PlantasDto> exists(String id);
}
