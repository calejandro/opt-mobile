package com.andinaargentina.core_services.providers.apiv2.clients;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.reactivex.Single;

public class ShiftService extends BaseService<List<ShiftDto>> {

    public Single<List<ShiftDto>> listShifts(Context context) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return super.getSingleObservable(OptimusServices.getInstance().getTurnosService().list(simpleDateFormat.format(new Date())),context);
    }

}
