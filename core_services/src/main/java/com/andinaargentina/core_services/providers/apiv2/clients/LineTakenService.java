package com.andinaargentina.core_services.providers.apiv2.clients;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LineTakenService extends BaseService<List<LineasProduccionUsuarioDto>> {

    public Single<List<LineasProduccionUsuarioDto>> getLinesTakenByPlant(String idPlant, Date dateFrom, Date dateTo, Context context) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return super.getSingleObservable(OptimusServices.getInstance()
                .getLineasProduccionUsuarioService().list(new String[]{"sysUsers", "sapOrdenesProduccion", "lineaProduccion"},
                idPlant, true, null, simpleDateFormat.format(dateFrom), simpleDateFormat.format(dateTo)), context);
    }
}
