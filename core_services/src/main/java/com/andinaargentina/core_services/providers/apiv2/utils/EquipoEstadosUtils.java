package com.andinaargentina.core_services.providers.apiv2.utils;

import com.andinaargentina.core_services.providers.apiv2.models.EquiposEstadosDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EquipoEstadosUtils {
    private static EquipoEstadosUtils instance;

    private List<EquiposEstadosDto> estados;

    private Observable<List<EquiposEstadosDto>> observable = Observable.create(new ObservableOnSubscribe<List<EquiposEstadosDto>>() {
        @Override
        public void subscribe(@NonNull ObservableEmitter<List<EquiposEstadosDto>> emitter) throws Exception {
            if(estados == null) {
                OptimusServices.getInstance().getEquiposEstadosService().list().enqueue(new Callback<List<EquiposEstadosDto>>() {
                    @Override
                    public void onResponse(Call<List<EquiposEstadosDto>> call, Response<List<EquiposEstadosDto>> response) {
                        estados = response.body();
                        emitter.onNext(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<EquiposEstadosDto>> call, Throwable t) {

                    }
                });
            } else {
                emitter.onNext(estados);
            }
        }
    });

    private EquipoEstadosUtils() {

    }

    public static EquipoEstadosUtils getInstance() {
        if(instance == null) {
            instance = new EquipoEstadosUtils();
        }
        return instance;
    }

    public List<EquiposEstadosDto> getEstados() {
        return estados;
    }

    public Observable<List<EquiposEstadosDto>> getObservable() {
        return observable;
    }

}
