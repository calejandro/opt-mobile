
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EfficiencyLineChart implements Serializable
{

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("value")
    @Expose
    private double value;

    private final static long serialVersionUID = -5002794655115375120L;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

}
