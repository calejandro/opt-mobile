package com.andinaargentina.core_services.providers.apiv2.models;

public class LinkTemporalOrderDto {
    private int idOrdenSap;
    private int idOrdenSapTemporal;

    public int getIdOrdenSap() {
        return idOrdenSap;
    }

    public void setIdOrdenSap(int idOrdenSap) {
        this.idOrdenSap = idOrdenSap;
    }

    public int getIdOrdenSapTemporal() {
        return idOrdenSapTemporal;
    }

    public void setIdOrdenSapTemporal(int idOrdenSapTemporal) {
        this.idOrdenSapTemporal = idOrdenSapTemporal;
    }
}
