package com.andinaargentina.core_services.providers.api.clients.lineselection.selected;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class SelectedDto {

    @SerializedName("id")//Id de toma de línea
    private int idTakenLine;

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("userId")
    private int userId;

    @SerializedName("productionOrderId")
    private int productionOrderId;

    @SerializedName("startProductionDate")
    private Date startProductionDate;//"2020-01-02T14:00:22.532Z"

    public SelectedDto(int productionLineId, int userId, int productionOrderId, Date startProductionDate) {
        this.productionLineId = productionLineId;
        this.userId = userId;
        this.productionOrderId = productionOrderId;
        if (startProductionDate!=null)
            this.startProductionDate = startProductionDate;
        else{
            this.startProductionDate = null;
        }
    }

    public SelectedDto(int productionLineId, int userId, int productionOrderId, Date startProductionDate,
                       int idTakenLine) {
        this.idTakenLine = idTakenLine;
        this.productionLineId = productionLineId;
        this.userId = userId;
        this.productionOrderId = productionOrderId;
        if (startProductionDate!=null)
            this.startProductionDate = startProductionDate;
        else{
            this.startProductionDate = null;
        }
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductionOrderId() {
        return productionOrderId;
    }

    public void setProductionOrderId(int productionOrderId) {
        this.productionOrderId = productionOrderId;
    }

    public int getIdTakenLine() {
        return idTakenLine;
    }

    public void setIdTakenLine(int idTakenLine) {
        this.idTakenLine = idTakenLine;
    }

    public Date getStartProductionDate() {
        return startProductionDate;
    }

    public void setStartProductionDate(Date startProductionDate) {
        this.startProductionDate = startProductionDate;
    }
}
