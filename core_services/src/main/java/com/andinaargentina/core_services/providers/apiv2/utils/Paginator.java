package com.andinaargentina.core_services.providers.apiv2.utils;

public class Paginator {

    private int rowsPage;
    private int page;

    public Paginator() {

    }

    public Paginator(int rowsPage, int page) {
        this.rowsPage = rowsPage;
        this.page = page;
    }

    public int getRowsPage() {
        return rowsPage;
    }

    public void setRowsPage(int rowsPage) {
        this.rowsPage = rowsPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
