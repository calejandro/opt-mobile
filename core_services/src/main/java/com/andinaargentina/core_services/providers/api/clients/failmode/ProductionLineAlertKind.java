package com.andinaargentina.core_services.providers.api.clients.failmode;

public class ProductionLineAlertKind {
    public static final int MachineActualStatus = 1;
    public static final int KpiDepletion = 2;
}
