
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Machine implements Serializable
{

    @SerializedName("machineName")
    @Expose
    private String machineName;
    @SerializedName("minutes")
    @Expose
    private int minutes;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("details")
    @Expose
    private Details_ details;
    private final static long serialVersionUID = 5148101491656737803L;

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getType() {
        return type;
    }

    public String getFullNameType(){
        String typeFullName = "Parada propia (PP)";
        switch (type){
            case "PO":{
                typeFullName = "Parada operacional (PO)";
                break;
            }
            case "FP":{
                typeFullName = "Fuera de producción (FP)";
                break;
            }
            case "EP":{
                typeFullName = "En producción (EP)";
                break;
            }
            case "PF":{
                typeFullName = "Parada funcional (PF)";
                break;
            }
            case "PP":{
                typeFullName = "Parada propia (PP)";
                break;
            }
            case "PA":{
                typeFullName = "Parada de abastecimiento (PA)";
                break;
            }case "PD":{
                typeFullName = "Parada de descarga (PD)";
                break;
            }
            case "SV":{
                typeFullName = "Sub velocidad (SV)";
                break;
            }
            case "0":{
                typeFullName = "No determinado (0)";
            }
        }
        return typeFullName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Details_ getDetails() {
        return details;
    }

    public void setDetails(Details_ details) {
        this.details = details;
    }

}
