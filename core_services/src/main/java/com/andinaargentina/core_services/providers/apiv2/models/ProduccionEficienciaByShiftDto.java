package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class ProduccionEficienciaByShiftDto {
    private String descripcionLinea;
    private Date fechaHora;
    private int botellasProducidas;
    private Double eficiencia;
    private int idOrdenSAP;
    private int objetivo;

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getBotellasProducidas() {
        return botellasProducidas;
    }

    public void setBotellasProducidas(int botellasProducidas) {
        this.botellasProducidas = botellasProducidas;
    }

    public Double getEficiencia() {
        return eficiencia;
    }

    public void setEficiencia(Double eficiencia) {
        this.eficiencia = eficiencia;
    }

    public int getIdOrdenSAP() {
        return idOrdenSAP;
    }

    public void setIdOrdenSAP(int idOrdenSAP) {
        this.idOrdenSAP = idOrdenSAP;
    }

    public int getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(int objetivo) {
        this.objetivo = objetivo;
    }
}
