
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Details_ implements Serializable
{

    @SerializedName("historical")
    @Expose
    private List<Historical> historical = null;

    @SerializedName("shift")
    @Expose
    private TmefTmprDto shift;

    @SerializedName("time")
    @Expose
    private TmefTmprDto time;

    @SerializedName("evolutionChart")
    @Expose
    private EvolutionChart evolutionChart;

    private final static long serialVersionUID = -8287894215352290792L;

    public List<Historical> getHistorical() {
        return historical;
    }

    public void setHistorical(List<Historical> historical) {
        this.historical = historical;
    }

    public TmefTmprDto getShift() {
        return shift;
    }

    public void setShift(TmefTmprDto shift) {
        this.shift = shift;
    }

    public TmefTmprDto getTime() {
        return time;
    }

    public void setTime(TmefTmprDto time) {
        this.time = time;
    }

    public EvolutionChart getEvolutionChart() {
        return evolutionChart;
    }

    public void setEvolutionChart(EvolutionChart evolutionChart) {
        this.evolutionChart = evolutionChart;
    }

    public class TmefTmprDto{
        private TMNodeDto tmef;
        private TMNodeDto tmpr;

        public TmefTmprDto(TMNodeDto tmef, TMNodeDto tmpr) {
            this.tmef = tmef;
            this.tmpr = tmpr;
        }

        public TmefTmprDto() {
        }

        public TMNodeDto getTmef() {
            return tmef;
        }

        public void setTmef(TMNodeDto tmef) {
            this.tmef = tmef;
        }

        public TMNodeDto getTmpr() {
            return tmpr;
        }

        public void setTmpr(TMNodeDto tmpr) {
            this.tmpr = tmpr;
        }
    }

    public class TMNodeDto{
        @SerializedName("value")
        @Expose
        private double value;

        @SerializedName("threshold")
        @Expose
        private String threshold;

        public TMNodeDto(double value, String threshold) {
            this.value = value;
            this.threshold = threshold;
        }

        public TMNodeDto() {
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public String getThreshold() {
            return threshold;
        }

        public void setThreshold(String threshold) {
            this.threshold = threshold;
        }
    }

}
