
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Historical_ implements Serializable
{

    @SerializedName("hour")
    @Expose
    private String hour;
    @SerializedName("depletionValue")
    @Expose
    private double depletionValue;
    @SerializedName("umbralType")
    @Expose
    private String umbralType;
    private final static long serialVersionUID = -3948766702583414737L;

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public double getDepletionValue() {
        return depletionValue;
    }

    public void setDepletionValue(double depletionValue) {
        this.depletionValue = depletionValue;
    }

    public String getUmbralType() {
        return umbralType;
    }

    public void setUmbralType(String umbralType) {
        this.umbralType = umbralType;
    }

}
