package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

@Parcel
public class PaisesDto {
    private int idPais;
    private String descPais;
    private String fhUltModif;
    private boolean activo;

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getDescPais() {
        return descPais;
    }

    public void setDescPais(String descPais) {
        this.descPais = descPais;
    }

    public String getFhUltModif() {
        return fhUltModif;
    }

    public void setFhUltModif(String fhUltModif) {
        this.fhUltModif = fhUltModif;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
