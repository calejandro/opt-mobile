package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.BuildConfig;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.api.core.gson.DoubleTypeAdapter;
import com.andinaargentina.core_services.providers.api.core.gson.IntTypeAdapter;
import com.andinaargentina.core_services.providers.api.core.gson.LongTypeAdapter;
import com.andinaargentina.core_services.providers.apiv2.interceptors.AuthenticationInterceptor;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class OptimusServices {

    private static final int TIMEOUT_CONNECT = 60;   //In seconds
    private static final int TIMEOUT_WRITE = 60;   //In seconds
    private static final int TIMEOUT_READ = 60;   //In seconds
    private static final String FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static OptimusServices optimusServices;
    private CausasFallosService causasFallosService;
    private AlertaMermaConfiguracionesService alertaMermaConfiguracionesService;
    private ConfiguracionAlertasParadaMaquinaService configuracionAlertasParadaMaquinaService;
    private ConfiguracionUmbralesTmefTmprService configuracionUmbralesTmefTmprService;
    private EficienciaService eficienciaService;
    private EquiposService equiposService;
    private EquiposEstadosService equiposEstadosService;
    private LineasProduccionService lineasProduccionService;
    private LineasProduccionAlertasService lineasProduccionAlertasService;
    private LineasProduccionEficienciaMesService lineasProduccionEficienciaMesService;
    private LineasProduccionUsuarioService lineasProduccionUsuarioService;
    private MermasKPIService mermasKPIService;
    private PaisesService paisesService;
    private PerformanceEquiposService performanceEquiposService;
    private PlantasService plantasService;
    private SapOrdenesProduccionService sapOrdenesProduccionService;
    private SapSkuService sapSkuService;
    private SysUserReportConfigurationService sysUserReportConfigurationService;
    private SysUsersService sysUsersService;
    private TiposFallosServices tiposFallosServices;
    private TurnosService turnosService;
    private UserService userService;
    private UserGroupService userGroupService;
    private String baseApiUrl;

    private OptimusServices() {

    }

    public  static OptimusServices getInstance() {
        return  getInstance(null);
    }
    public static OptimusServices getInstance(@Nullable String baseApiUrl) {
        if(OptimusServices.optimusServices == null) {
            OptimusServices.optimusServices = new OptimusServices();
            if(baseApiUrl != null) {
                OptimusServices.optimusServices.baseApiUrl = baseApiUrl;
            }
        }
        return  OptimusServices.optimusServices;
    }

    private Retrofit createRetroFit(String resource) {
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor();
        OkHttpClient httpClient = getOkHttpClient(interceptor);
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl(this.optimusServices.baseApiUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(getRXAdapterFactory());


        return  retrofit.build();
    }

    private Retrofit createRetroFitCommon() {
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor();
        OkHttpClient httpClient = getCommonOkHttpClient(interceptor);
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl(this.optimusServices.baseApiUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()));

        return  retrofit.build();
    }

    private Gson getGson(){
        return new GsonBuilder()
                .registerTypeAdapter(int.class, new IntTypeAdapter())
                .registerTypeAdapter(Integer.class, new IntTypeAdapter())
                .registerTypeAdapter(double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(Double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(long.class, new LongTypeAdapter())
                .registerTypeAdapter(Long.class, new LongTypeAdapter())
                .registerTypeAdapter(Date.class, new ApiRestModule.DateDeserializer())
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                //.setDateFormat(FORMAT_DATE)
                .create();
    }

    private RxJava2CallAdapterFactory getRXAdapterFactory(){
        return RxJava2CallAdapterFactory
                .createWithScheduler(Schedulers.io());
    }

    private SSLSocketFactory getSocketFactory() throws KeyManagementException, NoSuchAlgorithmException {
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };
        final SSLContext sslContext;
        sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        return sslSocketFactory;
    }

    public OkHttpClient getOkHttpClient(Interceptor interceptor) {
        try {

            OkHttpClient.Builder builder = new OkHttpClient
                    .Builder()
                    .sslSocketFactory(getSocketFactory())
                    .connectTimeout(TIMEOUT_CONNECT, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_WRITE, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
                    .addInterceptor(getRequestInterceptor())
                    .addInterceptor(new StethoInterceptor())
                    .addInterceptor(interceptor)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    });

            return builder.build();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

        return  null;



    }

    public OkHttpClient getCommonOkHttpClient(Interceptor interceptor) {
        try {
            return new OkHttpClient
                    .Builder()
                    .sslSocketFactory(getSocketFactory())
                    .connectTimeout(TIMEOUT_CONNECT, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_WRITE, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
                    .addInterceptor(getRequestInterceptor())
                    .addInterceptor(interceptor)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    private HttpLoggingInterceptor getRequestInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(
                BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    public CausasFallosService getCausasFallosService() {
        if(causasFallosService == null) {
           causasFallosService = this.createRetroFit("CausasFallos").create(CausasFallosService.class);
        }
        return causasFallosService;
    }
    public AlertaMermaConfiguracionesService getAlertaMermaConfiguracionesService(){
        if(alertaMermaConfiguracionesService == null) {
            alertaMermaConfiguracionesService = this.createRetroFit("AlertaMermaConfiguraciones").create(AlertaMermaConfiguracionesService.class);
        }
        return alertaMermaConfiguracionesService;
    }
    public ConfiguracionAlertasParadaMaquinaService getConfiguracionAlertasParadaMaquinaService(){
        if(configuracionAlertasParadaMaquinaService == null) {
            configuracionAlertasParadaMaquinaService = this.createRetroFit("ConfiguracionAlertasParadaMaquina").create(ConfiguracionAlertasParadaMaquinaService.class);
        }
        return configuracionAlertasParadaMaquinaService;
    }
    public ConfiguracionUmbralesTmefTmprService getConfiguracionUmbralesTmefTmprService(){
        if(configuracionUmbralesTmefTmprService == null) {
            configuracionUmbralesTmefTmprService = this.createRetroFit("ConfiguracionUmbralesTmefTmpr").create(ConfiguracionUmbralesTmefTmprService.class);
        }
        return configuracionUmbralesTmefTmprService;
    }
    public EficienciaService getEficienciaService(){
        if(eficienciaService == null) {
            eficienciaService = this.createRetroFit("Eficiencia").create(EficienciaService.class);
        }
        return eficienciaService;
    }
    public EquiposService getEquiposService(){
        if(equiposService == null) {
            equiposService = this.createRetroFit("Equipos").create(EquiposService.class);
        }
        return equiposService;
    }
    public EquiposEstadosService getEquiposEstadosService(){
        if(equiposEstadosService == null) {
            equiposEstadosService = this.createRetroFit("EquiposEstados").create(EquiposEstadosService.class);
        }
        return equiposEstadosService;
    }
    public LineasProduccionService getLineasProduccionService(){
        if(lineasProduccionService == null) {
            lineasProduccionService = this.createRetroFit("LineasProduccion").create(LineasProduccionService.class);
        }
        return lineasProduccionService;
    }
    public LineasProduccionAlertasService getLineasProduccionAlertasService(){
        if(lineasProduccionAlertasService == null) {
            lineasProduccionAlertasService = this.createRetroFit("LineasProduccionAlertas").create(LineasProduccionAlertasService.class);
        }
        return lineasProduccionAlertasService;
    }
    public LineasProduccionEficienciaMesService getLineasProduccionEficienciaMesService(){
        if(lineasProduccionEficienciaMesService == null) {
            lineasProduccionEficienciaMesService = this.createRetroFit("LineasProduccionEficienciaMes").create(LineasProduccionEficienciaMesService.class);
        }
        return lineasProduccionEficienciaMesService;
    }
    public LineasProduccionUsuarioService getLineasProduccionUsuarioService(){
        if(lineasProduccionUsuarioService == null) {
            lineasProduccionUsuarioService = this.createRetroFit("LineasProduccionUsuario").create(LineasProduccionUsuarioService.class);
        }
        return lineasProduccionUsuarioService;
    }
    public MermasKPIService getMermasKPIService(){
        if(mermasKPIService == null) {
            mermasKPIService = this.createRetroFit("MermasKPI").create(MermasKPIService.class);
        }
        return mermasKPIService;
    }
    public PaisesService getPaisesService(){
        if(paisesService == null) {
            paisesService = this.createRetroFit("Paises").create(PaisesService.class);
        }
        return paisesService;
    }
    public PerformanceEquiposService getPerformanceEquiposService(){
        if(performanceEquiposService == null) {
            performanceEquiposService = this.createRetroFit("PerformanceEquipos").create(PerformanceEquiposService.class);
        }
        return performanceEquiposService;
    }
    public PlantasService getPlantasService(){
        if(plantasService == null) {
            plantasService = this.createRetroFit("Plantas").create(PlantasService.class);
        }
        return plantasService;
    }
    public SapOrdenesProduccionService getSapOrdenesProduccionService(){
        if(sapOrdenesProduccionService == null) {
            sapOrdenesProduccionService = this.createRetroFit("SapOrdenesProduccion").create(SapOrdenesProduccionService.class);
        }
        return sapOrdenesProduccionService;
    }
    public SapSkuService getSapSkuService(){
        if(sapSkuService == null) {
            sapSkuService = this.createRetroFit("SapSku").create(SapSkuService.class);
        }
        return sapSkuService;
    }
    public SysUserReportConfigurationService getSysUserReportConfigurationService(){
        if(sysUserReportConfigurationService == null) {
            sysUserReportConfigurationService = this.createRetroFit("SysUserReportConfiguration").create(SysUserReportConfigurationService.class);
        }
        return sysUserReportConfigurationService;
    }
    public SysUsersService getSysUsersService(){
        if(sysUsersService == null) {
            sysUsersService = this.createRetroFit("SysUsers").create(SysUsersService.class);
        }
        return sysUsersService;
    }
    public TiposFallosServices getTiposFallosServices(){
        if(tiposFallosServices == null) {
            tiposFallosServices = this.createRetroFit("TiposFallos").create(TiposFallosServices.class);
        }
        return tiposFallosServices;
    }
    public TurnosService getTurnosService(){
        if(turnosService == null) {
            turnosService = this.createRetroFitCommon().create(TurnosService.class);
        }
        return turnosService;
    }
    public UserService getUserService(){
        if(userService == null) {
            userService = this.createRetroFit("User").create(UserService.class);
        }
        return userService;
    }
    public UserGroupService getUserGroupService(){
        if(userGroupService == null) {
            userGroupService = this.createRetroFit("UserGroup").create(UserGroupService.class);
        }
        return userGroupService;
    }
}
