package com.andinaargentina.core_services.providers.apiv2.models;

public class LineasProdSapToLineasProdElpDto {
    private  int idLineaElp;
    private  int idLineaSap;
    private LineasProduccionElpDto lineasProduccionElp;
    private LineasProduccionSapDto lineasProduccionSap;

    public int getIdLineaElp() {
        return idLineaElp;
    }

    public void setIdLineaElp(int idLineaElp) {
        this.idLineaElp = idLineaElp;
    }

    public int getIdLineaSap() {
        return idLineaSap;
    }

    public void setIdLineaSap(int idLineaSap) {
        this.idLineaSap = idLineaSap;
    }

    public LineasProduccionElpDto getLineasProduccionElp() {
        return lineasProduccionElp;
    }

    public void setLineasProduccionElp(LineasProduccionElpDto lineasProduccionElp) {
        this.lineasProduccionElp = lineasProduccionElp;
    }

    public LineasProduccionSapDto getLineasProduccionSap() {
        return lineasProduccionSap;
    }

    public void setLineasProduccionSap(LineasProduccionSapDto lineasProduccionSap) {
        this.lineasProduccionSap = lineasProduccionSap;
    }
}
