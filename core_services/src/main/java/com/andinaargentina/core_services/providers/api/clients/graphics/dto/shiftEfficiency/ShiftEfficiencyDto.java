
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShiftEfficiencyDto implements Serializable {

    @SerializedName("productionLineId")
    @Expose
    private int productionLineId;

    @SerializedName("average")
    @Expose
    private double average;

    @SerializedName("combinedChart")
    @Expose
    private CombinedChartDto combinedChartDto;

    private final static long serialVersionUID = 4481004296698611473L;

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public CombinedChartDto getCombinedChartDto() {
        return combinedChartDto;
    }

    public void setCombinedChartDto(CombinedChartDto combinedChartDto) {
        this.combinedChartDto = combinedChartDto;
    }

}
