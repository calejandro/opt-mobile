package com.andinaargentina.core_services.providers.apiv2.models;

public class LineasProduccionSapDto {
    private int idLinea;
    private String descLinea;
    private int idPlanta;
    private int tipoLinea;
    private Integer puedeRecuperarHistoricos;
    private String fhUltModif;
    private boolean activo;
    private PlantasDto planta;
    private LineasProdSapToLineasProdElpDto lineasProdSapToLineasProdElp;
}
