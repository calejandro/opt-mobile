package com.andinaargentina.core_services.providers.api.clients.orders;

import com.google.gson.annotations.SerializedName;

public class ChangeOrderRequestDto {

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("productionOrderId")
    private int productionOrderId;

    @SerializedName("userId")
    private int userId;

    public ChangeOrderRequestDto(int productionLineId, int productionOrderId, int userId) {
        this.productionLineId = productionLineId;
        this.productionOrderId = productionOrderId;
        this.userId = userId;
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public int getProductionOrderId() {
        return productionOrderId;
    }

    public void setProductionOrderId(int productionOrderId) {
        this.productionOrderId = productionOrderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
