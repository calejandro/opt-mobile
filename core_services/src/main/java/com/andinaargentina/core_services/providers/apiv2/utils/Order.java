package com.andinaargentina.core_services.providers.apiv2.utils;

public class Order {
    private String property;
    public boolean descending;

    public  Order(String property, boolean descending) {
        this.property = property;
        this.descending = descending;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public boolean isDescending() {
        return descending;
    }

    public void setDescending(boolean descending) {
        this.descending = descending;
    }
}
