package com.andinaargentina.core_services.providers.api.clients.lineselection.delete;

import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeleteLineSelectedDto {

    @SerializedName("isSupervision")
    private boolean isSupervision;

    @SerializedName("ids")
    private List<String> listIdForDeleteSelections;

    public DeleteLineSelectedDto() {
    }

    public DeleteLineSelectedDto(boolean isSupervision, List<String> listIdForDeleteSelections) {
        this.isSupervision = isSupervision;
        this.listIdForDeleteSelections = listIdForDeleteSelections;
    }

}
