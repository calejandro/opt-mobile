package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionEficienciaMesDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface LineasProduccionEficienciaMesService {

    @GET("LineasProduccionEficienciaMes")
    Call<List<LineasProduccionEficienciaMesDto>> list(@Query("idLinea") Integer lineaId, @Header("plantaId") int plantaId);

    @GET("LineasProduccionEficienciaMes")
    Call<List<LineasProduccionEficienciaMesDto>> list(@Query("idLinea") Integer lineaId, @Header("plantaId") int plantaId, @Query("order") String order);
}
