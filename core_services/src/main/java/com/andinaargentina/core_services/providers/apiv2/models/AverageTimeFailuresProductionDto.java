package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class AverageTimeFailuresProductionDto {
    private int idFila;
    private int idLinea;
    private String descripcionLinea;
    private int idEquipo;
    private String descripcionEquipo;
    private int ordenVisualizacion;
    private Double tmefTurno;
    private Double tmprTurno;
    private Date horaActual;
    private Double tmefHora;
    private Double tmprHora;

    public int getIdFila() {
        return idFila;
    }

    public void setIdFila(int idFila) {
        this.idFila = idFila;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getDescripcionEquipo() {
        return descripcionEquipo;
    }

    public void setDescripcionEquipo(String descripcionEquipo) {
        this.descripcionEquipo = descripcionEquipo;
    }

    public int getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(int ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public Double getTmefTurno() {
        return tmefTurno;
    }

    public void setTmefTurno(Double tmefTurno) {
        this.tmefTurno = tmefTurno;
    }

    public Double getTmprTurno() {
        return tmprTurno;
    }

    public void setTmprTurno(Double tmprTurno) {
        this.tmprTurno = tmprTurno;
    }

    public Date getHoraActual() {
        return horaActual;
    }

    public void setHoraActual(Date horaActual) {
        this.horaActual = horaActual;
    }

    public Double getTmefHora() {
        return tmefHora;
    }

    public void setTmefHora(Double tmefHora) {
        this.tmefHora = tmefHora;
    }

    public Double getTmprHora() {
        return tmprHora;
    }

    public void setTmprHora(Double tmprHora) {
        this.tmprHora = tmprHora;
    }
}
