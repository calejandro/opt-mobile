package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Parcel
public class LineasProduccionUsuarioDto implements Serializable {
    private int id;
    private int idLinea;
    private int userId;
    private Date fechaToma;
    private Boolean supervisada;
    private Integer idOrdenSap;
    private Boolean ordenCumplimentada;
    private Boolean borrado;
    private Boolean extiendeTurno;
    private Date fechaLiberacion;
    private Boolean turnoFinalizado;
    private LineasProduccionDto lineaProduccion;
    private SapOrdenesProduccionDto sapOrdenesProduccion;
    private SysUsersDto sysUsers;
    private List<SapOrdenesProduccionDto> orders;

    public LineasProduccionUsuarioDto() {

    }

    public  LineasProduccionUsuarioDto(int userId, int idLinea, LineasProduccionDto lineaProduccion, boolean supervisada) {
        this.idLinea = idLinea;
        this.userId = userId;
        borrado = false;
        fechaLiberacion = null;
        turnoFinalizado = false;
        this.supervisada = supervisada;
        this.lineaProduccion = lineaProduccion;
        this.ordenCumplimentada = false;
    }

    public List<SapOrdenesProduccionDto> getOrders() {
        return orders;
    }

    public void setOrders(List<SapOrdenesProduccionDto> orders) {
        this.orders = orders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getFechaToma() {
        return fechaToma;
    }

    public void setFechaToma(Date fechaToma) {
        this.fechaToma = fechaToma;
    }

    public Boolean getSupervisada() {
        return supervisada;
    }

    public void setSupervisada(Boolean supervisada) {
        this.supervisada = supervisada;
    }

    public Integer getIdOrdenSap() {
        return idOrdenSap;
    }

    public void setIdOrdenSap(Integer idOrdenSap) {
        this.idOrdenSap = idOrdenSap;
    }

    public Boolean getOrdenCumplimentada() {
        return ordenCumplimentada;
    }

    public void setOrdenCumplimentada(Boolean ordenCumplimentada) {
        this.ordenCumplimentada = ordenCumplimentada;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public Boolean getExtiendeTurno() {
        return extiendeTurno;
    }

    public void setExtiendeTurno(Boolean extiendeTurno) {
        this.extiendeTurno = extiendeTurno;
    }

    public Date getFechaLiberacion() {
        return fechaLiberacion;
    }

    public void setFechaLiberacion(Date fechaLiberacion) {
        this.fechaLiberacion = fechaLiberacion;
    }

    public Boolean getTurnoFinalizado() {
        return turnoFinalizado;
    }

    public void setTurnoFinalizado(Boolean turnoFinalizado) {
        this.turnoFinalizado = turnoFinalizado;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public SapOrdenesProduccionDto getSapOrdenesProduccion() {
        return sapOrdenesProduccion;
    }

    public void setSapOrdenesProduccion(SapOrdenesProduccionDto sapOrdenesProduccion) {
        this.sapOrdenesProduccion = sapOrdenesProduccion;
    }

    public SysUsersDto getSysUsers() {
        return sysUsers;
    }

    public void setSysUsers(SysUsersDto sysUsers) {
        this.sysUsers = sysUsers;
    }
}
