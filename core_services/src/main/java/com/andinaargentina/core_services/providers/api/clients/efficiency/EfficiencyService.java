package com.andinaargentina.core_services.providers.api.clients.efficiency;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class EfficiencyService extends BaseService<List<EfficiencyLineDto>> {

    private ApiRestService apiRestService;

    @Inject
    public EfficiencyService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<List<EfficiencyLineDto>> getEfficiencyReport(String idUser, Context context) {
        return super.getSingleObservable(apiRestService.getEfficiencyReport(idUser),context);
    }

    public void cancel(){
        if (getCall()!=null)
            getCall().cancel();
    }
}
