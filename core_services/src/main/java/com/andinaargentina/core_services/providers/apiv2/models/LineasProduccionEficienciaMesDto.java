package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class LineasProduccionEficienciaMesDto {
    private int id;
    private Integer idLinea;
    private Double eficienciaMensual;
    private Date fechaCreacion;
    private Boolean borrado;
    private int idPlanta;
    private PlantasDto planta;
    private LineasProduccionDto lineaProduccion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Integer idLinea) {
        this.idLinea = idLinea;
    }

    public Double getEficienciaMensual() {
        return eficienciaMensual;
    }

    public void setEficienciaMensual(Double eficienciaMensual) {
        this.eficienciaMensual = eficienciaMensual;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }
}
