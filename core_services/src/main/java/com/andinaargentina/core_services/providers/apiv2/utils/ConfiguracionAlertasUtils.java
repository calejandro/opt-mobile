package com.andinaargentina.core_services.providers.apiv2.utils;

import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionAlertasParadaMaquinaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionUmbralesTmefTmprDto;
import com.andinaargentina.core_services.providers.apiv2.models.EquiposEstadosDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfiguracionAlertasUtils {

    private static ConfiguracionAlertasUtils instance;

    private HashMap<Integer,List<ConfiguracionAlertasParadaMaquinaDto>> estadosAlertas;

    private HashMap<Integer, Observable<List<ConfiguracionAlertasParadaMaquinaDto>>> observable;

    private ConfiguracionAlertasUtils() {
        observable = new HashMap<>();
        estadosAlertas = new HashMap<>();

    }

    public static ConfiguracionAlertasUtils getInstance() {
        if(instance == null) {
            instance = new ConfiguracionAlertasUtils();
        }
        return instance;
    }

    public List<ConfiguracionAlertasParadaMaquinaDto> getEstadosAlertas(int idLinea) {
        return estadosAlertas.get(idLinea);
    }

    public Observable<List<ConfiguracionAlertasParadaMaquinaDto>> getObservable(int idPlanta, int idLinea) {
        Observable<List<ConfiguracionAlertasParadaMaquinaDto>> o;
        if(!observable.containsKey(idLinea)) {
            o = Observable.create(new ObservableOnSubscribe<List<ConfiguracionAlertasParadaMaquinaDto>>() {
                @Override
                public void subscribe(@NonNull ObservableEmitter<List<ConfiguracionAlertasParadaMaquinaDto>> emitter) throws Exception {
                    if(!estadosAlertas.containsKey(idLinea)) {
                        OptimusServices.getInstance().getConfiguracionAlertasParadaMaquinaService().list(idPlanta, idLinea).enqueue(new Callback<List<ConfiguracionAlertasParadaMaquinaDto>>() {
                            @Override
                            public void onResponse(Call<List<ConfiguracionAlertasParadaMaquinaDto>> call, Response<List<ConfiguracionAlertasParadaMaquinaDto>> response) {
                                estadosAlertas.put(idLinea,response.body());
                                emitter.onNext(response.body());
                            }

                            @Override
                            public void onFailure(Call<List<ConfiguracionAlertasParadaMaquinaDto>> call, Throwable t) {
                                System.out.println(t.getMessage());
                            }
                        });
                    } else {
                        emitter.onNext(estadosAlertas.get(idLinea));
                    }
                }
            });
        } else {
            o = observable.get(idLinea);
        }


        return o;
    }
}
