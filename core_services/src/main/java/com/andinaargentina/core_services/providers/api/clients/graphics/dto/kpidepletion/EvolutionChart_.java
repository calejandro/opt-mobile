
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EvolutionChart_ implements Serializable
{

    @SerializedName("axisYName")
    @Expose
    private String axisYName;
    @SerializedName("axisXName")
    @Expose
    private String axisXName;
    @SerializedName("axisYMaxValue")
    @Expose
    private double axisYMaxValue;
    @SerializedName("generalChart")
    @Expose
    private List<GeneralChart> generalChart = null;
    private final static long serialVersionUID = 7237156618730045138L;

    public String getAxisYName() {
        return axisYName;
    }

    public void setAxisYName(String axisYName) {
        this.axisYName = axisYName;
    }

    public String getAxisXName() {
        return axisXName;
    }

    public void setAxisXName(String axisXName) {
        this.axisXName = axisXName;
    }

    public double getAxisYMaxValue() {
        return axisYMaxValue;
    }

    public void setAxisYMaxValue(double axisYMaxValue) {
        this.axisYMaxValue = axisYMaxValue;
    }

    public List<GeneralChart> getGeneralChart() {
        return generalChart;
    }

    public void setGeneralChart(List<GeneralChart> generalChart) {
        this.generalChart = generalChart;
    }

}
