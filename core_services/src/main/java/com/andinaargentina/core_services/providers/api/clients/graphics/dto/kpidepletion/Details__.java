
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Details__ implements Serializable
{

    @SerializedName("historical")
    @Expose
    private List<Historical_> historical = null;

    @SerializedName("evolutionChart")
    @Expose
    private EvolutionChart_ evolutionChart;

    @SerializedName("treeMapChart")
    @Expose
    private List<TreeMapChart> treeMapChart = null;
    private final static long serialVersionUID = 8566990372122991768L;

    public List<Historical_> getHistorical() {
        return historical;
    }

    public void setHistorical(List<Historical_> historical) {
        this.historical = historical;
    }

    public EvolutionChart_ getEvolutionChart() {
        return evolutionChart;
    }

    public void setEvolutionChart(EvolutionChart_ evolutionChart) {
        this.evolutionChart = evolutionChart;
    }

    public List<TreeMapChart> getTreeMapChart() {
        return treeMapChart;
    }

    public void setTreeMapChart(List<TreeMapChart> treeMapChart) {
        this.treeMapChart = treeMapChart;
    }

}
