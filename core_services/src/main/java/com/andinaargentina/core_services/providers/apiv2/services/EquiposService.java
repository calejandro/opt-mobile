package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.EquiposDto;

public interface EquiposService extends BaseService<EquiposDto, BaseFilter>{
}
