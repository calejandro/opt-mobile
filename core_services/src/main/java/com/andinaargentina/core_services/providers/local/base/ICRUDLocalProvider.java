package com.andinaargentina.core_services.providers.local.base;

import androidx.annotation.NonNull;

public interface ICRUDLocalProvider<T> {

    T readData(@NonNull String key);

    void writeData(@NonNull String key, @NonNull T value);

    void deleteData(@NonNull String key);

    void deleteAllData();

    boolean containData(@NonNull String key);
}
