
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TreeMapChart implements Serializable
{

    @SerializedName("machineName")
    @Expose
    private String machineName;

    @SerializedName("depletionValue")
    @Expose
    private double depletionValue;

    @SerializedName("umbraType")
    @Expose
    private String umbraType;

    private final static long serialVersionUID = -2703453991793734661L;

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public double getDepletionValue() {
        return depletionValue;
    }

    public void setDepletionValue(double depletionValue) {
        this.depletionValue = depletionValue;
    }

    public String getUmbraType() {
        return umbraType;
    }

    public void setUmbraType(String umbraType) {
        this.umbraType = umbraType;
    }

}
