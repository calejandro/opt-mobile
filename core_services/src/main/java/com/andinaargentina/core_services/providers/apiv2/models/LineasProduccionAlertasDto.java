package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;
import java.util.List;

public class LineasProduccionAlertasDto {
    private int idAlerta;
    private int idLinea;
    private int idEquipo;
    private String estado;
    private Double valorAcumulado;
    private boolean envioDesvio;
    private Date fechaCreacion;
    private Boolean borrado;
    private String tipoAlerta;
    private String clasificacion;
    private String descripcion;
    private String tipoMerma;
    private Date fechaFin;
    private String umbral;
    private Date fechaActualizacion;
    private Boolean notificacionEnviada;
    private LineasProduccionDto lineaProduccion;
    private EquiposDto equipo;
    private List<RegistrosFallosAlertasDto> registrosFallosAlertas;

    public int getIdAlerta() {
        return idAlerta;
    }

    public void setIdAlerta(int idAlerta) {
        this.idAlerta = idAlerta;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getValorAcumulado() {
        return valorAcumulado;
    }

    public void setValorAcumulado(Double valorAcumulado) {
        this.valorAcumulado = valorAcumulado;
    }

    public boolean isEnvioDesvio() {
        return envioDesvio;
    }

    public void setEnvioDesvio(boolean envioDesvio) {
        this.envioDesvio = envioDesvio;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public String getTipoAlerta() {
        return tipoAlerta;
    }

    public void setTipoAlerta(String tipoAlerta) {
        this.tipoAlerta = tipoAlerta;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoMerma() {
        return tipoMerma;
    }

    public void setTipoMerma(String tipoMerma) {
        this.tipoMerma = tipoMerma;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getUmbral() {
        return umbral;
    }

    public void setUmbral(String umbral) {
        this.umbral = umbral;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Boolean getNotificacionEnviada() {
        return notificacionEnviada;
    }

    public void setNotificacionEnviada(Boolean notificacionEnviada) {
        this.notificacionEnviada = notificacionEnviada;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public EquiposDto getEquipo() {
        return equipo;
    }

    public void setEquipo(EquiposDto equipo) {
        this.equipo = equipo;
    }

    public List<RegistrosFallosAlertasDto> getRegistrosFallosAlertas() {
        return registrosFallosAlertas;
    }

    public void setRegistrosFallosAlertas(List<RegistrosFallosAlertasDto> registrosFallosAlertas) {
        this.registrosFallosAlertas = registrosFallosAlertas;
    }

    public enum STATE {
        NEW, //0
        INFORMED, //1
        SOLVED, //2
        CLOSED, //3
        READED //4 TODO este tipo falta agregar
    }
}
