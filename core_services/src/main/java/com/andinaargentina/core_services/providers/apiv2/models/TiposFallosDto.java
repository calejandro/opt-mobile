package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class TiposFallosDto {
    private int idTip;
    private String nombre;
    private Integer tipo;
    private Date fechaCreacion;
    private Boolean borrado;
    private Boolean activo;
    private Integer idEquipo;
    private Integer idLinea;
    private Integer tipoAlerta;
    private Integer idPlanta;
    private LineasProduccionDto lineaProduccion;
    private EquiposDto equipo;
    private PlantasDto planta;

    public int getIdTip() {
        return idTip;
    }

    public void setIdTip(int idTip) {
        this.idTip = idTip;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }

    public Integer getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Integer idLinea) {
        this.idLinea = idLinea;
    }

    public Integer getTipoAlerta() {
        return tipoAlerta;
    }

    public void setTipoAlerta(Integer tipoAlerta) {
        this.tipoAlerta = tipoAlerta;
    }

    public Integer getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(Integer idPlanta) {
        this.idPlanta = idPlanta;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public EquiposDto getEquipo() {
        return equipo;
    }

    public void setEquipo(EquiposDto equipo) {
        this.equipo = equipo;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }
}
