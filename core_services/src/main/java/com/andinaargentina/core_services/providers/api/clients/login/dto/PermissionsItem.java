
package com.andinaargentina.core_services.providers.api.clients.login.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class PermissionsItem {

    @SerializedName("mPermissionItemId")
    @Expose
    private Integer mPermissionItemId;
    @SerializedName("mParentPermissionItemId")
    @Expose
    private Integer mParentPermissionItemId;
    @SerializedName("mResourceName")
    @Expose
    private String mResourceName;
    @SerializedName("mUrl")
    @Expose
    private String mUrl;
    @SerializedName("mUrlAppMobile")
    @Expose
    private String mUrlAppMobile;
    @SerializedName("mTarget")
    @Expose
    private String mTarget;
    @SerializedName("mMenuItemOrder")
    @Expose
    private Integer mMenuItemOrder;
    @SerializedName("mImage")
    @Expose
    private String mImage;
    @SerializedName("mImageHeight")
    @Expose
    private Integer mImageHeight;
    @SerializedName("mImageWidth")
    @Expose
    private Integer mImageWidth;
    @SerializedName("mIsAuthenticationRequired")
    @Expose
    private String mIsAuthenticationRequired;
    @SerializedName("mIsEnabled")
    @Expose
    private String mIsEnabled;
    @SerializedName("mIsVisible")
    @Expose
    private String mIsVisible;
    @SerializedName("mIsMenu")
    @Expose
    private String mIsMenu;
    @SerializedName("mMenuName")
    @Expose
    private String mMenuName;
    @SerializedName("mParentPermissionItem")
    @Expose
    private String mParentPermissionItem;
    @SerializedName("mSubsistemaId")
    @Expose
    private Integer mSubsistemaId;
    @SerializedName("<Children>k__BackingField")
    @Expose
    private List<Object> childrenKBackingField = new ArrayList<Object>();
    @SerializedName("mIDbTransaction")
    @Expose
    private Object mIDbTransaction;
    @SerializedName("mIDbConn")
    @Expose
    private Object mIDbConn;

    /**
     * 
     * @return
     *     The mPermissionItemId
     */
    public Integer getMPermissionItemId() {
        return mPermissionItemId;
    }

    /**
     * 
     * @param mPermissionItemId
     *     The mPermissionItemId
     */
    public void setMPermissionItemId(Integer mPermissionItemId) {
        this.mPermissionItemId = mPermissionItemId;
    }

    /**
     * 
     * @return
     *     The mParentPermissionItemId
     */
    public Integer getMParentPermissionItemId() {
        return mParentPermissionItemId;
    }

    /**
     * 
     * @param mParentPermissionItemId
     *     The mParentPermissionItemId
     */
    public void setMParentPermissionItemId(Integer mParentPermissionItemId) {
        this.mParentPermissionItemId = mParentPermissionItemId;
    }

    /**
     * 
     * @return
     *     The mResourceName
     */
    public String getMResourceName() {
        return mResourceName;
    }

    /**
     * 
     * @param mResourceName
     *     The mResourceName
     */
    public void setMResourceName(String mResourceName) {
        this.mResourceName = mResourceName;
    }

    /**
     * 
     * @return
     *     The mUrl
     */
    public String getMUrl() {
        return mUrl;
    }

    /**
     * 
     * @param mUrl
     *     The mUrl
     */
    public void setMUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    /**
     * 
     * @return
     *     The mUrlAppMobile
     */
    public String getMUrlAppMobile() {
        return mUrlAppMobile;
    }

    /**
     * 
     * @param mUrlAppMobile
     *     The mUrlAppMobile
     */
    public void setMUrlAppMobile(String mUrlAppMobile) {
        this.mUrlAppMobile = mUrlAppMobile;
    }

    /**
     * 
     * @return
     *     The mTarget
     */
    public String getMTarget() {
        return mTarget;
    }

    /**
     * 
     * @param mTarget
     *     The mTarget
     */
    public void setMTarget(String mTarget) {
        this.mTarget = mTarget;
    }

    /**
     * 
     * @return
     *     The mMenuItemOrder
     */
    public Integer getMMenuItemOrder() {
        return mMenuItemOrder;
    }

    /**
     * 
     * @param mMenuItemOrder
     *     The mMenuItemOrder
     */
    public void setMMenuItemOrder(Integer mMenuItemOrder) {
        this.mMenuItemOrder = mMenuItemOrder;
    }

    /**
     * 
     * @return
     *     The mImage
     */
    public String getMImage() {
        return mImage;
    }

    /**
     * 
     * @param mImage
     *     The mImage
     */
    public void setMImage(String mImage) {
        this.mImage = mImage;
    }

    /**
     * 
     * @return
     *     The mImageHeight
     */
    public Integer getMImageHeight() {
        return mImageHeight;
    }

    /**
     * 
     * @param mImageHeight
     *     The mImageHeight
     */
    public void setMImageHeight(Integer mImageHeight) {
        this.mImageHeight = mImageHeight;
    }

    /**
     * 
     * @return
     *     The mImageWidth
     */
    public Integer getMImageWidth() {
        return mImageWidth;
    }

    /**
     * 
     * @param mImageWidth
     *     The mImageWidth
     */
    public void setMImageWidth(Integer mImageWidth) {
        this.mImageWidth = mImageWidth;
    }

    /**
     * 
     * @return
     *     The mIsAuthenticationRequired
     */
    public String getMIsAuthenticationRequired() {
        return mIsAuthenticationRequired;
    }

    /**
     * 
     * @param mIsAuthenticationRequired
     *     The mIsAuthenticationRequired
     */
    public void setMIsAuthenticationRequired(String mIsAuthenticationRequired) {
        this.mIsAuthenticationRequired = mIsAuthenticationRequired;
    }

    /**
     * 
     * @return
     *     The mIsEnabled
     */
    public String getMIsEnabled() {
        return mIsEnabled;
    }

    /**
     * 
     * @param mIsEnabled
     *     The mIsEnabled
     */
    public void setMIsEnabled(String mIsEnabled) {
        this.mIsEnabled = mIsEnabled;
    }

    /**
     * 
     * @return
     *     The mIsVisible
     */
    public String getMIsVisible() {
        return mIsVisible;
    }

    /**
     * 
     * @param mIsVisible
     *     The mIsVisible
     */
    public void setMIsVisible(String mIsVisible) {
        this.mIsVisible = mIsVisible;
    }

    /**
     * 
     * @return
     *     The mIsMenu
     */
    public String getMIsMenu() {
        return mIsMenu;
    }

    /**
     * 
     * @param mIsMenu
     *     The mIsMenu
     */
    public void setMIsMenu(String mIsMenu) {
        this.mIsMenu = mIsMenu;
    }

    /**
     * 
     * @return
     *     The mMenuName
     */
    public String getMMenuName() {
        return mMenuName;
    }

    /**
     * 
     * @param mMenuName
     *     The mMenuName
     */
    public void setMMenuName(String mMenuName) {
        this.mMenuName = mMenuName;
    }

    /**
     * 
     * @return
     *     The mParentPermissionItem
     */
    public String getMParentPermissionItem() {
        return mParentPermissionItem;
    }

    /**
     * 
     * @param mParentPermissionItem
     *     The mParentPermissionItem
     */
    public void setMParentPermissionItem(String mParentPermissionItem) {
        this.mParentPermissionItem = mParentPermissionItem;
    }

    /**
     * 
     * @return
     *     The mSubsistemaId
     */
    public Integer getMSubsistemaId() {
        return mSubsistemaId;
    }

    /**
     * 
     * @param mSubsistemaId
     *     The mSubsistemaId
     */
    public void setMSubsistemaId(Integer mSubsistemaId) {
        this.mSubsistemaId = mSubsistemaId;
    }

    /**
     * 
     * @return
     *     The childrenKBackingField
     */
    public List<Object> getChildrenKBackingField() {
        return childrenKBackingField;
    }

    /**
     * 
     * @param childrenKBackingField
     *     The <Children>k__BackingField
     */
    public void setChildrenKBackingField(List<Object> childrenKBackingField) {
        this.childrenKBackingField = childrenKBackingField;
    }

    /**
     * 
     * @return
     *     The mIDbTransaction
     */
    public Object getMIDbTransaction() {
        return mIDbTransaction;
    }

    /**
     * 
     * @param mIDbTransaction
     *     The mIDbTransaction
     */
    public void setMIDbTransaction(Object mIDbTransaction) {
        this.mIDbTransaction = mIDbTransaction;
    }

    /**
     * 
     * @return
     *     The mIDbConn
     */
    public Object getMIDbConn() {
        return mIDbConn;
    }

    /**
     * 
     * @param mIDbConn
     *     The mIDbConn
     */
    public void setMIDbConn(Object mIDbConn) {
        this.mIDbConn = mIDbConn;
    }

}
