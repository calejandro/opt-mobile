package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LineasProduccionAlertasService {

    @GET("LineasProduccionAlertas")
    Call<List<LineasProduccionAlertasDto>> list(@Query("includes") String[] includes, @Query("FechaInicio") String fechaInicio,
                                          @Query("FechaFin") String fechaFin, @Query("IdLinea") int idLinea);
}
