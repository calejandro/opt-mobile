package com.andinaargentina.core_services.providers.api.clients.lineselection.modify;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LineModifyService extends BaseService<List<SelectedDto>> {

    private ApiRestService apiRestService;

    @Inject
    public LineModifyService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<List<SelectedDto>> postLineModify(LineSelectedDto lineSelectedDto, Context context) {
        return super.getSingleObservable(apiRestService.modifyLineSelected(lineSelectedDto),context);
    }
}
