package com.andinaargentina.core_services.providers.api.clients.extendshift;

import com.google.gson.annotations.SerializedName;

public class ExtendShiftResponseDto {

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("userId")
    private int userId;

    @SerializedName("extendProduccionShift")
    private boolean extendProductionShift;

    @SerializedName("endTimeforShift")
    private String endTimeforShift;

    public ExtendShiftResponseDto(int productionLineId,
                                  int userId,
                                  boolean extendProductionShift,
                                  String endTimeforShift) {
        this.productionLineId = productionLineId;
        this.userId = userId;
        this.extendProductionShift = extendProductionShift;
        this.endTimeforShift = endTimeforShift;
    }

    public ExtendShiftResponseDto() {
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isExtendProductionShift() {
        return extendProductionShift;
    }

    public void setExtendProductionShift(boolean extendProductionShift) {
        this.extendProductionShift = extendProductionShift;
    }

    public String getEndTimeforShift() {
        return endTimeforShift;
    }

    public void setEndTimeforShift(String endTimeforShift) {
        this.endTimeforShift = endTimeforShift;
    }
}
