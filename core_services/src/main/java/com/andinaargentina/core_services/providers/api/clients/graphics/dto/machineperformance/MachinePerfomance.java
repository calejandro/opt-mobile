
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MachinePerfomance implements Serializable
{

    @SerializedName("productionLineId")
    @Expose
    private int productionLineId;

    @SerializedName("updateTime")
    @Expose
    private String updateTime;

    @SerializedName("machines")
    @Expose
    private List<Machine> machines = null;

    private final static long serialVersionUID = 3010497014951053620L;

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public List<Machine> getMachines() {
        return machines;
    }

    public void setMachines(List<Machine> machines) {
        this.machines = machines;
    }

}
