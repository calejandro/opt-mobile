package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionEficienciaMesDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaByShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface EficienciaService {

    @GET("Eficiencia")
    Call<ArrayList<ProduccionEficienciaDto>> list(@Query("idLinea") int idLinea, @Query("fechaHoraInicio") String fechaHoraInicio, @Query("fechaHoraFin") String fechaHoraFin );

    @GET("Eficiencia")
    Call<ArrayList<ProduccionEficienciaDto>> list(@Query("idLineas") List<Integer> idLineas, @Query("fechaHoraInicio") String fechaHoraInicio, @Query("fechaHoraFin") String fechaHoraFin );

    @GET("Eficiencia/ByShift")
    Call<ArrayList<ProduccionEficienciaByShiftDto>> listByShift(@Query("idLinea") int idLinea, @Query("fechaHoraInicio") String fechaHoraInicio, @Query("fechaHoraFin") String fechaHoraFin);

    @GET("Eficiencia/ByShift")
    Call<ArrayList<ProduccionEficienciaByShiftDto>> listByShift(@Query("idLineas") List<Integer> idLineas, @Query("fechaHoraInicio") String fechaHoraInicio, @Query("fechaHoraFin") String fechaHoraFin);

}
