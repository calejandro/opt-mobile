package com.andinaargentina.core_services.providers.api.clients.alerts;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class AlertLineDto {

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("productionLineName")
    private String productionLineName;

    @SerializedName("alertRaised")
    private List<AlertDto> alertRaised;

    public AlertLineDto(int productionLineId,
                        String productionLineName,
                        List<AlertDto> alertRaised) {
        this.productionLineId = productionLineId;
        this.productionLineName = productionLineName;
        this.alertRaised = alertRaised;
    }

    public AlertLineDto() {
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public String getProductionLineName() {
        return productionLineName;
    }

    public void setProductionLineName(String productionLineName) {
        this.productionLineName = productionLineName;
    }

    public List<AlertDto> getAlertRaised() {
        return alertRaised;
    }

    public void setAlertRaised(List<AlertDto> alertRaised) {
        this.alertRaised = alertRaised;
    }
}
