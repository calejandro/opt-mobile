package com.andinaargentina.core_services.providers.api.clients.efficiency;

import com.google.gson.annotations.SerializedName;

public class EfficiencyDto {

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("productionLineName")
    private String productionLineName;

    @SerializedName("shiftEfficiencyPercentaje")
    private double shiftEfficiencyPercentaje;

    @SerializedName("shiftPlanEfficiencyPercentaje")
    private double shiftPlanEfficiencyPercentaje;

    @SerializedName("dailyPlanEfficiencyPercentaje")
    private double dailyPlanEfficiencyPercentaje;

    @SerializedName("dailyEfficiencyPercentaje")
    private double dailyEfficiencyPercentaje;

    @SerializedName("objectiveEfficiencyPercentaje")
    private double objectiveEfficiencyPercentaje;

    @SerializedName("objectivePlanEfficiencyPercentaje")
    private double objectivePlanEfficiencyPercentaje;

    public EfficiencyDto(int productionLineId,
                         String productionLineName,
                         int shiftEfficiencyPercentaje,
                         int dailyEfficiencyPercentaje,
                         int objectiveEfficiencyPercentaje) {
        this.productionLineId = productionLineId;
        this.productionLineName = productionLineName;
        this.shiftEfficiencyPercentaje = shiftEfficiencyPercentaje;
        this.dailyEfficiencyPercentaje = dailyEfficiencyPercentaje;
        this.objectiveEfficiencyPercentaje = objectiveEfficiencyPercentaje;
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public String getProductionLineName() {
        return productionLineName;
    }

    public void setProductionLineName(String productionLineName) {
        this.productionLineName = productionLineName;
    }

    public double getShiftEfficiencyPercentaje() {
        return shiftEfficiencyPercentaje;
    }

    public void setShiftEfficiencyPercentaje(double shiftEfficiencyPercentaje) {
        this.shiftEfficiencyPercentaje = shiftEfficiencyPercentaje;
    }

    public double getDailyEfficiencyPercentaje() {
        return dailyEfficiencyPercentaje;
    }

    public void setDailyEfficiencyPercentaje(double dailyEfficiencyPercentaje) {
        this.dailyEfficiencyPercentaje = dailyEfficiencyPercentaje;
    }

    public double getObjectiveEfficiencyPercentaje() {
        return objectiveEfficiencyPercentaje;
    }

    public void setObjectiveEfficiencyPercentaje(double objectiveEfficiencyPercentaje) {
        this.objectiveEfficiencyPercentaje = objectiveEfficiencyPercentaje;
    }

    public double getShiftPlanEfficiencyPercentaje() {
        return shiftPlanEfficiencyPercentaje;
    }

    public void setShiftPlanEfficiencyPercentaje(double shiftPlanEfficiencyPercentaje) {
        this.shiftPlanEfficiencyPercentaje = shiftPlanEfficiencyPercentaje;
    }

    public double getDailyPlanEfficiencyPercentaje() {
        return dailyPlanEfficiencyPercentaje;
    }

    public void setDailyPlanEfficiencyPercentaje(double dailyPlanEfficiencyPercentaje) {
        this.dailyPlanEfficiencyPercentaje = dailyPlanEfficiencyPercentaje;
    }

    public double getObjectivePlanEfficiencyPercentaje() {
        return objectivePlanEfficiencyPercentaje;
    }

    public void setObjectivePlanEfficiencyPercentaje(double objectivePlanEfficiencyPercentaje) {
        this.objectivePlanEfficiencyPercentaje = objectivePlanEfficiencyPercentaje;
    }
}
