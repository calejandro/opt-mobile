package com.andinaargentina.core_services.providers.api.clients.extendshift;

import com.google.gson.annotations.SerializedName;

public class ExtendShiftRequestDto {

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("userId")
    private int userId;

    @SerializedName("extendProduccionShift")
    private boolean extendProductionShift;

    public ExtendShiftRequestDto(int productionLineId,
                                 int userId,
                                 boolean extendProductionShift) {
        this.productionLineId = productionLineId;
        this.userId = userId;
        this.extendProductionShift = extendProductionShift;
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isExtendProductionShift() {
        return extendProductionShift;
    }

    public void setExtendProductionShift(boolean extendProductionShift) {
        this.extendProductionShift = extendProductionShift;
    }
}
