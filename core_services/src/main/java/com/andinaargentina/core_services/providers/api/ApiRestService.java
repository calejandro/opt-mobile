package com.andinaargentina.core_services.providers.api;

import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertFailModeInformRequestDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.device.AddDeviceRequestDto;
import com.andinaargentina.core_services.providers.api.clients.device.AddDeviceResponseDto;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyLineDto;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftRequestDto;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftResponseDto;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeDto;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.GraphicsDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.LineSelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.selected.SelectedDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.api.clients.logout.UserLogoutDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderRequestDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderResponseDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiRestService {

    @GET("Login/Login")
    Call<User> login(@Query("wUserName") String user,
                     @Query("wPassword") String password,
                     @Query("wDevice") String device,
                     @Query("wCodSubsistema") String wCodSubsistema);

    @GET("Login/ValidarVersion")
    Call<String> validateVersion(@Query("wAppCode") String appCode,
                                 @Query("wVersionCode") String versionCode);

    @GET("Login/CambioPassword")
    Call<String> changePassword(@Query("wUserName") String username,
                                @Query("wPassActual") String oldPassword,
                                @Query("wNuevoPassword") String password);

    //@POST("User/logout")
    //Call<UserLogoutDto> logout(@Body UserLogoutDto userLogout);

    @GET("ProductionPlant/list")
    Call<List<PlantDto>> getPlants();

    @GET("ProductionLine/getByPlant/{idPlant}")
    Call<List<LineDto>> getLinesByPlant(@Path("idPlant") String idPlant);

    /**
     * Método para seleccionar líneas,
     * ejecutar con las líneas que van a ser seleccionadas
     * @return
     */
    @POST("ProducionLineUser/select")
    Call<List<SelectedDto>> postLineSelected(@Body LineSelectedDto lineSelectedDto);

    @GET("ProducionLineUser/linesByUser/{isSupervision}")
    Call<List<SelectedDto>> getLineSelected(@Path("isSupervision") boolean isSupervision);

    /**
     * Método para seleccionar líneas,
     * ejecutar con las líneas que van a ser modificada con la orden nueva
     * @return
     */
    @POST("ProducionLineUser/modify")
    Call<List<SelectedDto>> modifyLineSelected(@Body LineSelectedDto lineSelectedDto);

    /**
     * Método para deseleccionar líneas,
     * ejecutar con las líneas que van a ser deseleccionadas
     * @param idsForDeleteSelections
     * @return
     */
    @POST("ProducionLineUser/unselect")
    Call<List<SelectedDto>> deleteLines(@Body LineSelectedDto idsForDeleteSelections);

    @GET("ProductionLine/efficiencyReport/{userId}")
    Call<List<EfficiencyLineDto>> getEfficiencyReport(@Path("userId") String userId);

    @GET("ProductionLine/list/alerts/{userId}")
    Call<List<AlertLineDto>> getAlerts(@Path("userId") String userId);

    @GET("/api/FailType/list/failMode/{machineId}/{productionLineId}")
    Call<FailModeDto> getFailMode(@Path("machineId") String machineId,@Path("productionLineId") String lineId);

    @GET("/api/FailType/list/failMode/all/{machineId}/{productionLineId}/{productionAlertKind}")
    Call<FailModeDto> getFailModeByAlertKind(@Path("machineId") String machineId,@Path("productionLineId") String lineId, @Path("productionAlertKind") int productionAlertKind);

    //Cambio de orden a una linea dada
    @POST("/api/ProducionLineUser/change/order")
    Call<ChangeOrderResponseDto> postChangeLineOrder(@Body ChangeOrderRequestDto changeOrderRequestDto);

    @POST("/api/ProducionLineUser/extend/shift")
    Call<ExtendShiftResponseDto> postExtendShift(@Body ExtendShiftRequestDto extendShiftRequestDto);

    //Enpoint de Graficos
    @GET("/api/ProductionLine/visualizationReports/{userId}")
    Call<List<GraphicsDto>> getGraphicsMonitor(@Path("userId") String userId);

    @PUT("/api/Device/add")
    Call<AddDeviceResponseDto> addDevice(@Body AddDeviceRequestDto addDeviceRequestDto);

    @PUT("/api/ProductionLineAlert/inform/{userId}")
    Call<List<AlertLineDto>> informAlert(@Body AlertDto informAlertDto, @Path("userId") String userId);

    @PUT("/api/ProductionLineAlertReport/add/{userId}")
    Call<List<AlertLineDto>> informFailModeAlert(@Body AlertFailModeInformRequestDto alertInformRequestDto,
                                                 @Path("userId") String userId);
}
