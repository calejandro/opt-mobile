package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionAlertasParadaMaquinaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionUmbralesTmefTmprDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ConfiguracionUmbralesTmefTmprService {
    @GET("ConfiguracionUmbralesTmefTmpr")
    Call<List<ConfiguracionUmbralesTmefTmprDto>> list(@Header("plantaId") int plantaId, @Query("idLinea") int idLinea);
}
