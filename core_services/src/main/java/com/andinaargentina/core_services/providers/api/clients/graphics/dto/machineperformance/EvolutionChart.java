
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EvolutionChart implements Serializable
{

    @SerializedName("axisYName")
    @Expose
    private String axisYName;

    @SerializedName("axisXName")
    @Expose
    private String axisXName;

    @SerializedName("axisYMaxValue")
    @Expose
    private int axisYMaxValue;

    @SerializedName("values")
    @Expose
    private List<Value> values = null;

    private final static long serialVersionUID = -2911218529410520241L;

    public String getAxisYName() {
        return axisYName;
    }

    public void setAxisYName(String axisYName) {
        this.axisYName = axisYName;
    }

    public String getAxisXName() {
        return axisXName;
    }

    public void setAxisXName(String axisXName) {
        this.axisXName = axisXName;
    }

    public int getAxisYMaxValue() {
        return axisYMaxValue;
    }

    public void setAxisYMaxValue(int axisYMaxValue) {
        this.axisYMaxValue = axisYMaxValue;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

}
