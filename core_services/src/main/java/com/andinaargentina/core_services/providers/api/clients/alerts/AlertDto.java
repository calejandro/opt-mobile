package com.andinaargentina.core_services.providers.api.clients.alerts;


import com.andinaargentina.core_services.UtilDate;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

@Parcel
public class AlertDto implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("machineId")
    private int machineId;

    @SerializedName("productionLineId")
    private int productionLineId;

    @SerializedName("machineName")
    private String machineName;

    @SerializedName("state")
    private String state;

    @SerializedName("type")
    private String type;

    @SerializedName("aggregatedValue")
    private double aggregatedValue;

    @SerializedName("sendReport")
    private boolean sendReport;

    @SerializedName("createdOn")
    private String createdOn;

    @SerializedName("endDate")
    private String endDateAlert;

    @SerializedName("umbral")
    private String threshold;

    @SerializedName("description")
    private String description;

    @SerializedName("classification")
    private String classification;

    @SerializedName("isDecreaseAlert")
    private boolean isDecreaseAlert;

    @SerializedName("isDeleted")
    private boolean isDeleted;

    public AlertDto() {
    }

    public long getMinuteDifference() {
        return UtilDate.calculateDifference(getCreatedOn(), GregorianCalendar.getInstance().getTime(),false,false,true
                ,false);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreatedOn() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        try {
            date = format.parse(createdOn);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void setCreatedOn(Date createdOn) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String strDate = dateFormat.format(createdOn);
        this.createdOn = strDate;
    }

    public Date getEndDateAlert() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date = null;
        if (endDateAlert!=null){
            date = new Date();
            try {
                date = format.parse(endDateAlert);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }

    public void setEndDateAlert(String endDateAlert) {
        this.endDateAlert = endDateAlert;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAggregatedValue() {
        return aggregatedValue;
    }

    public void setAggregatedValue(double aggregatedValue) {
        this.aggregatedValue = aggregatedValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMachineId() {
        return machineId;
    }

    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }

    public String getMachineName() {
        String name = machineName;
        if (isDecreaseAlert)
            name = "ROTURA DE ENVASES";
        return name;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public boolean isSendReport() {
        return sendReport;
    }

    public void setSendReport(boolean sendReport) {
        this.sendReport = sendReport;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public boolean isDecreaseAlert() {
        return isDecreaseAlert;
    }

    public void setDecreaseAlert(boolean decreaseAlert) {
        isDecreaseAlert = decreaseAlert;
    }

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public enum STATE {
        NEW, //0
        INFORMED, //1
        SOLVED, //2
        CLOSED, //3
        READED //4 TODO este tipo falta agregar
    }

    public enum TYPE {
        WARNING, //[Description("8 a 15")]
        DANGER, //[Description("15 a 60")]
        OK, //[Description("8-")]
        FAILURE //[Description("61+")]
    }

    public STATE getStateAlertEnum(){
        //int typeValue = Integer.valueOf(state);
        String typeValue = state;
        STATE resultType = STATE.NEW;
        switch (typeValue){
            case "New":{
                resultType = STATE.NEW;
                break;
            }
            case "Informed":{
                resultType = STATE.INFORMED;
                break;
            }
            case "Readed":{
                resultType = STATE.READED;
                break;
            }
            case "Solved":{
                resultType = STATE.SOLVED;
                break;
            }
            case "Closed":{
                resultType = STATE.CLOSED;
                break;
            }
        }
        return resultType;
    }

    public TYPE getTypeAlertEnum(){
        String typeValue = type;
        TYPE resultType = TYPE.WARNING;
        switch (typeValue){
            case "Ok":{
                resultType = TYPE.OK;
                break;
            }
            case "Warning":{
                resultType = TYPE.WARNING;
                break;
            }
            case "Danger":{
                resultType = TYPE.DANGER;
                break;
            }
            case "Failure":{
                resultType = TYPE.FAILURE;
                break;
            }
        }
        return resultType;
    }


    public boolean isNecesaryShowing(){
        boolean show = true;
        if (this.getStateAlertEnum() == STATE.SOLVED ||
        this.getStateAlertEnum() == STATE.INFORMED ||
        this.getStateAlertEnum() == STATE.CLOSED){
            show = false;
        }
        return show;
    }
}
