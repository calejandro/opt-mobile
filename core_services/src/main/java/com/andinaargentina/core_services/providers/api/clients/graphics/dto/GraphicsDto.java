package com.andinaargentina.core_services.providers.api.clients.graphics.dto;

import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.KpiDepletion;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance.MachinePerfomance;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment.ProductionFulfillment;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency.ShiftEfficiencyDto;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GraphicsDto implements Serializable {

    @SerializedName("productionLineId")
    @Expose
    private int productionLineId;

    @SerializedName("shiftEfficiency")
    @Expose
    private ShiftEfficiencyDto shiftEfficiencyDto;

    @SerializedName("productionFulfillment")
    @Expose
    private ProductionFulfillment productionFulfillment;

    @SerializedName("machinePerfomance")
    @Expose
    private MachinePerfomance machinePerfomance;

    @SerializedName("kpiDepletion")
    @Expose
    private KpiDepletion kpiDepletion;


    private final static long serialVersionUID = -2837007774233965982L;

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public ShiftEfficiencyDto getShiftEfficiencyDto() {
        return shiftEfficiencyDto;
    }

    public void setShiftEfficiencyDto(ShiftEfficiencyDto shiftEfficiencyDto) {
        this.shiftEfficiencyDto = shiftEfficiencyDto;
    }

    public ProductionFulfillment getProductionFulfillment() {
        return productionFulfillment;
    }

    public void setProductionFulfillment(ProductionFulfillment productionFulfillment) {
        this.productionFulfillment = productionFulfillment;
    }

    public MachinePerfomance getMachinePerfomance() {
        return machinePerfomance;
    }

    public void setMachinePerfomance(MachinePerfomance machinePerfomance) {
        this.machinePerfomance = machinePerfomance;
    }

    public KpiDepletion getKpiDepletion() {
        return kpiDepletion;
    }

    public void setKpiDepletion(KpiDepletion kpiDepletion) {
        this.kpiDepletion = kpiDepletion;
    }

}
