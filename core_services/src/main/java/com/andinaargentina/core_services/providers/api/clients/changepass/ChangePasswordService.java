package com.andinaargentina.core_services.providers.api.clients.changepass;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import io.reactivex.Single;

public class ChangePasswordService extends BaseService<String> {

    private ApiRestService apiRestService;

    public Single<String> getChangePasswordMethod(String userName, String oldPassword,
                                       String newPassword, Context context) {
        return super.getSingleObservable(apiRestService.changePassword(userName, oldPassword, newPassword),context);
    }
}
