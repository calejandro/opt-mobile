package com.andinaargentina.core_services.providers.api.clients.logout;

import com.google.gson.annotations.SerializedName;

public class UserLogoutDto {

    @SerializedName("id")
    private int id;

    @SerializedName("token")
    private String token;

    @SerializedName("isSuperviser")
    private boolean isSuperviser;

    @SerializedName("isShiftEnded")
    private boolean isShiftFinalized;

    public UserLogoutDto(int id, String token,boolean isSuperviser, boolean isShiftFinalized) {
        this.id = id;
        this.token = token;
        this.isShiftFinalized = isShiftFinalized;
        this.isSuperviser = isSuperviser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
