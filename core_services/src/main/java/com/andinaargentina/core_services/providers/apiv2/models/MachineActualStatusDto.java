package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class MachineActualStatusDto {
    private int idLinea;
    private String descripcionLinea;
    private Date fechaHora;
    private int idEquipo;
    private String descripcionEquipo;
    private int ordenVisualizacion;
    private int idEstado;
    private String descripcionEstado;
    private int minutosAcumulados;
    private int minutosDesdeUltimaParadaPropia;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getDescripcionEquipo() {
        return descripcionEquipo;
    }

    public void setDescripcionEquipo(String descripcionEquipo) {
        this.descripcionEquipo = descripcionEquipo;
    }

    public int getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(int ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }

    public int getMinutosAcumulados() {
        return minutosAcumulados;
    }

    public void setMinutosAcumulados(int minutosAcumulados) {
        this.minutosAcumulados = minutosAcumulados;
    }

    public int getMinutosDesdeUltimaParadaPropia() {
        return minutosDesdeUltimaParadaPropia;
    }

    public void setMinutosDesdeUltimaParadaPropia(int minutosDesdeUltimaParadaPropia) {
        this.minutosDesdeUltimaParadaPropia = minutosDesdeUltimaParadaPropia;
    }
}
