package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class KpiDepletionDto {
    private  int idPais;
    private  String descripcionPais;
    private  int idPlanta;
    private  String descripcionPlanta;
    private  int idLinea;
    private  String descripcionLinea;
    private int idEquipo;
    private String descripcionEquipo;
    private  int ordenVisualizacion;
    private  Date fechaHora;
    private  String tipoProduccionLinea;
    private  Double kpi;
    private  String descripcionKPI;
    private  String tipoProduccion;
    private  Integer idSku;
    private  String descripcionSKU;
    private  Integer idOrdenSAP;

    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getDescripcionPais() {
        return descripcionPais;
    }

    public void setDescripcionPais(String descripcionPais) {
        this.descripcionPais = descripcionPais;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public String getDescripcionPlanta() {
        return descripcionPlanta;
    }

    public void setDescripcionPlanta(String descripcionPlanta) {
        this.descripcionPlanta = descripcionPlanta;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getDescripcionEquipo() {
        return descripcionEquipo;
    }

    public void setDescripcionEquipo(String descripcionEquipo) {
        this.descripcionEquipo = descripcionEquipo;
    }

    public int getOrdenVisualizacion() {
        return ordenVisualizacion;
    }

    public void setOrdenVisualizacion(int ordenVisualizacion) {
        this.ordenVisualizacion = ordenVisualizacion;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getTipoProduccionLinea() {
        return tipoProduccionLinea;
    }

    public void setTipoProduccionLinea(String tipoProduccionLinea) {
        this.tipoProduccionLinea = tipoProduccionLinea;
    }

    public Double getKpi() {
        return kpi;
    }

    public void setKpi(Double kpi) {
        this.kpi = kpi;
    }

    public String getDescripcionKPI() {
        return descripcionKPI;
    }

    public void setDescripcionKPI(String descripcionKPI) {
        this.descripcionKPI = descripcionKPI;
    }

    public String getTipoProduccion() {
        return tipoProduccion;
    }

    public void setTipoProduccion(String tipoProduccion) {
        this.tipoProduccion = tipoProduccion;
    }

    public Integer getIdSku() {
        return idSku;
    }

    public void setIdSku(Integer idSku) {
        this.idSku = idSku;
    }

    public String getDescripcionSKU() {
        return descripcionSKU;
    }

    public void setDescripcionSKU(String descripcionSKU) {
        this.descripcionSKU = descripcionSKU;
    }

    public Integer getIdOrdenSAP() {
        return idOrdenSAP;
    }

    public void setIdOrdenSAP(Integer idOrdenSAP) {
        this.idOrdenSAP = idOrdenSAP;
    }
}
