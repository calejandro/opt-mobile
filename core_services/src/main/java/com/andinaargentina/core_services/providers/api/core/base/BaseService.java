package com.andinaargentina.core_services.providers.api.core.base;

import android.accounts.NetworkErrorException;
import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.core.util.NetworkUtils;
import com.andinaargentina.core_services.providers.api.core.util.ObjectUtil;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.UnknownHostException;

import io.reactivex.Single;
import retrofit2.Call;

import static com.andinaargentina.core_services.providers.api.core.base.ServiceError.NETWORK_ERROR;
import static com.andinaargentina.core_services.providers.api.core.base.ServiceError.NO_VPN_CODE;
import static com.andinaargentina.core_services.providers.api.core.base.ServiceError.SUCCESS_CODE;

public abstract class BaseService<T>{

    public static final int ERROR_UNDEFINED = -1;

    private Call<T> call;

    public Single<T> getSingleObservable(Call<T> call, Context context) {
        this.call = call;
        return Single.create(singleOnSubscribe -> {
                    if (!NetworkUtils.isConnected(context)) {
                        Exception exception = new NetworkErrorException();
                        singleOnSubscribe.onError(exception);
                    } else {
                        try {
                            ServiceResponse serviceResponse = processCall(call, false, context);
                            if (serviceResponse.getCode() == SUCCESS_CODE) {
                                singleOnSubscribe.onSuccess((T)serviceResponse.getData());
                            } else {
                                singleOnSubscribe.onError(serviceResponse.getServiceError());
                            }
                        } catch (Exception e) {
                            singleOnSubscribe.onError(e);
                        }
                    }
                }
        );
    }

    @NonNull
    private ServiceResponse processCall(Call call, boolean isVoid, Context context) {
        if (!NetworkUtils.isConnected(context)) {
            return new ServiceResponse(new ServiceError());
        }
        try {
            retrofit2.Response response = call.execute();
            Gson gson = new Gson();
            if (ObjectUtil.isNull(response)) {
                return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
            }
            int responseCode = response.code();
            if (response.isSuccessful()) {
                return new ServiceResponse(responseCode, isVoid ? null : response.body());
            } else {
                ServiceError ServiceError;
                ServiceError = new ServiceError(response.message(), responseCode);
                return new ServiceResponse(ServiceError);
            }
        }
        catch (UnknownHostException e) {
            return new ServiceResponse(new ServiceError(NETWORK_ERROR, NO_VPN_CODE));
        }
        catch (IOException e) {
            return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
        }
    }

    public Call getCall(){
        return call;
    }
}