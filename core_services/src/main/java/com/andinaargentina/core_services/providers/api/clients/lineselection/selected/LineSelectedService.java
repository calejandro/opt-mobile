package com.andinaargentina.core_services.providers.api.clients.lineselection.selected;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LineSelectedService extends BaseService<List<SelectedDto>> {

    private ApiRestService apiRestService;

    @Inject
    public LineSelectedService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<List<SelectedDto>> postLineSelected(LineSelectedDto lineSelectedDto, Context context) {
        return super.getSingleObservable(apiRestService.postLineSelected(lineSelectedDto),context);
    }

    public Single<List<SelectedDto>> getLineSelected(boolean isSupervision, Context context) {
        return super.getSingleObservable(apiRestService.getLineSelected(isSupervision),context);
    }
}
