package com.andinaargentina.core_services.providers.apiv2.filters;

public class LineasProduccionFilter {
    private int plantaId;
    private int tipoLinea;
    private boolean activo;

    public LineasProduccionFilter(int plantaId, int tipoLinea, boolean activo) {
        this.plantaId = plantaId;
        this.tipoLinea = tipoLinea;
        this.activo = activo;
    }

    public int getPlantaId() {
        return plantaId;
    }

    public void setPlantaId(int plantaId) {
        this.plantaId = plantaId;
    }

    public int getTipoLinea() {
        return tipoLinea;
    }

    public void setTipoLinea(int tipoLinea) {
        this.tipoLinea = tipoLinea;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
