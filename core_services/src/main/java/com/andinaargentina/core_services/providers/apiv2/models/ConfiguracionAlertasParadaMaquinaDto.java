package com.andinaargentina.core_services.providers.apiv2.models;

public class ConfiguracionAlertasParadaMaquinaDto {
    private int id;
    private int idPlanta;
    private int idLinea;
    private int idEquipo;
    private Integer valorMinimo;
    private Integer valorMaximo;
    private String tipoAlerta;
    private Integer porcMinimo;
    private Integer porcMaximo;
    private LineasProduccionDto lineaProduccion;
    private EquiposDto equipo;
    private PlantasDto planta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public Integer getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Integer valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public Integer getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(Integer valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public String getTipoAlerta() {
        return tipoAlerta;
    }

    public void setTipoAlerta(String tipoAlerta) {
        this.tipoAlerta = tipoAlerta;
    }

    public Integer getPorcMinimo() {
        return porcMinimo;
    }

    public void setPorcMinimo(Integer porcMinimo) {
        this.porcMinimo = porcMinimo;
    }

    public Integer getPorcMaximo() {
        return porcMaximo;
    }

    public void setPorcMaximo(Integer porcMaximo) {
        this.porcMaximo = porcMaximo;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public EquiposDto getEquipo() {
        return equipo;
    }

    public void setEquipo(EquiposDto equipo) {
        this.equipo = equipo;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }
}
