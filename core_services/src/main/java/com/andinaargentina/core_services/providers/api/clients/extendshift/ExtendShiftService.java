package com.andinaargentina.core_services.providers.api.clients.extendshift;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderRequestDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderResponseDto;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import javax.inject.Inject;

import io.reactivex.Single;

public class ExtendShiftService extends BaseService<ExtendShiftResponseDto> {

    private ApiRestService apiRestService;

    @Inject
    public ExtendShiftService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<ExtendShiftResponseDto> postExtendShift(ExtendShiftRequestDto changeOrderRequestDto, Context context){
        return super.getSingleObservable(apiRestService.postExtendShift(changeOrderRequestDto),context);
    }
}
