package com.andinaargentina.core_services.providers.api.clients.logout;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import javax.inject.Inject;

import io.reactivex.Single;

public class LogoutService extends BaseService<UserLogoutDto> {

    private ApiRestService apiRestService;

    @Inject
    public LogoutService(ApiRestService apiRestService){
        this.apiRestService = apiRestService;
    }

    /*public Single<UserLogoutDto> postLogout(UserLogoutDto userLogoutDto, Context context) {
        return super.getSingleObservable(apiRestService.logout(userLogoutDto),context);
    }*/
}
