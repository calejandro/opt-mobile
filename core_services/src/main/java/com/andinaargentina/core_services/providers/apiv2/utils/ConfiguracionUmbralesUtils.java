package com.andinaargentina.core_services.providers.apiv2.utils;

import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionUmbralesTmefTmprDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfiguracionUmbralesUtils {
    private static ConfiguracionUmbralesUtils instance;

    private HashMap<Integer, List<ConfiguracionUmbralesTmefTmprDto>> estadosUmbrales;

    private HashMap<Integer, Observable<List<ConfiguracionUmbralesTmefTmprDto>>> observable;

    private ConfiguracionUmbralesUtils() {
        estadosUmbrales = new HashMap<>();
        observable = new HashMap<>();
    }

    public static ConfiguracionUmbralesUtils getInstance() {
        if(instance == null) {
            instance = new ConfiguracionUmbralesUtils();
        }
        return instance;
    }

    public List<ConfiguracionUmbralesTmefTmprDto> getEstadosUmbrales(int idLnea) {
        return estadosUmbrales.get(idLnea);
    }

    public Observable<List<ConfiguracionUmbralesTmefTmprDto>> getObservable(int idPlanta, int idLinea) {
        Observable<List<ConfiguracionUmbralesTmefTmprDto>> o;
        if(!observable.containsKey(idLinea)) {
            o = Observable.create(new ObservableOnSubscribe<List<ConfiguracionUmbralesTmefTmprDto>>() {
                @Override
                public void subscribe(@NonNull ObservableEmitter<List<ConfiguracionUmbralesTmefTmprDto>> emitter) throws Exception {
                    if(!estadosUmbrales.containsKey(idLinea)) {
                        OptimusServices.getInstance().getConfiguracionUmbralesTmefTmprService().list(idPlanta, idLinea).enqueue(new Callback<List<ConfiguracionUmbralesTmefTmprDto>>() {
                            @Override
                            public void onResponse(Call<List<ConfiguracionUmbralesTmefTmprDto>> call, Response<List<ConfiguracionUmbralesTmefTmprDto>> response) {
                                estadosUmbrales.put(idLinea, response.body());
                                emitter.onNext(response.body());
                            }

                            @Override
                            public void onFailure(Call<List<ConfiguracionUmbralesTmefTmprDto>> call, Throwable t) {
                                System.out.println(t.getMessage());
                            }
                        });
                    } else {
                        emitter.onNext(estadosUmbrales.get(idLinea));
                    }
                }
            });
        } else {
            o = observable.get(idLinea);
        }


        return o;
    }
}
