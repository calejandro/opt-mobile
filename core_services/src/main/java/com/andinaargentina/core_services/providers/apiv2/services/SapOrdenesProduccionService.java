package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.LinkTemporalOrderDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SapOrdenesProduccionService extends BaseService<SapOrdenesProduccionDto, BaseFilter> {

    @POST("linkTemporalOrder")
    Call<SapOrdenesProduccionDto> update(@Body() LinkTemporalOrderDto entity);
}
