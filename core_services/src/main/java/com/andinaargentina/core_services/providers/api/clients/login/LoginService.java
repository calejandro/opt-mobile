package com.andinaargentina.core_services.providers.api.clients.login;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.pixplicity.easyprefs.library.Prefs;

import javax.inject.Inject;
import io.reactivex.Single;

public class LoginService extends BaseService<User> {

    private ApiRestService apiRestService;

    @Inject
    public LoginService(ApiRestService apiRestService){
        this.apiRestService = apiRestService;
    }

    public Single<User> getLoginMethod(String user, String password,
                                       String device, String subsistem, Context context) {
        Prefs.putString("username", user);
        Prefs.putString("password", password);
        return super.getSingleObservable(apiRestService.login(user, password, device, subsistem),context);
    }
}
