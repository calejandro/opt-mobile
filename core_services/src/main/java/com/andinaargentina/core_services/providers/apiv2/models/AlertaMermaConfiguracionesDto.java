package com.andinaargentina.core_services.providers.apiv2.models;

public class AlertaMermaConfiguracionesDto {
    private int id;
    private int idLinea;
    private int idEquipo;
    private Integer minimoPrimerAlerta;
    private Integer minimoSegundaAlerta;
    private Integer tipoMerma;
    private Integer idSku;
    private Integer idPlanta;
    private LineasProduccionDto lineaProduccion;
    private EquiposDto equipo;
    private PlantasDto planta;
    private SapSkuDto sapSku;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public Integer getMinimoPrimerAlerta() {
        return minimoPrimerAlerta;
    }

    public void setMinimoPrimerAlerta(Integer minimoPrimerAlerta) {
        this.minimoPrimerAlerta = minimoPrimerAlerta;
    }

    public Integer getMinimoSegundaAlerta() {
        return minimoSegundaAlerta;
    }

    public void setMinimoSegundaAlerta(Integer minimoSegundaAlerta) {
        this.minimoSegundaAlerta = minimoSegundaAlerta;
    }

    public Integer getTipoMerma() {
        return tipoMerma;
    }

    public void setTipoMerma(Integer tipoMerma) {
        this.tipoMerma = tipoMerma;
    }

    public Integer getIdSku() {
        return idSku;
    }

    public void setIdSku(Integer idSku) {
        this.idSku = idSku;
    }

    public Integer getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(Integer idPlanta) {
        this.idPlanta = idPlanta;
    }

    public LineasProduccionDto getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(LineasProduccionDto lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public EquiposDto getEquipo() {
        return equipo;
    }

    public void setEquipo(EquiposDto equipo) {
        this.equipo = equipo;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }

    public SapSkuDto getSapSku() {
        return sapSku;
    }

    public void setSapSku(SapSkuDto sapSku) {
        this.sapSku = sapSku;
    }
}
