package com.andinaargentina.core_services.providers.api.clients.alerts;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class InformAlertResponseDto {

    private List<AlertLineDto> response;

    public List<AlertLineDto> getResponse() {
        return response;
    }

    public void setResponse(List<AlertLineDto> response) {
        this.response = response;
    }
}
