package com.andinaargentina.core_services.providers.api.clients.graphics;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.GraphicsDto;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class GraphicsService extends BaseService<List<GraphicsDto>> {

    private ApiRestService apiRestService;

    @Inject
    public GraphicsService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<List<GraphicsDto>> getGraphicsReport(String idUser, Context context) {
        return super.getSingleObservable(apiRestService.getGraphicsMonitor(idUser),context);
    }

    public void cancel(){
        if (getCall()!=null)
            getCall().cancel();
    }
}
