package com.andinaargentina.core_services.providers.api.clients.lineselection.sites;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.List;

@Parcel
public class LineDto implements Serializable {

    @SerializedName("id")
    int id;

    @SerializedName("description")
    String description;

    @SerializedName("takenBy")
    TakenByDto takenBy;

    @SerializedName("isProductionLineTaken")
    boolean isProductionLineTaken;

    @SerializedName("productionPlant")
    ProductionPlant productionPlant;

    @SerializedName("visualizedBy")
    List<UserDto> visualizedBy;

    @SerializedName("productionOrders")
    List<ProductionOrders> productionOrders;

    @SerializedName("extendProduccionShift")
    boolean extendProduccionShift;

    //Id de toma de línea
    private int idTakenLine;

    @SerializedName("productionOrderIdTaken")
    private int productionOrderIdAssigned;

    public LineDto() {
    }

    public LineDto(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public boolean itemEnabled = true;

    public boolean itemChecked;

    public int orderIdSelected;

    public TakenByDto getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(TakenByDto takenBy) {
        this.takenBy = takenBy;
    }

    public boolean isProductionLineTaken() {
        return isProductionLineTaken;
    }

    public void setProductionLineTaken(boolean productionLineTaken) {
        isProductionLineTaken = productionLineTaken;
    }

    public ProductionPlant getProductionPlant() {
        return productionPlant;
    }

    public void setProductionPlant(ProductionPlant productionPlant) {
        this.productionPlant = productionPlant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isItemEnabled() {
        return itemEnabled;
    }

    public void setItemEnabled(boolean itemEnabled) {
        this.itemEnabled = itemEnabled;
    }

    public boolean isItemChecked() {
        return itemChecked;
    }

    public void setItemChecked(boolean itemChecked) {
        this.itemChecked = itemChecked;
    }

    public List<UserDto> getVisualizedBy() {
        return visualizedBy;
    }

    public void setVisualizedBy(List<UserDto> visualizedBy) {
        this.visualizedBy = visualizedBy;
    }

    public List<ProductionOrders> getProductionOrders() {
        return productionOrders;
    }

    public void setProductionOrders(List<ProductionOrders> productionOrders) {
        this.productionOrders = productionOrders;
    }

    public int getOrderIdSelected() {
        return orderIdSelected;
    }

    public void setOrderIdSelected(int orderIdSelected) {
        this.orderIdSelected = orderIdSelected;
    }

    public boolean isExtendProduccionShift() {
        return extendProduccionShift;
    }

    public void setExtendProduccionShift(boolean extendProduccionShift) {
        this.extendProduccionShift = extendProduccionShift;
    }

    public int getIdTakenLine() {
        return idTakenLine;
    }

    public void setIdTakenLine(int idTakenLine) {
        this.idTakenLine = idTakenLine;
    }

    public int getProductionOrderIdAssigned() {
        return productionOrderIdAssigned;
    }

    public void setProductionOrderIdAssigned(int productionOrderIdAssigned) {
        this.productionOrderIdAssigned = productionOrderIdAssigned;
    }
}
