
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Historical implements Serializable
{

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("minutes")
    @Expose
    private int minutes;
    @SerializedName("threshold")
    @Expose
    private String threshold;
    private final static long serialVersionUID = -4370397910891317737L;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

}
