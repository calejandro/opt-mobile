package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class LineasProduccionDto implements Serializable {
    private int idLinea;
    private String descLinea;
    private int idPlanta;
    private int tipoLinea;
    private Integer puedeRecuperarHistoricos;
    private String fhUltModif;
    private boolean activo;
    private PlantasDto planta;
    private LineasProduccionUsuarioDto lineasProduccionUsuarioDto;
    private boolean hasProductionOrder;
    //model
    private boolean itemChecked;
    private boolean itemEnabled;

    public LineasProduccionUsuarioDto getLineasProduccionUsuarioDto() {
        return lineasProduccionUsuarioDto;
    }

    public void setLineasProduccionUsuarioDto(LineasProduccionUsuarioDto lineasProduccionUsuarioDto) {
        this.lineasProduccionUsuarioDto = lineasProduccionUsuarioDto;
    }

    public boolean isHasProductionOrder() {
        return hasProductionOrder;
    }

    public void setHasProductionOrder(boolean hasProductionOrder) {
        this.hasProductionOrder = hasProductionOrder;
    }

    public boolean isItemChecked() {
        return itemChecked;
    }

    public void setItemChecked(boolean itemChecked) {
        this.itemChecked = itemChecked;
    }

    public boolean isItemEnabled() {
        return itemEnabled;
    }

    public void setItemEnabled(boolean itemEnabled) {
        this.itemEnabled = itemEnabled;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescLinea() {
        return descLinea;
    }

    public void setDescLinea(String descLinea) {
        this.descLinea = descLinea;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public int getTipoLinea() {
        return tipoLinea;
    }

    public void setTipoLinea(int tipoLinea) {
        this.tipoLinea = tipoLinea;
    }

    public Integer getPuedeRecuperarHistoricos() {
        return puedeRecuperarHistoricos;
    }

    public void setPuedeRecuperarHistoricos(Integer puedeRecuperarHistoricos) {
        this.puedeRecuperarHistoricos = puedeRecuperarHistoricos;
    }

    public String getFhUltModif() {
        return fhUltModif;
    }

    public void setFhUltModif(String fhUltModif) {
        this.fhUltModif = fhUltModif;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }
}
