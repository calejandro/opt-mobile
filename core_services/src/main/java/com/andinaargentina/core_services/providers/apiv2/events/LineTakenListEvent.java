package com.andinaargentina.core_services.providers.apiv2.events;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;

import java.util.List;

public class LineTakenListEvent {

    private List<LineasProduccionUsuarioDto> response;

    public LineTakenListEvent(List<LineasProduccionUsuarioDto> response) {
        this.response = response;
    }

    public List<LineasProduccionUsuarioDto> getResponse() {
        return response;
    }

    public void setResponse(List<LineasProduccionUsuarioDto> response) {
        this.response = response;
    }
}
