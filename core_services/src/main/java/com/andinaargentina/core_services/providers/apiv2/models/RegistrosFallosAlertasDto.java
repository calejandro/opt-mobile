package com.andinaargentina.core_services.providers.apiv2.models;

import java.util.Date;

public class RegistrosFallosAlertasDto {
    private int id;
    private int idAlerta;
    private int idModoFallo;
    private int idFallo;
    private int idCausa;
    private String accionTomada;
    private Date fechaCreacion;
    private Boolean borrado;
    private ModoFalloDto modoFallo;
    private ModoFalloDto fallo;
    private ModoFalloDto causa;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAlerta() {
        return idAlerta;
    }

    public void setIdAlerta(int idAlerta) {
        this.idAlerta = idAlerta;
    }

    public int getIdModoFallo() {
        return idModoFallo;
    }

    public void setIdModoFallo(int idModoFallo) {
        this.idModoFallo = idModoFallo;
    }

    public int getIdFallo() {
        return idFallo;
    }

    public void setIdFallo(int idFallo) {
        this.idFallo = idFallo;
    }

    public int getIdCausa() {
        return idCausa;
    }

    public void setIdCausa(int idCausa) {
        this.idCausa = idCausa;
    }

    public String getAccionTomada() {
        return accionTomada;
    }

    public void setAccionTomada(String accionTomada) {
        this.accionTomada = accionTomada;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public ModoFalloDto getModoFallo() {
        return modoFallo;
    }

    public void setModoFallo(ModoFalloDto modoFallo) {
        this.modoFallo = modoFallo;
    }

    public ModoFalloDto getFallo() {
        return fallo;
    }

    public void setFallo(ModoFalloDto fallo) {
        this.fallo = fallo;
    }

    public ModoFalloDto getCausa() {
        return causa;
    }

    public void setCausa(ModoFalloDto causa) {
        this.causa = causa;
    }
}
