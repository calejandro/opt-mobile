package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

import java.io.Serializable;
import java.util.Date;

@Parcel
public class SapOrdenesProduccionDto implements Serializable {
    private int idOrdenSap;
    private int idSku;
    private int cantidadTotalCajones;
    private String fhInicio;
    private String fhFin;
    private String idCentro;
    private int idPlanta;
    private int idLinea;
    private String grupoRecetas;
    private int cantidadEntregada;
    private int velocidadNominalLlenadora;
    private Date fhIncioProduccion;
    private Date fhFinProduccion;
    private Date fechaSupervision;
    private Boolean esTemporal;
    private Integer idOrdenVinculada;
    private SapSkuDto sapSku;
    private LineasProduccionDto lineasProduccion;

    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getIdOrdenSap() {
        return idOrdenSap;
    }

    public void setIdOrdenSap(int idOrdenSap) {
        this.idOrdenSap = idOrdenSap;
    }

    public int getIdSku() {
        return idSku;
    }

    public void setIdSku(int idSku) {
        this.idSku = idSku;
    }

    public int getCantidadTotalCajones() {
        return cantidadTotalCajones;
    }

    public void setCantidadTotalCajones(int cantidadTotalCajones) {
        this.cantidadTotalCajones = cantidadTotalCajones;
    }

    public String getFhInicio() {
        return fhInicio;
    }

    public void setFhInicio(String fhInicio) {
        this.fhInicio = fhInicio;
    }

    public String getFhFin() {
        return fhFin;
    }

    public void setFhFin(String fhFin) {
        this.fhFin = fhFin;
    }

    public String getIdCentro() {
        return idCentro;
    }

    public void setIdCentro(String idCentro) {
        this.idCentro = idCentro;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getGrupoRecetas() {
        return grupoRecetas;
    }

    public void setGrupoRecetas(String grupoRecetas) {
        this.grupoRecetas = grupoRecetas;
    }

    public int getCantidadEntregada() {
        return cantidadEntregada;
    }

    public void setCantidadEntregada(int cantidadEntregada) {
        this.cantidadEntregada = cantidadEntregada;
    }

    public int getVelocidadNominalLlenadora() {
        return velocidadNominalLlenadora;
    }

    public void setVelocidadNominalLlenadora(int velocidadNominalLlenadora) {
        this.velocidadNominalLlenadora = velocidadNominalLlenadora;
    }

    public Date getFhIncioProduccion() {
        return fhIncioProduccion;
    }

    public void setFhIncioProduccion(Date fhIncioProduccion) {
        this.fhIncioProduccion = fhIncioProduccion;
    }

    public Date getFhFinProduccion() {
        return fhFinProduccion;
    }

    public void setFhFinProduccion(Date fhFinProduccion) {
        this.fhFinProduccion = fhFinProduccion;
    }

    public Date getFechaSupervision() {
        return fechaSupervision;
    }

    public void setFechaSupervision(Date fechaSupervision) {
        this.fechaSupervision = fechaSupervision;
    }

    public Boolean getEsTemporal() {
        return esTemporal;
    }

    public void setEsTemporal(Boolean esTemporal) {
        this.esTemporal = esTemporal;
    }

    public Integer getIdOrdenVinculada() {
        return idOrdenVinculada;
    }

    public void setIdOrdenVinculada(Integer idOrdenVinculada) {
        this.idOrdenVinculada = idOrdenVinculada;
    }

    public SapSkuDto getSapSku() {
        return sapSku;
    }

    public void setSapSku(SapSkuDto sapSku) {
        this.sapSku = sapSku;
    }

    public LineasProduccionDto getLineasProduccion() {
        return lineasProduccion;
    }

    public void setLineasProduccion(LineasProduccionDto lineasProduccion) {
        this.lineasProduccion = lineasProduccion;
    }
}
