package com.andinaargentina.core_services.providers.api.clients.version;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import javax.inject.Inject;

import io.reactivex.Single;

public class ValidateVersionService extends BaseService<String> {

    private ApiRestService apiRestService;

    @Inject
    public ValidateVersionService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<String> getValidateVersionMethod(String appCode, String versionCode, Context context) {
        return super.getSingleObservable(apiRestService.validateVersion(appCode, versionCode),context);
    }
}
