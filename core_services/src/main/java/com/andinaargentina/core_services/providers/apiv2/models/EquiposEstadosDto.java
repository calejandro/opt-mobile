package com.andinaargentina.core_services.providers.apiv2.models;

public class EquiposEstadosDto {
    private int idRegistro;
    private int tipoEquipo;
    private int estado;
    private String descEstado;
    private String fhUltModif;
    private boolean activo;
    private String color;

    public int getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(int idRegistro) {
        this.idRegistro = idRegistro;
    }

    public int getTipoEquipo() {
        return tipoEquipo;
    }

    public void setTipoEquipo(int tipoEquipo) {
        this.tipoEquipo = tipoEquipo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescEstado() {
        return descEstado;
    }

    public void setDescEstado(String descEstado) {
        this.descEstado = descEstado;
    }

    public String getFhUltModif() {
        return fhUltModif;
    }

    public void setFhUltModif(String fhUltModif) {
        this.fhUltModif = fhUltModif;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
