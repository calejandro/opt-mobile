package com.andinaargentina.core_services.providers.apiv2.events;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;

import java.util.List;

public class ShiftsListSuccessEvent {

    private List<ShiftDto> response;

    public ShiftsListSuccessEvent(List<ShiftDto> response) {
        this.response = response;
    }

    public List<ShiftDto> getResponse() {
        return response;
    }

    public void setResponse(List<ShiftDto> response) {
        this.response = response;
    }
}
