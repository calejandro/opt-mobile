package com.andinaargentina.core_services.providers.api.clients.lineselection.sites;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class TakenByDto implements Serializable {

    @SerializedName("id")
    int id;

    @SerializedName("userName")
    String userName;

    @SerializedName("fullName")
    String fullName;

    @SerializedName("email")
    String email;

    @SerializedName("isSuperviser")
    boolean isSuperviser;

    public TakenByDto(int id, String userName, String fullName, String email, boolean isSuperviser) {
        this.id = id;
        this.userName = userName;
        this.fullName = fullName;
        this.email = email;
        this.isSuperviser = isSuperviser;
    }

    public TakenByDto() {
    }

    public boolean isSuperviser() {
        return isSuperviser;
    }

    public void setSuperviser(boolean superviser) {
        isSuperviser = superviser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
