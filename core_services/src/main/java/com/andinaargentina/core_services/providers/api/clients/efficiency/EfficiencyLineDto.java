package com.andinaargentina.core_services.providers.api.clients.efficiency;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EfficiencyLineDto {

    @SerializedName("userId")
    private int userId;

    @SerializedName("totalDailyEfficiencyPercentaje")
    private double totalDailyEfficiencyPercentaje;

    @SerializedName("totalDailyPlanEfficiencyPercentaje")
    private double totalDailyPlanEfficiencyPercentaje;

    @SerializedName("totalObjectiveEfficiencyPercentaje")
    private double totalObjectiveEfficiencyPercentaje;

    @SerializedName("totalObjectivePlanEfficiencyPercentaje")
    private double totalObjectivePlanEfficiencyPercentaje;

    @SerializedName("totalShifEfficiencyPercentaje")
    private double totalShifEfficiencyPercentaje;

    @SerializedName("totalShifPlanEfficiencyPercentaje")
    private double totalShifPlanEfficiencyPercentaje;

    @SerializedName("productionLinePercentajes")
    private List<EfficiencyDto> listLines;

    public EfficiencyLineDto(int userId, double totalDailyEfficiencyPercentaje, double totalDailyPlanEfficiencyPercentaje, double totalObjectiveEfficiencyPercentaje, double totalObjectivePlanEfficiencyPercentaje, double totalShifEfficiencyPercentaje, double totalShifPlanEfficiencyPercentaje, List<EfficiencyDto> listLines) {
        this.userId = userId;
        this.totalDailyEfficiencyPercentaje = totalDailyEfficiencyPercentaje;
        this.totalDailyPlanEfficiencyPercentaje = totalDailyPlanEfficiencyPercentaje;
        this.totalObjectiveEfficiencyPercentaje = totalObjectiveEfficiencyPercentaje;
        this.totalObjectivePlanEfficiencyPercentaje = totalObjectivePlanEfficiencyPercentaje;
        this.totalShifEfficiencyPercentaje = totalShifEfficiencyPercentaje;
        this.totalShifPlanEfficiencyPercentaje = totalShifPlanEfficiencyPercentaje;
        this.listLines = listLines;
    }

    public EfficiencyLineDto() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getTotalDailyEfficiencyPercentaje() {
        return totalDailyEfficiencyPercentaje;
    }

    public void setTotalDailyEfficiencyPercentaje(double totalDailyEfficiencyPercentaje) {
        this.totalDailyEfficiencyPercentaje = totalDailyEfficiencyPercentaje;
    }

    public double getTotalDailyPlanEfficiencyPercentaje() {
        return totalDailyPlanEfficiencyPercentaje;
    }

    public void setTotalDailyPlanEfficiencyPercentaje(double totalDailyPlanEfficiencyPercentaje) {
        this.totalDailyPlanEfficiencyPercentaje = totalDailyPlanEfficiencyPercentaje;
    }

    public double getTotalObjectiveEfficiencyPercentaje() {
        return totalObjectiveEfficiencyPercentaje;
    }

    public void setTotalObjectiveEfficiencyPercentaje(double totalObjectiveEfficiencyPercentaje) {
        this.totalObjectiveEfficiencyPercentaje = totalObjectiveEfficiencyPercentaje;
    }

    public double getTotalObjectivePlanEfficiencyPercentaje() {
        return totalObjectivePlanEfficiencyPercentaje;
    }

    public void setTotalObjectivePlanEfficiencyPercentaje(double totalObjectivePlanEfficiencyPercentaje) {
        this.totalObjectivePlanEfficiencyPercentaje = totalObjectivePlanEfficiencyPercentaje;
    }

    public double getTotalShifEfficiencyPercentaje() {
        return totalShifEfficiencyPercentaje;
    }

    public void setTotalShifEfficiencyPercentaje(double totalShifEfficiencyPercentaje) {
        this.totalShifEfficiencyPercentaje = totalShifEfficiencyPercentaje;
    }

    public double getTotalShifPlanEfficiencyPercentaje() {
        return totalShifPlanEfficiencyPercentaje;
    }

    public void setTotalShifPlanEfficiencyPercentaje(double totalShifPlanEfficiencyPercentaje) {
        this.totalShifPlanEfficiencyPercentaje = totalShifPlanEfficiencyPercentaje;
    }

    public List<EfficiencyDto> getListLines() {
        return listLines;
    }

    public void setListLines(List<EfficiencyDto> listLines) {
        this.listLines = listLines;
    }
}
