package com.andinaargentina.core_services.providers.api.clients.device;

import com.google.gson.annotations.SerializedName;

public class AddDeviceResponseDto {

    @SerializedName("userId")
    private int userId;

    @SerializedName("deviceUniqueIdentier")
    private String deviceUniqueIdentier;

    @SerializedName("applicationName")
    private String applicationName;

    @SerializedName("registrationId")
    private String registrationId;

    @SerializedName("isActive")
    private boolean isActive;

    public AddDeviceResponseDto(int userId, String deviceUniqueIdentier, String applicationName, boolean isActive, String registrationId) {
        this.userId = userId;
        this.deviceUniqueIdentier = deviceUniqueIdentier;
        this.applicationName = applicationName;
        this.isActive = isActive;
        this.registrationId = registrationId;
    }

    public AddDeviceResponseDto() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceUniqueIdentier() {
        return deviceUniqueIdentier;
    }

    public void setDeviceUniqueIdentier(String deviceUniqueIdentier) {
        this.deviceUniqueIdentier = deviceUniqueIdentier;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}
