package com.andinaargentina.core_services.providers.apiv2.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShiftUtils {

    private static ShiftUtils instance;

    private List<ShiftDto> shifts;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private Observable<List<ShiftDto>> observable = Observable.create(new ObservableOnSubscribe<List<ShiftDto>>() {
        @Override
        public void subscribe(@NonNull ObservableEmitter<List<ShiftDto>> emitter) throws Exception {
            if(shifts == null || shifts.get(2).getFin().before(new Date())) {
                OptimusServices.getInstance().getTurnosService().list(simpleDateFormat.format(new Date())).enqueue(new Callback<List<ShiftDto>>() {
                    @Override
                    public void onResponse(Call<List<ShiftDto>> call, Response<List<ShiftDto>> response) {
                        shifts = response.body();
                        emitter.onNext(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<ShiftDto>> call, Throwable t) {

                    }
                });
            } else {
                emitter.onNext(shifts);
            }
        }
    });

    private ShiftUtils() {

    }

    public static ShiftUtils getInstance() {
        if(instance == null) {
            instance = new ShiftUtils();
        }
        return instance;
    }

    public List<ShiftDto> getShifts() {
        return shifts;
    }

    public Observable<List<ShiftDto>> getObservable() {
        return observable;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public ShiftDto getCurrentShift() {
        if(shifts != null) {
            Optional<ShiftDto> shiftDto = this.shifts.stream()
                    .filter(f -> f.getInicio().before(new Date()) && f.getFin().after(new Date()))
                    .findFirst();
            if(shiftDto.isPresent()) {
                return  shiftDto.get();
            }
        }

        return null;
    }
}
