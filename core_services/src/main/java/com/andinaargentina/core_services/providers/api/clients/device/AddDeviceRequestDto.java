package com.andinaargentina.core_services.providers.api.clients.device;

import com.google.gson.annotations.SerializedName;

public class AddDeviceRequestDto {

    @SerializedName("userId")
    private int userId;

    @SerializedName("registrationId")
    private String registrationId;

    @SerializedName("deviceUniqueIdentier")
    private String tokenHub;

    @SerializedName("applicationName")
    private String applicationName = "OPTIMUS";

    public AddDeviceRequestDto() {
    }

    public AddDeviceRequestDto(int userId, String registrationId, String tokenHub) {
        this.userId = userId;
        this.registrationId = registrationId;
        this.tokenHub = tokenHub;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTokenHub() {
        return tokenHub;
    }

    public void setTokenHub(String tokenHub) {
        this.tokenHub = tokenHub;
    }
}
