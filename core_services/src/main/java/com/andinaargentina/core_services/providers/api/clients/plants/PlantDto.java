package com.andinaargentina.core_services.providers.api.clients.plants;

import org.parceler.Parcel;

@Parcel
public class PlantDto {

    public long id;
    public String description;
}
