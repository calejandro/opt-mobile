package com.andinaargentina.core_services.providers.api.clients.alerts;

import com.andinaargentina.core_services.providers.api.clients.failmode.DescriptionDto;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class AlertFailModeInformRequestDto implements Serializable {

    @SerializedName("alert")
    private AlertDto alertDto;

    @SerializedName("failModeId")
    private int failModeId;

    @SerializedName("failId")
    private int failId;

    @SerializedName("causeId")
    private int causeId;

    @SerializedName("actionsTaken")
    private String actionsTaken;

    public AlertFailModeInformRequestDto() {
    }

    public AlertFailModeInformRequestDto(AlertDto alertDto, int failModeId, int failId, int causeId, String actionsTaken) {
        this.alertDto = alertDto;
        this.failModeId = failModeId;
        this.failId = failId;
        this.causeId = causeId;
        this.actionsTaken = actionsTaken;
    }

    public AlertDto getAlertDto() {
        return alertDto;
    }

    public void setAlertDto(AlertDto alertDto) {
        this.alertDto = alertDto;
    }

    public int getFailModeId() {
        return failModeId;
    }

    public void setFailModeId(int failModeId) {
        this.failModeId = failModeId;
    }

    public int getFailId() {
        return failId;
    }

    public void setFailId(int failId) {
        this.failId = failId;
    }

    public int getCauseId() {
        return causeId;
    }

    public void setCauseId(int causeId) {
        this.causeId = causeId;
    }

    public String getActionsTaken() {
        return actionsTaken;
    }

    public void setActionsTaken(String actionsTaken) {
        this.actionsTaken = actionsTaken;
    }
}
