
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable
{

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("planBottles")
    @Expose
    private int planBottles;
    @SerializedName("produceBottles")
    @Expose
    private int produceBottles;
    private final static long serialVersionUID = -8175019503543468246L;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPlanBottles() {
        return planBottles;
    }

    public void setPlanBottles(int planBottles) {
        this.planBottles = planBottles;
    }

    public int getProduceBottles() {
        return produceBottles;
    }

    public void setProduceBottles(int produceBottles) {
        this.produceBottles = produceBottles;
    }

}
