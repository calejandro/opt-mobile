package com.andinaargentina.core_services.providers.apiv2.interceptors;

import androidx.annotation.Nullable;

import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.api.core.gson.DoubleTypeAdapter;
import com.andinaargentina.core_services.providers.api.core.gson.IntTypeAdapter;
import com.andinaargentina.core_services.providers.api.core.gson.LongTypeAdapter;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.oauth2.AuthStateManager;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.ClientAuthentication;
import net.openid.appauth.ClientSecretPost;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class AuthenticationInterceptor implements Interceptor{

    public AuthenticationInterceptor() {

    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request original = chain.request();
        AuthStateManager authStateManager = AuthStateManager.getInstance();
        if(authStateManager.getCurrent().getNeedsTokenRefresh()) {
            OkHttpClient okHttpClient = new OkHttpClient();
            final FormBody.Builder builderRefresh = new FormBody.Builder();
            builderRefresh.addEncoded ("client_id", "34pi17dnbnmqp4dc9j69b24l4s");
            builderRefresh.addEncoded ("client_secret", "1jue9p8538hqpcu4m9ock4c6k4uqonak92qhhlv5114nqsupm34m");
            builderRefresh.addEncoded("grant_type", "refresh_token");
            builderRefresh.addEncoded("refresh_token", authStateManager.getCurrent().getRefreshToken());
            builderRefresh.addEncoded("redirect_uri", "koandina://optimus.com");
            Request requestRefresh = new Request.Builder()
                    .url(String.valueOf(authStateManager.getCurrent().getAuthorizationServiceConfiguration().tokenEndpoint))
                    .post(builderRefresh.build())
                    .build();

            Response responseRefresh = okHttpClient.newCall(requestRefresh).execute();
            String tokenString = responseRefresh.body().string();
            JsonObject tokenJson = new JsonParser().parse(tokenString).getAsJsonObject();
            JSONObject lastTokenJson = authStateManager.getCurrent().getLastTokenResponse().jsonSerialize();
            try {
                lastTokenJson.put("access_token", tokenJson.get("access_token"));
                lastTokenJson.put("refresh_token", tokenJson.get("refresh_token"));
                lastTokenJson.put("expires_at", new Date().getTime() + tokenJson.get("expires_in").getAsLong());
                authStateManager.getCurrent().update(TokenResponse.jsonDeserialize(lastTokenJson), null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        String token = AuthStateManager.getInstance().getCurrent().getAccessToken();
        Request.Builder builder = original.newBuilder()
                .header("Authorization", "Bearer " + token);
        Request request = builder.build();
        Response response = chain.proceed(request);
        MediaType contentType = response.body().contentType();
        Type type = new TypeToken<Content>(){}.getType();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(int.class, new IntTypeAdapter())
                .registerTypeAdapter(Integer.class, new IntTypeAdapter())
                .registerTypeAdapter(double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(Double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(long.class, new LongTypeAdapter())
                .registerTypeAdapter(Long.class, new LongTypeAdapter())
                .registerTypeAdapter(Date.class, new ApiRestModule.DateDeserializer())
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                //.setDateFormat(FORMAT_DATE)
                .create();
        String bodyResponse = response.body().string();
        JsonObject jsonResponse = new JsonParser().parse(bodyResponse).getAsJsonObject();
        String json = gson.toJson(jsonResponse.get("result"));
        ResponseBody body = ResponseBody.create(contentType, json);
        return response.newBuilder().body(body).build();
    }
}
