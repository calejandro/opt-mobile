package com.andinaargentina.core_services.providers.local.base;

import androidx.annotation.NonNull;

import com.orhanobut.logger.Logger;

import io.paperdb.Book;
import io.paperdb.Paper;

public abstract class CrudLocalProvider<T> implements ICRUDLocalProvider<T> {

    private Book book;

    public CrudLocalProvider() {
        this.book = Paper.book("SGI-BOOK");
        Logger.d("A crud repository was instantiated");
    }

    @Override
    public T readData(@NonNull String key) {
        return book.read(key);
    }

    @Override
    public void writeData(@NonNull String key, @NonNull T value) {
        book.write(key, value);
    }

    @Override
    public void deleteData(@NonNull String key) {
        book.delete(key);
    }

    @Override
    public void deleteAllData() {
        book.destroy();
    }

    @Override
    public boolean containData(@NonNull String key) {
        return book.contains(key);
    }
}
