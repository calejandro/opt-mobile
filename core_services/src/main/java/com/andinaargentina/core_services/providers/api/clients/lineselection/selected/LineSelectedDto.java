package com.andinaargentina.core_services.providers.api.clients.lineselection.selected;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LineSelectedDto {

    @SerializedName("isSupervision")
    private boolean isSupervision;

    @SerializedName("unSelectAllOtherPlants")
    private Boolean unSelectAllOtherPlants;

    @SerializedName("assigments")
    private List<SelectedDto> assigments;

    public LineSelectedDto(boolean isSupervision, List<SelectedDto> assigments) {
        this.isSupervision = isSupervision;
        this.assigments = assigments;
        this.unSelectAllOtherPlants = null;
    }

    public LineSelectedDto(boolean isSupervision, List<SelectedDto> assigments, Boolean unSelectAllOtherPlants) {
        this.isSupervision = isSupervision;
        this.assigments = assigments;
        this.unSelectAllOtherPlants = unSelectAllOtherPlants;
    }

    public boolean isSupervision() {
        return isSupervision;
    }

    public void setSupervision(boolean supervision) {
        isSupervision = supervision;
    }

    public List<SelectedDto> getAssigments() {
        return assigments;
    }

    public void setAssigments(List<SelectedDto> assigments) {
        this.assigments = assigments;
    }

    public Boolean getUnSelectAllOtherPlants() {
        return unSelectAllOtherPlants;
    }

    public void setUnSelectAllOtherPlants(Boolean unSelectAllOtherPlants) {
        this.unSelectAllOtherPlants = unSelectAllOtherPlants;
    }
}
