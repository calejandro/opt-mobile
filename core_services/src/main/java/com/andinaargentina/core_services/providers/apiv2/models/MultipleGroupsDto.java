package com.andinaargentina.core_services.providers.apiv2.models;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class MultipleGroupsDto implements Serializable {
    private int multipleGroupId;
    private Integer userId;
    private Integer userGroupId;
    private UserGroupDto userGroup;

    public int getMultipleGroupId() {
        return multipleGroupId;
    }

    public void setMultipleGroupId(int multipleGroupId) {
        this.multipleGroupId = multipleGroupId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(Integer userGroupId) {
        this.userGroupId = userGroupId;
    }

    public UserGroupDto getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroupDto userGroup) {
        this.userGroup = userGroup;
    }
}
