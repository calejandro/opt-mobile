package com.andinaargentina.core_services.providers.api.clients.device;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderRequestDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderResponseDto;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import javax.inject.Inject;

import io.reactivex.Single;

public class AddDeviceService extends BaseService<AddDeviceResponseDto> {

    private ApiRestService apiRestService;

    @Inject
    public AddDeviceService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<AddDeviceResponseDto> adddDevice(AddDeviceRequestDto addDeviceRequestDto, Context context){
        return super.getSingleObservable(apiRestService.addDevice(addDeviceRequestDto),context);
    }
}
