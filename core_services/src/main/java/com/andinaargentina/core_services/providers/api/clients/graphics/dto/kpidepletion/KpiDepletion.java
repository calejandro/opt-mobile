
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class KpiDepletion implements Serializable
{

    @SerializedName("productionLineId")
    @Expose
    private int productionLineId;

    @SerializedName("depletions")
    @Expose
    private List<Depletion> depletions = null;

    @SerializedName("lastRecordHour")
    @Expose
    private int lastRecordHour;

    private final static long serialVersionUID = 4226194411583391004L;

    public int getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(int productionLineId) {
        this.productionLineId = productionLineId;
    }

    public List<Depletion> getDepletions() {
        return depletions;
    }

    public void setDepletions(List<Depletion> depletions) {
        this.depletions = depletions;
    }

    public int getLastRecordHour() {
        return lastRecordHour;
    }

    public void setLastRecordHour(int lastRecordHour) {
        this.lastRecordHour = lastRecordHour;
    }

}
