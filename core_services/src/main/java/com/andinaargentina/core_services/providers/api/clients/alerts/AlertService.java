package com.andinaargentina.core_services.providers.api.clients.alerts;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class AlertService extends BaseService<List<LineasProduccionAlertasDto>> {


    @Inject
    public AlertService() {
    }

    public Single<List<LineasProduccionAlertasDto>> getAlerts(int idLinea, String fechaInicio, String fechaFin , Context context) {
        return super.getSingleObservable(OptimusServices.getInstance().getLineasProduccionAlertasService().list(
                new String[]{"equipo", "lineaProduccion"}, fechaInicio, fechaFin, idLinea),context);
    }

    public void cancel(){
        if (getCall()!=null)
            getCall().cancel();
    }
}
