package com.andinaargentina.core_services.providers.local.user;

import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.local.base.CrudLocalProvider;

public class UserProvider extends CrudLocalProvider<SysUsersDto> {
}
