package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.models.LinkTemporalOrderDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.models.UserDto;
import com.andinaargentina.core_services.providers.apiv2.models.UserEmailDto;
import com.andinaargentina.core_services.providers.apiv2.models.UserResetPasswordDto;
import com.andinaargentina.core_services.providers.apiv2.models.UserSendResetDto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserService extends BaseService<UserDto, BaseFilter>{

    @POST("confirm")
    Call<UserDto> create(@Body() UserEmailDto entity);

    @POST("SendResetPassword")
    Call<UserDto> createSendReset(@Body() UserSendResetDto entity);

    @POST("ResetPassword")
    Call<UserDto> createResetPassword(@Body() UserResetPasswordDto entity);

    @POST("Undelete")
    Call<UserDto> update(@Body() UserDto entity);

    @GET("exisit-by-email")
    Call<ArrayList<UserDto>> list(@Query("email") String email);
}
