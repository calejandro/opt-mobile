package com.andinaargentina.core_services.providers.api.clients.lineselection;

import android.content.Context;
import android.telecom.Call;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.filters.LineasProduccionFilter;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LineSelectionService extends BaseService<List<LineasProduccionDto>> {

    @Inject
    public LineSelectionService() {
    }

    public Single<List<LineasProduccionDto>> getLinesByPlant(String idPlant, Context context) {
        return super.getSingleObservable(OptimusServices.getInstance().getLineasProduccionService().list(new String[]{"planta"},
                 idPlant  , true, 1),context);
    }
}
