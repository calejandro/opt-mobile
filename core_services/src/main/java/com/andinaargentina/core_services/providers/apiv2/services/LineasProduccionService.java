package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.filters.BaseFilter;
import com.andinaargentina.core_services.providers.apiv2.filters.LineasProduccionFilter;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.utils.Paginator;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LineasProduccionService {

    @GET("LineasProduccion")
    Call<List<LineasProduccionDto>> list(@Query("includes") String[] includes, @Header("plantaId") String plantaId, @Query("activo") boolean activo, @Query("tipoLinea") int tipoLinea );

    @GET("LineasProduccion/{id}/has-production-order")
    Call<Boolean> hasProdcutionOrder(@Path("id") int id);

    @GET("LineasProduccion/{id}/orders")
    Call<List<SapOrdenesProduccionDto>> orders(@Path("id") int id, @Header("idPlanta") int idPlanta, @Query("fechaInicio") String[] fechaInicio,
                                               @Query("includes") String[] includes);


}
