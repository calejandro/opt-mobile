
package com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CombinedChart_ implements Serializable
{

    @SerializedName("axisYName")
    @Expose
    private String axisYName;
    @SerializedName("axisXName")
    @Expose
    private String axisXName;
    @SerializedName("axisYMaxValue")
    @Expose
    private int axisYMaxValue;
    @SerializedName("nominalMaximumVelocity")
    @Expose
    private int nominalMaximumVelocity;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    private final static long serialVersionUID = -7593700478922534153L;

    public String getAxisYName() {
        return axisYName;
    }

    public void setAxisYName(String axisYName) {
        this.axisYName = axisYName;
    }

    public String getAxisXName() {
        return axisXName;
    }

    public void setAxisXName(String axisXName) {
        this.axisXName = axisXName;
    }

    public int getAxisYMaxValue() {
        return axisYMaxValue;
    }

    public void setAxisYMaxValue(int axisYMaxValue) {
        this.axisYMaxValue = axisYMaxValue;
    }

    public int getNominalMaximumVelocity() {
        return nominalMaximumVelocity;
    }

    public void setNominalMaximumVelocity(int nominalMaximumVelocity) {
        this.nominalMaximumVelocity = nominalMaximumVelocity;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
