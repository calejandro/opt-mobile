package com.andinaargentina.core_services.providers.api.clients.orders;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.ApiRestService;
import com.andinaargentina.core_services.providers.api.core.base.BaseService;

import javax.inject.Inject;

import io.reactivex.Single;

public class ChangeOrderService extends BaseService<ChangeOrderResponseDto> {

    private ApiRestService apiRestService;

    @Inject
    public ChangeOrderService(ApiRestService apiRestService) {
        this.apiRestService = apiRestService;
    }

    public Single<ChangeOrderResponseDto> postChangeOrder(ChangeOrderRequestDto changeOrderRequestDto, Context context){
        return super.getSingleObservable(apiRestService.postChangeLineOrder(changeOrderRequestDto),context);
    }
}
