package com.andinaargentina.core_services.providers.apiv2.models;

public class LineasProduccionElpDto {
    private int idLinea;
    private String descLinea;
    private int idPlanta;
    private int tipoLinea;
    private Integer puedeRecuperarHistoricos;
    private String fhUltModif;
    private boolean activo;
    private PlantasDto planta;
    private LineasProdSapToLineasProdElpDto lineasProdSapToLineasProdElp;

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getDescLinea() {
        return descLinea;
    }

    public void setDescLinea(String descLinea) {
        this.descLinea = descLinea;
    }

    public int getIdPlanta() {
        return idPlanta;
    }

    public void setIdPlanta(int idPlanta) {
        this.idPlanta = idPlanta;
    }

    public int getTipoLinea() {
        return tipoLinea;
    }

    public void setTipoLinea(int tipoLinea) {
        this.tipoLinea = tipoLinea;
    }

    public Integer getPuedeRecuperarHistoricos() {
        return puedeRecuperarHistoricos;
    }

    public void setPuedeRecuperarHistoricos(Integer puedeRecuperarHistoricos) {
        this.puedeRecuperarHistoricos = puedeRecuperarHistoricos;
    }

    public String getFhUltModif() {
        return fhUltModif;
    }

    public void setFhUltModif(String fhUltModif) {
        this.fhUltModif = fhUltModif;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public PlantasDto getPlanta() {
        return planta;
    }

    public void setPlanta(PlantasDto planta) {
        this.planta = planta;
    }

    public LineasProdSapToLineasProdElpDto getLineasProdSapToLineasProdElp() {
        return lineasProdSapToLineasProdElp;
    }

    public void setLineasProdSapToLineasProdElp(LineasProdSapToLineasProdElpDto lineasProdSapToLineasProdElp) {
        this.lineasProdSapToLineasProdElp = lineasProdSapToLineasProdElp;
    }
}
