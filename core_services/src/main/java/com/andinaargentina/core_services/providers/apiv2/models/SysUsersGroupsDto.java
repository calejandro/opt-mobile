package com.andinaargentina.core_services.providers.apiv2.models;

public class SysUsersGroupsDto {
    private int userGroupId;
    private String description;
    private Integer level;
    private String initUrl;
    private Integer subsistemaId;
    private String isActive;

    public int getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(int userGroupId) {
        this.userGroupId = userGroupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getInitUrl() {
        return initUrl;
    }

    public void setInitUrl(String initUrl) {
        this.initUrl = initUrl;
    }

    public Integer getSubsistemaId() {
        return subsistemaId;
    }

    public void setSubsistemaId(Integer subsistemaId) {
        this.subsistemaId = subsistemaId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
