package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.models.KpiDepletionDto;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MermasKPIService {

    @GET("MermasKPI/ForBottles")
    Call<ArrayList<KpiDepletionDto>> list(@Query("idLinea") int idLinea,
                                                 @Query("fechaHoraInicio") String fechaHoraInicio,
                                                 @Query("fechaHoraFin") String fechaHoraFin,
                                                 @Query("tipoAgrupacion") int tipoAgrupacion);
}
