package com.andinaargentina.core_services.providers.api.clients.failmode;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class DescriptionDto {

    @SerializedName("id")
    private int id;

    @SerializedName("description")
    private String description;

    @SerializedName("productionLineAlertKind")
    private Integer productionLineAlertKind;

    public DescriptionDto() {
    }

    public DescriptionDto(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProductionLineAlertKind() {
        return productionLineAlertKind;
    }

    public void setProductionLineAlertKind(Integer productionLineAlertKind) {
        this.productionLineAlertKind = productionLineAlertKind;
    }
}
