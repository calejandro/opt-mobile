package com.andinaargentina.core_services.providers.apiv2.services;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LineasProduccionUsuarioService {

    @GET("LineasProduccionUsuario")
    Call<List<LineasProduccionUsuarioDto>> list(@Query("includes") String[] includes,
                                                @Header("plantaId") String plantaId,
                                                @Query("isTkae") boolean isTake,
                                                @Query("supervisada") Boolean supervisada,
                                                @Query("fechaInicio") String fechaInicio,
                                                @Query("fechaFin") String fechaFin);



    @POST("LineasProduccionUsuario/take")
    Call<List<LineasProduccionUsuarioDto>> take(@Body() List<LineasProduccionUsuarioDto> lineasProduccionUsuarioDto);

    @POST("LineasProduccionUsuario/clear")
    Call<Object> clear();
}
