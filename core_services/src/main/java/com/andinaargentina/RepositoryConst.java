package com.andinaargentina;

public class RepositoryConst {

    public static final String IS_OBSERVER_MODE = "is_observer_mode";
    public static final String LINES_SELECTED = "Lines_selected";
    public static final String USER_LOGGED = "User";
    public static final String ALL_LINES = "ALL_LINES";
    public static final String PLANT_SELECTED = "plant_selected";
    public static final String PLANTS_SAVED = "plants_saved";
    public static final String REMEMBER_DIALOG_TAKE_COMFIRM = "remember_dialog_take_comfirm";
    public static final String REMEMBER_DIALOG_OBSERVER_MODE = "remember_dialog_observer_mode";
    public static final String FCM_REGISTRATION_ID = "registrationID";
    public static final String FCM_TOKEN = "registrationID";

    public static final String GRAPHICS_DATA = "graphics_local_data";
    public static final String ALERTS_DATA = "alerts_local_data";
    public static final String MODO_DE_FALLO = "fail_modes";
}
