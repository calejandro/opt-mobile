package com.andinaargentina.app_template;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.andinaargentina.app_template.client.ApiClientService;
import com.andinaargentina.app_template.client.ClientService;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.models.UserDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.app_template.fragments.MainFragment;
import com.andinaargentina.app_template.utils.UserUtils;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Andina on 05/06/18.
 */

public class MainActivity extends AppCompatActivity
                    implements NavigationView.OnNavigationItemSelectedListener {

    static final String TAG = "MainActivity";

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    private ProgressDialog pDialog;

    private boolean home = true;

    private TextView infoUserUsername;
    private TextView infoUserlastUpdate;
    private TextView infoUserlastLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        OptimusServices.getInstance(BuildConfig.API_V2_URL);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        View hView = mNavigationView.getHeaderView(0);

        infoUserUsername = ((TextView) hView.findViewById(R.id.user_info_username));
        infoUserlastLogin = ((TextView) hView.findViewById(R.id.header_txt_user_info_last_login));
        infoUserlastUpdate = ((TextView) hView.findViewById(R.id.header_txt_info_last_update));

        SysUsersDto user = UserUtils.getInstance().getUser();

        infoUserUsername.setText(user.getName() + " (" + user.getUserId() + ")");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        infoUserlastLogin.setText(sharedPreferences.getString("config.lastLogin",""));
        infoUserlastUpdate.setText(sharedPreferences.getString("config.lastUpdate",""));

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.getMenu().getItem(0).setChecked(true);

        AplicarPermisos();

        mFragmentManager = getSupportFragmentManager();
        openPrimary();
    }

    private void AplicarPermisos()
    {
        // Mostrar / esconder menu items .::. EJEMPLO PERMISOS
        boolean visible = UserUtils.getInstance().TienePermiso("Menu_App_Alertas");
        mNavigationView.getMenu().findItem(R.id.nav_alertas).setVisible(visible);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @SuppressLint("RestrictedApi")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_home:
                List<Fragment> frags = mFragmentManager.getFragments();
                for (Fragment frag : frags) {
                    if (frag != null) {
                        mFragmentManager.beginTransaction().remove(frag).commit();
                        mFragmentManager.popBackStackImmediate();
                    }
                }

                openPrimary();
                break;

            case R.id.nav_alertas:
                Intent intent=new Intent(MainActivity.this,AlertsActivity.class);
                startActivity(intent);
                break;

            case R.id.nav_logout:
                logout();
                break;

            default:

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openPrimary() {
        home = true;
        invalidateOptionsMenu();

        mNavigationView.getMenu().findItem(R.id.nav_home).setChecked(true);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();

        MainFragment fragment = new MainFragment();
        /*Bundle b = new Bundle();
        fragment.setArguments(b);*/

        mFragmentTransaction.replace(R.id.layout_content_main, fragment, MainFragment.TAG)
                .commit();

    }

    public void logout() {

        logoutApi();

        SharedPreferences preferences = getSharedPreferences(getText(R.string.shared_pref_name).toString(), Context.MODE_PRIVATE);
        preferences.edit().putString("token", "").apply();

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void logoutApi() {
        /*Retrofit retrofit = ClientService.getRetro();

        ApiClientService service = retrofit.create(ApiClientService.class);
        Call<User> call = service.logout(UserUtils.getInstance().getToken());
        try {
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            Log.d("MAIN", e.getMessage(), e);
        }*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (home)
                finish();
            else
                openPrimary();
        }
    }



}
