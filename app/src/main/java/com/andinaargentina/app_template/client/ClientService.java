package com.andinaargentina.app_template.client;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import androidx.core.app.NotificationCompat;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andina on 05/06/18.
 */

public class ClientService {

    private Context context;
    private Notification notif;
    private Notification errorNotif;
    private NotificationManager mNotificationManager;
    private ApiClientService service;

    private boolean isRunning = false;
    
    public ClientService(Context context) {
        this.context = context;

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.stat_notify_sync)
                        .setContentTitle("Sincronizando")
                        .setContentText("La aplicación esta sincronizando la informacion!");

        notif = builder.build();

        builder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.stat_notify_error)
                .setContentTitle("Error sincronizando")
                .setContentText("Ocurrio un error al sincronizar la informacion");
        errorNotif = builder.build();

        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        service = getRetro().create(ApiClientService.class);
    }

    private void showNotification() {
        mNotificationManager.notify(123, notif);
    }

    private void showErrorNotification() {
        mNotificationManager.notify(123, errorNotif);
    }

    private void hideNotif() {
        mNotificationManager.cancel(123);
    }

    public static Retrofit getRetro() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new StethoInterceptor())
                .build();

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        return new Retrofit.Builder()
                .baseUrl(ApiClientService.apiUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
