package com.andinaargentina.app_template.client;

import com.andinaargentina.app_template.BuildConfig;
import com.andinaargentina.legacy.domain.RegistrationResponse;
import com.andinaargentina.legacy.domain.Alertas;
import com.andinaargentina.legacy.domain.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Andina on 05/06/18.
 */

public interface ApiClientService {

    String codigoSubSistema = "SGIAM";

    String apiUrl = BuildConfig.API_URL;
    String changePassUrl = BuildConfig.RECOVERY_PASS_URL;

    @GET("Login/Login")
    Call<User> login(@Query("wUserName") String user,
                     @Query("wPassword") String password,
                     @Query("wDevice") String device, //FCMID -->
                     @Query("wCodSubsistema") String wCodSubsistema);

    @GET("Login/CambioPassword")
    Call<String> changePassword(@Query("wUserName") String username,
                                @Query("wPassActual") String oldPassword,
                                @Query("wNuevoPassword") String password);

    @GET("Login/Validar")
    Call<User> validateToken(@Query("wUserId") String userId,
                             @Query("wToken") String token,
                             @Query("wCodSubsistema") String wCodSubsistema);

    @GET("Login/ValidarVersion")
    Call<String> validateVersion(@Query("wAppCode") String appCode,
                                 @Query("wVersionCode") String versionCode);

    @GET("Login/Logout")
    Call<User> logout(@Query("wToken") String token);

    @GET("Login/NuevoDispositivo")
    Call<RegistrationResponse> nuevoDispositivo(@Query("wUserId") int userId, //--> User logueado
                                                @Query("wRegistrationId") String registrationId, //FCMID
                                                @Query("wAppName") String appName, // --> OPTIMUS
                                                @Query("wToken")String token, // --> el mismo que envió el login
                                                @Query("wDeviceId")int deviceId); // --> 0

    @GET("Alertas/AlertasYAvisosPortadaRuta")
    Call<Alertas> AlertasYAvisosPortada(@Query("wUserId") int userId,@Query("wToken")String token);

    @PUT("Alertas/{id}/MarkAsRead")
    Call<Alertas> MarkAsRead(@Path("id") String id);

    @PUT("Alertas/{id}/MarkAsDeleted")
    Call<Alertas> MarkAsDeleted(@Path("id") String id);
}
