package com.andinaargentina.app_template.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.andinaargentina.app_template.R;

/**
 * Created by Andina on 05/06/18.
 */

public class PrimaryFragment extends Fragment {

    public static final String TAG = "Primary";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_primary_layout, null);

        return v;
    }

}