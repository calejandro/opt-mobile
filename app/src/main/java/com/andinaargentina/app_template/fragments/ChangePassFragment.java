package com.andinaargentina.app_template.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.andinaargentina.app_template.LoginActivity;
import com.andinaargentina.app_template.R;
import com.andinaargentina.app_template.client.ApiClientService;
import com.andinaargentina.app_template.client.ClientService;
import com.andinaargentina.app_template.utils.UserUtils;
import com.andinaargentina.app_template.utils.Utils;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_lineselection.monolitic.LineSelectorActivity;
import com.pixplicity.easyprefs.library.Prefs;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Andina on 05/06/18.
 */

public class ChangePassFragment extends Fragment implements Callback<String> {

    public static final String TAG = "ChangePassFragment";

    private String username;

    private TextView oldPassword, password, retypePassword;
    private ProgressDialog progress;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Cambiar contraseña");

        View v = inflater.inflate(R.layout.fragment_change_pass, null);

        username = UserUtils.getInstance().getUser().getName();
        oldPassword = (TextView) v.findViewById(R.id.change_password_old);
        password = (TextView) v.findViewById(R.id.change_password_password);
        retypePassword = (TextView) v.findViewById(R.id.change_password_retype_password);
        
        v.findViewById(R.id.change_pass_button).setOnClickListener(this::cambioPassword);

        return v;
    }
    
    
    public void cambioPassword(View view) {
        String oldPass = oldPassword.getText().toString();
        String newPass = password.getText().toString();

        if(newPass.length() < 5){
            Toast.makeText(getContext(), "La contraseña debe tener al menos 5 caracteres.", Toast.LENGTH_LONG).show();
            return;
        }

        hideKeyboard(view);
        if (!password.getText().toString().equals(retypePassword.getText().toString())) {
            showMessage("Las contraseñas no coinciden!");
        } else if (oldPass.trim().isEmpty() || newPass.trim().isEmpty()) {
            showMessage("Las contraseña no puede estar vacia.");
        } else {
            progress = new ProgressDialog(getContext());
            progress.setCancelable(false);
            progress.setMessage("Espere por favor...");
            progress.show();

            Retrofit retrofit = ClientService.getRetro();

            ApiClientService service = retrofit.create(ApiClientService.class);
            Call<String> call = service.changePassword(username, oldPass, newPass);

            try {
                call.enqueue(this);
            } catch (Exception e) {
                progress.dismiss();
                progress = null;
                Log.d(TAG, e.getMessage(), e);
            }
        }
    }

    private void login() {
        /*Retrofit retrofit = ClientService.getRetro();

        ApiClientService service = retrofit.create(ApiClientService.class);
        Prefs.putString("username", username);
        Prefs.putString("password", password.getText().toString());
        Call<User> call = service.login(username, password.getText().toString(), Utils.getDeviceInfo(), ApiClientService.codigoSubSistema);

        try {

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    progress.dismiss();
                    progress = null;

                    try {
                        String msg;
                        if (response.body() == null) {
                            msg = response.errorBody().string();
                        }

                        msg = response.body().getMMensajeLogin();
                        if (msg.equals("EXITO")) {

                            LoginActivity.saveLogin(getActivity(), response.body());

                            Intent intent = new Intent(getContext(), LineSelectorActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            try {
                                int id = getResources().getIdentifier(msg, "string", getContext().getPackageName());
                                msg = getString(id);
                            } catch (Exception e) {
                            }
                            showMessage(msg);
                        }
                    }
                    catch (Exception e) {
                        
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    progress.dismiss();
                    progress = null;
                    try {
                        if (t instanceof UnknownHostException ||
                                t instanceof SocketTimeoutException ||
                                t instanceof ConnectException) {
                            showMessage(getString(R.string.error_no_wifi));
                        } else {
                            showMessage(getString(R.string.error_network_generic));
                        }
                    } catch (Exception ex) {
                        showMessage(getString(R.string.error_network_generic));
                    }
                }
            });
        } catch (Exception ex) {
            progress.dismiss();
            progress = null;
            showMessage(getString(R.string.error_no_wifi));
        }*/
    }

    private void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {

        try {
            String msg = response.body();
            if (msg.equals("EXITO")) {
                showMessage("Su contraseña fue cambiada exitosamente.");
                login();
            } else {
                progress.dismiss();
                progress = null;
                try {
                    int id = getResources().getIdentifier(msg, "string", getContext().getPackageName());
                    msg = getString(id);
                } catch (Exception e) {
                }
                showMessage(msg);
            }
        } catch (Exception ex) {
            progress.dismiss();
            progress = null;
            showMessage(getString(R.string.error_no_wifi));
        }
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        progress.dismiss();
        progress = null;
        try {
            if (t instanceof UnknownHostException ||
                    t instanceof SocketTimeoutException ||
                    t instanceof ConnectException) {
                showMessage(getString(R.string.error_no_wifi));
            } else {
                showMessage(getString(R.string.error_network_generic));
            }
        } catch (Exception ex) {
            showMessage(getString(R.string.error_network_generic));
        }
    }
}
