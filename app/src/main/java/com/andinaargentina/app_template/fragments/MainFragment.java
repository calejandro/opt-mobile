package com.andinaargentina.app_template.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.andinaargentina.app_template.R;

/**
 * Created by Andina on 05/06/18.
 */

public class MainFragment extends Fragment {

    public static final String TAG = "MainFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("SGI - MOBILE");

        View v = inflater.inflate(R.layout.fragment_main, container, false);

        /* EJEMPLO PERMISO
        PermissionsItem item = UserUtils.getInstance().getPermissionByResource("Resource");
        if (item == null || !"y".equals(item.getMIsEnabled()))
            view.setVisibility(View.GONE);
        */

        return v;
    }
}