package com.andinaargentina.app_template.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Andina on 05/06/18.
 */

public class Utils {
    private static final String TAG = "Utils";
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static boolean isConfigDateValid(String configDate) {

        boolean ret = false;
        if (configDate != null) {
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(fromDbToDate(configDate));
                int seted = cal.get(Calendar.DAY_OF_YEAR);
                cal.setTime(new Date());
                int today = cal.get(Calendar.DAY_OF_YEAR);

                ret = seted - today == 0;
            } catch (Exception e) {
            }
        }
        return ret;
    }

    public static Date fromDbToDate(String date) {
        Date fecha = null;
        if (date != null) {
            try {
                sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
                fecha = sdf.parse(date);
            } catch (Exception e) {
            }
        }
        return fecha;
    }

    public static String fromDateToDb(Date date) {
        String formatted = null;
        if (date != null) {
            sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
            formatted = sdf.format(date);
        }
        return formatted;
    }

    public static String toDisplayDate(Date date) {
        String formatted = "";
        if (date != null) {
            sdf.applyPattern("dd/MM/yyyy");
            formatted = sdf.format(date);
        }
        return formatted;
    }

    public static String toDisplayDateTime(Date date) {
        String formatted = "";
        if (date != null) {
            sdf.applyPattern("dd/MM/yyyy HH:mm:ss");
            formatted = sdf.format(date);
        }
        return formatted;
    }

    public static Date formatFecha(String dateString, SimpleDateFormat format) {
        try {
            Date date = format.parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String fromDateToString(Date date,String pattern) {
        String formatted = null;
        if (date != null) {
            sdf.applyPattern(pattern);
            formatted = sdf.format(date);
        }
        return formatted;
    }

    public static void showMessage(Context context, String message) {
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static int generateRandomId() {
        Random rnd = new Random();
        return rnd.nextInt(9000000) + 1000;
    }

    public static String generateRandomIdentifier() {
        /*Random rnd = new Random();
        long n = 10000000000000l + rnd.nextLong();
        return String.valueOf(n).substring(0, 15);*/
        return UUID.randomUUID().toString();
    }

    public static String getDeviceInfo() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String version = Build.VERSION.RELEASE;
        return "Android - " + manufacturer + " " + model + " " + version;
    }

    public static String getAppId() {
        return "1";
    }


    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static boolean saveBitmapToFile(String fileName, Bitmap bm) {

        File imageFile = new File(fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
            return true;
        } catch (IOException e) {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    public static String objectToHtml(Object o){
        StringBuilder b = new StringBuilder();
        Field[] fields = o.getClass().getDeclaredFields();

        b.append("<ul>");
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                b.append("<li>");

                b.append("<b>").append(field.getName()).append(":</b>");
                b.append(field.get(o));
                b.append("</li>");
            } catch (IllegalAccessException | IllegalArgumentException e) {
            }
        }
        b.append("</ul>");
        return b.toString();
    }
    
    public static String concatenateStrings(List<String> strings, String delimiter) {
        StringBuilder buffer = new StringBuilder();
        for (String s: strings) {
            buffer.append(s);
            buffer.append(delimiter);
        }
        if (buffer.length() >= delimiter.length())
            buffer.delete(buffer.length()-delimiter.length(), buffer.length());
        return buffer.toString();
    }

    private static final String preferencesName = "FleterosApp";
    
    public static void setPreferences(Context context, String prefName, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE).edit();
        editor.putString(prefName, value).apply();
    }
    
    public static String getPreferences(Context context, String prefName) {
        SharedPreferences preferences = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE);
        String s = preferences.getString(prefName, "");
        Log.d(TAG, "getPreferences(): " + prefName + " = " + s);
        return s;
    }

    public static String getDate(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String strDate =  mdformat.format(calendar.getTime());
        return strDate;
    }
    
}
