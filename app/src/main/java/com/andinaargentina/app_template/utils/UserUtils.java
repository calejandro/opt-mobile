package com.andinaargentina.app_template.utils;

import android.content.Context;
import android.content.pm.PackageInfo;

import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.legacy.domain.Alertas;
import com.andinaargentina.legacy.domain.PermissionsItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andina on 05/06/18.
 */

public class UserUtils {

    private static UserUtils instance;

    private String currentUser = "";
    private String currentClient = "";
    private String version;
    private String versionCode;

    private String latitude;
    private String longitude;

    private int x;
    private int y;

    private Alertas alertas;

    private SysUsersDto user;
    private String token;

    private Map<String, PermissionsItem> permissionsItemMap;

    private UserUtils() {

    }

    public static UserUtils getInstance() {
        if (instance == null) {
            instance = new UserUtils();
        }
        return instance;
    }


    public String getCurrentUser() {
        return currentUser;
    }

    public String getCurrentClient() {
        return currentClient;
    }

    public void saveScrollPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return y;
    }

    public String getVersion(Context context) {

        if (version == null) {
            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                version = pInfo.versionName;
            } catch (Exception e) {

            }
        }
        return version;
    }

    public String getVersionCode(Context context) {
        if (versionCode == null) {
            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                versionCode = String.valueOf(pInfo.versionCode);
            } catch (Exception e) {
            }
        }
        return versionCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setUser(SysUsersDto user) {
        this.user = user;
        if (user != null) {
            currentUser = String.valueOf(user.getUserId());
            //currentClient = user.getMNroCliente();
            //saveUser(context);
        }
    }

    public SysUsersDto getUser() {
        return user;
    }

    public String getToken() {
        return token;
}

    public void setToken(String token) {
        this.token = token;
    }

    public PermissionsItem getPermissionByResource(String resourceName) {
        if (permissionsItemMap == null) {
            loadPermissionsMap();
        }
        return permissionsItemMap.get(resourceName);
    }

    public boolean TienePermiso(String pResourceName)
    {
        // Mostrar / esconder menu items .::. EJEMPLO PERMISOS
        PermissionsItem item = UserUtils.getInstance().getPermissionByResource("Mensajería Directa");
        boolean permiso = (item != null && "y".equals(item.getMIsEnabled()));
        return permiso;
    }

    private void loadPermissionsMap() {
        permissionsItemMap = new HashMap<>();
        /*List<PermissionsItem> items = user.getPermissionsItems();
        for (PermissionsItem item : items) {
            permissionsItemMap.put(item.getMResourceName(), item);
        }*/
    }

    public void setAlertas(Alertas alertas) {
        this.alertas = alertas;
    }

    public Alertas getAlertas() {
        return alertas;
    }

}
