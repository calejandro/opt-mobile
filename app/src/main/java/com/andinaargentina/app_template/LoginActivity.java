package com.andinaargentina.app_template;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.app_template.client.ApiClientService;
import com.andinaargentina.app_template.client.ClientService;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.oauth2.AuthStateManager;
import com.andinaargentina.core_services.azure.RegistrationIntentService;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.app_template.utils.UserUtils;
import com.andinaargentina.app_template.utils.Utils;
import com.andinaargentina.sgi.android.feature_lineselection.monolitic.LineSelectorActivity;
import com.andinaargentina.sgi.core_sdk.base.times.UtilTimes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.pixplicity.easyprefs.library.Prefs;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ResponseTypeValues;
import net.openid.appauth.TokenResponse;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.sgi.android.feature_splash.view.SplashActivity.PLAY_SERVICES_RESOLUTION_REQUEST;

/**
 * Created by Andina on 05/06/18.
 */
@Route(path = "/legacy/login")
public class LoginActivity extends AppCompatActivity {

    private TextView username;
    private TextView password;
    private TextView message;
    private TextView version;
    private AppCompatCheckBox remenberChk;
    private ImageView imgEyePassword;
    private ProgressDialog progress;
    private AuthStateManager authStateManager;
    private AuthorizationService authService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        progress = new ProgressDialog(LoginActivity.this);
        progress.setMessage(getString(com.andinaargentina.sgi.android.feature_home.R.string.loading));
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

        setContentView(R.layout.login_activity_layout);
        OptimusServices.getInstance(BuildConfig.API_V2_URL);
        this.authService = new AuthorizationService(this);
        this.authStateManager = AuthStateManager.getInstance(this);

        username = (TextView) findViewById(R.id.login_username);
        password = (TextView) findViewById(R.id.login_password);
        remenberChk = (AppCompatCheckBox) findViewById(R.id.remember_check);

        message = (TextView) findViewById(R.id.login_mensaje);
        version = (TextView) findViewById(R.id.login_version);
        imgEyePassword = findViewById(R.id.imgEyePassword);
        password.setActivated(false);
        imgEyePassword.setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.color_gray_bg), android.graphics.PorterDuff.Mode.MULTIPLY);
        imgEyePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!password.isActivated()){
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    imgEyePassword.setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.color_bg_alert_red), android.graphics.PorterDuff.Mode.MULTIPLY);
                    password.setActivated(true);
                }else{
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    imgEyePassword.setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.color_gray_bg), android.graphics.PorterDuff.Mode.MULTIPLY);
                    password.setActivated(false);
                }


            }
        });
        version.setText("V." + UserUtils.getInstance().getVersion(LoginActivity.this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!Utils.getPreferences(this, Utils.USERNAME).isEmpty()){
            username.setText(Utils.getPreferences(this, Utils.USERNAME));
            password.setText(Utils.getPreferences(this, Utils.PASSWORD));
            remenberChk.setChecked(true);
        }else{
            username.setText("");
            password.setText("");
            remenberChk.setChecked(false);
        }
    }

    public void login(View view) {

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        message.setText("");
        progress = new ProgressDialog(LoginActivity.this);
        progress.setMessage(getString(com.andinaargentina.sgi.android.feature_home.R.string.loading));
        progress.show();

        String user = username.getText().toString();
        String pass = password.getText().toString();

        this.oldLogin();

        //setAlarm();

    }

    private void oldLogin() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String imei = "";
        imei = Utils.generateRandomIdentifier();
        sharedPreferences.edit().putString(imei, "imei");
        OptimusServices.getInstance().getSysUsersService().me(new String[]{"groups"}).enqueue(new Callback<SysUsersDto>() {
            @Override
            public void onResponse(Call<SysUsersDto> call, Response<SysUsersDto> response) {
                progress.hide();
                if(response.errorBody() != null) {
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Mensaje")
                            .setMessage("No tiene permisos para acceder a esta aplicación, por favor contáctese con el administrador")
                            .setPositiveButton("Aceptar", null)
                            .show();
                } else {
                    saveLogin(LoginActivity.this, response.body());

                    Intent intent = new Intent(LoginActivity.this, LineSelectorActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void onFailure(Call<SysUsersDto> call, Throwable t) {
                progress.hide();
            }
        });

        /*ApiClientService service = retrofit.create(ApiClientService.class);
        Prefs.putString("username", "DarwoftAdmin");
        Prefs.putString("password", "Darwoft1234");
        Call<User> call = service.login("DarwoftAdmin", "Darwoft1234", imei, ApiClientService.codigoSubSistema);

        try {

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    if(progress != null) {
                        progress.dismiss();
                    }

                    String msg = "";
                    try {

                        if (response.body() == null) {
                            msg = response.errorBody().string();
                        }

                        msg = response.body().getMMensajeLogin();
                        if (msg.equals("EXITO")) {
                            saveLogin(LoginActivity.this, response.body());
                            Utils.setPreferences(LoginActivity.this, Utils.USERNAME, remenberChk.isChecked()? username.getText().toString() : "");
                            Utils.setPreferences(LoginActivity.this, Utils.PASSWORD, remenberChk.isChecked()? password.getText().toString() : "");
                            Intent intent = new Intent(LoginActivity.this, LineSelectorActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (msg.equals("Debe cambiar el password.")) {
                            saveLogin(LoginActivity.this, response.body());
                            //UserUtils.getInstance().setUser(response.body());

                            Intent intent = new Intent(LoginActivity.this, ChangePassActivity.class);
                            intent.putExtra("username",username.getText().toString());
                            intent.putExtra("oldPassword",password.getText().toString());
                            startActivity(intent);
                            finish();

                        } else {
                            try {
                                int id = getResources().getIdentifier(msg, "string", getPackageName());
                                msg = getString(id);
                            } catch (Exception e) {
                            }
                            message.setText(msg);
                        }
                    } catch (Exception ex) {
                        message.setText(msg);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    progress.dismiss();
                    try {
                        if (t instanceof UnknownHostException ||
                                t instanceof SocketTimeoutException ||
                                t instanceof ConnectException) {
                            showMessage(getString(R.string.error_no_wifi));
                        } else {
                            showMessage(getString(R.string.error_network_generic));
                        }
                    } catch (Exception ex) {
                        showMessage(getString(R.string.error_network_generic));
                    }
                }
            });

        } catch (Exception ex) {
        }*/
    }

    public void cognitoLogin(View view) {

        AuthorizationRequest.Builder authRequestBuilder =
                new AuthorizationRequest.Builder(
                        authStateManager.getCurrent().getAuthorizationServiceConfiguration(), // the authorization service configuration
                        BuildConfig.COGNITO_CLIENT_ID, // the client ID, typically pre-registered and static
                        ResponseTypeValues.CODE, // the response_type value: we want a code
                        Uri.parse(BuildConfig.COGNITO_CALLBACK_URI));

        AuthorizationRequest authRequest = authRequestBuilder
                .setScope("openid email")
                .build();

        Intent authIntent = authService.getAuthorizationRequestIntent(authRequest);
        startActivityForResult(authIntent, 20000);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 20000) {
            AuthorizationResponse resp = AuthorizationResponse.fromIntent(data);
            AuthorizationException ex = AuthorizationException.fromIntent(data);
            if(ex == null) {
                this.progress.show();
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("client_secret", BuildConfig.COGNITO_CLIENT_SECRET);
                authService.performTokenRequest(
                        resp.createTokenExchangeRequest(param),
                        new AuthorizationService.TokenResponseCallback() {
                            @Override public void onTokenRequestCompleted(
                                    TokenResponse resp, AuthorizationException ex) {
                                if (resp != null) {
                                    authStateManager.updateAfterTokenResponse(resp,ex);
                                    authStateManager.replace(authStateManager.getCurrent());
                                    oldLogin();
                                } else {
                                    // authorization failed, check ex for more details
                                }
                            }
                        });
            }
        }
    }

    public void resetPassword(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApiClientService.changePassUrl));
        startActivity(browserIntent);
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
    
    public static void saveLogin(FragmentActivity context, SysUsersDto user) {
        Paper.book("SGI-BOOK").write(USER_LOGGED,user);
        //Pedimos le FCM-ID
        /*if (checkPlayServices(context)) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(context, RegistrationIntentService.class);
            context.startService(intent);
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences( context);
            UserUtils.getInstance().setUser(user);
            String token = user.getMToken();
            UserUtils.getInstance().setToken(token);
        
            SharedPreferences prefs = context.getSharedPreferences(
                    context.getText(R.string.shared_pref_name).toString(), Context.MODE_PRIVATE);
            prefs.edit()
                    .putString("token", token)
                    .putInt("user_id", user.getMUserId())
                    .apply();

            //if it's another user
            String userId = user.getMUserId().toString();

        sharedPreferences.edit().putString("config.lastLogin", Utils.toDisplayDateTime(new Date()));
            if (sharedPreferences.getString("config.lastUser",null) == null || !sharedPreferences.getString("config.lastUser",null).equals(userId)) {
                sharedPreferences.edit().putString("config.lastUser", userId);
                sharedPreferences.edit().putString("config.date", null);
                sharedPreferences.edit().putString("config.sincro", null);
            }
        */
        UtilTimes.setAlarmForChangeTurn(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        finish();
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog box that enables  users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    private static boolean checkPlayServices(FragmentActivity context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("splash", "This device is not supported by Google Play Services.");
                context.finish();
            }
            return false;
        }
        return true;
    }
}
