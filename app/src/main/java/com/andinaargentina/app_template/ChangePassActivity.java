package com.andinaargentina.app_template;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.andinaargentina.app_template.fragments.ChangePassFragment;

/**
 * Created by Andina on 05/06/18.
 */
public class ChangePassActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_pass_layout);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.layout_content, new ChangePassFragment(), ChangePassFragment.TAG)
                .commit();

    }
}
