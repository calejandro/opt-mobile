package com.andinaargentina.app_template.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.andinaargentina.legacy.domain.Config;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.Map;

/**
 * Created by Andina on 05/06/18.
 */

public class DatabaseHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "app_template.sqlite";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {

        }

        if (oldVersion < 3) {

        }
    }

    public Config getConfig() {
        Config config = new Config();
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("configuracion");

        Cursor c = qb.query(db, null, null, null, null, null, null);
        c.moveToPosition(-1);

        while (c.moveToNext()) {
            config.put(c.getString(0), c.getString(1));
        }

        db.close();

        return config;
    }

    public void saveConfig(Config config) {

        SQLiteDatabase db = getWritableDatabase();
        String sql = "INSERT OR REPLACE INTO configuracion ([key],value) VALUES (?,?);";
        String[] values = new String[2];
        for (Map.Entry<String, String> entry : config.entrySet()) {
            values[0] = entry.getKey();
            values[1] = entry.getValue();
            db.execSQL(sql, values);
        }
        db.close();

    }


}
