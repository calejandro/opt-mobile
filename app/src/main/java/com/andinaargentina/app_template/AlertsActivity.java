package com.andinaargentina.app_template;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.andinaargentina.app_template.adapter.AlertsAdapter;
import com.andinaargentina.app_template.client.ApiClientService;
import com.andinaargentina.app_template.client.ClientService;
import com.andinaargentina.legacy.domain.Alerta;
import com.andinaargentina.legacy.domain.Alertas;
import com.andinaargentina.app_template.utils.UserUtils;
import com.andinaargentina.app_template.utils.Utils;

import java.net.ConnectException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@RequiresApi(api = Build.VERSION_CODES.N)
public class AlertsActivity extends AppCompatActivity implements AlertsAdapter.ClickListener{
    private RecyclerView rv_alerts;
    private TextView tv_message;
    private AlertsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mRefresh;
    private ProgressBar pb_loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerts);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_alert);
        toolbar.setTitle("ALERTAS");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        tv_message=(TextView) findViewById(R.id.tv_message);
        rv_alerts = (RecyclerView) findViewById(R.id.rv_alerts);
        mRefresh=(SwipeRefreshLayout)findViewById(R.id.sr_refresh);
        mRefresh.setColorSchemeColors(getResources().getColor(R.color.colorDarkRed));
        pb_loading=(ProgressBar)findViewById(R.id.pb_loading);

        rv_alerts.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        rv_alerts.setLayoutManager(mLayoutManager);

        mAdapter = new AlertsAdapter(this,this);
        rv_alerts.setAdapter(mAdapter);
        setActions();

        loadAlerts();
    }

    private void loadAlerts(){
        showLoading(true);
        int userId = UserUtils.getInstance().getUser().getUserId();
        /*String token = UserUtils.getInstance().getUser().getMToken();

        Retrofit retrofit = ClientService.getRetro();
        ApiClientService service = retrofit.create(ApiClientService.class);
        Call<Alertas> call = service.AlertasYAvisosPortada(userId, token);
        try{
            call.enqueue(new Callback<Alertas>() {
                @Override
                public void onResponse(Call<Alertas> call, Response<Alertas> response) {
                    try {
                        showLoading(false);
                        if (response.body().getMensaje().equalsIgnoreCase("exito")) {

                            UserUtils.getInstance().setAlertas(response.body());
                            ArrayList<Alerta> list=response.body().getAlertas();
                            if(!list.isEmpty()) {
                                mAdapter.setData(list);
                                rv_alerts.setVisibility(View.VISIBLE);
                                tv_message.setVisibility(View.GONE);
                            }
                            else{
                                setMessage(getResources().getString(R.string.get_alertas_empty));
                            }

                        } else {
                            setMessage(getResources().getString(R.string.get_alertas_error));

                        }
                    } catch (Exception ex) {
                        setMessage(getResources().getString(R.string.get_alertas_error));

                    }
                }

                @Override
                public void onFailure(Call<Alertas> call, Throwable t) {
                    try {
                        showLoading(false);
                        if(t instanceof ConnectException){
                            setMessage(getResources().getString(R.string.error_no_wifi));
                        }else{
                            setMessage(getResources().getString(R.string.error_network_generic));
                        }
                    } catch (Exception ex) {
                        setMessage(getResources().getString(R.string.error_cargar_datos));
                    }
                }
            });
        }catch(Exception e){

        }*/

    }

    private void setMessage(String message){
        tv_message.setText(message);
        rv_alerts.setVisibility(View.GONE);
        tv_message.setVisibility(View.VISIBLE);
    }

    private void showLoading(boolean show){
        if(show){
            pb_loading.setVisibility(View.VISIBLE);
            rv_alerts.setVisibility(View.GONE);
            tv_message.setVisibility(View.GONE);
        }else{
            mRefresh.setRefreshing(false);
            pb_loading.setVisibility(View.GONE);
            rv_alerts.setVisibility(View.VISIBLE);
            tv_message.setVisibility(View.VISIBLE);
        }
    }

    private void setActions(){
        final Context mContext=this;
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,  ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
                //awesome code to run when user drops card and completes reorder
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.RIGHT) {
                    int position=viewHolder.getAdapterPosition();
                    Alerta alerta=mAdapter.getData().get(position);
                    marcarComoBorrada(alerta.getIdAlert());

                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                        // Get RecyclerView item from the ViewHolder
                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 3;
                        Paint p = new Paint();
                        if (dX > 0) {
                        /* Set your color for positive displacement */
                            p.setColor(mContext.getResources().getColor(R.color.colorDarkRed));
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                            c.drawRect(background,p);
                            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white_24dp);
                            RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                            // Draw Rect with varying right side, equal to displacement dX
                            c.drawBitmap(icon, null, icon_dest, p);
//                            c.drawRect((float) itemView.getLeft(), (float) itemView.getTop(), dX,
//                                    (float) itemView.getBottom(), p);
                        } else {
                            /* Set your color for negative displacement */

                            // Draw Rect with varying left side, equal to the item's right side plus negative displacement dX
//                            c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
//                                    (float) itemView.getRight(), (float) itemView.getBottom(), p);
                        }

                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                    }

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rv_alerts);

        mRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                       loadAlerts();
                    }
                }
        );
    }

    @Override
    public void onClick(Alerta alerta) {

        marcarComoLeida(alerta.getIdAlert());
    }

    public void mostrarMensaje(Alerta alerta){
        TextView msg = new TextView(this);
        msg.setPadding(16,5,8,5);
        msg.setText(Html.fromHtml(alerta.getMessage()));

        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(alerta.getSubject());
        ab.setView(msg);
        ab.setCancelable(true);
        ab.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ab.show();
    }

    private void marcarComoLeida(final long idAlerta){
        final Context mContext=this;
        Retrofit retrofit = ClientService.getRetro();
        ApiClientService service = retrofit.create(ApiClientService.class);
        Call<Alertas> call = service.MarkAsRead(String.valueOf(idAlerta));
        try{
            call.enqueue(new Callback<Alertas>() {
                @Override
                public void onResponse(Call<Alertas> call, Response<Alertas> response) {
                    try {
                        if (response.body().getMensaje().equalsIgnoreCase("exito")) {

                            ArrayList<Alerta> list=UserUtils.getInstance().getAlertas().getAlertas();
                            for(Alerta alerta : list){
                                if(alerta.getIdAlert()== idAlerta){
                                    alerta.setFechaLeida(Utils.getDate());
                                    mostrarMensaje(alerta);
                                    break;
                                }
                            }
                            mAdapter.setData(list);

                        } else {
                            Utils.showMessage(mContext,getResources().getString(R.string.set_alertas_read));
                            mAdapter.notifyData();

                        }
                    } catch (Exception ex) {
                        Utils.showMessage(mContext,getResources().getString(R.string.set_alertas_read));
                        mAdapter.notifyData();
                    }
                }

                @Override
                public void onFailure(Call<Alertas> call, Throwable t) {
                    mAdapter.notifyData();
                    try {

                        if(t instanceof ConnectException){
                            Utils.showMessage(mContext,getResources().getString(R.string.error_no_wifi));
                        }else{
                            Utils.showMessage(mContext,getResources().getString(R.string.error_network_generic));
                        }
                    } catch (Exception ex) {
                        Utils.showMessage(mContext,getResources().getString(R.string.error_cargar_datos));
                    }
                }
            });
        }catch(Exception e){

        }
    }

    private void marcarComoBorrada(final long idAlerta){
        final Context mContext=this;
        Retrofit retrofit = ClientService.getRetro();
        ApiClientService service = retrofit.create(ApiClientService.class);
        Call<Alertas> call = service.MarkAsDeleted(String.valueOf(idAlerta));
        try{
            call.enqueue(new Callback<Alertas>() {
                @Override
                public void onResponse(Call<Alertas> call, Response<Alertas> response) {
                    try {
                        if (response.body().getMensaje().equalsIgnoreCase("exito")) {

                            ArrayList<Alerta> list=UserUtils.getInstance().getAlertas().getAlertas();
                            int posicion=0;
                            for(Alerta alerta : list){
                                if(alerta.getIdAlert()== idAlerta){
                                    break;
                                }
                                posicion++;
                            }
                            list.remove(posicion);
                            mAdapter.setData(list);

                        } else {
                            Utils.showMessage(mContext,getResources().getString(R.string.set_alertas_delete));
                            mAdapter.notifyData();
                        }
                    } catch (Exception ex) {
                        mAdapter.notifyData();
                        Utils.showMessage(mContext,getResources().getString(R.string.set_alertas_delete));

                    }
                }

                @Override
                public void onFailure(Call<Alertas> call, Throwable t) {
                    mAdapter.notifyData();
                    try {

                        if(t instanceof ConnectException){
                            Utils.showMessage(mContext,getResources().getString(R.string.error_no_wifi));
                        }else{
                            Utils.showMessage(mContext,getResources().getString(R.string.error_network_generic));
                        }
                    } catch (Exception ex) {
                        Utils.showMessage(mContext,getResources().getString(R.string.error_cargar_datos));
                    }
                }
            });
        }catch(Exception e){

        }
    }

    private void volver(){
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId=item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                volver();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        volver();
    }

}
