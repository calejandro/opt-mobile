package com.andinaargentina.app_template.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.legacy.domain.Alerta;
import com.andinaargentina.app_template.utils.Utils;
import com.andinaargentina.app_template.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Andina on 05/06/2018.
 */

public class AlertsAdapter extends RecyclerView.Adapter<AlertsAdapter.AlertsViewHolder> {

    private ArrayList<Alerta> mDataset;
    private Context mContext;
    private ClickListener listener;


    public interface ClickListener{
        public void onClick(Alerta alerta);
    }

    public AlertsAdapter(Context mContext, ClickListener listener) {
        this.mContext=mContext;
        mDataset=new ArrayList<>();
        this.listener=listener;
    }

    @Override
    public AlertsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        int layoutIdForListItem = R.layout.rv_item_alerta;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        AlertsViewHolder viewHolder = new AlertsViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AlertsViewHolder holder, int position) {

        final Alerta mAlerta= mDataset.get(position);

        holder.bind(mAlerta);
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void cleanList(){
        mDataset=new ArrayList<>();
        notifyDataSetChanged();
    }


    public void setData(ArrayList<Alerta> list){
        cleanList();
        mDataset.addAll(list);
        notifyDataSetChanged();
    }

    public void notifyData(){
        notifyDataSetChanged();
    }

    public ArrayList<Alerta> getData(){
        return mDataset;
    }
    class AlertsViewHolder extends RecyclerView.ViewHolder {
        long idAlerta;
        TextView mTv_titulo;
        TextView mTv_date;

        View itemView;

        public AlertsViewHolder(View itemView) {
            super(itemView);

            this.itemView=itemView;
            mTv_titulo = (TextView) itemView.findViewById(R.id.tv_title);
            mTv_date= (TextView) itemView.findViewById(R.id.tv_date);
        }

        void bind(final Alerta alerta) {
            idAlerta=alerta.getIdAlert();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date=Utils.formatFecha(alerta.getStartDate(),format);

            mTv_date.setText(Utils.fromDateToString(date,"dd-MM-yyyy HH:mm:ss"));
            mTv_titulo.setText(alerta.getSubject());

            if(alerta.getFechaLeida()!=null){
                itemView.setBackgroundColor(mContext.getResources().getColor(R.color.colorLightGrey));
                mTv_titulo.setTypeface(mTv_titulo.getTypeface(), Typeface.NORMAL);
            }else{
                itemView.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
                mTv_titulo.setTypeface(mTv_titulo.getTypeface(), Typeface.BOLD);
            }
            //itemView.setBackgroundColor();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(alerta);
                }
            });


        }

    }
}
