package com.andinaargentina.app_template.application

import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.net.Uri
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.alibaba.android.arouter.launcher.ARouter
import com.andinaargentina.app_template.BuildConfig
import com.andinaargentina.app_template.R
import com.andinaargentina.core_services.providers.apiv2.oauth2.AuthStateManager
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices
import com.facebook.stetho.Stetho
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.AppCenter.start
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.microsoft.appcenter.distribute.Distribute
import com.microsoft.appcenter.distribute.UpdateTrack
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger.addLogAdapter
import com.pixplicity.easyprefs.library.Prefs
import io.paperdb.Paper
import net.openid.appauth.AuthState
import net.openid.appauth.AuthorizationServiceConfiguration
import uk.co.chrisjenx.calligraphy.CalligraphyConfig


class MainApplication : MultiDexApplication() {

    private var context: Context? = null

    var serviceConfig = AuthorizationServiceConfiguration(
            Uri.parse("https://koandina.auth.us-east-1.amazoncognito.com/oauth2/authorize"),  // authorization endpoint
            Uri.parse("https://koandina.auth.us-east-1.amazoncognito.com/oauth2/token")) // token endpoint


    override fun onCreate() {
        super.onCreate()
        context = this
        OptimusServices.getInstance(BuildConfig.API_V2_URL)
        if (!AuthStateManager.getInstance(this).existState()) {
            AuthStateManager.getInstance().replace(AuthState(serviceConfig))
        }
        initLogger()
        initConfigRouter()
        Paper.init(this)
        Stetho.initializeWithDefaults(this)
        initAppCenterServices(this)
        // Initialize the Prefs class
        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()

    }

    private fun initAppCenterServices(application: Application) {
        AppCenter.setLogLevel(Log.VERBOSE);
        Distribute.setUpdateTrack(UpdateTrack.PUBLIC)
        start(application, BuildConfig.APP_CENTER_KEY,
                Analytics::class.java, Crashes::class.java)
        Distribute.setEnabled(false)
        Distribute.setEnabledForDebuggableBuild(false)
        Distribute.checkForUpdate()
    }

    private fun initLogger() {
        addLogAdapter(object : AndroidLogAdapter() {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    fun getContext(): Context? {
        return context
    }


    private fun initConfigRouter() {
        if (isDebug()) {           // These two lines must be written before init, otherwise these configurations will be invalid in the init process
            ARouter.openLog();     // Print log
            //TODO exe: Configurar el loger para que el router muestre los logs con logger tmb :)
            ARouter.openDebug();   // Turn on debugging mode (If you are running in InstantRun mode, you must turn on debug mode! Online version needs to be closed, otherwise there is a security risk)
        }
        ARouter.init(this); // As early as possible, it is recommended to initialize in the Application
    }

    private fun isDebug() = BuildConfig.DEBUG

    private fun initConfigCalligraphy() {
        val calligraphy = CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/OpenSansLight.ttf").setFontAttrId(R.attr.fontPath).build()
        CalligraphyConfig.initDefault(calligraphy)
    }

}