package com.andinaargentina.sgi.android.feature_splash.uitesting.screens;

import com.kheera.robot.BaseRobot;
import com.pacoworks.rxpaper2.RxPaperBook;


public class SplashRobot extends BaseRobot {

    public SplashRobot(){
        super();
        RxPaperBook.init(getContext());
    }

    public static void enterEmail(String email) {
        //onView(withId(R.id.splash_txt_name_version)).perform(typeText(email));
    }

    public static void enterPassword(String password) {
        //onView(withId(R.id.password)).perform(typeText(password));
    }

    public static void performLogin() {
        //onView(withId(R.id.email_sign_in_button)).perform(click());
    }
}
