package com.andinaargentina.sgi.android.feature_splash.uitesting.stepdefs;

import android.content.Context;
import android.os.Build;

import androidx.test.espresso.intent.Intents;

import com.andinaargentina.sgi.android.feature_splash.uitesting.screens.SplashRobot;
import com.andinaargentina.sgi.android.feature_splash.view.SplashActivity;
import com.kheera.annotations.OnFinishTest;
import com.kheera.annotations.TestModule;
import com.kheera.annotations.TestStep;
import com.kheera.internal.BaseStepDefinition;
import com.kheera.internal.TestRunnerConfig;
import com.kheera.robot.BaseRobot;
import com.pacoworks.rxpaper2.RxPaperBook;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@TestModule(featureFile = "splash.feature")
public class SplashStepsDef extends BaseStepDefinition {

    private BaseRobot splashRobot;

    @Override
    public void onCreate(Context context, TestRunnerConfig runnerConfig) {
        super.onCreate(context, runnerConfig);
        splashRobot = new SplashRobot();
        RxPaperBook.init(context);
    }

    @OnFinishTest()
    public void onFinishTest(String featureName, String scenarioName) {
        getInstrumentation().waitForIdleSync();
        if (SplashRobot.ActivityInstance != null) {
            if (Build.VERSION.SDK_INT > 20) {
                SplashRobot.ActivityInstance.finishAndRemoveTask();
            }
        }
        Intents.release();
    }

    @TestStep("^Estoy en la vista Splash$")
    public void iAmOnTheLoginPage() throws Throwable {
        splashRobot.launchActivity(SplashActivity.class);
    }

}
