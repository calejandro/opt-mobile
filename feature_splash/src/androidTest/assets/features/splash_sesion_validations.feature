Feature: Splash Page
  Como usuario de la aplicación Mobile
  Quiero que la aplicacion me muestre una pantalla inicial de Splash
  Para indicarme que estoy accediendo a la aplicación SGI

  @ignore @TODO
  Scenario: Verifico que la versión actual de la app no es la última
    Given Estoy en la pantalla de splash
    And El dispositivo tiene conexión a Internet
    When Se verifica que la versión de la app no es la última
    Then La app debe mostrar la pantalla de actualizar app
    And Redirigirme a la playstore

  @ignore @TODO
  Scenario: Hay usuario logueado en la app
    Given Estoy en la pantalla de splash
    And Existe un usuario logeado de una sesión previa
    And La sesión está activa
    When La app valida el usario previo
    And La app realiza todas las conexiones necesarias
    Then El icono del progress no está visible
    And La app redirige a la pantalla principal

  @ignore @TODO
  Scenario: Hay usuario logueado en la app, pero perdió la sesión pasadas las 8hrs
    Given Estoy en la pantalla de splash
    And Existe un usuario logeado de una sesión previa
    But La sesión no está activa        ########## Pasaron 8horas de sesión
    When La app valida esta situación
    Then El icono del progress no está visible
    And Debo ver el dialogo "Ha perdido la sesión"
    And La app redirige a la pantalla login

  @ignore @automatic
  Scenario: No hay usuario logueado en la app
    Given Estoy en la pantalla de splash
    And No existe un usuario logeado de una sesión previa
    And El icono del progress está visible
    When La app realiza todas las conexiones necesarias
    Then El icono del progress no está visible
    And La app redirige a la pantalla login

  #Este escenario va a validar este requerimiento, pero el sistema operativo
  #puede tomar decisiones que vallan en contra de él
  #supongamos que la app estuvo en la pila por más de 5 minutos
  #y en ese tiempo el usuario usó otras apps y el teléfono se quedó
  #sin memoria, el S.O va a olvidar el estado anterior y lanzará la aplicación
  #como si fuera la primera ejecución
  @ignore @manual
  Scenario: Verifico que el splash no se visualize al reinicio de la app
    Given Estoy con la aplicación SGI - mobile en primer plano
    And Presiono el botón home del Sistema Operativo Android
    And La aplicación SGI - mobile está en la pila de apps de Android
    When Presiono el icono de la app
    Then No debo ver la pantalla de splash
    And No debo ver el progress de cargando la sesión