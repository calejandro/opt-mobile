Feature: Splash Page
  Como usuario de la aplicación Mobile
  Quiero ver una pantalla inicial de Splash
  Para indicarme que estoy accediendo a la aplicación SGI

  @ignore @manual
  Scenario: Verifico la orientación del device
    Given Estoy en la vista Splash
    And La orientación del dispositivo es Portrait-Mode
    When Giro el device a Landscape-Mode
    Then La app seguirá mostrando sus componentes en Portrait-Mode

  #Escenarios de navegabilidad
  @ignore @manual
  Scenario: Verifico que el splash se visualiza correctamente desde el menu
    Given Estoy en el menú home del Sistema Operativo Android
    And La aplicación no se encuentra en la pila de apps de Android
    When Presiono el icono de la app
    Then Veré la pantalla de splash
    And Veré el progress de cargando la sesión

  @ignore @manual @TODO
  Scenario: Verifico que el splash se visualiza correctamente desde la notificación push
    Given Estoy en el menú home del Sistema Operativo Android
    And La aplicación no se encuentra en la pila de apps de Android
    When Aparece una notificación push
    And Presiono la notificación
    Then Veré la pantalla de splash
    And Veré el progress de cargando la sesión

  #Escenarios commons de http
  @ignore @manual
  Scenario: Verifico Splash sin conexión a internet
    Given Estoy en la vista Splash
    And El dispositivo no tiene conexión a Internet
    Then Debo ver la notificación "Usted no posee internet"

  @ignore @manual
  Scenario: Verifico reconexión exitosa de Splash cuando no halla internet
    Given Estoy en la vista splash
    And El dispositivo no tiene conexión a Internet
    And La notificación "Usted no posee internet" está visible
    And Pasaron 3 segundos de espera
    When El dispositivo recupera la conexión a internet
    Then No debo ver la notificación "Usted no posee internet"
    And El icono del progress está visible

  @ignore @manual
  Scenario: Verifico Splash con problemas de respuesta del servidor
    Given Estoy en la pantalla Splash
    And Hay conexión a internet
    And Veo un dialogo de progreso
    When El servidor devuelve un error
    Then El dialogo de progreso estará invisible
    And Debo ver la notificación "Error con el servidor"

  @ignore @manual
  Scenario: Verifico splash con comunicación interrumpida
    Given Estoy en la pantalla Splash
    And Hay conexión a internet
    And Veo un dialogo de progreso
    When El dispositivo se queda sin internet
    Then El dialogo de progreso estará invisible
    And Debo ver la notificación "Error en la comunicación"

  #Escenarios base de feature
  @ignore @automatic
  Scenario: Verifico Splash exitoso
    Given Estoy en la pantalla de splash
    And El dispositivo tiene conexión a Internet
    And El dialogo de progreso está visible
    When Se realizan las comprobaciones y conexiones iniciales necesarias
    Then El dialogo de progreso está invisible
    And Debo ver un la imagen con la versión de la app

  @ignore @manual
  Scenario: Verifico interrupción de la carga del Splash por el usuario
    Given Estoy en la pantalla de splash
    And El dispositivo tiene conexión a Internet
    And El icono del progress está visible
    When El usuario sale de la pantalla Splash
    Then Los procesos iniciales deben ser interrumpidos