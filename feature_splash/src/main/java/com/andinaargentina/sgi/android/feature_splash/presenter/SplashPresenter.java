package com.andinaargentina.sgi.android.feature_splash.presenter;

import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.events.ValidateVersion.ValidateVersionErrorEvent;
import com.andinaargentina.core_services.events.ValidateVersion.ValidateVersionSuccessEvent;
import com.andinaargentina.core_services.events.internet.InternetUpdateEvent;
import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;
import com.andinaargentina.core_services.providers.api.core.util.NetworkUtils;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.sgi.android.feature_splash.router.ISplashNavigationHandler;
import com.andinaargentina.sgi.android.feature_splash.usescases.http.ISplashResponseHandler;
import com.andinaargentina.sgi.android.feature_splash.view.ISplashViewEventsHandler;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

public class SplashPresenter extends BasePresenter implements ISplashViewEventsHandler, ISplashResponseHandler {

    private ISplashRequestHandler requestHandler;
    private ISplashViewUpdateHandler viewUpdateHandler;
    private ISplashNavigationHandler router;

    public SplashPresenter(@NonNull ISplashViewUpdateHandler viewUpdateHandler,
                           @NonNull ISplashNavigationHandler router,
                           @NonNull ISplashRequestHandler requestHandler){
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        this.viewUpdateHandler = viewUpdateHandler;
        this.router = router;
        this.requestHandler = requestHandler;
    }

    private boolean existAnyUserLogged;

    @Override
    public void onStart() {
        super.onStart();
        requestHandler.saveFirstEntryToApp();
        SysUsersDto userLogged = requestHandler.getUserLogged();
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        if (user!=null){
            existAnyUserLogged = true;
        }else{
            existAnyUserLogged = false;
        }
        String versionName = requestHandler.getAppVersionName(viewUpdateHandler.getContext());
        viewUpdateHandler.showAppVersionName(versionName);
        requestHandler.initWebSocketConnection(viewUpdateHandler.getContext().getApplicationContext());
    }

    @Override
    protected void doOnDestroy() {
        super.doOnDestroy();
        unregisterObserver();
        requestHandler = null;
        viewUpdateHandler = null;
        router = null;
    }

    private void unregisterObserver() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void retryHttpValidations(@NonNull final Context context) {
        tryConnect();
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void validateVersionSuccess(ValidateVersionSuccessEvent event) {
        viewUpdateHandler.hideProgressDialog();
        viewUpdateHandler.finishView();
        String response = event.getResponse();
        if ("0".equals(response)){
            if (existAnyUserLogged){
                router.goToMainPageLikeUser1();
            }else{
                router.goToLoginPage();
            }
        }else{
            router.goToUpdateAplication();
        }
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
        public void validateVersionError(ValidateVersionErrorEvent event) {
        viewUpdateHandler.hideProgressDialog();
        viewUpdateHandler.showNotificationServerError(event.getError().isVpnError());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void internetConnectivityChange(InternetUpdateEvent event){
        Logger.d("Internet status changed, isConnected:" + event.isConnected());
        tryConnect();
    }

    private void tryConnect() {
        if (NetworkUtils.isConnected(viewUpdateHandler.getContext())){
            viewUpdateHandler.showNotificationInternetIsBack();
            viewUpdateHandler.hideProgressDialog();
            viewUpdateHandler.finishView();
            /*viewUpdateHandler.showProgressDialog();
            requestHandler.validateVersionApp(viewUpdateHandler.getContext());*/
            if (existAnyUserLogged){
                router.goToMainPageLikeUser1();
            }else{
                router.goToLoginPage();
            }
        }else{
            viewUpdateHandler.hideProgressDialog();
            viewUpdateHandler.showNotificationInternetIsGone();
        }
    }

    @NotNull
    @Override
    public IBaseViewModel getViewModel() {
        return null;
    }

    @NotNull
    @Override
    public String getViewModelName() {
        return "SplashViewModel";
    }
}
