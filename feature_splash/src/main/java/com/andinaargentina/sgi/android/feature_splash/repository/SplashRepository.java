package com.andinaargentina.sgi.android.feature_splash.repository;

import android.content.Context;

import com.andinaargentina.core_services.events.ValidateVersion.ValidateVersionErrorEvent;
import com.andinaargentina.core_services.events.ValidateVersion.ValidateVersionSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.api.clients.version.ValidateVersionService;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.local.base.CrudLocalProvider;
import com.andinaargentina.core_services.providers.local.flaglogin.FlagFirstEntryProvider;
import com.andinaargentina.core_services.providers.local.user.UserProvider;
import com.andinaargentina.sgi.android.feature_splash.usescases.http.ISplashTransactionHandler;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SplashRepository implements ISplashTransactionHandler {

    public static final String FIRST_ENTRY = "FIRST_ENTRY";
    public static final String EXIST_LOGGED_USER = "LOGGED_USER";
    private ValidateVersionService validateVersionService;
    private CompositeDisposable compositeDisposable;
    private CrudLocalProvider<Boolean> flagFirstEntryProvider;
    private CrudLocalProvider<SysUsersDto> userProvider;

    public SplashRepository(ValidateVersionService validateVersionService,
                            CompositeDisposable compositeDisposable) {
        this.validateVersionService = validateVersionService;
        this.compositeDisposable = compositeDisposable;
        this.flagFirstEntryProvider = new FlagFirstEntryProvider();
        this.userProvider = new UserProvider();
    }


    @Override
    public void validateVersionApp(String versionCode, Context context) {
        Logger.t("Exequiel").d("debuggg entro a validateVersionApp");
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = validateVersionService
                    .getValidateVersionMethod(
                            "1", versionCode, context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getValidateVersionObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void validateActiveSession(String token) {
        //TODO implementar la validación de la sesión activa
    }

    @Override
    public void initWebSocketConnection() {
        //TODO implementar la conexión con el websocket
    }

    @Override
    public SysUsersDto getUserLoggedIn() {
        return userProvider.readData(EXIST_LOGGED_USER);
    }

    @Override
    public void saveTheUserEnteredForTheFirstTime() {
        flagFirstEntryProvider.writeData(FIRST_ENTRY,true);
    }

    @Override
    public boolean isFirstEntryToTheApp() {
        return flagFirstEntryProvider.readData(FIRST_ENTRY);
    }

    private DisposableSingleObserver<String> getValidateVersionObserver(){
        return new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(String response) {
                EventBus.getDefault().post(new ValidateVersionSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                ServiceError error;
                if (e instanceof ServiceError){
                    error = (ServiceError) e;
                }else{
                    error = new ServiceError();
                }
                EventBus.getDefault().post(new ValidateVersionErrorEvent(error));
            }
        };
    }
}
