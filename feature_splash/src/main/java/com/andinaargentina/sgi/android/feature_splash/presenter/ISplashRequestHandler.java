package com.andinaargentina.sgi.android.feature_splash.presenter;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;

public interface ISplashRequestHandler {

    void validateVersionApp(Context context);

    void validateLoggedUser(Context context, String token);

    void initWebSocketConnection(Context context);

    void saveFirstEntryToApp();

    boolean isFirstEntryToApp();

    SysUsersDto getUserLogged();

    boolean deviceIsConnectedToInternet(Context context);

    String getAppVersionName(Context context);
}
