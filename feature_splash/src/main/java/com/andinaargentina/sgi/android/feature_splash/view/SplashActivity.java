package com.andinaargentina.sgi.android.feature_splash.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.azure.FirebaseService;
import com.andinaargentina.core_services.azure.RegistrationIntentService;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_splash.R;
import com.andinaargentina.sgi.android.feature_splash.presenter.ISplashViewUpdateHandler;
import com.andinaargentina.sgi.android.feature_splash.presenter.SplashPresenter;
import com.andinaargentina.sgi.android.feature_splash.router.SplashRouter;
import com.andinaargentina.sgi.android.feature_splash.usescases.http.SplashInteractor;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter;
import com.andinaargentina.sgi.core_sdk.base.components.view.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;


import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

@Route(path = ViewsNames.ROUTE_VIEW_NAME_SPLASH)
public class SplashActivity extends BaseActivity implements ISplashViewUpdateHandler {

    private TextView txtVersionName;
    private SplashPresenter eventsHandler;
    private View loaderView;

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        eventsHandler = new SplashPresenter(this, new SplashRouter(), new SplashInteractor());
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        registerWithNotificationHubs();
        FirebaseService.createChannelAndHandleNotifications(getApplicationContext());
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void initViews(){
        super.initViews();
        txtVersionName = findViewById(R.id.splash_txt_name_version);
        loaderView = findViewById(R.id.splash_loader);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.splash_layout;
    }

    @NonNull
    @Override
    public String getTagName() {
        return getString(R.string.name_view);
    }

    @Override
    public void showNotificationInternetIsGone() {
        Logger.t("view").d("showNotificationInternetIsGone");
        this.showSnackBar(getString(R.string.sin_internet));
    }

    @Override
    public void showNotificationInternetIsBack() {
        Logger.t("view").d("showNotificationInternetIsBack");
        this.hideSnackBar();
    }

    @Override
    public void showNotificationServerError(boolean isVpnError) {
        String errorMessage = getString(R.string.server_error);
        if (isVpnError){
            errorMessage = getString(R.string.vpn_error);
        }
        this.showSnackBar(errorMessage,
                getString(R.string.retry_connection), () -> {
                    eventsHandler.retryHttpValidations(SplashActivity.this);
                    return null;
                });
    }

    @Override
    public void showNotificationConectionLost() {
        this.showSnackBar(getString(R.string.connection_lost),getString(R.string.retry_connection), () -> {
            eventsHandler.retryHttpValidations(SplashActivity.this);
            return null;
        });
    }

    @Override
    public void hideProgressDialog() {
        this.loaderView.setVisibility(View.GONE);
    }

    @Override
    public void showProgressDialog() {
        this.loaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showAppVersionName(String appName) {
        this.txtVersionName.setText(appName);
    }

    @Override
    public void finishView() {
        this.finish();
    }

    @Override
    public Context getContext() {
        return getApplication().getApplicationContext();
    }

    @NotNull
    @Override
    public BasePresenter getBasePresenter() {
        return eventsHandler;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventsHandler = null;
    }

    public void registerWithNotificationHubs()
    {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        if (checkPlayServices() && user!=null) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog box that enables  users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("splash", "This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }


}
