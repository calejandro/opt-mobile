package com.andinaargentina.sgi.android.feature_splash.view;

import android.content.Context;

public interface ISplashViewEventsHandler {

    void retryHttpValidations(Context context);
}
