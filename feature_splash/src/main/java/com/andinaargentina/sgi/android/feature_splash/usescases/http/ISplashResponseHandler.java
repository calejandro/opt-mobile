package com.andinaargentina.sgi.android.feature_splash.usescases.http;

import com.andinaargentina.core_services.events.ValidateVersion.ValidateVersionErrorEvent;
import com.andinaargentina.core_services.events.ValidateVersion.ValidateVersionSuccessEvent;

public interface ISplashResponseHandler {

    void validateVersionSuccess(ValidateVersionSuccessEvent event);

    void validateVersionError(ValidateVersionErrorEvent event);
}
