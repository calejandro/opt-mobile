package com.andinaargentina.sgi.android.feature_splash.usescases.http;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;

public interface ISplashTransactionHandler {

    /**
     *
     * @param versionCode
     * @param context
     */
    void validateVersionApp(String versionCode, Context context);

    /**
     * Si es que hay un usuario logueado, validamos
     * si su sesión se encuentr activa.
     * @param token es el dato que le corresponde
     *              al usuario logueado actualmente.
     */
    void validateActiveSession(String token);

    /**
     * Inicializamos la conexión del
     * web socket
     */
    void initWebSocketConnection();

    /**
     * Verificamos si el usuario
     * ya ha entrado previamente
     * a la aplicación.
     * @return true o false
     */
    boolean isFirstEntryToTheApp();

    /**
     * El token para validar la sesión del
     * usuario está dentro del objeto User.
     * @return Si existe algún usuario logueado
     * devolveremos dicho usuario, si no existe
     * devolveremos null.
     */
    SysUsersDto getUserLoggedIn();

    //Write methods

    /**
     * Guardo la bandera
     * que me indica que el user ya vio
     * el splash por primera vez
     */
    void saveTheUserEnteredForTheFirstTime();

}
