package com.andinaargentina.sgi.android.feature_splash.presenter;

import android.content.Context;

public interface ISplashViewUpdateHandler {

    void showNotificationInternetIsGone();

    void showNotificationInternetIsBack();

    void showNotificationServerError(boolean isVpnError);

    void showNotificationConectionLost();

    void hideProgressDialog();

    void showProgressDialog();

    void showAppVersionName(String nameVersion);

    void finishView();

    Context getContext();

}
