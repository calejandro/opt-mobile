package com.andinaargentina.sgi.android.feature_splash.router;

public interface ISplashNavigationHandler {

    void goToLoginPage();

    void goToUpdateAplication();

    //Perfiles de usuarios diferentes, entradas diferentes
    void goToMainPageLikeUser1();

    void goToMainPageLikeUser2();
}
