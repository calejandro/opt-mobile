package com.andinaargentina.sgi.android.feature_splash.usescases.http;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.andinaargentina.core_services.providers.api.clients.login.dto.User;
import com.andinaargentina.core_services.providers.api.clients.version.ValidateVersionService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.api.core.util.NetworkUtils;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.sgi.android.feature_splash.presenter.ISplashRequestHandler;
import com.andinaargentina.sgi.android.feature_splash.repository.SplashRepository;
import com.andinaargentina.signalr.SignalRService;

import io.paperdb.Paper;
import io.reactivex.disposables.CompositeDisposable;

import static com.andinaargentina.RepositoryConst.MODO_DE_FALLO;

public class SplashInteractor implements ISplashRequestHandler {

    private SplashRepository splashRepository;

    public SplashInteractor(){
        splashRepository = new SplashRepository(
                new ValidateVersionService(ApiRestModule.providesApiService(true)),
                new CompositeDisposable());
    }

    @Override
    public void validateVersionApp(Context context) {
        /*try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String versionCode;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                versionCode = String.valueOf(pInfo.getLongVersionCode());
            }else{
                versionCode = String.valueOf(pInfo.versionCode);
            }
            splashRepository.validateVersionApp(versionCode, context);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void validateLoggedUser(Context context, String token) {
        splashRepository.validateActiveSession("someToken");
    }

    @Override
    public void initWebSocketConnection(Context context) {
        context.startService(new Intent(context, SignalRService.class));
    }

    @Override
    public void saveFirstEntryToApp() {
        splashRepository.saveTheUserEnteredForTheFirstTime();
    }

    @Override
    public boolean isFirstEntryToApp() {
        return splashRepository.isFirstEntryToTheApp();
    }

    @Override
    public SysUsersDto getUserLogged() {
        return splashRepository.getUserLoggedIn();
    }

    @Override
    public boolean deviceIsConnectedToInternet(Context context) {
        return NetworkUtils.isOnline(context);
    }

    @Override
    public String getAppVersionName(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return "V "+pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "Name version not found";
        }
    }
}
