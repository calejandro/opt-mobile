package com.andinaargentina.sgi.android.feature_splash.router;

import android.content.Intent;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.router.ViewsNames;

import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.LINES_SELECTED;

public class SplashRouter implements ISplashNavigationHandler{

    @Override
    public void goToLoginPage() {
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .navigation();
    }

    @Override
    public void goToUpdateAplication() {
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_UPDATE).navigation();
    }

    @Override
    public void goToMainPageLikeUser1() {
        List<LineasProduccionDto> linesSelected = Paper.book("SGI-BOOK").read(LINES_SELECTED);
        if (linesSelected != null && !linesSelected.isEmpty()){
            ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_HOME)
                    .navigation();
        }else{
            ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LINE_SELECTOR)
                    .navigation();
        }
    }

    @Override
    public void goToMainPageLikeUser2() {

    }
}
