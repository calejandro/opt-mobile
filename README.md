# SGI Android Mobile

Este es el cliente Android Mobile que dará soporte a SGI COCA-COLA ANDINA

###Development environment
[![Azure-build-dev](https://build.appcenter.ms/v0.1/apps/90d92950-39e9-4f6f-b2ff-178b80cffe89/branches/dev/badge)]
(https://appcenter.ms/orgs/CocaAndina/apps/SGI-Dev/build/branches/dev)


### QA environment
[![Azure-build-qa](https://build.appcenter.ms/v0.1/apps/a2af43e2-c349-46ab-8487-e74da54b456f/branches/qa/badge)]
(https://appcenter.ms/orgs/CocaAndina/apps/SGI-QA/build/branches/qa)
### Prerequisites

Configure your environment

* Install the latest version of [Android Studio](https://developer.android.com/studio)
* Install the latest version of [GIT](https://git-scm.com/downloads)

## Getting Started

1.- Ask the administrator for your permissions to access the repository.

2.- Configure your ssh key in azure devops.

* If you don't know how to do it, check this [link for generate ssh key and setup to azure devops](https://docs.microsoft.com/en-us/azure/devops/repos/git/use-ssh-keys-to-authenticate?view=azure-devops) or this other [link]((https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent))

3.- Verify your git user (username and email)

3.- Clone this repository and import into **Android Studio**.

```bash
$ git clone git@ssh.dev.azure.com:v3/CocaAndina/SGI/SGI.Android
```

4.- Open Android Studio
5.- Go to Open --> Select your root file with name (SGI.Android) and run

* You may have to wait for Android Studio to download its dependencies, and then you can press the play button

For now that's all :)

The things that I leave below, are to be completing

----------------

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

## Configuration
### Keystores:
Create `app/keystore.gradle` with the following info:
```gradle
ext.key_alias='...'
ext.key_password='...'
ext.store_password='...'
```
And place both keystores under `app/keystores/` directory:
- `playstore.keystore`
- `stage.keystore`


## Build variants
Use the Android Studio *Build Variants* button to choose between **production** and **staging** flavors combined with debug and release build types


## Generating signed APK
From Android Studio:
1. ***Build*** menu
2. ***Generate Signed APK...***
3. Fill in the keystore information *(you only need to do this once manually and then let Android Studio remember it)*

## Maintainers
This project is mantained by:
* [Federico Ramundo](http://github.com/framundo)


## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -m 'Add some feature')
4. Run the linter (ruby lint.rb').
5. Push your branch (git push origin my-new-feature)
6. Create a new Pull Request