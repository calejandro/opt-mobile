package com.kheera.internal;

public class AutomationRunnerException extends Exception {
    public AutomationRunnerException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
