package com.kheera.internal;

import android.content.Context;

public interface StepDefinition {
    void onCreate(Context context, TestRunnerConfig runnerConfig);
}
