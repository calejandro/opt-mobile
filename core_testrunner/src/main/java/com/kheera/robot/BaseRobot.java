package com.kheera.robot;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.test.espresso.Espresso;

import static android.content.Context.KEYGUARD_SERVICE;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static java.lang.Thread.sleep;

public abstract class BaseRobot {

    public static Activity ActivityInstance;

    public void launchActivity(Class<?> cls){
        boolean mInitialTouchMode = false;
        getInstrumentation().setInTouchMode(mInitialTouchMode);

        Intent newIntent = new Intent(getInstrumentation().getTargetContext(),cls);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityInstance = getInstrumentation().startActivitySync(newIntent);

        getInstrumentation().waitForIdleSync();

        ((KeyguardManager) getInstrumentation().getContext().getSystemService(KEYGUARD_SERVICE))
                .newKeyguardLock(KEYGUARD_SERVICE).disableKeyguard();

        //turn the screen on
        ActivityInstance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ActivityInstance.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
            }
        });
    }

    public void selectViewWithTap(int viewId){
        onView(withId(viewId)).perform(click());
    }

    public void typeTextOnView(int viewId, @NonNull String text){
        onView(withId(viewId)).perform(typeText(text));
    }

    public void closeKeyboard() throws InterruptedException {
        Espresso.closeSoftKeyboard();
        sleep(100);
    }

    public void checkViewIsDisplayed(int viewId){
        onView(withId(viewId)).check(matches(isDisplayed()));
    }

    public void checkViewIsDisplayed(int viewId, @NonNull String withText){
        onView(withId(viewId)).check(matches(withText(withText)));
    }

    protected Context getContext(){
        return getInstrumentation().getContext();
    }
}
