package com.andinaargentina.ui_sdk.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.andinaargentina.ui_sdk.R;

public class ObserverModeDialog extends Dialog {
    private View view;
    private View dialog;
    private TextView tv_Cancel, tv_Ok;
    private TextView tv_title, tv_message;
    private CheckBox checkBox;
    private DialogClickHandler clickHandler;
    private String positivetext,negativetext;

    private String title,message;



    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == tv_Cancel) {
                clickHandler.negativeClick();
            } else if (v == tv_Ok) {
                clickHandler.positiveClick(checkBox.isChecked());
            }
        }
    };

    public ObserverModeDialog(Context context, String title, String message, String negativetext, String positivetext) {
        super(context, R.style.ThemeDialogCustom);
        this.title=title;
        this.message=message;
        this.positivetext=positivetext;
        this.negativetext=negativetext;
        init(context);
    }

    public void setClickHandler(DialogClickHandler clickHandler)
    {
        this.clickHandler=clickHandler;
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.dialog_confirm_observer_lines, null);
        setContentView(view);
        setCancelable(true);
        init_component();
        visible_option_menu();
    }

    private void init_component() {
        tv_Cancel = findViewById(R.id.txt_cancel);
        tv_Ok = findViewById(R.id.txt_ok);

        tv_title = findViewById(R.id.dialog_title);
        tv_message = findViewById(R.id.dialog_message);
        checkBox = findViewById(R.id.observer_mode_dialog_checkbox);

        dialog = findViewById(R.id.dialog);

        if(TextUtils.isEmpty(title))
        {
            tv_title.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(message))
        {
            tv_message.setVisibility(View.INVISIBLE);
        }
        tv_title.setText(title);
        tv_message.setText(message);

        if(!TextUtils.isEmpty(positivetext))
        {
            tv_Ok.setText(positivetext);
        }
        if(!TextUtils.isEmpty(negativetext))
        {
            tv_Cancel.setText(negativetext);
        }

        tv_Cancel.setOnClickListener(onClickListener);
        tv_Ok.setOnClickListener(onClickListener);
    }

    private void visible_option_menu() {
        show();
        dialog.setVisibility(View.VISIBLE);

    }

    public void hideMessageView()
    {
        tv_message.setVisibility(View.GONE);
    }

    public void setCustomColor(int color)
    {
        tv_Ok.setTextColor(color);
        tv_Cancel.setTextColor(color);
    }

    public void hideNegativeOption()
    {
        tv_Cancel.setVisibility(View.GONE);
    }

    @Override
    public void show() {
        super.show();
    }

    public interface DialogClickHandler
    {
         void positiveClick(boolean isChecked);

         void negativeClick();

    }
}