package com.andinaargentina.ui_sdk.components.dialog.lineorder;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.services.SapOrdenesProduccionService;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.ui_sdk.R;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.group.LineGroupViewDelegate;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.N)
public class TakeLineConfirmDialog extends Dialog {

    private TextView txtCancel, txtOk;

    private LineGroupViewDelegate[] groupView1Delegate = new LineGroupViewDelegate[3];

    private DialogTakeLineClickHandler clickHandler;
    private List<LineasProduccionUsuarioDto> listLinesWithOrders;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss");

    public TakeLineConfirmDialog(@NonNull Context context,
                                 @NonNull List<LineasProduccionUsuarioDto> listLinesWithOrders,
                                 @NonNull DialogTakeLineClickHandler clickHandler) {
        super(context, R.style.ThemeDialogCustom);
        this.listLinesWithOrders = listLinesWithOrders;
        this.clickHandler = clickHandler;
        init(context);
        registerEvent();
        checkBtnOkEnabled();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerEvent();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterEvent();
    }

    private void registerEvent() {
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    private void unregisterEvent(){
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    public void setClickHandler(DialogTakeLineClickHandler clickHandler)
    {
        this.clickHandler=clickHandler;
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_confirm_take_lines, null);
        setContentView(view);
        setCancelable(false);
        initComponent();
        visible_option_menu();
    }

    private void initComponent() {
        txtCancel = findViewById(R.id.txt_cancel);
        txtOk = findViewById(R.id.dialog_confirm_take_txt_ok);
        for (LineasProduccionUsuarioDto linea: listLinesWithOrders
        ) {
            addLine(listLinesWithOrders.indexOf(linea), linea);
        }

        txtCancel.setOnClickListener(onClickListener);
        txtOk.setOnClickListener(onClickListener);

    }

    private void addLine(int position, LineasProduccionUsuarioDto lineasProduccionUsuarioDto) {
        TextView textView = findViewById(this.getContext().getResources().getIdentifier("dialog_confirm_take_line"+(position + 1), "id", getContext().getPackageName()));
        RecyclerView recyclerView = findViewById(this.getContext().getResources().getIdentifier("dialog_confirm_take_line"+(position + 1)+"_recycler", "id", getContext().getPackageName()));
        View line = findViewById(this.getContext().getResources().getIdentifier("dialog_confirm_take_line"+(position + 1)+"_view", "id", getContext().getPackageName()));
        this.groupView1Delegate[position] = new LineGroupViewDelegate(textView,recyclerView,line,String.valueOf(lineasProduccionUsuarioDto.getIdLinea()));
        LineGroupViewDelegate lineGroupViewDelegate = this.groupView1Delegate[position];
        lineGroupViewDelegate.setVisibilityGroup(true);
        lineGroupViewDelegate.setTxtNameLine(lineasProduccionUsuarioDto.getLineaProduccion().getDescLinea());
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date1 = calendar.getTime();

        OptimusServices.getInstance().getLineasProduccionService().orders(lineasProduccionUsuarioDto.getIdLinea()
        , lineasProduccionUsuarioDto.getLineaProduccion().getIdPlanta(),
                new String[]{simpleDateFormat.format(date), simpleDateFormat.format(date1)},
                new String[]{"sapSku"})
                .enqueue(new Callback<List<SapOrdenesProduccionDto>>() {
                    @Override
                    public void onResponse(Call<List<SapOrdenesProduccionDto>> call, Response<List<SapOrdenesProduccionDto>> response) {
                        lineasProduccionUsuarioDto.setOrders(response.body());
                        if(lineasProduccionUsuarioDto.getIdOrdenSap() != null) {
                            Optional<SapOrdenesProduccionDto> orden = lineasProduccionUsuarioDto.getOrders().stream()
                                    .filter(f -> f.getIdOrdenSap() == lineasProduccionUsuarioDto.getIdOrdenSap())
                                    .findAny();
                            if(orden.isPresent()) {
                                orden.get().setChecked(true);
                            }
                        }
                        lineGroupViewDelegate.setOrderGroupData(getContext(), lineasProduccionUsuarioDto);
                    }

                    @Override
                    public void onFailure(Call<List<SapOrdenesProduccionDto>> call, Throwable t) {
                        System.out.println(t.getMessage());
                    }
                });

    }

    private void visible_option_menu() {
        show();
    }

    @Override
    public void show() {
        super.show();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == txtCancel) {
                clickHandler.negativeClick();
            } else if (v == txtOk) {
                LineAndOrderSelected selection1 = null;
                LineAndOrderSelected selection2 = null;
                LineAndOrderSelected selection3 = null;

                if (groupView1Delegate[0]!=null){
                    String idOrderSelected = groupView1Delegate[0].getIdOrderSelected();
                    String idLineSelected = groupView1Delegate[0].getIdLine();
                    selection1 = new LineAndOrderSelected(idLineSelected, idOrderSelected);
                }

                if (groupView1Delegate[1]!=null){
                    String idOrderSelected = groupView1Delegate[1].getIdOrderSelected();
                    String idLineSelected = groupView1Delegate[1].getIdLine();
                    selection2 = new LineAndOrderSelected(idLineSelected, idOrderSelected);
                }

                if (groupView1Delegate[2]!=null){
                    String idOrderSelected = groupView1Delegate[2].getIdOrderSelected();
                    String idLineSelected = groupView1Delegate[2].getIdLine();
                    selection3 = new LineAndOrderSelected(idLineSelected, idOrderSelected);
                }

                clickHandler.positiveClick(selection1, selection2,selection3);
            }
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemOrderSelected(OrderSelectedEvent event){
        Log.d("Confirm lines dialog","Se checkeo una orden");
        //Checkear si existe al menos una ordes asiganada a cada línea
        /*if (listLinesWithOrders!=null){
            for (LineWithOrderDialogModel lineWithOrder: listLinesWithOrders) {
                if (lineWithOrder.getIdLine() == event.productionLineId){
                    lineWithOrder.setHasOrderAssigned(event.isChecked);
                    lineWithOrder.setOrderAssigned(event.productionOrderId);
                }
            }
        }*/
        checkBtnOkEnabled();
    }

    private void checkBtnOkEnabled() {
        boolean allLinesHasOrderAssigned = true;
        /*for (LineWithOrderDialogModel lineWithOrder: listLinesWithOrders) {
            if (!lineWithOrder.isHasOrderAssigned()){
                allLinesHasOrderAssigned = false;
            }
        }*/
        txtOk.setEnabled(allLinesHasOrderAssigned);
    }

    public static class OrderSelectedEvent{
        private String productionLineId;
        private String productionOrderId;
        private boolean isChecked;

        public OrderSelectedEvent(String productionLineId, String productionOrderId, boolean isChecked) {
            this.productionLineId = productionLineId;
            this.productionOrderId = productionOrderId;
            this.isChecked = isChecked;
        }
    }

    public interface DialogClickHandler
    {
        void positiveClick(boolean isChecked);

        void negativeClick();

    }

    public interface DialogTakeLineClickHandler
    {
        void positiveClick(@Nullable LineAndOrderSelected line1Selected,
                           @Nullable LineAndOrderSelected line2Selected,
                           @Nullable LineAndOrderSelected line3Selected);

        void negativeClick();
    }

    public class LineAndOrderSelected{
        @NonNull
        private String lineId;
        @NonNull
        private String orderId;

        public LineAndOrderSelected(@NonNull String lineId, @NonNull String orderId) {
            this.lineId = lineId;
            this.orderId = orderId;
        }

        @NonNull
        public String getLineId() {
            return lineId;
        }

        public void setLineId(@NonNull String lineId) {
            this.lineId = lineId;
        }

        @NonNull
        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(@NonNull String orderId) {
            this.orderId = orderId;
        }
    }
}