package com.andinaargentina.ui_sdk.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class SendReportDialog extends Dialog {

    private TextView txtCancel, txtOk;
    private DialogClickHandler clickHandler;
    private int itemSelectedPosition;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == txtCancel) {
                clickHandler.negativeClick();
            } else if (v == txtOk) {
                clickHandler.positiveClick(itemSelectedPosition);
            }
        }
    };

    public SendReportDialog(@NonNull Context context, @NonNull DialogClickHandler clickHandler,
                            int itemSelectedPosition) {
        super(context, com.andinaargentina.ui_sdk.R.style.ThemeDialogCustom);
        this.clickHandler = clickHandler;
        this.itemSelectedPosition = itemSelectedPosition;
        initParentView(context);
    }

    private void initParentView(@NonNull Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(com.andinaargentina.ui_sdk.R.layout.dialog_confirm_send_report, null);
        setContentView(view);
        initViewComponents();
    }

    private void initViewComponents() {
        txtCancel = findViewById(com.andinaargentina.ui_sdk.R.id.send_report_dialog_cancel);
        txtOk = findViewById(com.andinaargentina.ui_sdk.R.id.send_report_dialog_ok);
        txtCancel.setOnClickListener(onClickListener);
        txtOk.setOnClickListener(onClickListener);
        setCancelable(false);
    }

    public interface DialogClickHandler
    {
        void positiveClick(int position);
        void negativeClick();
    }
}
