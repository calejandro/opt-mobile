package com.andinaargentina.ui_sdk.components.base.connection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;

import androidx.annotation.NonNull;

import com.andinaargentina.ui_sdk.R;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;

public class ConnectionViewDelegate implements IConnectionStatusViewUpdateHandler, ICompositeView{

    private WeakReference<Activity> view;
    private Snackbar snackbar;
    private ProgressDialog progress;

    public ConnectionViewDelegate(Activity view){
        this.view = new WeakReference<>(view);
    }

    private void showSnackBar(@NonNull String title, @NonNull String titleAction,
                              @NonNull View.OnClickListener listener){
        View parentLayout = view.get().findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout,title, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(titleAction, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v);
                if (snackbar!=null){
                    snackbar.dismiss();
                    snackbar = null;
                }
            }
        });
        snackbar.show();
    }


    @Override
    public void showNotificationInternetIsGone(View.OnClickListener listener) {
        String title = this.view.get().getResources().getString(R.string.sin_internet);
        String titleAction = this.view.get().getResources().getString(R.string.retry_connection);
        showSnackBar(title, titleAction, listener);
    }

    @Override
    public void showNotificationServerError(View.OnClickListener listener) {
        String title = this.view.get().getResources().getString(R.string.server_error);
        String titleAction = this.view.get().getResources().getString(R.string.retry_connection);
        showSnackBar(title, titleAction, listener);
    }

    @Override
    public void showNotificationConectionLost(View.OnClickListener listener) {
        String title = this.view.get().getResources().getString(R.string.connection_lost);
        String titleAction = this.view.get().getResources().getString(R.string.retry_connection);
        showSnackBar(title, titleAction, listener);
    }

    @Override
    public void showProgressDialog() {
        String message = this.view.get().getResources().getString(R.string.loading);
        progress.setMessage(message);
        progress.show();
    }

    @Override
    public void hideProgressDialog() {
        progress.dismiss();
    }

    @Override
    public void hideNotificationConnection() {
        if (snackbar!=null){
            snackbar.dismiss();
            snackbar = null;
        }
    }

    @Override
    public void onCreatedView() {
        progress = new ProgressDialog(this.view.get());
        progress.dismiss();
    }

    @Override
    public void onDestroy() {
        if (progress!=null){
            progress.dismiss();
            progress = null;
        }
        if (snackbar!=null){
            snackbar.dismiss();
            snackbar = null;
        }
        if (view!=null){
            view.clear();
            view = null;
        }
    }
}
