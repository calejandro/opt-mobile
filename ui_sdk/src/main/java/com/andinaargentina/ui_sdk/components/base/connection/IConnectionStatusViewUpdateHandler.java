package com.andinaargentina.ui_sdk.components.base.connection;

import android.view.View;

/**
 * Esta interfaz define
 * el cambio de estado de red
 */
public interface IConnectionStatusViewUpdateHandler {

    void showNotificationInternetIsGone(View.OnClickListener listener);

    void showNotificationServerError(View.OnClickListener listener);

    void showNotificationConectionLost(View.OnClickListener listener);

    void showProgressDialog();

    void hideProgressDialog();

    void hideNotificationConnection();
}
