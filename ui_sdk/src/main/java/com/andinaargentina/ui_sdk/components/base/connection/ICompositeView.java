package com.andinaargentina.ui_sdk.components.base.connection;

public interface ICompositeView {

    void onCreatedView();

    void onDestroy();
}
