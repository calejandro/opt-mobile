package com.andinaargentina.ui_sdk.components.dialog.lineorder;

import androidx.annotation.NonNull;

public class OrderDialogModel {

    private String orderName;
    private String orderId;
    private boolean isChecked;

    public OrderDialogModel(@NonNull String orderName,
                            @NonNull String orderId,
                            boolean isChecked) {
        this.orderName = orderName;
        this.orderId = orderId;
        this.isChecked = isChecked;
    }

    @NonNull
    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(@NonNull String orderName) {
        this.orderName = orderName;
    }

    @NonNull
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(@NonNull String orderId) {
        this.orderId = orderId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
