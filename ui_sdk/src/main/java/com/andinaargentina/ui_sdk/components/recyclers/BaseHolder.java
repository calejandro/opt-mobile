package com.andinaargentina.ui_sdk.components.recyclers;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseHolder<T> extends RecyclerView.ViewHolder {

    private T data;

    public BaseHolder(View itemView) {
        super(itemView);
    }

    public View getThisView(){
        return itemView;
    }

    public void setHolderItemClick(final BaseAdapter.IHolderItemClick clickListener){
        itemView.setOnClickListener(v -> {
            if (clickListener!=null)
                clickListener.onItemClick(v,getLayoutPosition(), null);
        });
    }

    public abstract void setDataInHolder(T data);


    public void viewDetached()
    {
        itemView.setOnClickListener(null);
    }
}
