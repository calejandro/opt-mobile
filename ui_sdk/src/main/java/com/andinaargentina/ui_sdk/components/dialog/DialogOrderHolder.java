package com.andinaargentina.ui_sdk.components.dialog;

import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.ui_sdk.R;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

public class DialogOrderHolder extends BaseHolder<SapOrdenesProduccionDto> {

    @NonNull
    private CheckBox checkBoxName;

    @Nullable
    private SapOrdenesProduccionDto data;

    public DialogOrderHolder(@NonNull View itemView, @NonNull DialogOrderClickListener listener) {
        super(itemView);
        this.checkBoxName = itemView.findViewById(R.id.item_name_order_txt);
        this.checkBoxName.setOnClickListener(v -> {
            this.data.setChecked(this.checkBoxName.isChecked());
            listener.onClick(getAdapterPosition(),this.data.isChecked());
        });
    }

    @Override
    public void setDataInHolder(@NonNull SapOrdenesProduccionDto data) {
        this.data = data;
        checkBoxName.setText(String.format("%s - %s", this.data.getIdOrdenSap(), data.getSapSku().getDescSku()));
        checkBoxName.setChecked(this.data.isChecked());
    }

    public interface DialogOrderClickListener {
        void onClick(int position, boolean isChecked);
    }
}
