package com.andinaargentina.ui_sdk.components.dialog.lineorder;

import androidx.annotation.NonNull;

import java.util.List;

public class LineWithOrderDialogModel {

    @NonNull
    private String idLine;
    @NonNull
    private String nameLine;
    @NonNull
    private List<OrderDialogModel> orderList;

    private boolean hasOrderAssigned;

    private String orderAssigned;

    public LineWithOrderDialogModel(@NonNull String nameLine,
                                    @NonNull String idLine,
                                    @NonNull List<OrderDialogModel> orderList) {
        this.nameLine = nameLine;
        this.orderList = orderList;
        this.idLine = idLine;
    }

    @NonNull
    public String getNameLine() {
        return nameLine;
    }

    public void setNameLine(@NonNull String nameLine) {
        this.nameLine = nameLine;
    }

    @NonNull
    public List<OrderDialogModel> getOrderList() {
        return orderList;
    }

    public void setOrderList(@NonNull List<OrderDialogModel> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    public String getIdLine() {
        return idLine;
    }

    public boolean isHasOrderAssigned() {
        return hasOrderAssigned;
    }

    public void setHasOrderAssigned(boolean hasOrderAssigned) {
        this.hasOrderAssigned = hasOrderAssigned;
    }

    public String getOrderAssigned() {
        return orderAssigned;
    }

    public void setOrderAssigned(String orderAssigned) {
        this.orderAssigned = orderAssigned;
    }
}
