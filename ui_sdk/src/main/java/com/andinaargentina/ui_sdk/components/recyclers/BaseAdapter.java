package com.andinaargentina.ui_sdk.components.recyclers;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseHolder<T>> {

    private List<T> dataList;

    @Nullable
    private IHolderItemClick click;

    public BaseAdapter(List<T> dataList){
        if (dataList==null){
            dataList=new ArrayList<T>();
        }
        this.dataList=dataList;
    }

    public BaseAdapter(List<T> dataList, IHolderItemClick click){
        this(dataList);
        this.click = click;
    }

    public void swipeListData(List<T> dataList){
        if (dataList==null){
            dataList=new ArrayList<T>();
        }
        this.dataList=dataList;
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        holder.setHolderItemClick(click);
        holder.setDataInHolder(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (dataList!=null)
            count = dataList.size();
        return count;
    }

    public void onDestroy(){
        this.click = null;
    }

    public List<T> getListData(){
        return dataList;
    }

    public void setListData(List<T> dataList){
        this.dataList = dataList;
    }

    public interface IHolderItemClick {
        public void onItemClick(View caller, int position, @Nullable Bundle bundle);
    }
}
