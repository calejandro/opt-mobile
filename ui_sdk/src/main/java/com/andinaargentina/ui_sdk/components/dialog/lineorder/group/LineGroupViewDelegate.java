package com.andinaargentina.ui_sdk.components.dialog.lineorder.group;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.ui_sdk.components.dialog.DialogOrderAdapter;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.TakeLineConfirmDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class LineGroupViewDelegate {

    @NonNull
    private TextView txtNameLine;
    @NonNull
    private RecyclerView recyclerViewOrdersGroup;
    @NonNull
    private View viewSeparator;

    @NonNull
    private String idOrderSelected;

    @NonNull
    private String idLine;

    private LineasProduccionUsuarioDto lineasProduccionUsuarioDto;

    public LineGroupViewDelegate(@NonNull TextView txtNameLine,
                                 @NonNull RecyclerView recyclerViewOrdersGroup,
                                 @NonNull View viewSeparator,
                                 @Nullable String idLine) {
        this.txtNameLine = txtNameLine;
        this.recyclerViewOrdersGroup = recyclerViewOrdersGroup;
        this.viewSeparator = viewSeparator;
        this.idLine = idLine;
    }

    public void setVisibilityGroup(boolean isVisible){
        int visibilityFlag = (isVisible) ? View.VISIBLE : View.GONE;
        this.txtNameLine.setVisibility(visibilityFlag);
        this.recyclerViewOrdersGroup.setVisibility(visibilityFlag);
        this.viewSeparator.setVisibility(visibilityFlag);
    }

    public void setTxtNameLine(@NonNull final String nameLine) {
        this.txtNameLine.setText(nameLine);
    }

    public void setOrderGroupData(@NonNull final Context context,
                                  @NonNull final LineasProduccionUsuarioDto lineasProduccionUsuarioDto){
        this.lineasProduccionUsuarioDto = lineasProduccionUsuarioDto;
        for (SapOrdenesProduccionDto orderModel: lineasProduccionUsuarioDto.getOrders()) {
            if (orderModel.isChecked()){
                this.idOrderSelected = String.valueOf(orderModel.getIdOrdenSap());
            }
        }
        //TODO dejo este codigo para marcar punto en donde checkear o deschekear el item de la orden
        //orders.get(0).setChecked(true);
        DialogOrderAdapter adapter = new DialogOrderAdapter(lineasProduccionUsuarioDto, (idOrderSelected, isChecked) -> {
            LineGroupViewDelegate.this.idOrderSelected = idOrderSelected;
            EventBus.getDefault().post(new TakeLineConfirmDialog.OrderSelectedEvent(idLine, idOrderSelected, isChecked));
        });
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false);
        this.recyclerViewOrdersGroup.setAdapter(adapter);
        this.recyclerViewOrdersGroup.setNestedScrollingEnabled(true);
        this.recyclerViewOrdersGroup.setLayoutManager(layoutManager);
    }

    @NonNull
    public String getIdOrderSelected() {
        return idOrderSelected;
    }

    @NonNull
    public String getIdLine() {
        return idLine;
    }

}
