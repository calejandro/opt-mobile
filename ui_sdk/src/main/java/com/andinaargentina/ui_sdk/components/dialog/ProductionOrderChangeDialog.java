package com.andinaargentina.ui_sdk.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.ui_sdk.R;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;

import java.util.List;

public class ProductionOrderChangeDialog extends Dialog {

    private TextView txtCancel, txtOk;
    private TextView txtLineTitle;
    private DialogClickHandler clickHandler;

    private String nameLine;
    private List<OrderDialogModel> ordersList;

    @NonNull
    private RecyclerView recyclerViewOrdersGroup;

    @NonNull
    private String idOrderSelected;

    private LineasProduccionUsuarioDto lineasProduccionUsuarioDto;

    public ProductionOrderChangeDialog(@NonNull Context context,
                                       @NonNull DialogClickHandler clickHandler,
                                       @NonNull String nameLine,
                                       @NonNull LineasProduccionUsuarioDto lineasProduccionUsuarioDto,
                                       @NonNull String idOrderSelected) {
        super(context, R.style.ThemeDialogCustom);
        this.lineasProduccionUsuarioDto = lineasProduccionUsuarioDto;
        this.clickHandler = clickHandler;
        init(context);
        this.nameLine = nameLine;
        this.idOrderSelected = idOrderSelected;
        this.setOrderGroupData(context, lineasProduccionUsuarioDto);
    }

    private void init(@NonNull Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_change_production_order, null);
        setContentView(view);
        setCancelable(false);
        initComponent();
        show();
    }

    private void initComponent() {
        txtCancel = findViewById(R.id.dialog_change_order_txt_cancel);
        txtOk = findViewById(R.id.dialog_change_order_txt_ok);
        txtLineTitle = findViewById(R.id.dialog_change_order_txt_line_title);
        recyclerViewOrdersGroup = findViewById(R.id.dialog_change_order_recycler);

        txtCancel.setOnClickListener(onClickListener);
        txtOk.setOnClickListener(onClickListener);
    }

    public void setOrderGroupData(@NonNull final Context context,
                                  @NonNull final LineasProduccionUsuarioDto lineasProduccionUsuarioDto){
        this.txtLineTitle.setText(String.format("• %s", this.nameLine));
        //Buscar orden seleccionada
        int i=0;
        for (SapOrdenesProduccionDto dto: lineasProduccionUsuarioDto.getOrders()) {
            if (String.valueOf(dto.getIdOrdenSap()).equals(this.idOrderSelected)){
                dto.setChecked(true);
            }
            i++;
        }
        DialogOrderAdapter adapter = new DialogOrderAdapter(lineasProduccionUsuarioDto, (idOrderSelected, isChecked)-> ProductionOrderChangeDialog.this.idOrderSelected = idOrderSelected);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false);
        this.recyclerViewOrdersGroup.setAdapter(adapter);
        this.recyclerViewOrdersGroup.setNestedScrollingEnabled(true);
        this.recyclerViewOrdersGroup.setLayoutManager(layoutManager);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == txtCancel) {
                dismiss();
            } else if (v == txtOk) {
                dismiss();
                clickHandler.positiveClick(Integer.valueOf(idOrderSelected));
            }
        }
    };

    public interface DialogClickHandler
    {
        void positiveClick(int idOrderSelected);
    }

}
