package com.andinaargentina.ui_sdk.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.andinaargentina.ui_sdk.R;

import java.util.List;

public class ChangeSiteDialog extends Dialog {
    private View view;
    private View dialog;
    private TextView tv_Cancel, tv_Ok;
    private TextView tv_title, tv_message;
    private View btnClose;
    private ChangeSiteDialogClickHandler clickHandler;
    private String positivetext,negativetext;

    private String title,message;

    private TextView txt1;
    private View container1;

    private TextView txt2;
    private View container2;

    private TextView txt3;
    private View container3;

    private RadioButton radio1;
    private RadioButton radio2;
    private RadioButton radio3;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v ==  btnClose) {
                clickHandler.closeClick();
            } else if (v == tv_Ok) {
                int positionSelected = 0;
                if (radio1.isChecked())
                    positionSelected = 0;
                if (radio2.isChecked())
                    positionSelected = 1;
                if (radio3.isChecked())
                    positionSelected = 2;
                clickHandler.positiveClick(positionSelected);
            }
        }
    };

    private View.OnClickListener radioOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View buttonView) {
            if (buttonView ==  radio1) {
                radio1.setChecked(radio1.isChecked());
                radio2.setChecked(false);
                radio3.setChecked(false);
            } else if (buttonView == radio2) {
                radio2.setChecked(radio2.isChecked());
                radio1.setChecked(false);
                radio3.setChecked(false);
            } else if (buttonView == radio3) {
                radio3.setChecked(radio3.isChecked());
                radio1.setChecked(false);
                radio2.setChecked(false);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onCheckListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView ==  radio1) {
                radio1.setChecked(isChecked);
                radio2.setChecked(false);
                radio3.setChecked(false);
            } else if (buttonView == radio2) {
                radio2.setChecked(isChecked);
                radio1.setChecked(false);
                radio3.setChecked(false);
            } else if (buttonView == radio3) {
                radio3.setChecked(isChecked);
                radio1.setChecked(false);
                radio2.setChecked(false);
            }
        }
    };

    private List<String> lisNames;
    private int positionSelected;

    public ChangeSiteDialog(Context context, String title, String message, String negativetext,
                            String positivetext, List<String> lisNames, int positionSelected) {
        super(context, R.style.ThemeDialogCustom);
        this.lisNames = lisNames;
        this.positionSelected = positionSelected;
        this.title=title;
        this.message=message;
        this.positivetext=positivetext;
        this.negativetext=negativetext;
        init(context);
    }

    public void setClickHandler(ChangeSiteDialogClickHandler clickHandler)
    {
        this.clickHandler=clickHandler;
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.dialog_change_site, null);
        setContentView(view);
        setCancelable(true);
        init_component();
        visible_option_menu();
    }

    private void init_component() {
        tv_Cancel = (TextView) findViewById(R.id.txt_cancel);
        tv_Ok = findViewById(R.id.txt_ok);
        btnClose = findViewById(R.id.change_site_dialog_btn_close);

        tv_title = (TextView) findViewById(R.id.dialog_title);
        tv_message = (TextView) findViewById(R.id.dialog_message);

        dialog = findViewById(R.id.dialog);

        txt1 = findViewById(R.id.dialog_change_site_txt_1);
        container1 = findViewById(R.id.dialog_change_site_container_txt_1);

        txt2 = findViewById(R.id.dialog_change_site_txt_2);
        container2 = findViewById(R.id.dialog_change_site_container_txt_2);

        txt3 = findViewById(R.id.dialog_change_site_txt_3);
        container3 = findViewById(R.id.dialog_change_site_container_txt_3);

        radio1 = findViewById(R.id.site_dialog_radio_1);
        radio2 = findViewById(R.id.site_dialog_radio_2);
        radio3 = findViewById(R.id.site_dialog_radio_3);

        if(TextUtils.isEmpty(title))
        {
            tv_title.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(message))
        {
            tv_message.setVisibility(View.INVISIBLE);
        }
        tv_title.setText(title);
        tv_message.setText(message);

        if(!TextUtils.isEmpty(positivetext))
        {
            tv_Ok.setText(positivetext);
        }
        if(!TextUtils.isEmpty(negativetext))
        {
            tv_Cancel.setText(negativetext);
        }

        tv_Cancel.setOnClickListener(onClickListener);
        tv_Ok.setOnClickListener(onClickListener);
        btnClose.setOnClickListener(onClickListener);

        radio1.setOnClickListener(radioOnClickListener);
        radio2.setOnClickListener(radioOnClickListener);
        radio3.setOnClickListener(radioOnClickListener);

        for (int i = 0; i < this.lisNames.size(); i++) {
            switch (i){
                case 0:{
                    container1.setVisibility(View.VISIBLE);
                    txt1.setText(lisNames.get(i));
                    if (positionSelected==i) radio1.setChecked(true);
                    break;
                }
                case 1:{
                    container2.setVisibility(View.VISIBLE);
                    txt2.setText(lisNames.get(i));
                    if (positionSelected==i) radio2.setChecked(true);
                    break;
                }
                case 3:{
                    container3.setVisibility(View.VISIBLE);
                    txt3.setText(lisNames.get(i));
                    if (positionSelected==i) radio3.setChecked(true);
                    break;
                }
            }
        }
    }

    private void visible_option_menu() {
        show();
        dialog.setVisibility(View.VISIBLE);

    }

    public void hideMessageView()
    {
        tv_message.setVisibility(View.GONE);
    }

    public void setCustomColor(int color)
    {
        tv_Ok.setTextColor(color);
        tv_Cancel.setTextColor(color);
    }

    public void hideNegativeOption()
    {
        tv_Cancel.setVisibility(View.GONE);
    }

    @Override
    public void show() {
        super.show();
    }

    public interface ChangeSiteDialogClickHandler
    {
         void positiveClick(int positionSelected);

         void closeClick();

    }
}