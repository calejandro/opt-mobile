package com.andinaargentina.ui_sdk.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.andinaargentina.ui_sdk.R;

public class LogoutConfirmDialog extends Dialog {

    private TextView txtCancel, txtOk;
    private CheckBox chckBox;

    private DialogClickHandler clickHandler;
    private boolean isSupervisorMode;

    public LogoutConfirmDialog(@NonNull Context context,
                               @NonNull DialogClickHandler clickHandler,
                               boolean isSupervisorMode) {
        super(context, R.style.ThemeDialogCustom);
        this.clickHandler = clickHandler;
        this.isSupervisorMode = isSupervisorMode;
        init(context);
    }

    public void setClickHandler(DialogClickHandler clickHandler)
    {
        this.clickHandler = clickHandler;
    }

    private void init(@NonNull Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_confirm_logout, null);
        setContentView(view);
        setCancelable(false);
        initComponent();
        show();
    }

    private void initComponent() {
        txtCancel = findViewById(R.id.txt_cancel);
        txtOk = findViewById(R.id.txt_ok);
        chckBox = findViewById(R.id.dialog_logout_checkbox);
        if (isSupervisorMode)
            chckBox.setVisibility(View.VISIBLE);
        else
            chckBox.setVisibility(View.GONE);
        txtCancel.setOnClickListener(onClickListener);
        txtOk.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == txtCancel) {
                clickHandler.negativeClick();
            } else if (v == txtOk) {
                clickHandler.positiveClick(chckBox.isChecked());
            }
        }
    };


    public interface DialogClickHandler
    {
        void positiveClick(boolean isChecked);

        void negativeClick();

    }

}