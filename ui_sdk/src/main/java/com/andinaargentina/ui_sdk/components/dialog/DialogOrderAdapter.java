package com.andinaargentina.ui_sdk.components.dialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionUsuarioDto;
import com.andinaargentina.core_services.providers.apiv2.models.SapOrdenesProduccionDto;
import com.andinaargentina.ui_sdk.R;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.util.List;

public class DialogOrderAdapter extends BaseAdapter<SapOrdenesProduccionDto> {

    private IOrderSelected orderSelectedListener;
    private LineasProduccionUsuarioDto lineasProduccionUsuarioDto;
    public DialogOrderAdapter(@NonNull LineasProduccionUsuarioDto lineasProduccionUsuarioDto,
                              @NonNull IOrderSelected orderSelectedListener) {
        super(lineasProduccionUsuarioDto.getOrders());
        this.lineasProduccionUsuarioDto = lineasProduccionUsuarioDto;
        this.orderSelectedListener = orderSelectedListener;
    }

    private void uncheckAllItems(int position, boolean isChecked) {
        int i = 0;
        for (SapOrdenesProduccionDto dto: getListData()) {
            if (i != position){
                dto.setChecked(false);
            }else{
                orderSelectedListener.orderChange(String.valueOf(dto.getIdOrdenSap()), isChecked);
            }
            i++;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BaseHolder<SapOrdenesProduccionDto> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name_order,parent,false);
        return new DialogOrderHolder(view, this::uncheckAllItems);
    }

    public interface IOrderSelected {
        void orderChange(String idOrderSelected, boolean isChecked);
    }
}