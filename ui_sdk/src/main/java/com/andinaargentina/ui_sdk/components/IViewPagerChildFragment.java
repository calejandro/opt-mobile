package com.andinaargentina.ui_sdk.components;

public interface IViewPagerChildFragment {

    void fragmentSelected();
}
