package com.andinaargentina.ui_sdk;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public abstract class FrameworkBaseFragment extends Fragment {

    public enum FRAGMENT_BASE_TYPE {
        FRAGMENT_NORMAL,
        FRAGMENT_ELEMENTO_SWIPE
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState,
                             int idResources) {
        return inflater.inflate(idResources, container, false);
    }

    public FRAGMENT_BASE_TYPE getTypeFragment(){
        return FRAGMENT_BASE_TYPE.FRAGMENT_NORMAL;
    }

    public abstract String getTagFragment();


}