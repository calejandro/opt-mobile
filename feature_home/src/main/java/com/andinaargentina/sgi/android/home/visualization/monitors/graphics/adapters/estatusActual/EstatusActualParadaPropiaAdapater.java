package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.apiv2.models.TimeStopsDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.util.List;

public class EstatusActualParadaPropiaAdapater extends BaseAdapter<TimeStopsDto> {

    public EstatusActualParadaPropiaAdapater(List<TimeStopsDto> dataList) {
        super(dataList);
    }

    @NonNull
    @Override
    public BaseHolder<TimeStopsDto> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parada_propia,parent,false);
        return new EstatusActualParadaPropiaHolder(v);
    }
}
