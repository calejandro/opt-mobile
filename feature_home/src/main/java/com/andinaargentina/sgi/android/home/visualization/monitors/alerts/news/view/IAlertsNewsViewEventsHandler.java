package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.view;

import com.andinaargentina.core_services.azure.FirebaseService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

public interface IAlertsNewsViewEventsHandler {

    //Llegó nueva alerta
    void onNewAlert(FirebaseService.OnMessageReceiver receiver);

    void onChangeStatusAlert();

    //Se seleccionó la alerta
    void onClickAlert(int position);

    //Se confirmó la toma de la alerta
    void onConfirmAlert(int position);

    void setLineDto(LineasProduccionDto lineDto);

    void setIsInformedAlert(boolean isInformedAlert);
}
