package com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode.repository;

import android.content.Context;

import com.andinaargentina.core_services.events.efficiency.EfficiencyErrorEvent;
import com.andinaargentina.core_services.events.efficiency.EfficiencySuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyLineDto;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyService;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class EfficiencyRepository {

    private EfficiencyService efficiencyService;
    private CompositeDisposable compositeDisposable;

    public EfficiencyRepository(EfficiencyService efficiencyService,
                                CompositeDisposable compositeDisposable) {
        this.efficiencyService = efficiencyService;
        this.compositeDisposable = compositeDisposable;
    }

    /*public void getEfficiency(String userId, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = efficiencyService
                    .getEfficiencyReport(userId,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getEfficiencyObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }*/

    /*private DisposableSingleObserver<List<EfficiencyLineDto>> getEfficiencyObserver(){
        return new DisposableSingleObserver<List<EfficiencyLineDto>>() {
            @Override
            public void onSuccess(List<EfficiencyLineDto> response) {
                EventBus.getDefault().post(new EfficiencySuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                ServiceError error;
                if (e instanceof ServiceError){
                    error = (ServiceError) e;
                }else{
                    error = new ServiceError();
                }
                EventBus.getDefault().post(new EfficiencyErrorEvent(error));
            }
        };
    }*/

    /*public void cancelRequest(){
        if (this.efficiencyService!=null)
            this.efficiencyService.cancel();
    }*/

}
