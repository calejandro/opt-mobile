package com.andinaargentina.sgi.android.home.visualization.monitors.graphics;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.events.efficiency.EfficiencySuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyDto;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyLineDto;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyService;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.GraphicsDto;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.GeneralChart;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment.Details;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency.ProductionBarChart;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionAlertasParadaMaquinaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionUmbralesTmefTmprDto;
import com.andinaargentina.core_services.providers.apiv2.models.EquiposEstadosDto;
import com.andinaargentina.core_services.providers.apiv2.models.EvolucionParadaPropiaDto;
import com.andinaargentina.core_services.providers.apiv2.models.KpiDepletionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionEficienciaMesDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaByShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.utils.ConfiguracionAlertasUtils;
import com.andinaargentina.core_services.providers.apiv2.utils.ConfiguracionUmbralesUtils;
import com.andinaargentina.core_services.providers.apiv2.utils.EquipoEstadosUtils;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual.EstatusActualDto;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.cumplimientoPlan.CumplimientoPlanFragment;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.dialogs.CumplimientoPlanColorsReferenceDialog;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.dialogs.EficienciaTurnoColorsReferenceDialog;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.eficienciaTurno.EficienciaTurnoFragment;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.mermas.MermasViewDelegate;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.statusactual.EstatusActualViewDelegate;
import com.andinaargentina.sgi.android.home.repository.GraphicsRepository;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.statusactual.SelectMachineEvent;
import com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode.repository.EfficiencyRepository;
import com.andinaargentina.sgi.core_sdk.base.UtilMathematica;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.andinaargentina.ui_sdk.components.IViewPagerChildFragment;
import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.TreeDataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.TreeMap;
import com.anychart.core.cartesian.series.Column;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.SelectionMode;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.enums.TreeFillingMethod;
import com.microsoft.appcenter.crashes.Crashes;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function4;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.andinaargentina.RepositoryConst.GRAPHICS_DATA;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_FRAGMENT_VIEW_NAME_GRAPHICS_ITEM;
import static com.andinaargentina.sgi.android.home.visualization.monitors.graphics.cumplimientoPlan.CumplimientoPlanFragment.KEY_INPUT_DATA;


@SuppressLint({"CheckResult", "SetTextI18n", "SimpleDateFormat"})
@RequiresApi(api = Build.VERSION_CODES.N)
@Route(path = ROUTE_FRAGMENT_VIEW_NAME_GRAPHICS_ITEM)
public class GraphicsFragment extends FrameworkBaseFragment implements IViewPagerChildFragment {

    private LineasProduccionDto lineDto;
    public static final String KEY_DATA_INPUT  = "KEY_LINE_DTO";
    public static final String RED_COLOR = "#F83F37";
    public static final String GRAY_COLOR = "#FAFAFA";
    public static final String YELLOW_COLOR = "#FEDF3E";

    //Top bar
    private View btnLeftContainer;
    private View btnRightContainer;

    //Eficiencia Turno
    private View containerEficienciaDeTurnoChart;
    private EficienciaTurnoFragment eficienciaTurnoFragmentChart;
    private TextView txtEficienciaTurnoAverage;
    private TextView txtTotalProduced;

    //Cumplimiento plan
    private View containerCumplimientoPlanChart;
    private CumplimientoPlanFragment cumplimientoPlanFragment;
    private TextView txtCumplimientoPlanAverage;
    private TextView txtCumplimientoPlanLineName;
    private TextView txtSkuValue;
    private TextView txtCantBottles;
    private TextView txtCantBottlesProducedLastHour;
    private TextView txtRemainingHourBottles;
    private TextView txtRemainingOrden;
    private TextView txtRemainingTitleOrder;
    private EfficiencyRepository efficiencyRepository;

    //Dialogs
    private Dialog dialogEficienciaTurnos;
    private Dialog dialogCumplimientoPlan;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private MermasViewDelegate mermasViewDelegate;
    private EstatusActualViewDelegate estatusActualDelegate;
    private AnyChartView performanceMachinesEvolutionChart;
    private SimpleDateFormat simpleDateFormatHour = new SimpleDateFormat("HH:mm");
    private Cartesian cartesian;
    private AnyChartView mermasTreeMapChart;
    private AnyChartView mermasEvolutionChart;
    private TreeMap treeMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.lineDto = (LineasProduccionDto) getArguments().getSerializable(KEY_DATA_INPUT);
        initUi();
        return super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_graphics);
    }

    @Override
    public void onDestroyView() {
        Log.i("GraphicTag"," Se destruyo la vista de la linea: "+ lineDto.getIdLinea());
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerObserver();
    }

    private void setEficienciaTurno() {
        ShiftDto turno = ShiftUtils.getInstance().getCurrentShift();

        OptimusServices.getInstance().getLineasProduccionEficienciaMesService().list(lineDto.getIdLinea(), lineDto.getIdPlanta()).enqueue(new Callback<List<LineasProduccionEficienciaMesDto>>() {
            @Override
            public void onResponse(Call<List<LineasProduccionEficienciaMesDto>> call, Response<List<LineasProduccionEficienciaMesDto>> respnseObjetivo) {
                OptimusServices.getInstance().getEficienciaService().listByShift(
                        lineDto.getIdLinea(),
                        simpleDateFormat.format(turno.getInicio()),
                        simpleDateFormat.format(turno.getFin())
                ).enqueue(new Callback<ArrayList<ProduccionEficienciaByShiftDto>>() {

                    @Override
                    public void onResponse(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Response<ArrayList<ProduccionEficienciaByShiftDto>> response) {
                        getActivity().runOnUiThread(() -> {
                            Double eficiencia = 0d;
                            int totalBotellas = 0;
                            if(response.body().size() > 0) {
                                eficiencia = response.body().get(0).getEficiencia();
                                totalBotellas = response.body().get(0).getBotellasProducidas();
                            }
                            txtTotalProduced.setText("Total de botellas producidas : " + totalBotellas);
                            txtEficienciaTurnoAverage.setText(eficiencia + " %");
                            int drawableForEficiencia = 0;
                            if (eficiencia<=respnseObjetivo.body().get(0).getEficienciaMensual()){
                                drawableForEficiencia = R.drawable.bg_button_red_ripple;
                            }else{
                                drawableForEficiencia = R.drawable.bg_button_green_ripple;
                            }
                            View containerEficienciaTurnoAverage = getView().findViewById(R.id.fragment_graphics_container_btn_left);
                            containerEficienciaTurnoAverage.setBackgroundResource(drawableForEficiencia);
                        });
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<LineasProduccionEficienciaMesDto>> call, Throwable t) {

            }
        });

    }

    private void initUi() {
        if (getView()!=null){

            mermasTreeMapChart = getView().findViewById(R.id.mermas_detalle_chart_tree_map);
            mermasTreeMapChart.setProgressBar(getView().findViewById(R.id.mermas_detalle_chart_tree_map_progress_bar));
            mermasEvolutionChart = getView().findViewById(R.id.mermas_detalle_chart_evolucion_general);
            APIlib.getInstance().setActiveAnyChartView(mermasTreeMapChart);
            if (mermasViewDelegate == null){
                mermasViewDelegate = new MermasViewDelegate(lineDto);
                mermasViewDelegate.initView(getView());
                treeMap = AnyChart.treeMap();
                treeMap();
                mermaEvolutionGraph();
            }

            Observable.zip(
                    ShiftUtils.getInstance().getObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()),
                    EquipoEstadosUtils.getInstance().getObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()),
                    ConfiguracionUmbralesUtils.getInstance().getObservable(this.lineDto.getIdPlanta(), this.lineDto.getIdLinea())
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()),
                    ConfiguracionAlertasUtils.getInstance().getObservable(this.lineDto.getIdPlanta(), this.lineDto.getIdLinea())
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()),
                    new Function4<List<ShiftDto>, List<EquiposEstadosDto>, List<ConfiguracionUmbralesTmefTmprDto>, List<ConfiguracionAlertasParadaMaquinaDto>, Object>() {
                        @io.reactivex.annotations.NonNull
                        @Override
                        public List<Object> apply(
                                                  @io.reactivex.annotations.NonNull List<ShiftDto> shiftDtoList,
                                                  @io.reactivex.annotations.NonNull List<EquiposEstadosDto> equiposEstadosDtos,
                                                  @io.reactivex.annotations.NonNull List<ConfiguracionUmbralesTmefTmprDto> configuracionUmbralesTmefTmprDtos,
                                                  @io.reactivex.annotations.NonNull List<ConfiguracionAlertasParadaMaquinaDto> configuracionAlertasParadaMaquinaDtos
                        ) throws Exception {
                            return new ArrayList<Object>();
                        }
                    } ).subscribe(new Consumer<Object>() {
                @Override
                public void accept(Object o) throws Exception {
                    btnLeftContainer = getView().findViewById(R.id.fragment_graphics_container_btn_left);
                    btnLeftContainer.setEnabled(true);
                    btnLeftContainer.setActivated(false);
                    btnLeftContainer.setOnClickListener(v -> {
                        if (v.isEnabled() && v.isActivated()){
                            hideEficienciaTurno();
                        }else if (v.isEnabled() && !v.isActivated()){
                            showEficienciaTurno();
                        }
                    });

                    //Button right
                    btnRightContainer = getView().findViewById(R.id.fragment_graphics_container_btn_right);
                    btnRightContainer.setEnabled(true);
                    btnRightContainer.setActivated(false);
                    btnRightContainer.setOnClickListener(v->{
                        if (v.isEnabled() && v.isActivated()){
                            hideCumplimientoPlan();
                        }else if (v.isEnabled() && !v.isActivated()){
                            showCumplimientoPlan();
                        }
                    });


                    View btnEficienciaTurno = getView().findViewById(R.id.eficiencia_turno_btn_dialog);
                    btnEficienciaTurno.setOnClickListener(v -> showEficienciaTurnoDialog());
                    ImageView eficienciaTurnoBtnClose = getView().findViewById(R.id.eficiencia_turno_btn_close);
                    eficienciaTurnoBtnClose.setOnClickListener(v-> hideEficienciaTurno());
                    txtTotalProduced = getView().findViewById(R.id.eficiencia_turno_txt_total_produced);

                    //Cumplimiento Plan
                    txtEficienciaTurnoAverage = getView().findViewById(R.id.eficiencia_turno_txt_average);
                    txtCumplimientoPlanAverage = getView().findViewById(R.id.cumplimiento_plan_txt_average);
                    txtCumplimientoPlanLineName = getView().findViewById(R.id. cumplimiento_plan_txt_orden_produccion_title);
                    txtSkuValue = getView().findViewById(R.id.cumplimiento_plan_txt_orden_produccion_sku_value);
                    txtCantBottles = getView().findViewById(R.id.cumplimiento_plan_txt_orden_produccion_cant_botellas_value);
                    txtCantBottlesProducedLastHour = getView().findViewById(R.id.cumplimiento_plan_txt_botellas_ultima_hora_value);
                    txtRemainingHourBottles = getView().findViewById(R.id.cumplimiento_plan_txt_restantes_obetivo);
                    txtRemainingOrden = getView().findViewById(R.id.cumplimiento_plan_txt_restantes_orden_turno_actual_value);
                    txtRemainingTitleOrder = getView().findViewById(R.id.cumplimiento_plan_txt_restantes_orden_turno_actual);

                    //setDataCumplimientoPlan();

                    containerCumplimientoPlanChart = getView().findViewById(R.id.fragment_graphics_cumplimiento_plan_container);
                    View btnCumplimientoPlan = getView().findViewById(R.id.cumplimiento_plan_btn_dialog);
                    btnCumplimientoPlan.setOnClickListener(v -> showCumplimientoPlanDialog());
                    ImageView cumplimientoPlanBtnClose = getView().findViewById(R.id.cumplimiento_plan_btn_close);
                    cumplimientoPlanBtnClose.setOnClickListener(v-> hideCumplimientoPlan());
                    cumplimientoPlanFragment = (CumplimientoPlanFragment) getChildFragmentManager().findFragmentById( R.id.fragment_graphics_cumplimiento_plan_column_bar_simple);
                    Bundle bundleCumplimientoPlan = new Bundle();
                    cumplimientoPlanFragment.setArguments(bundleCumplimientoPlan);


                    setStatusActual();

                    setEficienciaTurno();


                }
            });

        }
    }

    private void mermaEvolutionGraph() {

        OptimusServices.getInstance().getMermasKPIService().list(this.lineDto.getIdLinea(),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()), 4).enqueue(new Callback<ArrayList<KpiDepletionDto>>() {
            @Override
            public void onResponse(Call<ArrayList<KpiDepletionDto>> call, Response<ArrayList<KpiDepletionDto>> response) {
                APIlib.getInstance().setActiveAnyChartView(mermasEvolutionChart);

                Cartesian cartesian = AnyChart.column();
                cartesian.animation(false);
                cartesian.padding(20d, 20d, 5d, 20d);
                cartesian.crosshair().enabled(true);
                cartesian.crosshair(false);
                cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
                cartesian.tooltip()
                        .useHtml(true)
                        .format("function() {\n" +
                                "      return '<span style=\"color: #bfbfbf\">merma: </span>' +\n" +
                                "        anychart.format.number(this.value, {\n" +
                                "          groupsSeparator: ' '\n" +
                                "        });\n" +
                                "    }");
                cartesian.yAxis(0).title("Porcentaje");
                cartesian.xAxis(0).title("Hora");
                cartesian.xAxis(0).labels().rotation(-90);
                Set evolutionGeneralSet = Set.instantiate();
                List<DataEntry> seriesData = new ArrayList<>();
                for (KpiDepletionDto value: response.body()) {
                    seriesData.add(new GraphicsFragment.CustomDataEntry(simpleDateFormatHour.format(value.getFechaHora()), value.getKpi()));
                }
                evolutionGeneralSet.data(seriesData);
                Mapping series1Mapping = evolutionGeneralSet.mapAs("{ x: 'x', value: 'value'}");

                Column series1 = cartesian.column(series1Mapping);

                series1.hovered().markers().enabled(true);
                series1.hovered().markers()
                        .type(MarkerType.CIRCLE)
                        .size(4d);
                series1.tooltip()
                        .position("right")
                        .anchor(Anchor.LEFT_CENTER)
                        .offsetX(5d)
                        .offsetY(5d);

                cartesian.legend().enabled(false);

                mermasEvolutionChart.setChart(cartesian);
            }

            @Override
            public void onFailure(Call<ArrayList<KpiDepletionDto>> call, Throwable t) {

            }
        });

    }

    private void treeMap() {
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                OptimusServices.getInstance().getMermasKPIService().list(lineDto.getIdLinea(),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()), 2).enqueue(new Callback<ArrayList<KpiDepletionDto>>() {
                    @Override
                    public void onResponse(Call<ArrayList<KpiDepletionDto>> call, Response<ArrayList<KpiDepletionDto>> response) {
                        List<DataEntry> data = new ArrayList<>();
                        data.add(new GraphicsFragment.CustomTreeDataEntry("Maquina", null, "Maquina",GRAY_COLOR));
                        for (KpiDepletionDto dto:response.body()) {
                            String colorSelected = GRAY_COLOR;
                    /*UmbralUtil.UMBRAL_TYPE umbralType = UmbralUtil.getCOLORType(dto.getUmbraType());
                    switch (umbralType){
                        case NORMAL:{
                            colorSelected = GRAY_COLOR;
                            break;
                        }
                        case WARNING:{
                            colorSelected = YELLOW_COLOR;
                            break;
                        }
                        case CRITICAL:{
                            colorSelected = RED_COLOR;
                            break;
                        }
                    }*/
                            data.add(new GraphicsFragment.CustomTreeDataEntry(dto.getDescripcionEquipo(), "Maquina", dto.getDescripcionEquipo(), 1, dto.getKpi(),colorSelected));
                        }
                        APIlib.getInstance().setActiveAnyChartView(mermasTreeMapChart);
                        treeMap.data(data, TreeFillingMethod.AS_TABLE);

                        treeMap.padding(10d, 10d, 10d, 10d);
                        treeMap.maxDepth(2d);
                        treeMap.hovered().fill("#bdbdbd", 1d);
                        treeMap.selectionMode(SelectionMode.NONE);

                        treeMap.legend().enabled(false);
                        treeMap.headers(false);
                        treeMap.padding(10d, 20d, 20d, 20d);


                        treeMap.labels().useHtml(true);
                        treeMap.labels().fontColor("#212121");
                        treeMap.labels().fontSize(12d);
                        treeMap.labels().format(
                                "function() {\n" +
                                        "      return this.getData('product');\n" +
                                        "    }");

                        treeMap.tooltip()
                                .useHtml(true)
                                .titleFormat("{%product}")
                                .format("function() {\n" +
                                        "      return '<span style=\"color: #bfbfbf\">Merma: </span>' +\n" +
                                        "        anychart.format.number(this.getData('mermaPercent'), {\n" +
                                        "          groupsSeparator: ' '\n" +
                                        "        });\n" +
                                        "    }");


                        mermasTreeMapChart.setChart(treeMap);
                        mermasTreeMapChart.refreshDrawableState();

                    }

                    @Override
                    public void onFailure(Call<ArrayList<KpiDepletionDto>> call, Throwable t) {

                    }
                });
            }
        });

    }

    private void setStatusActual() {

        OptimusServices.getInstance().getPerformanceEquiposService().listActualStatus(lineDto.getIdLinea()).enqueue(new Callback<ArrayList<MachineActualStatusDto>>() {
            @Override
            public void onResponse(Call<ArrayList<MachineActualStatusDto>> call, Response<ArrayList<MachineActualStatusDto>> response) {
                estatusActualDelegate = new EstatusActualViewDelegate(lineDto, response.body());
                performanceMachinesEvolutionChart = getView().findViewById(R.id.estatus_actual_equipos_chart_evolucion_general);
                estatusActualDelegate.initView(getView());
            }

            @Override
            public void onFailure(Call<ArrayList<MachineActualStatusDto>> call, Throwable t) {

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        initUi();
    }

    @Override
    public void fragmentSelected(){
        if (getView()!=null){
            Log.d("Graphics","Start animation");
        }
    }

    public static class CustomTreeDataEntry extends TreeDataEntry {
        public CustomTreeDataEntry(String id, String parent, String product, Integer value, Double mermaPercent, String customColor) {
            super(id, parent, value);
            setValue("fill",customColor);
            setValue("product", product);
            setValue("mermaPercent",mermaPercent);
        }

        public CustomTreeDataEntry(String id, String parent, String product, String customColor) {
            super(id, parent);
            setValue("fill",customColor);
            setValue("product", product);
        }
    }

    public void showEficienciaTurno(){
        OptimusServices.getInstance().getEficienciaService().list(lineDto.getIdLinea(),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin())).enqueue(new Callback<ArrayList<ProduccionEficienciaDto>>() {
            @Override
            public void onResponse(Call<ArrayList<ProduccionEficienciaDto>> call, Response<ArrayList<ProduccionEficienciaDto>> response) {

                containerEficienciaDeTurnoChart = getView().findViewById(R.id.fragment_graphics_frame_eficiencia_turno);
                eficienciaTurnoFragmentChart = (EficienciaTurnoFragment) getChildFragmentManager().findFragmentById( R.id.fragment_graphics_column_bar_simple );
                Bundle bundleForEfficiencyFragment = new Bundle();
                bundleForEfficiencyFragment.putSerializable(EficienciaTurnoFragment.KEY_INPUT_DATA, response.body());
                eficienciaTurnoFragmentChart.setArguments(bundleForEfficiencyFragment);
                btnLeftContainer.setActivated(true);
                btnRightContainer.setEnabled(false);
                containerEficienciaDeTurnoChart.setAlpha(0.0f);
                containerEficienciaDeTurnoChart.setVisibility(View.VISIBLE);
                containerEficienciaDeTurnoChart.animate().alpha(1.0f).setDuration(500).setListener(null);
                eficienciaTurnoFragmentChart.startCombinedChart();
            }

            @Override
            public void onFailure(Call<ArrayList<ProduccionEficienciaDto>> call, Throwable t) {

            }
        });


    }

    public void hideEficienciaTurno(){
        enableTopButtons();
        this.containerEficienciaDeTurnoChart.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                GraphicsFragment.this.containerEficienciaDeTurnoChart.setVisibility(View.GONE);
            }
        });
    }

    public void showCumplimientoPlan(){
        this.btnRightContainer.setActivated(true);
        this.btnLeftContainer.setEnabled(false);
        this.containerCumplimientoPlanChart.setAlpha(0.0f);
        this.containerCumplimientoPlanChart.setVisibility(View.VISIBLE);
        this.containerCumplimientoPlanChart.animate().alpha(1.0f).setDuration(500).setListener(null);
        this.cumplimientoPlanFragment.startChart();
    }

    private void enableTopButtons(){
        btnRightContainer.setActivated(false);
        btnRightContainer.setEnabled(true);
        btnLeftContainer.setActivated(false);
        btnLeftContainer.setEnabled(true);
    }

    public void hideCumplimientoPlan(){
        enableTopButtons();
        this.containerCumplimientoPlanChart.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                GraphicsFragment.this.containerCumplimientoPlanChart.setVisibility(View.GONE);
            }
        });
    }

    static public class CustomDataEntry extends ValueDataEntry {
        public CustomDataEntry(String x, Number value) {
            super(x, value);
        }
    }


    @Override
    public String getTagFragment() {
        return "GraphicsFragment";
    }

    public void showEficienciaTurnoDialog(){
        this.dialogEficienciaTurnos = new EficienciaTurnoColorsReferenceDialog(getContext());
        this.dialogEficienciaTurnos.show();
    }

    public void showCumplimientoPlanDialog(){
        this.dialogCumplimientoPlan = new CumplimientoPlanColorsReferenceDialog(getContext());
        this.dialogCumplimientoPlan.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterObserver();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void selectMachine(SelectMachineEvent selectMachineEvent) {
        OptimusServices.getInstance().getPerformanceEquiposService().listEvolucionParadaPropia(
                selectMachineEvent.getIdLinea(), selectMachineEvent.getIdEquipo())
        .enqueue(new Callback<ArrayList<EvolucionParadaPropiaDto>>() {
            @Override
            public void onResponse(Call<ArrayList<EvolucionParadaPropiaDto>> call, Response<ArrayList<EvolucionParadaPropiaDto>> response) {
                APIlib.getInstance().setActiveAnyChartView(performanceMachinesEvolutionChart);
                Set performanceEvolutionDataSet = Set.instantiate();
                if(cartesian == null) {
                    cartesian = AnyChart.line();
                    cartesian.animation(false);
                    cartesian.padding(20d, 20d, 5d, 20d);
                    cartesian.crosshair().enabled(true);
                    cartesian.crosshair(false);
                    cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
                    cartesian.tooltip()
                            .useHtml(true)
                            .format("function() {\n" +
                                    "      return '<span style=\"color: #bfbfbf\">Minutos: </span>' +\n" +
                                    "        anychart.format.number(this.value, {\n" +
                                    "          groupsSeparator: ' '\n" +
                                    "        });\n" +
                                    "    }");

                    cartesian.yAxis(0).title("Minutos");
                    cartesian.xAxis(0).title("Hora");
                    cartesian.xAxis(0).labels().rotation(-90);
                    performanceMachinesEvolutionChart.setChart(cartesian);
                }

                List<DataEntry> seriesData = new ArrayList<>();
                for (EvolucionParadaPropiaDto value:response.body()) {
                    GraphicsFragment.CustomDataEntry items = new GraphicsFragment.CustomDataEntry(
                            simpleDateFormatHour.format(value.getBanda_Horaria()),
                            value.getMinutos_Acumulados());
                    seriesData.add(items);
                }

                performanceEvolutionDataSet.data(seriesData);

                Mapping series1Mapping = performanceEvolutionDataSet.mapAs("{ x: 'x', value: 'value'}");
                cartesian.removeAllSeries();
                Column series1 = cartesian.column(series1Mapping);
                series1.hovered().markers().enabled(true);
                series1.hovered().markers()
                        .type(MarkerType.CIRCLE)
                        .size(4d);
                series1.tooltip()
                        .position("right")
                        .anchor(Anchor.LEFT_CENTER)
                        .offsetX(5d)
                        .offsetY(5d);
                cartesian.legend().enabled(false);
                performanceMachinesEvolutionChart.refreshDrawableState();
            }

            @Override
            public void onFailure(Call<ArrayList<EvolucionParadaPropiaDto>> call, Throwable t) {

            }
        });
    }

    private void registerObserver() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unregisterObserver() {
        if (EventBus.getDefault().isRegistered(this)) {
            //EventBus.getDefault().unregister(this);
        }
    }

}
