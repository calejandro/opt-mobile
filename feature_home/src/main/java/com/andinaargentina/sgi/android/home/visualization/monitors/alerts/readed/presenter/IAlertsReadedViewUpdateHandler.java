package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.presenter;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;

/**
 * Esta interfaz define la comunicación
 * entre el presenter y la vista, es decir la actualización
 * de la vista.
 * Esta interfaz la debe implementar la vista
 */
public interface IAlertsReadedViewUpdateHandler {

    //Vacío lista
    void clearList();

    //Agregamos una nueva alerta
    void addAlert(@NonNull LineasProduccionAlertasDto alertDto);

    //Quitamos una alerta
    void removeAlert(int position);

    //Esconder modulo de alertas leidas
    void hideComponent();

    //Mostrar el componente
    void showComponent();

    //Cambiar el type de una alerta
    void changeStatus(int position);

    //Mostramos un estado vacio
    void showEmptyState();

    //Mostrar el dialogo correspondiente al tipo de alerta leída
    void showConfirmDialog(int position);
}
