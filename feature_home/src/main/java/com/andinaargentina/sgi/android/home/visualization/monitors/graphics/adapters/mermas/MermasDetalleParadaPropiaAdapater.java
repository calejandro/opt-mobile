package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.apiv2.models.KpiDepletionDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.util.List;

public class MermasDetalleParadaPropiaAdapater extends BaseAdapter<KpiDepletionDto> {

    public MermasDetalleParadaPropiaAdapater(List<KpiDepletionDto> dataList) {
        super(dataList);
    }

    @NonNull
    @Override
    public BaseHolder<KpiDepletionDto> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mermas_detalle_parada_propia,parent,false);
        return new MermasDetalleParadaPropiaHolder(v);
    }

}
