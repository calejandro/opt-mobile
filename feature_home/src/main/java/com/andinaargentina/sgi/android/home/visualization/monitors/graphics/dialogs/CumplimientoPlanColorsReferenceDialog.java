package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.andinaargentina.sgi.android.feature_home.R;

public class CumplimientoPlanColorsReferenceDialog extends Dialog {

    public CumplimientoPlanColorsReferenceDialog(@NonNull Context context) {
        super(context, com.andinaargentina.ui_sdk.R.style.ThemeDialogCustom);
        initParentView(context);
    }

    private void initParentView(@NonNull Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_code_color_info_cumplimiento, null);
        View btnClose = view.findViewById(R.id.dialog_color_reference_cumplimiento_plan_btn_close);
        View okButton = view.findViewById(R.id.dialog_color_reference_cumplimiento_plan_btn_ok);
        btnClose.setOnClickListener(v -> v.postDelayed(this::closeDialog,170));
        okButton.setOnClickListener(v -> v.postDelayed(this::closeDialog,170));
        setContentView(view);
    }

    private void closeDialog(){
        this.hide();
    }
}
