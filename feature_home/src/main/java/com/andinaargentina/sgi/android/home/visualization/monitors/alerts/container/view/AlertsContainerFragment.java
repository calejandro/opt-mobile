package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.view.AlertsNewsFragment;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.view.AlertsReadedFragment;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.andinaargentina.ui_sdk.components.IViewPagerChildFragment;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.ALERTS_DATA;
import static com.andinaargentina.RepositoryConst.FCM_REGISTRATION_ID;
import static com.andinaargentina.RepositoryConst.FCM_TOKEN;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_FRAGMENT_VIEW_NAME_ALERTS_CONTAINER;

@Route(path = ROUTE_FRAGMENT_VIEW_NAME_ALERTS_CONTAINER)
@RequiresApi(api = Build.VERSION_CODES.N)
public class AlertsContainerFragment extends FrameworkBaseFragment implements IViewPagerChildFragment {

    private View emptyState;
    private FrameLayout frameLayoutAlertsNews;
    private FrameLayout frameLayoutReadedAlerts;

    private AlertsReadedFragment alertsReadedFragment;
    private AlertsNewsFragment alertsNewsFragment;

    private LineasProduccionDto lineDto;
    public static final String KEY_LINE_DTO = "KEY_LINE_DTO";
    public static final String KEY_IS_INFORMED_ALERTS = "KEY_IS_INFORMED_ALERT";

    private boolean isInformedAlerts;


    /**
     *  0- Recuperar alertas anteriores
     *  1- Validar si hay que mostrar alertas leídas o nuevas o ambas o  el emptystate
     *  2- Mostrar/Esconder alertas leídas
     *  3- Mostrar/Esconder alertas nuevas
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null && getArguments().containsKey(KEY_LINE_DTO)){
            lineDto = (LineasProduccionDto) getArguments().getSerializable(KEY_LINE_DTO);
            isInformedAlerts = getArguments().getBoolean(KEY_IS_INFORMED_ALERTS);
        }
        registerObserver();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState,
                R.layout.fragment_alerts_container);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterObserver();
    }

    private void registerObserver() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unregisterObserver() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //AlertsSuccessEvent alertsCache = Paper.book("SGI-BOOK").read(ALERTS_DATA);
        //onIReceiveBackendAlerts(alertsCache);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.emptyState = getView().findViewById(R.id.fragment_alerts_container_empty_state);
        this.frameLayoutAlertsNews = getView().findViewById(R.id.fragment_alerts_frame_container_news_alerts);
        this.frameLayoutReadedAlerts = getView().findViewById(R.id.fragment_alerts_frame_container_readed_alerts);
        this.alertsReadedFragment = (AlertsReadedFragment) getChildFragmentManager().findFragmentById(R.id.fragment_alerts_container_readed_alerts);
        this.alertsNewsFragment = (AlertsNewsFragment) getChildFragmentManager().findFragmentById(R.id.fragment_alerts_container_news_alerts);
        this.alertsNewsFragment.setLineDto(this.lineDto);
        this.alertsNewsFragment.setIsInformed(isInformedAlerts);
        this.alertsReadedFragment.setLineDto(this.lineDto);
        this.emptyState.setVisibility(View.GONE);
        this.showNewAlerts();
        this.showReadAlerts();

    }

    /**
     * We show the read alerts
     */
    public void showReadAlerts(){
        frameLayoutReadedAlerts.setAlpha(0.0f);
        frameLayoutReadedAlerts.setVisibility(View.VISIBLE);
        frameLayoutReadedAlerts
                .animate()
                .alpha(1.0f)
                .setDuration(800).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                frameLayoutReadedAlerts.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * We hide the read alerts
     */
    public void hideReadedAlerts(){
        frameLayoutReadedAlerts
                .animate()
                .alpha(0.0f)
                .setDuration(500).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        frameLayoutReadedAlerts.setVisibility(View.GONE);
                    }
                });
    }


    /**
     * We show the news alerts
     */
    public void showNewAlerts(){
        frameLayoutAlertsNews.setAlpha(0.0f);
        frameLayoutAlertsNews.setVisibility(View.VISIBLE);
        frameLayoutAlertsNews
                .animate()
                .alpha(1.0f)
                .setDuration(800);
    }

    /**
     * We hide the news alerts
     */
    public void hideNewAlerts(){
        frameLayoutAlertsNews
                .animate()
                .alpha(0.0f)
                .setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                frameLayoutAlertsNews.setVisibility(View.GONE);
            }
        });
    }

    /*
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addReadedAlert(AddNewReadedAlert event){
        //showReadAlerts();
        //showReadedAlert(event.alertDto);
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onIReceiveBackendAlerts(AlertsSuccessEvent response){
        List<LineasProduccionAlertasDto> alertsForThisLine = null;
        if (response!=null){
            alertsForThisLine = response.getResponse();
        }
        if (alertsForThisLine == null || alertsForThisLine.isEmpty()){
            //Mostrar empty state en la pantalla
            this.emptyState.setVisibility(View.VISIBLE);
        }else{
            this.emptyState.setVisibility(View.GONE);
            //Verificar si existen alertas leídas
            //Verificar si existen alertas nuevas
            boolean existReadAlerts = false;
            boolean existNewsAlerts = false;
            for (LineasProduccionAlertasDto alertDto: alertsForThisLine) {
                if (isInformedAlerts){
                    if (alertDto.getEstado().equals("Informada")){
                        existNewsAlerts = true;
                    }
                }else{
                    if (alertDto.getEstado().equals("Nueva")){
                        existNewsAlerts = true;
                    }
                    if (alertDto.getEstado().equals("Leida")){
                        existReadAlerts = true;
                    }
                }
            }
            if (existNewsAlerts){
                this.showNewAlerts();
                Log.d("Alertas testing", "linea:" + this.lineDto.getDescLinea() + " showNewAlerts: "+alertsForThisLine.size());
            }
            else{
                this.hideNewAlerts();
                Log.d("Alertas testing", "linea:" + this.lineDto.getDescLinea() + " hideNewAlerts: "+alertsForThisLine.size());
            }

            if (existReadAlerts){
                this.showReadAlerts();
                Log.d("Alertas testing", "linea:" + this.lineDto.getDescLinea() + " showReadAlerts: "+alertsForThisLine.size());
            }
            else{
                this.hideReadedAlerts();
                Log.d("Alertas testing", "linea:" + this.lineDto.getDescLinea() + " hideReadedAlerts: "+alertsForThisLine.size());
            }
            if (!existNewsAlerts && !existReadAlerts){
                this.emptyState.setVisibility(View.VISIBLE);
            }else{
                this.emptyState.setVisibility(View.GONE);
            }
        }
    }

    private void showReadedAlert(LineasProduccionAlertasDto alertDto) {
        alertsReadedFragment.addAlert(alertDto);
    }

    @Override
    public String getTagFragment() {
        return "Alerts fragment";
    }

    public void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();
        if (v.isShown()) {
            collapse(v);
        } else {
            v.getLayoutParams().height = 0;
            v.setVisibility(View.VISIBLE);
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime,
                                                   Transformation t) {
                    v.getLayoutParams().height = interpolatedTime == 1 ? ViewGroup.LayoutParams.WRAP_CONTENT
                            : (int) (targtetHeight * interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };
            a.setDuration((int) (targtetHeight + 500));
            v.startAnimation(a);
        }

    }

    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime,
                                               Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight
                            - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (v.getLayoutParams().height + 5000));
        v.startAnimation(a);
    }

    // slide the view from below itself to the current position
    public void slideUp(View view){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                frameLayoutAlertsNews.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    @Override
    public void fragmentSelected() {

    }

}
