package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.viewmodel;

import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;

import org.jetbrains.annotations.NotNull;

public class AlertsReadedViewModel implements IBaseViewModel {

    @NotNull
    @Override
    public String name() {
        return "AlertReadedViewModel";
    }
}
