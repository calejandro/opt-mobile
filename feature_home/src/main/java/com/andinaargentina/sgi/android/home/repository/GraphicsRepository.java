package com.andinaargentina.sgi.android.home.repository;

import android.content.Context;
import android.util.Log;

import com.andinaargentina.core_services.events.base.BaseHttpErrorEvent;
import com.andinaargentina.core_services.providers.api.clients.graphics.GraphicsService;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.GraphicsDto;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class GraphicsRepository {

    private GraphicsService graphicsService;
    private CompositeDisposable compositeDisposable;

    public GraphicsRepository(GraphicsService graphicsService,
                              CompositeDisposable compositeDisposable) {
        this.graphicsService = graphicsService;
        this.compositeDisposable = compositeDisposable;
    }

    public void cancelRequest(){
        if (this.graphicsService!=null)
            this.graphicsService.cancel();
    }
}
