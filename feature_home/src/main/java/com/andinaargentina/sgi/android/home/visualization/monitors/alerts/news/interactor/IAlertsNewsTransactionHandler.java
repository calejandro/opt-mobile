package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.interactor;

/**
 * Esta interfaz define el contrato
 * entre las respuestas que vienen desde
 * el interactor hacia el repository.
 *
 * La debe implementar el repository
 */
public interface IAlertsNewsTransactionHandler {


}
