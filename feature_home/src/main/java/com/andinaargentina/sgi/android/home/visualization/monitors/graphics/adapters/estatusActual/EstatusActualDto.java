package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual;

public class EstatusActualDto {

    private String name;
    private COLOR_TYPE COLORType;
    private double count;
    private boolean selected;

    public EstatusActualDto(String name, COLOR_TYPE COLORType, double count) {
        this.name = name;
        this.COLORType = COLORType;
        this.count = count;
    }

    public EstatusActualDto() {
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public COLOR_TYPE getCOLORType() {
        return COLORType;
    }

    public void setCOLORType(COLOR_TYPE COLORType) {
        this.COLORType = COLORType;
    }

    public enum COLOR_TYPE {
        FP,
        PO,
        EP,
        PF,
        PP,
        PA,
        PD,
        SV,
        ZERO
    }
}
