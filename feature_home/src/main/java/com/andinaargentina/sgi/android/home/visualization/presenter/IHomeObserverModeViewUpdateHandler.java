package com.andinaargentina.sgi.android.home.visualization.presenter;

import android.content.Context;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.home.visualization.view.HomeObserverModeViewDelegate;
import com.andinaargentina.sgi.android.home.visualization.view.ExtendTurnDialog;
import com.andinaargentina.ui_sdk.components.dialog.ProductionOrderChangeDialog;

import java.util.List;

/**
 * Operaciones de vista para el modo observacion
 */
public interface IHomeObserverModeViewUpdateHandler {

    //<editor-fold desc="Set top bar">

    void setPlantNameOnTopBar(@NonNull String plantNameOnTopBar);

    //</editor-fold>

    //<editor-fold desc="Set data en menu drawer">

    void setUserNameOnHeaderMenuDrawer(@NonNull String userName);

    void setTimerEndTurnInNavHeader();

    void setViewOnObserverMode();

    void setViewOnSupervisorMode();

    //Setear el tipo de usuario
    void setUserMode(@NonNull String name);

    void setUserType(@NonNull String name);

    void setHeaderModeType(boolean isSupervisor);

    void setPlantName(@NonNull String plantName);

    void setDate(@NonNull String date);

    void setTurn(@NonNull String turn);

    void setWorkMangerDate(@NonNull String workMangerDate);

    void setNameShift(@NonNull String shiftName);

    //Mostrar las lineas en el drawer
    void showLinesOnMenuDrawer(@NonNull List<String> listNamesLines, boolean isSupervisorMode);

    //</editor-fold>

    //<editor-fold desc="Set content">

    void setAlertVisualizationModeOnContainer();

    void setAlertSupervisorModeOnContainer();

    void setGraphicsOnContainer(int lineId);

    //</editor-fold>

    //<editor-fold desc="Set bottom menu">

    void setButtomAlertSelected(boolean selected);

    void setButtomGraphicSelected(boolean selected);

    //</editor-fold>

    Context getContext();

    void showLogoutDialog(HomeObserverModeViewDelegate.ILogout logoutCallback);

    void showExtendTurnDialog(@NonNull List<LineDto> listLines,
                              @NonNull ExtendTurnDialog.DialogClickHandler callback,
                              @NonNull String turn);

    void showOrderChangeDialog(@NonNull LineasProduccionDto lineDto, @NonNull ProductionOrderChangeDialog.DialogClickHandler callback);
}
