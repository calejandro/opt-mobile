package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas;

import android.view.View;
import android.widget.TextView;

import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.Depletion;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.KpiDepletion;
import com.andinaargentina.core_services.providers.apiv2.models.KpiDepletionDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.UmbralUtil;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

public class MermasEquiposHolder extends BaseHolder<KpiDepletionDto> {

    private TextView txtName;
    private TextView txtActualValue;
    private TextView txtAcumulatedValue;
    private View container;

    public MermasEquiposHolder(View itemView) {
        super(itemView);
        txtName = itemView.findViewById(R.id.item_merma_txt_name);
        txtActualValue = itemView.findViewById(R.id.item_merma_txt_actual_value);
        txtAcumulatedValue = itemView.findViewById(R.id.item_merma_txt_acumulated_value);
        container = itemView.findViewById(R.id.item_mermas_container);
    }

    @Override
    public void setDataInHolder(KpiDepletionDto data) {
        txtName.setText(data.getDescripcionKPI());
        txtActualValue.setText(String.format("%.2f", data.getKpi()));
        txtAcumulatedValue.setText(String.format("%.2f", data.getKpi()));
        container.setSelected(true);

        /*if(data.getAccumulatedUmbralType()!=null){
            UmbralUtil.UMBRAL_TYPE actual_umbral_type = UmbralUtil.getCOLORType(data.getActualUmbralType());
            switch (actual_umbral_type){
                case NORMAL:{
                    this.txtActualValue.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
                    break;
                }
                case WARNING:{
                    this.txtActualValue.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
                    break;
                }
                case CRITICAL:{
                    this.txtActualValue.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
                    break;
                }
            }
        }*/

       /* if (data.getAccumulatedUmbralType()!=null){
            UmbralUtil.UMBRAL_TYPE acumulated_umbral_type = UmbralUtil.getCOLORType(data.getAccumulatedUmbralType());
            switch (acumulated_umbral_type){
                case NORMAL:{
                    this.txtAcumulatedValue.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
                    break;
                }
                case WARNING:{
                    this.txtAcumulatedValue.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
                    break;
                }
                case CRITICAL:{
                    this.txtAcumulatedValue.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
                    break;
                }
            }
        }*/



    }

}
