package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.azure.FirebaseService;
import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.view.IAlertsNewsViewEventsHandler;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.viewmodel.AlertsNewsViewModel;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.ALERTS_DATA;

public class AlertsNewsPresenter extends BasePresenter<AlertsNewsViewModel>
                                        implements IAlertsNewsViewEventsHandler {

    private AlertsNewsViewModel viewModel;
    private IAlertsNewsViewUpdateHandler viewUpdateHandler;
    private LineasProduccionDto lineDto;
    private AlertsSuccessEvent alertsCache = null;
    private boolean isInformedAlert;

    /**
     *
     * 0- Cargo alertas cacheadas
     * 1- Comparo con alertas cacheadas y agrego las que sean necesarias
     * 2- Comparo con alertas cacheadas y elimino las que sean necesarias
     */

    public AlertsNewsPresenter(@NonNull AlertsNewsViewModel viewModel,
                               @NonNull IAlertsNewsViewUpdateHandler viewUpdateHandler) {
        this.viewModel = viewModel;
        this.viewUpdateHandler = viewUpdateHandler;
        EventBus.getDefault().register(this);
        this.alertsCache = Paper.book("SGI-BOOK").read(ALERTS_DATA);
    }

    @Override
    public void setIsInformedAlert(boolean isInformedAlert){
        this.isInformedAlert = isInformedAlert;
        this.updateAlerts(alertsCache);
    }

    @Nullable
    @Override
    public AlertsNewsViewModel getViewModel() {
        return this.viewModel;
    }

    @NotNull
    @Override
    public String getViewModelName() {
        return viewModel.name();
    }

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewAlert(FirebaseService.OnMessageReceiver receiver) {
        this.viewUpdateHandler.hideEmptyState();
        //TODO agregar alerta
        /*AlertDto newAlert = new AlertDto();
        newAlert.setDescription("Alerta generada en tiempo real");
        newAlert.setCreatedOn(new Date());
        newAlert.setState("New");
        newAlert.setType("Ok");
        this.viewUpdateHandler.addAlert(newAlert);*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateAlerts(AlertsSuccessEvent event){
        if (event!=null){
            Log.i("Alerts","llego data");
            this.viewUpdateHandler.clearList();
            List<LineasProduccionAlertasDto> list = event.getResponse();
            for ( LineasProduccionAlertasDto alertDto: list) {
                if (isInformedAlert){
                    if (alertDto.getEstado().equals("Informada")){
                        this.viewUpdateHandler.addAlert(alertDto);
                    }
                }else{
                    if (alertDto.getEstado().equals("Nueva")){
                        this.viewUpdateHandler.addAlert(alertDto);
                    }
                }
            }
        }
    }

    @Override
    public void onChangeStatusAlert() {
    }

    @Override
    public void onClickAlert(int position) {
        viewUpdateHandler.showConfirmDialog(position);
    }

    @Override
    public void onConfirmAlert(int position) {
        viewUpdateHandler.removeAlert(position);
    }

    @Override
    public void setLineDto(LineasProduccionDto lineDto){
        this.lineDto = lineDto;
    }
}