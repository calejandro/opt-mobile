package com.andinaargentina.sgi.android.home.visualization.presenter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftRequestDto;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftResponseDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderRequestDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.home.visualization.usescases.IHomeObserverModeResponseHandler;
import com.andinaargentina.sgi.android.home.visualization.view.IHomeObserverModeViewEventHandler;
import com.andinaargentina.sgi.android.home.visualization.viewmodel.HomeObserverModeViewModel;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.andinaargentina.sgi.core_sdk.base.times.UtilTimes;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.LINES_SELECTED;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;

public class HomeObserverModePresenter implements IHomeObserverModeViewEventHandler,
        IHomeObserverModeResponseHandler {

    private IHomeObserverModeViewUpdateHandler viewUpdateHandler;
    private IHomeObserverModeRequestHandler requestHandler;
    private HomeObserverModeViewModel viewModel;

    public HomeObserverModePresenter(@NonNull IHomeObserverModeViewUpdateHandler viewUpdateHandler,
                                     @NonNull IHomeObserverModeRequestHandler requestHandler,
                                     @NonNull HomeObserverModeViewModel viewModel) {
        this.viewUpdateHandler = viewUpdateHandler;
        this.requestHandler = requestHandler;
        this.viewModel = viewModel;
        EventBus.getDefault().register(this);
    }

    public void setViewModel(HomeObserverModeViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void onCreatedView() {
        if(this.viewModel == null){
            return;
        }
        if (this.viewModel.isSupervisor()){
            this.viewUpdateHandler.setViewOnSupervisorMode();
        }else{
            this.viewUpdateHandler.setViewOnObserverMode();
        }
        this.viewUpdateHandler.setPlantNameOnTopBar(this.viewModel.getPlantSelected().getDescPlanta());

        setInitialData();
        if (viewModel.isAlertSelected()) {
            setViewOnAlert();
        } else {
            setViewOnGraphics(null);
        }
    }

    private void setInitialData() {
        String typeUser = "";
        String modeUser = "";
        if (viewModel.isUserIsSupervisor()){
            typeUser = "Supervisor";
        }else{
            typeUser = "No supervisor";
        }
        if (viewModel.isSupervisor()){
            modeUser = "Modo supervisor";
        }else{
            modeUser = "Modo visualizador";
        }
        viewUpdateHandler.setUserMode(modeUser);
        viewUpdateHandler.setUserType(typeUser);
        viewUpdateHandler.setHeaderModeType(viewModel.isSupervisor());
        viewUpdateHandler.setUserNameOnHeaderMenuDrawer(viewModel.getNameUser());
        viewUpdateHandler.setPlantName(viewModel.getPlantSelected().getDescPlanta());
        viewUpdateHandler.setDate(viewModel.getDate());
        viewUpdateHandler.setTurn(viewModel.getTurn());
        viewUpdateHandler.setWorkMangerDate(viewModel.getWorkManagerDate());
        viewUpdateHandler.setNameShift(viewModel.getShiftName());
        viewUpdateHandler.showLinesOnMenuDrawer(viewModel.getListNames(),viewModel.isSupervisor());
        viewUpdateHandler.setTimerEndTurnInNavHeader();
    }

    private void setViewOnAlert() {
        viewUpdateHandler.setButtomAlertSelected(true);
        viewUpdateHandler.setButtomGraphicSelected(false);
        if (this.viewModel.isSupervisor())
            viewUpdateHandler.setAlertSupervisorModeOnContainer();
        else
            viewUpdateHandler.setAlertVisualizationModeOnContainer();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setViewOnGraphics(GraphicsOnEvent event) {
        viewUpdateHandler.setButtomAlertSelected(false);
        viewUpdateHandler.setButtomGraphicSelected(true);
        if (event!=null){
            viewUpdateHandler.setGraphicsOnContainer(event.getLineSelectedId());
        }else{
            viewUpdateHandler.setGraphicsOnContainer(-1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setExtendShift(ExtendShiftResponseDto changeOrderRequestDto) {

        //Actualizo el turno extendido
        List<LineasProduccionDto> lineDtoList = Paper.book("SGI-BOOK").read(LINES_SELECTED);
        /*if (lineDtoList!=null){
            for (LineasProduccionDto dto: lineDtoList) {
                if (dto.getIdLinea() == changeOrderRequestDto.getProductionLineId()){
                    dto.setExtendProduccionShift(changeOrderRequestDto.isExtendProductionShift());
                }
            }
        }*/
        Paper.book("SGI-BOOK").write(LINES_SELECTED,lineDtoList);
        //Filtro lineas
        List<LineasProduccionDto> lineSelected = new ArrayList<>();
        if (lineDtoList!=null){
            for (LineasProduccionDto dto: lineDtoList) {
                if (dto.isItemChecked()){
                    lineSelected.add(dto);
                }
            }
        }
        viewModel.setListLines(lineSelected);
        viewUpdateHandler.showLinesOnMenuDrawer(viewModel.getListNames(),viewModel.isSupervisor());
        //TODO Falta actualizar titulos de viewpager
    }

    @Override
    public void onSelectedLine(int position) {
        //TODO redirigir a la linea seleccionada
    }

    @Override
    //TODO Para esto vamos a tener que llamar a la api que nos devuelve las ordenes
    public void onChangeOrderInLine(int position) {
        LineasProduccionDto lineSelected = viewModel.getListLines().get(position);
        viewUpdateHandler.showOrderChangeDialog(lineSelected, idOrderSelected -> {
            SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
            ChangeOrderRequestDto dto = new ChangeOrderRequestDto(lineSelected.getIdLinea(),idOrderSelected,user.getUserId());
            requestHandler.changeOrder(viewUpdateHandler.getContext(), dto);
            //lineSelected.setOrderIdSelected(idOrderSelected);
            //lineSelected.setLineasProduccionUsuarioDto(idOrderSelected);
            viewUpdateHandler.showLinesOnMenuDrawer(viewModel.getListNames(),viewModel.isSupervisor());
        });
    }

    @Override
    public void onAlertsInformed() {
        //TODO ir a configuración
        Log.d("Home","Click en configuración");
        ARouter.getInstance()
                .build(ViewsNames.ROUTE_VIEW_NAME_ALERTS_INFORMED)
                .navigation(this.viewUpdateHandler.getContext());
    }

    @Override
    public void onLogout() {
        this.viewUpdateHandler.showLogoutDialog((isShiftEnded) -> {
            //TODO se podría manejar el caso de error
            requestHandler.logout(viewUpdateHandler.getContext(), isShiftEnded);
            UtilSesion.clearSession();
        });
    }

    @Override
    public void onExtendTurn() {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        /*this.viewUpdateHandler.showExtendTurnDialog(this.viewModel.getListLines(),
                idLineSelected -> {
                    Log.i("ExtendTurn","Se selecciono la linea" + idLineSelected);
                    ExtendShiftRequestDto dto = new ExtendShiftRequestDto(idLineSelected,user.getMUserId(),true);
                    requestHandler.extendShift(viewUpdateHandler.getContext(), dto);
                    UtilTimes.setAlarmForChangeTurn(true);
                    Prefs.putBoolean("isShiftExtended",true);
                },UtilTimes.getTurnByTime());*/
    }

    @Override
    public void onAlertIconPressed() {
        setViewOnAlert();
    }

    @Override
    public void onGraphicIconPressed() {
        setViewOnGraphics(null);
    }

    public static class GraphicsOnEvent{
        private int lineSelectedId;

        public GraphicsOnEvent(int lineSelectedId) {
            this.lineSelectedId = lineSelectedId;
        }

        public int getLineSelectedId() {
            return lineSelectedId;
        }

        public void setLineSelectedId(int lineSelectedId) {
            this.lineSelectedId = lineSelectedId;
        }
    }

}
