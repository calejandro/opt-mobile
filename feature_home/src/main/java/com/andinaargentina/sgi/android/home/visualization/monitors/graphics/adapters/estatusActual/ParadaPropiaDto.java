package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual;

public class ParadaPropiaDto {

    private String valueMinute;
    private String valueHour;
    private String threshold;

    public ParadaPropiaDto() {
    }

    public ParadaPropiaDto(String valueMinute, String valueHour, String threshold) {
        this.valueMinute = valueMinute;
        this.valueHour = valueHour;
        this.threshold = threshold;
    }



    public String getValueMinute() {
        return valueMinute;
    }

    public void setValueMinute(String valueMinute) {
        this.valueMinute = valueMinute;
    }

    public String getValueHour() {
        return valueHour;
    }

    public void setValueHour(String valueHour) {
        this.valueHour = valueHour;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }
}
