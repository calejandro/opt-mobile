package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.view;

import android.app.Dialog;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view.AlertHolder;
import com.andinaargentina.ui_sdk.R;
import com.andinaargentina.ui_sdk.components.dialog.SendReportDialog;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ConfirmReadAlertDialog  extends Dialog {

    private TextView txtCancel, txtOk;
    private ImageView imgClose;
    private SendReportDialog.DialogClickHandler clickHandler;
    private int itemSelectedPosition;
    private LineasProduccionAlertasDto alertDto;

    private View rootViewContainer;
    private ImageView imgClockIcon;
    private TextView txtMinutes;
    private TextView txtSubMinutes;
    private View imgBackgroundIcon;
    private TextView txtTitle;
    private TextView txtSubtitleDate;
    private TextView txtEndDate;
    private TextView txtIsActive;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == txtCancel || v == imgClose) {
                clickHandler.negativeClick();
            } else if (v == txtOk) {
                clickHandler.positiveClick(itemSelectedPosition);
            }
        }
    };

    public ConfirmReadAlertDialog(@NonNull Context context, @NonNull SendReportDialog.DialogClickHandler clickHandler,
                                  int itemSelectedPosition,  @NonNull LineasProduccionAlertasDto alertDto){
        super(context, com.andinaargentina.ui_sdk.R.style.ThemeDialogCustom);
        this.clickHandler = clickHandler;
        this.itemSelectedPosition = itemSelectedPosition;
        this.alertDto = alertDto;
        initParentView(context);
    }

    private void initParentView(@NonNull Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_confirm_read_alert, null);
        setContentView(view);
        initViewComponents();
    }

    private void initViewComponents() {

        rootViewContainer = findViewById(R.id.dialog_confirm_read_alert_root_view);
        txtCancel = findViewById(R.id.txt_cancel);
        txtOk = findViewById(R.id.txt_ok);
        imgClockIcon = findViewById(R.id.read_alert_clock_icon);

        txtEndDate = findViewById(R.id.alert_read_txt_end_date);
        txtIsActive = findViewById(R.id.alert_read_txt_is_active);
        txtMinutes = findViewById(R.id.read_alert_txt_minutes);
        txtSubMinutes = findViewById(R.id.read_alert_sub_txt_minutes);

        imgBackgroundIcon = findViewById(R.id.read_alert_selector_background);
        txtTitle = findViewById(R.id.alert_read_txt_title);
        txtSubtitleDate = findViewById(R.id.alert_read_txt_date_subtitle);
        imgClose = findViewById(R.id.change_site_dialog_btn_close);
        imgClose.setOnClickListener(onClickListener);
        txtCancel.setOnClickListener(onClickListener);
        txtOk.setOnClickListener(onClickListener);
        setCancelable(false);
        bindData();
    }

    private void bindData() {
        if (alertDto.getClasificacion().equals("Merma")){
            NumberFormat formatter = new DecimalFormat("#0.0");
            String value = formatter.format(alertDto.getValorAcumulado()) + "%";
            txtMinutes.setText(value);
        }else{
            txtMinutes.setText(String.valueOf((int) alertDto.getValorAcumulado().intValue()));
        }
        if (alertDto.getClasificacion().equals("Merma")){
            setDecreaseAlert();
        }else{
            setNormalAlert();
        }
        if (alertDto.getFechaFin() == null){
            txtIsActive.setText("Alerta activa");
            txtEndDate.setVisibility(View.GONE);
        }else{
            txtIsActive.setText("Alerta Finalizada");
            txtEndDate.setVisibility(View.VISIBLE);
            txtEndDate.setText(formatDate(alertDto.getFechaFin()));
        }
        switch (alertDto.getTipoAlerta()){
            case "Atencion":{
                rootViewContainer.setBackgroundResource(R.drawable.bg_confirm_readed_alert_orange);
                int color = getContext().getResources().getColor(R.color.color_bg_alert_orange);
                txtMinutes.setTextColor(color);
                break;
            }
            case "Peligro":{
                rootViewContainer.setBackgroundResource(R.drawable.bg_confirm_readed_alert_red);
                int color = getContext().getResources().getColor(R.color.color_bg_alert_red);
                txtMinutes.setTextColor(color);
                break;
            }
            case "Fallo":{
                rootViewContainer.setBackgroundResource(R.drawable.bg_confirm_readed_alert_black);
                int color = getContext().getResources().getColor(R.color.color_bg_alert_black);
                txtMinutes.setTextColor(color);
                break;
            }
            default:{
                break;
            }
        }
        this.txtTitle.setText(alertDto.getEquipo().getDescEquipo());
        this.txtSubtitleDate.setText(formatDate(alertDto.getFechaCreacion()));
    }

    private void setNormalAlert() {
        this.imgBackgroundIcon.setVisibility(View.VISIBLE);
        this.txtMinutes.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(com.andinaargentina.sgi.android.feature_home.R.dimen._30ssp));
        this.imgClockIcon.setImageResource(com.andinaargentina.sgi.android.feature_home.R.drawable.i_clock_black);
        this.txtSubMinutes.setText("MINUTOS");
    }

    private void setDecreaseAlert() {
        this.imgBackgroundIcon.setVisibility(View.GONE);
        this.txtMinutes.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(com.andinaargentina.sgi.android.feature_home.R.dimen._20ssp));
        this.imgClockIcon.setImageResource(com.andinaargentina.sgi.android.feature_home.R.drawable.icon_merma_dark);
        this.txtSubMinutes.setText("MERMA");
    }

    private String formatDate(Date originalDate){
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm a");
        String formattedDate = targetFormat.format(originalDate);  // 20120821
        return formattedDate;
    }

}
