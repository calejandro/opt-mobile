package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.eficienciaTurno;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency.CombinedChartDto;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency.EfficiencyLineChart;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency.ProductionBarChart;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.shiftEfficiency.ShiftEfficiencyDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Orientation;
import com.anychart.enums.ScaleStackMode;
import com.anychart.scales.Linear;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class EficienciaTurnoFragment extends FrameworkBaseFragment {

    public static final String KEY_INPUT_DATA = "KEY_EFFICIENCY_DATA";

    private List<ProduccionEficienciaDto> shiftEfficiencyDto;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState, R.layout.layout_column_bar_simple);
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        this.shiftEfficiencyDto = (List<ProduccionEficienciaDto>) getArguments().getSerializable(KEY_INPUT_DATA);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void startCombinedChart(){

        RelativeLayout containerChart = getView().findViewById(R.id.container_chart);
        AnyChartView anyChartView = new AnyChartView(this.getContext());
        anyChartView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        APIlib.getInstance().setActiveAnyChartView(anyChartView);
        anyChartView.setProgressBar(getView().findViewById(R.id.progress_bar));

        Cartesian cartesian = AnyChart.cartesian();
        cartesian.animation(true);

        cartesian.yScale().minimum(0d);
        cartesian.xAxis(0).title("Envases");
        cartesian.xAxis(0).labels().rotation(-90);
        cartesian.yAxis(0).title("Horas");
        cartesian.yAxis(1).title("Eficiencia").stroke("#FD963B");
        cartesian.yAxis(1).title().fontColor("#FD963B");
        cartesian.yAxis(1).title().fontWeight("bold");
        cartesian.yScale().stackMode(ScaleStackMode.VALUE);

        //Configuración de eje derecho de eficiencia (naranja)
        Linear scalesLinear = Linear.instantiate();
        scalesLinear.minimum(0d);
        scalesLinear.maximum(100);
        scalesLinear.ticks().interval(5);
        com.anychart.core.axes.Linear extraYAxis = cartesian.yAxis(1d);
        extraYAxis.orientation(Orientation.RIGHT)
                .stroke("#FD963B")
                .scale(scalesLinear);
        extraYAxis.labels().fontColor("#FD963B");
        extraYAxis.labels().background().enabled(true);
        extraYAxis.labels().background().stroke("#FD963B");
        extraYAxis.labels().background().cornerType("round");
        extraYAxis.labels().background().corners(3);
        extraYAxis.labels()
                .padding(2d, 5d, 2d, 5d)
                .format("{%Value}%");

        //Seteo datos para grafico de barras azul
        List<DataEntry> dataEntriesForColumnBar = new ArrayList<>();

        for (ProduccionEficienciaDto value : shiftEfficiencyDto) {
                dataEntriesForColumnBar.add(new CustomDataEntry(simpleDateFormat.format(value.getFechaHora()),
                        value.getBotellasProducidas()));
        }

        Set setColumnBar = Set.instantiate();
        setColumnBar.data(dataEntriesForColumnBar);
        Mapping column1Data = setColumnBar.mapAs("{ x: 'x', value: 'value2' }");
        cartesian.column(column1Data).fill("#1A528F").stroke("#1A528F").tooltip().format("Envases: {%value}") ;

        //Seteo datos para grafico de línea
        List<DataEntry> dataEntriesForGraphicLine = new ArrayList<>();
        for (ProduccionEficienciaDto value : shiftEfficiencyDto) {
            dataEntriesForGraphicLine.add(new CustomDataEntry(simpleDateFormat.format(value.getFechaHora()),
                    value.getEficiencia()));
        }

        Set setGraphicLine = Set.instantiate();
        setGraphicLine.data(dataEntriesForGraphicLine);
        Mapping lineData = setGraphicLine.mapAs("{ x: 'x', value: 'value' }");
        Line line = cartesian.line(lineData).stroke("2 #FD963B");
        line.yScale(scalesLinear);

        anyChartView.setChart(cartesian);
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                containerChart.removeAllViews();
                containerChart.addView(anyChartView);
                containerChart.refreshDrawableState();
            }
        });
    }

    public static class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value) {
            super(x, value);
        }
    }

    @Override
    public String getTagFragment() {
        return null;
    }
}
