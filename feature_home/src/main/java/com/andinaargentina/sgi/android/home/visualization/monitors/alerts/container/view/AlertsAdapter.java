package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;

import java.util.List;

public class AlertsAdapter extends BaseAdapter<LineasProduccionAlertasDto> {

    public AlertsAdapter(@NonNull List<LineasProduccionAlertasDto> dataList,
                         @NonNull IHolderItemClick clickListener){
        super(dataList, clickListener);
    }

    @NonNull
    @Override
    public AlertHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alert_standar,parent,false);
        return new AlertHolder(v);
    }
}
