package com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.events.efficiency.EfficiencyErrorEvent;
import com.andinaargentina.core_services.events.efficiency.EfficiencySuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyDto;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyLineDto;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionEficienciaMesDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaByShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.utils.Order;
import com.andinaargentina.core_services.providers.apiv2.utils.Orders;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode.repository.EfficiencyRepository;
import com.andinaargentina.sgi.android.home.visualization.presenter.HomeObserverModePresenter;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.andinaargentina.RepositoryConst.LINES_SELECTED;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_VIEW_NAME_VISUALIZATION_MODE;

@Route(path = ROUTE_VIEW_NAME_VISUALIZATION_MODE)
@RequiresApi(api = Build.VERSION_CODES.N)
public class VisualizationModeFragment extends FrameworkBaseFragment {

    private VisualizationModeAdapter adapter;
    private RecyclerView recyclerView;

    private TextView txtTitle;
    private TextView txtGralDiaria;
    private TextView txtGralTurno;
    private TextView txtGralObjetivo;

    private TextView txtEmptyState;
    private View containerData;

    private ToggleButton toggleButton;

    private EfficiencySuccessEvent successEvent;
    private boolean isPlanMode;
    //private EfficiencyRepository efficiencyRepository;
    private List<LineasProduccionDto> lineDtoList;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(UtilSesion.checkSession(this.getActivity())){
            return;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState,
                R.layout.fragment_mode_visualization_lines);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lineDtoList = Paper.book("SGI-BOOK").read(LINES_SELECTED);
        recyclerView = getView().findViewById(R.id.visualization_mode_recycler);
        txtTitle = view.findViewById(R.id.visualization_mode_txt_title);
        txtGralDiaria = view.findViewById(R.id.efficiency_gral_diaria);
        txtGralObjetivo = view.findViewById(R.id.efficiency_gral_objetivo);
        txtGralTurno = view.findViewById(R.id.efficiency_gral_turno);
        txtEmptyState = view.findViewById(R.id.visualization_mode_empty_state);
        containerData = view.findViewById(R.id.visualization_mode_list_data);
        toggleButton = view.findViewById(R.id.visualization_mode_toggle);
        toggleButton.setChecked(false);
        toggleButton.setOnClickListener((v -> onChangeMode()));
        txtTitle.setText("Eficiencia nominal");
        txtEmptyState.setText("Cargando datos de líneas ...");
        showListLines();
        containerData.setVisibility(View.VISIBLE);
        txtEmptyState.setVisibility(View.GONE);

    }

    private void onChangeMode() {
        isPlanMode =!isPlanMode;
        toggleButton.setChecked(toggleButton.isChecked());
        String title;
        if (toggleButton.isChecked()){
            title = "Eficiencia nominal";
        }else{
            title = "Eficiencia de plan";
        }
        txtTitle.setText(title);
        onEfficiencySuccess(successEvent);
    }

    public void showListLines(){
        adapter = new VisualizationModeAdapter(isPlanMode, lineDtoList, (caller, position, bundle) -> {
            //Abrir fragmento de graficos
            //Envíar la línea seleccionada
            Log.d(getTagFragment(),"Hizo click en position " + position);
            EventBus.getDefault().post(new HomeObserverModePresenter.GraphicsOnEvent(lineDtoList.get(position).getIdLinea()));
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        Orders orders = new Orders();
        orders.getOrders().add(new Order("fechaCreacion", true));
        List<Integer> linesIds = new ArrayList<>();
        for (LineasProduccionDto lineas:
             lineDtoList) {
            linesIds.add(lineas.getIdLinea());
        }
        OptimusServices.getInstance().getLineasProduccionEficienciaMesService().list(null, lineDtoList.get(0).getIdPlanta(), orders.toJson()).enqueue(new Callback<List<LineasProduccionEficienciaMesDto>>() {
            @Override
            public void onResponse(Call<List<LineasProduccionEficienciaMesDto>> call, Response<List<LineasProduccionEficienciaMesDto>> response) {
                String totalObjectiveEfficiency = String.format("%.1f", response.body().get(0).getEficienciaMensual()) + " %";
                txtGralObjetivo.setText(totalObjectiveEfficiency);

                ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
                    @Override
                    public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                        getEfificenciaDiariaPlanta(response.body().get(0).getEficienciaMensual(), linesIds,
                                simpleDateFormat.format(ShiftUtils.getInstance().getShifts().get(0).getInicio()),
                                simpleDateFormat.format(ShiftUtils.getInstance().getShifts().get(2).getFin()));

                        getEfificenciaTurnoPlanta(response.body().get(0).getEficienciaMensual(), linesIds,
                                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()));
                    }
                });

            }

            @Override
            public void onFailure(Call<List<LineasProduccionEficienciaMesDto>> call, Throwable t) {

            }
        });
    }

    private void getEfificenciaDiariaPlanta(double objetivoPlanta, List<Integer> idLineas, String inicio, String fin) {
        OptimusServices.getInstance().getEficienciaService().listByShift(idLineas, inicio, fin).enqueue(new Callback<ArrayList<ProduccionEficienciaByShiftDto>>() {
            @Override
            public void onResponse(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Response<ArrayList<ProduccionEficienciaByShiftDto>> response) {
                double total = 0;
                for (ProduccionEficienciaByShiftDto eficiencia:
                     response.body()) {
                    total += eficiencia.getEficiencia();
                }

                double eficiencia =  (total / response.body().size());
                String totalEfficiency = String.format("%.1f", eficiencia) + " %";

                txtGralDiaria.setText(totalEfficiency);
                int drawableForShift = 0;
                if (eficiencia <= objetivoPlanta){
                    drawableForShift = R.drawable.bg_visualization_mode_red;
                }else{
                    drawableForShift = R.drawable.bg_visualization_mode_green;
                }
                txtGralDiaria.setBackgroundResource(drawableForShift);
            }

            @Override
            public void onFailure(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Throwable t) {

            }
        });
    }

    private void getEfificenciaTurnoPlanta(double objetivoPlanta, List<Integer> idLineas, String inicio, String fin) {
        OptimusServices.getInstance().getEficienciaService().listByShift(idLineas, inicio, fin).enqueue(new Callback<ArrayList<ProduccionEficienciaByShiftDto>>() {
            @Override
            public void onResponse(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Response<ArrayList<ProduccionEficienciaByShiftDto>> response) {
                double total = 0;
                for (ProduccionEficienciaByShiftDto eficiencia:
                        response.body()) {
                    total += eficiencia.getEficiencia();
                }
                double eficiencia =  (total / response.body().size());
                String totalEfficiency = String.format("%.1f", eficiencia) + " %";

                txtGralTurno.setText(totalEfficiency);
                int drawableForShift = 0;
                if (eficiencia <= objetivoPlanta){
                    drawableForShift = R.drawable.bg_visualization_mode_red;
                }else{
                    drawableForShift = R.drawable.bg_visualization_mode_green;
                }
                txtGralTurno.setBackgroundResource(drawableForShift);

            }

            @Override
            public void onFailure(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Throwable t) {

            }
        });
    }

    @Override
    public String getTagFragment() {
        return "Eficiencia fragment";
    }

    @SuppressLint("DefaultLocale")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEfficiencySuccess(EfficiencySuccessEvent event) {
        showListLines();
        /*this.successEvent = event;
        if (event !=null && event.getResponse() !=null && !event.getResponse().isEmpty()){
            EfficiencyLineDto efficiencyDto = event.getResponse().get(0);
            String totalShiftEfficiency;
            String totalObjectiveEfficiency;
            String totalDailyEfficiency;

            int drawableForShift = 0;
            int drawableForDaily = 0;

            if (isPlanMode){
                totalShiftEfficiency = String.format("%.1f", efficiencyDto.getTotalShifPlanEfficiencyPercentaje()) + " %";
                totalObjectiveEfficiency = String.format("%.1f", efficiencyDto.getTotalObjectivePlanEfficiencyPercentaje()) + " %";
                totalDailyEfficiency = String.format("%.1f", efficiencyDto.getTotalDailyPlanEfficiencyPercentaje()) + " %";
                //Calculo el color en función del objetivo
                if (efficiencyDto.getTotalShifPlanEfficiencyPercentaje()<=efficiencyDto.getTotalObjectivePlanEfficiencyPercentaje()){
                    drawableForShift = R.drawable.bg_visualization_mode_red;
                }else{
                    drawableForShift = R.drawable.bg_visualization_mode_green;
                }
                if (efficiencyDto.getTotalDailyPlanEfficiencyPercentaje()<=efficiencyDto.getTotalObjectivePlanEfficiencyPercentaje()){
                    drawableForDaily = R.drawable.bg_visualization_mode_red;
                }else{
                    drawableForDaily = R.drawable.bg_visualization_mode_green;
                }
            }else{
                totalShiftEfficiency = String.format("%.1f", efficiencyDto.getTotalShifEfficiencyPercentaje()) + " %";
                totalObjectiveEfficiency = String.format("%.1f", efficiencyDto.getTotalObjectiveEfficiencyPercentaje()) + " %";
                totalDailyEfficiency = String.format("%.1f", efficiencyDto.getTotalDailyEfficiencyPercentaje()) + " %";
                //Calculo el color en función del objetivo
                if (efficiencyDto.getTotalShifEfficiencyPercentaje()<=efficiencyDto.getTotalObjectiveEfficiencyPercentaje()){
                    drawableForShift = R.drawable.bg_visualization_mode_red;
                }else{
                    drawableForShift = R.drawable.bg_visualization_mode_green;
                }
                if (efficiencyDto.getTotalDailyEfficiencyPercentaje()<=efficiencyDto.getTotalObjectiveEfficiencyPercentaje()){
                    drawableForDaily = R.drawable.bg_visualization_mode_red;
                }else{
                    drawableForDaily = R.drawable.bg_visualization_mode_green;
                }
            }

            txtGralTurno.setBackgroundResource(drawableForShift);
            txtGralTurno.setText(totalShiftEfficiency);
            txtGralObjetivo.setText(totalObjectiveEfficiency);
            txtGralDiaria.setBackgroundResource(drawableForDaily);
            txtGralDiaria.setText(totalDailyEfficiency);
            if (efficiencyDto.getListLines() == null || efficiencyDto.getListLines().isEmpty()){
                txtEmptyState.setText("No hay líneas seleccionadas para visualizar");
                txtEmptyState.setVisibility(View.VISIBLE);
                containerData.setVisibility(View.GONE);
            }else{
                txtEmptyState.setVisibility(View.GONE);
                containerData.setVisibility(View.VISIBLE);
            }


        }else{
            txtEmptyState.setText("No hay líneas seleccionadas para visualizar");
            txtEmptyState.setVisibility(View.VISIBLE);
            containerData.setVisibility(View.GONE);
        }*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEfficiencyError(EfficiencyErrorEvent event){
        if (event.getError().isVpnError())
            txtEmptyState.setText(getString(R.string.vpn_error));
        txtEmptyState.setVisibility(View.VISIBLE);
        containerData.setVisibility(View.GONE);
    }

}
