package com.andinaargentina.sgi.android.home.visualization.usescases;

/**
 * Esta interfaz define el contrato
 * entre las respuestas que vienen desde
 * el interactor hacia el presenter
 * La debe implementar el presenter
 */
public interface IHomeObserverModeResponseHandler {

}
