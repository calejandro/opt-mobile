package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.view.IAlertsReadedViewEventsHandler;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.viewmodel.AlertsReadedViewModel;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.ALERTS_DATA;

public class AlertsReadedPresenter extends BasePresenter<AlertsReadedViewModel>
                                        implements IAlertsReadedViewEventsHandler {

    private AlertsReadedViewModel viewModel;
    private IAlertsReadedViewUpdateHandler viewUpdateHandler;

    private LineasProduccionDto lineDto;
    private AlertsSuccessEvent alertsCache = null;


    public AlertsReadedPresenter(@NonNull AlertsReadedViewModel viewModel,
                                 @NonNull IAlertsReadedViewUpdateHandler viewUpdateHandler) {
        this.viewModel = viewModel;
        this.viewUpdateHandler = viewUpdateHandler;
        EventBus.getDefault().register(this);
        this.alertsCache = Paper.book("SGI-BOOK").read(ALERTS_DATA);

    }

    @Nullable
    @Override
    public AlertsReadedViewModel getViewModel() {
        return this.viewModel;
    }

    @NotNull
    @Override
    public String getViewModelName() {
        return viewModel.name();
    }

    @Override
    public void onNewAlert() {
        //TODO ver como hacer cuando llegue el
        //evento
    }

    @Override
    public void onChangeStatusAlert() {
        //TODO ver como hacer cuando llegue el
        //evento
    }

    @Override
    public void onClickAlert(int position) {
        viewUpdateHandler.showConfirmDialog(position);
    }

    @Override
    public void onConfirmAlert(int position) {
        viewUpdateHandler.removeAlert(position);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateAlerts(AlertsSuccessEvent event){
        if (event!=null){
            Log.i("Alerts","llego data");
            this.viewUpdateHandler.clearList();
            List<LineasProduccionAlertasDto> list = event.getResponse();
            for ( LineasProduccionAlertasDto alertLineDto: list) {
                if (alertLineDto.getIdLinea() == lineDto.getIdLinea()){
                    if (alertLineDto.getEstado().equals("Informada")){
                        this.viewUpdateHandler.addAlert(alertLineDto);
                    }
                }
            }
        }
    }

    @Override
    public void setLineDto(LineasProduccionDto lineDto){
        this.lineDto = lineDto;
        this.updateAlerts(alertsCache);
    }

    public LineasProduccionDto getLineDto() {
        return lineDto;
    }
}
