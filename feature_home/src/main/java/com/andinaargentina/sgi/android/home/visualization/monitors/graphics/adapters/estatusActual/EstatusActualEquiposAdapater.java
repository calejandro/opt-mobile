package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.util.List;

public class EstatusActualEquiposAdapater extends BaseAdapter<MachineActualStatusDto> {

    public EstatusActualEquiposAdapater(List<MachineActualStatusDto> dataList, IHolderItemClick listener) {
        super(dataList,listener);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public BaseHolder<MachineActualStatusDto> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_performace_actual_equipos_status,parent,false);
        return new EstatusActualEquiposHolder(v);
    }
}
