package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas;

public class TimeHistogramDataDto {

    private String hour;
    private String umbralType;
    private double value;

    public String getHour() {

        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getUmbralType() {
        return umbralType;
    }

    public void setUmbralType(String umbralType) {
        this.umbralType = umbralType;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
