package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual;

import android.os.Build;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionAlertasParadaMaquinaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionUmbralesTmefTmprDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.models.TimeStopsDto;
import com.andinaargentina.core_services.providers.apiv2.utils.ConfiguracionAlertasUtils;
import com.andinaargentina.core_services.providers.apiv2.utils.ConfiguracionUmbralesUtils;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.UmbralUtil;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.statusactual.EstatusActualViewDelegate;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@RequiresApi(api = Build.VERSION_CODES.N)
public class EstatusActualParadaPropiaHolder extends BaseHolder<TimeStopsDto> {

    private TextView txtHour;
    private TextView txtMinute;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH");
    private LineasProduccionDto lineDto;
    private List<MachineActualStatusDto> graphicsDto;
    private EstatusActualViewDelegate estatusActualDelegate;

    public EstatusActualParadaPropiaHolder(View itemView) {
        super(itemView);
        bindView(itemView);
    }

    private void bindView(View view) {
        txtHour = view.findViewById(R.id.item_parada_propia_txt_hour);
        txtMinute = view.findViewById(R.id.item_parada_propia_txt_minute);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void setDataInHolder(TimeStopsDto data) {
        Integer minutosAcumulados = data.getMinutosAcumulados();
        txtHour.setText(simpleDateFormat.format(data.getHora()));
        txtMinute.setText(String.valueOf(minutosAcumulados));

        Integer idEquipo = data.getIdEquipo();
        List<ConfiguracionAlertasParadaMaquinaDto> estadosAlertas = ConfiguracionAlertasUtils.getInstance().getEstadosAlertas(data.getIdLinea());

        Optional<ConfiguracionAlertasParadaMaquinaDto> optional =estadosAlertas.stream().filter(f ->
                idEquipo.equals(f.getIdEquipo()) && (data.getMinutosAcumulados() > f.getValorMinimo() &&
                        (data.getMinutosAcumulados() < f.getValorMaximo())) ||
                        (data.getMinutosAcumulados() >= f.getValorMinimo() && f.getValorMaximo() == null)).findFirst();

        txtMinute.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
        if(optional.isPresent()) {
            if (data.getMinutosAcumulados() > optional.get().getValorMinimo()
            && data.getMinutosAcumulados() <= optional.get().getValorMaximo()){
                txtMinute.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
            } else if(data.getMinutosAcumulados() > optional.get().getValorMaximo()) {
                txtMinute.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
            }
        }

    }

}
