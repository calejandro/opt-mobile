package com.andinaargentina.sgi.android.home.visualization.monitors.graphics;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.andinaargentina.sgi.android.feature_home.R;
import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.LegendLayout;
import com.anychart.enums.Orientation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ColumnChartActivity extends AppCompatActivity {

    private Set set;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_common);

        AnyChartView anyChartView = findViewById(R.id.any_chart_view);
        APIlib.getInstance().setActiveAnyChartView(anyChartView);


        Cartesian column = AnyChart.column();

        List<DataEntry> seriesData = new ArrayList<>();
        seriesData.add(new CustomDataEntry("1990", 10, 40, 10));
        seriesData.add(new CustomDataEntry("1991", 40, 5, 50));
        seriesData.add(new CustomDataEntry("1992", 80, 50, 30));
        seriesData.add(new CustomDataEntry("1993", 80, 20, 90));
        seriesData.add(new CustomDataEntry("1994", 40, 80, 85));
        seriesData.add(new CustomDataEntry("1995", 15, 50, 60));

        set = Set.instantiate();
        set.data(seriesData);
        Mapping series1Data = set.mapAs("{ x: 'x', value: 'value' }");
        Mapping series2Data = set.mapAs("{ x: 'x', value: 'value2' }");
        Mapping series3Data = set.mapAs("{ x: 'x', value: 'value3' }");

        Column series1 = column.column(series1Data);
        series1.name("Company 1")
                .color("#00ff00");

        Column series2 = column.column(series2Data);
        series2.name("Company 2")
                .color("#0000ff");

        Column series3 = column.column(series3Data);
        series3.name("Company 3")
                .color("#ffff00");

        column.xAxis(0).orientation(Orientation.TOP)
                .stroke(null)
                .ticks(false);
        column.xGrid(0).enabled(true);

        column.legend(true);
        column.legend()
                .position(Orientation.RIGHT)
                .itemsLayout(LegendLayout.VERTICAL);

        anyChartView.setChart(column);


        Button btnAbc = findViewById(R.id.btnAbc);
        btnAbc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<DataEntry> seriesData = new ArrayList<>();
                seriesData.add(new CustomDataEntry("Espresso", new Random().nextInt(10), new Random().nextInt(10), new Random().nextInt(10)));
                set.data(seriesData);
            }
        });
    }

    private class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value, Number value2, Number value3) {
            super(x, value);
            setValue("value2", value2);
            setValue("value3", value3);
        }
    }
}
