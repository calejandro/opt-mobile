package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.statusactual;

public class SelectMachineEvent {
    private int idEquipo;
    private int idLinea;

    public SelectMachineEvent(int idEquipo, int idLinea) {
        this.idEquipo = idEquipo;
        this.idLinea = idLinea;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }
}
