package com.andinaargentina.sgi.android.home.visualization.alertinformed;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.linescontainer.LinePageAdapter;
import com.andinaargentina.sgi.core_sdk.base.workers.GoToLoginEvent;
import com.danielecampogiani.underlinepageindicator.UnderlinePageIndicator;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.LINES_SELECTED;

@Route(path = ViewsNames.ROUTE_VIEW_NAME_ALERTS_INFORMED)
public class AlertsInformedActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{

    //Toolbar
    private View btnBack;
    private View imgChangeSite;
    private TextView titleBar;

    private TextView txtEmptyState;

    TabLayout tabs;
    LinePageAdapter linePageAdapter;
    public static final String KEY_IS_GRAPHICS = "IS_GRAPHICS";
    public static final String KEY_LINE_ID_SELECTED = "ID_SELECTED";

    private ViewPager mViewPager;
    private UnderlinePageIndicator mBottomNavigationTabStrip;
    private boolean isGraphics;
    private int lineSelectedId = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alerts_informed_activity);
        Bundle b = getIntent().getExtras();
        if (b!=null){
            isGraphics = b.getBoolean(KEY_IS_GRAPHICS);
            if (b.containsKey(KEY_LINE_ID_SELECTED) && b.getInt(KEY_LINE_ID_SELECTED)>=0)
                lineSelectedId = b.getInt(KEY_LINE_ID_SELECTED);
        }
        initViews();
    }

    private void initViews() {
        //Toolbar
        imgChangeSite = findViewById(R.id.toolbar_img);
        btnBack = findViewById(R.id.toolbar_menu);
        titleBar = findViewById(R.id.toolbar_menu_title);
        txtEmptyState = findViewById(R.id.alerts_empty_state_text_view);

        txtEmptyState.setText("No tiene alertas informadas");
        titleBar.setText("Alertas informadas");
        imgChangeSite.setVisibility(View.GONE);
        this.btnBack.setOnClickListener(v -> finish());

        tabs = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.vp);
        mBottomNavigationTabStrip = findViewById(R.id.nts_bottom);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerObserver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterObserver();
    }

    private void registerObserver() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unregisterObserver() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void setUI() {
        List<LineasProduccionDto> filteredList = new ArrayList<>();
        List<LineasProduccionDto> listLines = Paper.book("SGI-BOOK").read(LINES_SELECTED,new ArrayList<>());
        for (LineasProduccionDto dto: listLines) {
            if (dto.isItemChecked())
                filteredList.add(dto);
        }
        linePageAdapter = new LinePageAdapter(getSupportFragmentManager(),
                filteredList,isGraphics, true);

        mViewPager.setAdapter(linePageAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.addOnPageChangeListener(this);
        // do this in a runnable to make sure the viewPager's views are already instantiated before triggering the onPageSelected call
        mViewPager.post(() -> this.onPageSelected(mViewPager.getCurrentItem()));

        if (filteredList.size()>3){
            tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        mBottomNavigationTabStrip.setTabLayoutAndViewPager(tabs,mViewPager);
        int positionPage = 0;
        for (int i = 0; i < filteredList.size(); i++) {
            if (filteredList.get(i).getIdLinea() == lineSelectedId){
                positionPage = i;
            }
        }
        mViewPager.setCurrentItem(positionPage);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        new Handler().postDelayed(() -> {
            Log.d("Graphics","onPageSelected: " + position);
        }, 500);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d("Graphics","onPageScrollStateChanged");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGoToLogin(GoToLoginEvent event){
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(this);
        this.finish();
    }
}
