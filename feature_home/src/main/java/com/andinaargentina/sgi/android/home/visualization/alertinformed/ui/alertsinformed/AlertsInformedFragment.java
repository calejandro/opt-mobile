package com.andinaargentina.sgi.android.home.visualization.alertinformed.ui.alertsinformed;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andinaargentina.sgi.android.feature_home.R;

public class AlertsInformedFragment extends Fragment {

    private AlertsInformedViewModel mViewModel;

    public static AlertsInformedFragment newInstance() {
        return new AlertsInformedFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.alerts_informed_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AlertsInformedViewModel.class);
        // TODO: Use the ViewModel
    }

}
