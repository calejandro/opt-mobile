package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.InformAlertResponseDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.InformAlertService;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view.AlertsAdapter;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view.AlertsContainerFragment;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.presenter.AlertsNewsPresenter;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.presenter.IAlertsNewsViewUpdateHandler;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.viewmodel.AlertsNewsViewModel;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.view.FailModeDialog;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.andinaargentina.ui_sdk.components.dialog.SendReportDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_FRAGMENT_VIEW_NAME_ALERTS_NEWS;

@Route(path = ROUTE_FRAGMENT_VIEW_NAME_ALERTS_NEWS)
@RequiresApi(api = Build.VERSION_CODES.N)
public class AlertsNewsFragment extends FrameworkBaseFragment implements IAlertsNewsViewUpdateHandler {

    private AlertsAdapter adapter;
    private RecyclerView recyclerView;
    private TextView txtTitle;

    private View container;
    private View emptyState;
    private ConfirmReadAlertDialog dialog;
    private FailModeDialog failDialog;
    private IAlertsNewsViewEventsHandler presenter;

    private InformAlertService informAlertService;
    private CompositeDisposable compositeDisposable;

    private boolean isInformedAlerts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.informAlertService = new InformAlertService(ApiRestModule.providesApiService(false));
        this.compositeDisposable = new CompositeDisposable();
        presenter = new AlertsNewsPresenter(new AlertsNewsViewModel(),
                this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState,
                R.layout.fragment_alerts_group);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    public void setLineDto(LineasProduccionDto lineDto){
        this.presenter.setLineDto(lineDto);
    }

    private void initView() {
        container = getView().findViewById(R.id.fragment_alerts_readed_container);
        emptyState = getView().findViewById(R.id.fragment_alerts_readed_empty_state_container);
        recyclerView = getView().findViewById(R.id.alerts_readed_recycler);
        txtTitle = getView().findViewById(R.id.alerts_readed_txt_title);
        txtTitle.setText("Nuevas alertas");
        container.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        initRecyclerView();
    }

    private void initRecyclerView() {
        List<LineasProduccionAlertasDto> listData = new ArrayList<>();
        adapter = new AlertsAdapter(listData, (caller, position, bundle) -> presenter.onClickAlert(position));
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new SlideInLeftAnimator());
    }

    @Override
    public String getTagFragment() {
        return "Alerts readed fragment";
    }

    @Override
    public void clearList() {
        adapter.getListData().clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addAlert(LineasProduccionAlertasDto alertDto) {
        hideEmptyState();
        adapter.getListData().add(0,alertDto);
        adapter.notifyItemInserted(0);
        recyclerView.scrollToPosition(0);
    }

    @Override
    public void removeAlert(int position) {
        //Removido de item, meter en adapter
        if (adapter != null && adapter.getItemCount()>0){
            adapter.getListData().remove(position);
            adapter.notifyItemRemoved(position);
            //recyclerView.scrollToPosition(0);
        }
        if (adapter.getListData().isEmpty()){
            //EventBus.getDefault().post(new AlertsContainerFragment.RemoveNewAlertContainer());
        }
    }

    @Override
    public void hideComponentWithDownAnimation() {
        int parentWidth = ((View)getView().getParent()).getMeasuredHeight();
        //La altura del container es la mesma
        ValueAnimator widthAnimator = ValueAnimator.ofInt(container.getHeight(), parentWidth);
        widthAnimator.setDuration(2000);
        widthAnimator.setInterpolator(new DecelerateInterpolator());
        widthAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                container.setVisibility(View.GONE);
            }
        });
        widthAnimator.addUpdateListener(animation -> {
            //Hay que comunicar el otro container
            //para que las animaciones estén sincornizadas
            //readedAlertsContainer.getLayoutParams().height = (int) animation.getAnimatedValue();
            //readedAlertsContainer.requestLayout();
        });
        widthAnimator.start();
    }

    @Override
    public void hideComponentWithFadeAnimation() {
        container.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                container.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void showComponent() {
        container.setAlpha(0.0f);
        container.setVisibility(View.VISIBLE);
        container.animate().alpha(1.0f).setDuration(500);
    }

    @Override
    public void changeStatus(int position) {
        //Cambiar el estado de un item
    }

    @Override
    public void showEmptyState(){
        emptyState.setAlpha(0.0f);
        emptyState.setVisibility(View.VISIBLE);
        emptyState.animate().alpha(1.0f).setDuration(500);
    }

    @Override
    public void hideEmptyState() {
        emptyState.setAlpha(1.0f);
        emptyState.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                emptyState.setVisibility(View.GONE);
            }
        }).setDuration(500);
    }

    @Override
    public void showConfirmDialog(int position) {
        //TODO mostrar dialogo de confirmacion
        if (!adapter.getListData().get(position).getEstado().equals("Informada")){
            dialog = new ConfirmReadAlertDialog(getContext(), new SendReportDialog.DialogClickHandler() {
                @Override
                public void positiveClick(int position) {
                    //Notificar nueva alerta a leída
                    SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                    adapter.getListData().get(position).setEstado("Leida");
                    informAlert(adapter.getListData().get(position), String.valueOf(user.getUserId()));
                    removeAlert(position);
                    dialog.dismiss();
                    dialog = null;
                }

                @Override
                public void negativeClick() {
                    dialog.dismiss();
                    dialog = null;
                }
            },position, adapter.getListData().get(position));
            dialog.show();
        }
    }

    private void informAlert(LineasProduccionAlertasDto alertToInformDto, String userId){
        /*if (!compositeDisposable.isDisposed()){
            Disposable requestInformAlertDisposable = informAlertService
                    .informAlert(alertToInformDto,userId,getContext())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(informAlertObserver());
            compositeDisposable.add(requestInformAlertDisposable);
        }*/
    }

    /*private DisposableSingleObserver<List<AlertLineDto>> informAlertObserver() {
        return new DisposableSingleObserver<List<AlertLineDto>>() {
            @Override
            public void onSuccess(List<AlertLineDto> informAlertResponseDto) {
                Log.i("Inform alert", "Success" );
                EventBus.getDefault().post(new AlertsSuccessEvent(informAlertResponseDto));
            }

            @Override
            public void onError(Throwable e) {
                Log.i("Inform alert", "Error");
            }
        };
    }*/

    public void setIsInformed(boolean isInformedAlerts) {
        this.isInformedAlerts = isInformedAlerts;
        this.presenter.setIsInformedAlert(this.isInformedAlerts);
    }
}
