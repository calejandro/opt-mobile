package com.andinaargentina.sgi.android.home.visualization.view;

public interface IHomeObserverModeViewEventHandler {

    void onCreatedView();

    void onSelectedLine(int position);

    void onChangeOrderInLine(int position);

    void onAlertsInformed();

    void onLogout();

    void onExtendTurn();

    void onAlertIconPressed();

    void onGraphicIconPressed();

}
