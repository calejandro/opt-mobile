package com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionEficienciaMesDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaByShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.ProduccionEficienciaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisualizationModeHolder extends BaseHolder<LineasProduccionDto> {

    private TextView txtEficienciaName;
    private TextView txtEficienciaTurno;
    private TextView txtEficienciaDiaria;
    private TextView txtEficienciaObjetivo;
    private boolean isPlanMode;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private List<ShiftDto> turnos;
    public VisualizationModeHolder(View itemView, boolean isPlanMode) {
        super(itemView);
        this.txtEficienciaName = itemView.findViewById(R.id.item_eficiencia_name);
        this.txtEficienciaTurno = itemView.findViewById(R.id.item_eficiencia_turno);
        this.txtEficienciaDiaria = itemView.findViewById(R.id.item_eficiencia_diaria);
        this.txtEficienciaObjetivo = itemView.findViewById(R.id.item_eficiencia_objetivo);
        this.isPlanMode = isPlanMode;
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setEficienciaDiaria(LineasProduccionDto linea, LineasProduccionEficienciaMesDto lineasProduccionEficienciaMesDto) {
        OptimusServices.getInstance().getEficienciaService().listByShift(linea.getIdLinea(), simpleDateFormat.format(
                ShiftUtils.getInstance().getShifts().get(0).getInicio()),
                simpleDateFormat.format(ShiftUtils.getInstance().getShifts().get(2).getFin()))
                .enqueue(new Callback<ArrayList<ProduccionEficienciaByShiftDto>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Response<ArrayList<ProduccionEficienciaByShiftDto>> response) {
                        Double eficiencia = 0d;
                        if(response.body().size() > 0) {
                            eficiencia = response.body().get(0).getEficiencia();
                        }
                        txtEficienciaDiaria.setText(eficiencia + " %");
                        int drawableColor = 0;
                        if (eficiencia <= lineasProduccionEficienciaMesDto.getEficienciaMensual()){
                            drawableColor = R.color.color_alert_txt_red;
                        }else{
                            drawableColor = R.color.color_bg_alert_green;
                        }
                        txtEficienciaDiaria.setTextColor(itemView.getResources().getColor(drawableColor));
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Throwable t) {

                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setEficienciaTurno(LineasProduccionDto linea, LineasProduccionEficienciaMesDto lineasProduccionEficienciaMesDto) {
        OptimusServices.getInstance().getEficienciaService().listByShift(linea.getIdLinea(), simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()))
                .enqueue(new Callback<ArrayList<ProduccionEficienciaByShiftDto>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Response<ArrayList<ProduccionEficienciaByShiftDto>> response) {
                        Double eficiencia = 0d;
                        if(response.body().size() > 0) {
                            eficiencia = response.body().get(0).getEficiencia();
                        }
                        txtEficienciaTurno.setText(eficiencia + " %");
                        int drawableColor = 0;
                        if (eficiencia <= lineasProduccionEficienciaMesDto.getEficienciaMensual()){
                            drawableColor = R.color.color_alert_txt_red;
                        }else{
                            drawableColor = R.color.color_bg_alert_green;
                        }
                        txtEficienciaTurno.setTextColor(itemView.getResources().getColor(drawableColor));
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ProduccionEficienciaByShiftDto>> call, Throwable t) {

                    }
                });
    }

    private void setEficienciaObjetivo(LineasProduccionDto linea) {
        OptimusServices.getInstance().getLineasProduccionEficienciaMesService().list(linea.getIdLinea(), linea.getIdPlanta()).enqueue(new Callback<List<LineasProduccionEficienciaMesDto>>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<LineasProduccionEficienciaMesDto>> call, Response<List<LineasProduccionEficienciaMesDto>> response) {
                txtEficienciaObjetivo.setText(response.body().get(0).getEficienciaMensual() + " %");
                setEficienciaDiaria(linea, response.body().get(0));
                setEficienciaTurno(linea, response.body().get(0));
            }

            @Override
            public void onFailure(Call<List<LineasProduccionEficienciaMesDto>> call, Throwable t) {

            }
        });
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setDataInHolder(LineasProduccionDto data) {
        this.txtEficienciaName.setText(data.getDescLinea());
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                turnos = shiftDtoList;
                setEficienciaObjetivo(data);
            }
        });
        /*String eficienciaTurno;
        String eficienciaDiaria;
        String eficienciaObjetivo;

        int drawableForShift = 0;
        int drawableForDaily = 0;

        if (isPlanMode){
            eficienciaTurno = String.format("%.1f", data.getShiftPlanEfficiencyPercentaje())+" %";
            eficienciaDiaria = String.format("%.1f", data.getDailyPlanEfficiencyPercentaje())+" %";
            eficienciaObjetivo = String.format("%.1f", data.getObjectivePlanEfficiencyPercentaje())+" %";
            //Calculo el color en función del objetivo
            if (data.getShiftPlanEfficiencyPercentaje() <= data.getObjectivePlanEfficiencyPercentaje()){
                drawableForShift = R.color.color_alert_txt_red;
            }else{
                drawableForShift = R.color.color_bg_alert_green;
            }
            if (data.getDailyPlanEfficiencyPercentaje() <= data.getObjectivePlanEfficiencyPercentaje()){
                drawableForDaily = R.color.color_alert_txt_red;
            }else{
                drawableForDaily = R.color.color_bg_alert_green;
            }
        }else{
            eficienciaTurno = String.format("%.1f", data.getShiftEfficiencyPercentaje())+" %";
            eficienciaDiaria = String.format("%.1f", data.getDailyEfficiencyPercentaje())+" %";
            eficienciaObjetivo = String.format("%.1f", data.getObjectiveEfficiencyPercentaje())+" %";
            //Calculo el color en función del objetivo
            if (data.getShiftEfficiencyPercentaje() <= data.getObjectiveEfficiencyPercentaje()){
                drawableForShift = R.color.color_alert_txt_red;
            }else{
                drawableForShift = R.color.color_bg_alert_green;
            }
            this.txtEficienciaTurno.setTextColor(this.itemView.getResources().getColor(drawableForShift));
            if (data.getDailyEfficiencyPercentaje() <= data.getObjectiveEfficiencyPercentaje()){
                drawableForDaily = R.color.color_alert_txt_red;
            }else{
                drawableForDaily = R.color.color_bg_alert_green;
            }
        }
        this.txtEficienciaName.setText(data.getProductionLineName());
        this.txtEficienciaTurno.setText(eficienciaTurno);
        this.txtEficienciaTurno.setTextColor(this.itemView.getResources().getColor(drawableForShift));

        this.txtEficienciaDiaria.setText(eficienciaDiaria);
        this.txtEficienciaDiaria.setTextColor(this.itemView.getResources().getColor(drawableForDaily));

        this.txtEficienciaObjetivo.setText(eficienciaObjetivo);*/
    }
}
