package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;

import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto.STATE.INFORMED;

public class AlertHolder extends BaseHolder<LineasProduccionAlertasDto> {

    private View viewRootContainer;
    private View subViewRootContainer;

    //Standar
    private View standardContainer;
    private View typeAlertBackground;
    private TextView txtName;
    private TextView txtDate;
    private TextView txtIsActive;
    private ImageView imgIconRight;

    private View minuteContainer;
    private TextView txtMinute;
    private TextView txtUnderTxtMinute;
    private ImageView imgClock;

    //Black
    private View blackContainer;
    private TextView blackTxtName;
    private TextView blackTxtDescription;

    private LineasProduccionAlertasDto alertDto;

    public AlertHolder(View itemView) {
        super(itemView);
        viewRootContainer = itemView.findViewById(R.id.item_selector_root_container);
        subViewRootContainer = itemView.findViewById(R.id.item_selector_bg_color_view);
        standardContainer = itemView.findViewById(R.id.item_selector_standar_container);
        typeAlertBackground = itemView.findViewById(R.id.item_selector_img);
        txtName = itemView.findViewById(R.id.item_selector_txt_name);
        txtDate = itemView.findViewById(R.id.item_selector_txt_description);
        txtIsActive = itemView.findViewById(R.id.item_selector_txt_active);
        imgIconRight = itemView.findViewById(R.id.item_selector_img_icon);
        txtMinute = itemView.findViewById(R.id.item_alert_standar_minute);
        minuteContainer = itemView.findViewById(R.id.item_selector_standar_minute_container);
        txtUnderTxtMinute = itemView.findViewById(R.id.item_alert_standar_txt_minute);
        imgClock = itemView.findViewById(R.id.item_alert_standar_img_clock);

        blackContainer = itemView.findViewById(R.id.item_selector_black_container);
        blackTxtDescription = itemView.findViewById(R.id.item_selector_black_txt_description);
        blackTxtName = itemView.findViewById(R.id.item_selector_black_txt_name);
    }

    public static String formatDate(Date originalDate){
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy - kk:mm");
        String formattedDate = targetFormat.format(originalDate);  // 20120821
        return formattedDate;
    }

    @Override
    public void setDataInHolder(LineasProduccionAlertasDto data) {
        viewRootContainer.setBackgroundResource(R.drawable.bg_item_alert_white);
        this.alertDto = data;
        standardContainer.setVisibility(View.VISIBLE);
        blackContainer.setVisibility(View.GONE);
        //this.txtName.setText(data.getEquipo().getDescEquipo());
        this.txtName.setText("Equipo");
        this.txtDate.setText(formatDate(data.getFechaCreacion()));
        if (data.getClasificacion() == "Merma"){
            //merma
            this.txtMinute.setText(String.format("%.2f", data.getValorAcumulado())+" %");
            setDecreaseAlert();
        }else{
            //parada
            this.txtMinute.setText(String.valueOf(data.getValorAcumulado().intValue()));
            setNormalAlert();
        }
        if (data.getEstado().equals("Informada")){
            //informed
            setStateInformed();
        }else{
            switch (data.getTipoAlerta()){
                case "Atencion":{
                    setTypeWarningAlert();
                    break;
                }
                case "Peligro":{
                    setTypeDangerAlert();
                    break;
                }
                case "Fallo":{
                    setFailureAlert();
                    break;
                }
                case "Ok":{
                    setTypeOkAlert();
                    break;
                }
            }
            switch (data.getEstado()){
                case "Nueva":
                    setStateNew();
                    break;
                case "Leida":
                    setStateReaded();
                    break;
                case "Informada":
                    setStateInformed();
                    break;
                case "Resuelto":
                    //setStateSolved();
                    break;
                case "Cerrada":
                    //setStateClosed();
                    break;
            }
        }
        String txtIsActive;
        if (data.getFechaFin() == null){
            txtIsActive = "Alerta activa";
        }else{
            txtIsActive = "Alerta finalizada";
        }
        this.txtIsActive.setText(txtIsActive);
    }

    private void setNormalAlert() {
        this.typeAlertBackground.setVisibility(View.VISIBLE);
        this.txtMinute.setTextSize(TypedValue.COMPLEX_UNIT_PX, getThisView().getResources().getDimension(R.dimen._30ssp));
        this.imgClock.setImageResource(R.drawable.i_clock_black);
        this.txtUnderTxtMinute.setText("MINUTOS");
    }

    private void setDecreaseAlert() {
        this.typeAlertBackground.setVisibility(View.GONE);
        this.txtMinute.setTextSize(TypedValue.COMPLEX_UNIT_PX, getThisView().getResources().getDimension(R.dimen._20ssp));
        this.imgClock.setImageResource(R.drawable.icon_merma_dark);
        this.txtUnderTxtMinute.setText("MERMA");
    }

    private void setTypeOkAlert() {
    }

    private void setFailureAlert() {
        this.typeAlertBackground.setVisibility(View.GONE);
        int color = AlertHolder.this.getThisView().getResources().getColor(R.color.color_bg_alert_black);
        subViewRootContainer.setBackgroundResource(R.drawable.bg_item_alert_black);
        txtMinute.setTextColor(color);
        txtUnderTxtMinute.setTextColor(color);
        setTintColor(imgClock, R.color.color_bg_alert_black);
    }

    private void setTypeDangerAlert() {
        int color = AlertHolder.this.getThisView().getResources().getColor(R.color.color_alert_txt_red);
        subViewRootContainer.setBackgroundResource(R.drawable.bg_item_alert_red);
        txtMinute.setTextColor(color);
        txtUnderTxtMinute.setTextColor(color);
        setTintColor(imgClock, R.color.color_alert_txt_red);
    }

    private void setTypeWarningAlert() {
        subViewRootContainer.setBackgroundResource(R.drawable.bg_item_alert_orange);
        int color = AlertHolder.this.getThisView().getResources().getColor(R.color.color_alert_txt_yellow);
        txtMinute.setTextColor(color);
        txtUnderTxtMinute.setTextColor(color);
        setTintColor(imgClock, R.color.color_alert_txt_yellow);
    }

    private void setStateReaded(){
        //Alerta leída
        minuteContainer.setVisibility(View.VISIBLE);
        if (alertDto.getClasificacion().equals("Merma"))
            this.typeAlertBackground.setVisibility(View.GONE);
        subViewRootContainer.setBackgroundResource(R.drawable.bg_item_alert_blue);
        Picasso.with(itemView.getContext()).load(R.drawable.ialertread).into(this.imgIconRight);
    }


    private void setStateInformed() {
        //Si la alerta está informada no hay que mostrarla en el listado
        //de alertas, estas van en un listado aparte
        this.typeAlertBackground.setVisibility(View.GONE);
        minuteContainer.setVisibility(View.GONE);
        subViewRootContainer.setBackgroundResource(R.drawable.bg_item_selector_green);
        Picasso.with(itemView.getContext()).load(R.drawable.i_alert_done).into(this.imgIconRight);
    }

    private void setStateNew() {
        //Alerta nueva
        if (alertDto.getClasificacion().equals("Merma"))
            this.typeAlertBackground.setVisibility(View.GONE);
        Picasso.with(itemView.getContext()).load(R.drawable.i_alert_do).into(this.imgIconRight);
    }

    public static boolean isBetween(long x, long lower, long upper) {
        return lower <= x && x < upper;
    }

    private void setTintColor(@NonNull ImageView imageView, int color){
        ColorStateList colours = imageView.getResources()
                .getColorStateList(color);
        Drawable drawable = DrawableCompat.wrap(imageView.getDrawable());
        DrawableCompat.setTintList(drawable, colours);
    }
}
