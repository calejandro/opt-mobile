package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas;

import android.view.View;
import android.widget.TextView;

import com.andinaargentina.core_services.providers.apiv2.models.KpiDepletionDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.UmbralUtil;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.text.SimpleDateFormat;

public class MermasDetalleParadaPropiaHolder extends BaseHolder<KpiDepletionDto> {

    private TextView hour;
    private TextView value;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

    public MermasDetalleParadaPropiaHolder(View itemView) {
        super(itemView);
        this.hour = itemView.findViewById(R.id.mermas_detalle_pp_hour);
        this.value = itemView.findViewById(R.id.mermas_detalle_pp_value);
    }

    @Override
    public void setDataInHolder(KpiDepletionDto data) {
        this.hour.setText(simpleDateFormat.format(data.getFechaHora()));
        this.value.setText(String.format("%.2f", data.getKpi()));
//        UmbralUtil.UMBRAL_TYPE umbral_type = UmbralUtil.getCOLORType(data.getUmbralType());
//        switch (umbral_type){
//            case NORMAL:{
//                this.value.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
//                break;
//            }
//            case WARNING:{
//                this.value.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
//                break;
//            }
//            case CRITICAL:{
//                this.value.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
//                break;
//            }
//        }
    }

}
