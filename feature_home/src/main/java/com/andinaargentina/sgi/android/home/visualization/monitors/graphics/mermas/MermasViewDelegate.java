package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.mermas;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.core_services.providers.api.clients.graphics.dto.GraphicsDto;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.Depletion;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.EvolutionChart_;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.GeneralChart;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.Historical_;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.kpidepletion.TreeMapChart;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.KpiDepletionDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.GraphicsFragment;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.UmbralUtil;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas.MermasDetalleParadaPropiaAdapater;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas.MermasEquiposAdapater;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.mermas.TimeHistogramDataDto;
import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.TreeMap;
import com.anychart.core.cartesian.series.Column;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.SelectionMode;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.enums.TreeFillingMethod;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.N)
public class MermasViewDelegate {

    public static final String RED_COLOR = "#F83F37";
    public static final String GRAY_COLOR = "#FAFAFA";
    public static final String YELLOW_COLOR = "#FEDF3E";
    private LineasProduccionDto lineDto;

    //Mermas
    private TextView txtHourUpdated;
    private TextView txtTitleDetail;

    private RecyclerView listMermasRecyclerView;
    private RecyclerView detailHourMermasRecyclerView;

    private MermasEquiposAdapater mermasEquiposAdapater;
    private MermasDetalleParadaPropiaAdapater mermasDetalleParadaPropiaAdapater;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private SimpleDateFormat simpleDateFormatHour = new SimpleDateFormat("HH:mm");
    private int lasPositionSelected = 0;

    public MermasViewDelegate(LineasProduccionDto lineDto) {
        this.lineDto = lineDto;
    }

    public void initView(@NonNull View view){
        Log.i("MermasView","Se llamo al metodo initView");
        initListMermasRecycler(view);
        initListDetailHourRecycler(view,0);
    }

    private void listMermasOnClick(int position) {
        KpiDepletionDto dtoSelected = this.mermasEquiposAdapater.getListData().get(position);
        for (KpiDepletionDto dtoAux:this.mermasEquiposAdapater.getListData()) {
            if (dtoAux != dtoSelected){
                dtoAux.setSelected(false);
            }else{
                dtoAux.setSelected(true);
            }
        }
        this.mermasEquiposAdapater.notifyDataSetChanged();
    }

    @SuppressLint("DefaultLocale")
    private void initListMermasRecycler(@NonNull View view) {
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                OptimusServices.getInstance().getMermasKPIService().list(lineDto.getIdLinea(),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()), 3).enqueue(new Callback<ArrayList<KpiDepletionDto>>() {
                    @Override
                    public void onResponse(Call<ArrayList<KpiDepletionDto>> call, Response<ArrayList<KpiDepletionDto>> response) {
                        if(response.body().size() > 0) {
                            txtHourUpdated = view.findViewById(R.id.mermas_principal_graphic_txt_hr_title);
                            txtHourUpdated.setText(simpleDateFormatHour.format(response.body().get(0).getFechaHora()) + "hs");
                            txtTitleDetail = view.findViewById(R.id.mermas_detalle_title);
                            txtTitleDetail.setText("Detalle de merma: " + response.body().get(0).getDescripcionKPI());
                            listMermasRecyclerView = view.findViewById(R.id.mermas_recycler_equipos);
                            //selecciono el primer item
                            lasPositionSelected = 0;
                            mermasEquiposAdapater = new MermasEquiposAdapater(response.body(), (caller, position, bundle) -> {
                                lasPositionSelected = position;
                                listMermasOnClick(position);
                            });
                            final LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(),
                                    RecyclerView.VERTICAL, false);
                            listMermasRecyclerView.setAdapter(mermasEquiposAdapater);
                            listMermasRecyclerView.setNestedScrollingEnabled(false);
                            listMermasRecyclerView.setLayoutManager(layoutManager);
                        }

                    }

                    @Override
                    public void onFailure(Call<ArrayList<KpiDepletionDto>> call, Throwable t) {

                    }
                });
            }
        });

    }

    private void initListDetailHourRecycler(@NonNull View view, int positionSelected) {
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                OptimusServices.getInstance().getMermasKPIService().list(lineDto.getIdLinea(),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()), 4).enqueue(new Callback<ArrayList<KpiDepletionDto>>() {
                    @Override
                    public void onResponse(Call<ArrayList<KpiDepletionDto>> call, Response<ArrayList<KpiDepletionDto>> response) {
                        detailHourMermasRecyclerView = view.findViewById(R.id.mermas_detalle_recycler_parada_propia);
                        mermasDetalleParadaPropiaAdapater = new MermasDetalleParadaPropiaAdapater(response.body());
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(),
                                RecyclerView.HORIZONTAL, false);
                        detailHourMermasRecyclerView.setAdapter(mermasDetalleParadaPropiaAdapater);
                        detailHourMermasRecyclerView.setNestedScrollingEnabled(false);
                        detailHourMermasRecyclerView.setLayoutManager(layoutManager);
                    }

                    @Override
                    public void onFailure(Call<ArrayList<KpiDepletionDto>> call, Throwable t) {

                    }
                });
            }
        });




    }




    /*@SuppressLint("SetTextI18n")
    public void updateView(GraphicsDto graphicsDto){
        this.graphicsDto = graphicsDto;
        EvolutionChart_ evolutionChartData = graphicsDto.getKpiDepletion().getDepletions().get(lasPositionSelected).getDetails().getEvolutionChart();
        APIlib.getInstance().setActiveAnyChartView(mermasEvolutionChart);
        List<DataEntry> seriesData = new ArrayList<>();
        NumberFormat formatter = new DecimalFormat("#0.0");
        for (GeneralChart value: evolutionChartData.getGeneralChart()) {
            seriesData.add(new GraphicsFragment.CustomDataEntry(value.getHour(), Double.valueOf(formatter.format(value.getDepletionValue()).replace(",", "."))));
        }
        evolutionGeneralSet.data(seriesData);

        List<TimeHistogramDataDto> listData = new ArrayList<>();
        TimeHistogramDataDto dtoAux;
        for (Historical_ historicalDto: graphicsDto.getKpiDepletion().getDepletions().get(lasPositionSelected).getDetails().getHistorical()) {
            dtoAux = new TimeHistogramDataDto();
            dtoAux.setValue(historicalDto.getDepletionValue());
            dtoAux.setUmbralType(historicalDto.getUmbralType());
            dtoAux.setHour(historicalDto.getHour());
            listData.add(dtoAux);
        }
        mermasDetalleParadaPropiaAdapater.getListData().clear();
        mermasDetalleParadaPropiaAdapater.setListData(listData);
        mermasDetalleParadaPropiaAdapater.notifyDataSetChanged();

        initTreeMap(graphicsDto.getKpiDepletion().getDepletions().get(lasPositionSelected).getDetails().getTreeMapChart());
        this.txtTitleDetail.setText("Detalle de merma: " + graphicsDto.getKpiDepletion().getDepletions().get(lasPositionSelected).getName());
    }*/

    private class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value, Number value2, Number value3) {
            super(x, value);
            setValue("value2", value2);
            setValue("value3", value3);
        }
    }
}
