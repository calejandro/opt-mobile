package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.view;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;

public interface IAlertsReadedViewEventsHandler {

    //Llegó nueva alerta
    void onNewAlert();

    void onChangeStatusAlert();

    //Se seleccionó la alerta
    void onClickAlert(int position);

    //Se confirmó la toma de la alerta
    void onConfirmAlert(int position);

    void setLineDto(LineasProduccionDto lineDto);

    LineasProduccionDto getLineDto();

}
