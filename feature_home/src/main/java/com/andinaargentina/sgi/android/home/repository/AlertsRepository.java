package com.andinaargentina.sgi.android.home.repository;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.events.alerts.AlertsErrorEvent;
import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertService;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeDto;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeService;
import com.andinaargentina.core_services.providers.api.core.base.ServiceError;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

@RequiresApi(api = Build.VERSION_CODES.N)
public class AlertsRepository {

    private AlertService alertService;
    private FailModeService failModeService;
    private CompositeDisposable compositeDisposable;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public AlertsRepository(AlertService alertService,
                            FailModeService failModeService,
                            CompositeDisposable compositeDisposable) {
        this.alertService = alertService;
        this.failModeService = failModeService;
        this.compositeDisposable = compositeDisposable;
    }

    public void getAlerts(int lineaId, Context context) {
        if (!compositeDisposable.isDisposed()) {
            ShiftDto shiftDto = ShiftUtils.getInstance().getCurrentShift();
            Disposable requestGetProductsDisposable = alertService
                    .getAlerts(lineaId, simpleDateFormat.format(shiftDto.getInicio()),
                            simpleDateFormat.format(shiftDto.getFin()), context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getAlertsObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    private DisposableSingleObserver<List<LineasProduccionAlertasDto>> getAlertsObserver(){
        return new DisposableSingleObserver<List<LineasProduccionAlertasDto>>() {
            @Override
            public void onSuccess(List<LineasProduccionAlertasDto> response) {
                EventBus.getDefault().post(new AlertsSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                ServiceError error;
                if (e instanceof ServiceError){
                    error = (ServiceError) e;
                }else{
                    error = new ServiceError();
                }
                EventBus.getDefault().post(error);
            }
        };
    }

    public void getFailMode(String machineId, String lineId, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = failModeService
                    .getFailModes(machineId, lineId,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getFailModeObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    public void getFailModeByAlertKind(String machineId, String lineId, int productionAlertKind, Context context) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = failModeService
                    .getFailModesByAlertKind(machineId, lineId, productionAlertKind,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getFailModeObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    private DisposableSingleObserver<FailModeDto> getFailModeObserver(){
        return new DisposableSingleObserver<FailModeDto>() {
            @Override
            public void onSuccess(FailModeDto response) {
                EventBus.getDefault().post(new FailModeSuccessEvent(response));
            }

            @Override
            public void onError(Throwable e) {
                EventBus.getDefault().post(new FailModeErrorEvent(new ServiceError()));
            }
        };
    }

    public class FailModeSuccessEvent{
        private FailModeDto failModeDto;

        public FailModeSuccessEvent(FailModeDto failModeDto) {
            this.failModeDto = failModeDto;
        }

        public FailModeDto getFailModeDto() {
            return failModeDto;
        }

        public void setFailModeDto(FailModeDto failModeDto) {
            this.failModeDto = failModeDto;
        }
    }

    public class FailModeErrorEvent{

        private ServiceError serviceError;

        public FailModeErrorEvent(ServiceError serviceError) {
            this.serviceError = serviceError;
        }
    }

    public void cancelRequest(){
        if (this.failModeService!=null)
            this.failModeService.cancel();
        if (this.alertService!=null)
            this.alertService.cancel();
    }
}
