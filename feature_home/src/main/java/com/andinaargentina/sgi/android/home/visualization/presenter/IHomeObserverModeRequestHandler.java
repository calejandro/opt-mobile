package com.andinaargentina.sgi.android.home.visualization.presenter;

import android.content.Context;

import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftRequestDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderRequestDto;

/**
 * Esta interfaz define la comunicación
 * entre el presenter y el interactor
 * Esta interfaz debe ser implementada por el
 * interactor
 */
public interface IHomeObserverModeRequestHandler {

    void logout(Context context, boolean isShiftEnded);

    void changeOrder(Context context, ChangeOrderRequestDto changeOrderRequestDto);

    void extendShift(Context context, ExtendShiftRequestDto extendShiftRequestDto);
}
