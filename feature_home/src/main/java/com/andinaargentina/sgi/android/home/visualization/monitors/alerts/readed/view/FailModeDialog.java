package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertFailModeInformRequestDto;
import com.andinaargentina.core_services.providers.api.clients.failmode.DescriptionDto;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.ModoFalloDto;
import com.andinaargentina.core_services.providers.apiv2.models.RegistrosFallosAlertasDto;
import com.andinaargentina.ui_sdk.R;

import java.util.ArrayList;
import java.util.List;

public class FailModeDialog extends Dialog {

    private TextView txtOk, txtTitle, txtDescription;
    private Spinner spnMdode, spnFail, spnCause;
    private EditText editTextAction;
    private View btnClose;
    private DialogClickHandler clickHandler;
    private LineasProduccionAlertasDto alertDto;
    private List<ModoFalloDto> failModeDto;
    private RegistrosFallosAlertasDto failModeRequestDto;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v ==  btnClose) {
                dismiss();
            } else if (v == txtOk) {
                dismiss();
                failModeRequestDto.setAccionTomada(editTextAction.getText().toString());
                failModeRequestDto.setId(alertDto.getIdAlerta());
                clickHandler.positiveClick(alertDto, failModeRequestDto);
            }
        }
    };


    public FailModeDialog(Context context, @NonNull LineasProduccionAlertasDto alertDto,
                          @NonNull List<ModoFalloDto> failModeDto,
                          @NonNull DialogClickHandler clickHandler) {
        super(context, R.style.ThemeDialogCustom);
        this.clickHandler = clickHandler;
        this.alertDto = alertDto;
        this.failModeDto = failModeDto;
        this.failModeRequestDto = new RegistrosFallosAlertasDto();
        initParentView(context);
    }

    private void initParentView(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_fail_mode, null);
        setContentView(view);
        setCancelable(true);
        initViewComponents();
    }

    private void initViewComponents() {
        txtOk = findViewById(R.id.dialog_fail_mode_txt_ok);
        btnClose = findViewById(R.id.dialog_fail_mode_img_close);

        txtTitle = findViewById(R.id.dialog_fail_mode_txt_title);
        txtDescription = findViewById(R.id.dialog_fail_mode_txt_description);
        editTextAction = findViewById(R.id.dialog_fail_mode_edit_txt_action_taked);
        spnMdode = findViewById(R.id.dialog_fail_mode_spn_mode);
        spnFail = findViewById(R.id.dialog_fail_mode_spn_fail);
        spnCause = findViewById(R.id.dialog_fail_mode_spn_cause);

        txtTitle.setText(alertDto.getEquipo().getDescEquipo());
        String subtitle = "ESTUVO " + alertDto.getValorAcumulado() + " MINUTOS EN PP";
        txtDescription.setText(subtitle);
        txtOk.setOnClickListener(onClickListener);
        btnClose.setOnClickListener(onClickListener);

        setFailMode();
        setFail();
        setCause();
    }

    private void setCause() {
        ArrayList<String> arrayList = new ArrayList<>();
        for (ModoFalloDto descriptionDto: this.failModeDto) {
            arrayList.add(descriptionDto.getNombre());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCause.setAdapter(arrayAdapter);
        spnCause.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String causeName = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), "Cause selected: " + causeName, Toast.LENGTH_LONG).show();
                FailModeDialog.this.failModeRequestDto
                        .setIdCausa(FailModeDialog.this.failModeDto.get(position).getIdTipo());
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
            }
        });
    }

    private void setFail() {
        ArrayList<String> arrayList = new ArrayList<>();
        for (ModoFalloDto descriptionDto: this.failModeDto) {
            arrayList.add(descriptionDto.getNombre());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnFail.setAdapter(arrayAdapter);
        spnFail.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String failName = parent.getItemAtPosition(position).toString();
                FailModeDialog.this.failModeRequestDto
                        .setIdFallo(FailModeDialog.this.failModeDto.get(position).getIdTipo());
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
            }
        });
    }

    private void setFailMode() {
        ArrayList<String> arrayList = new ArrayList<>();
        for (ModoFalloDto descriptionDto: this.failModeDto) {
            arrayList.add(descriptionDto.getNombre());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMdode.setAdapter(arrayAdapter);
        spnMdode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String failModeName = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), "Fail mode selected: " + failModeName, Toast.LENGTH_LONG).show();
                FailModeDialog.this.failModeRequestDto.setIdFallo(FailModeDialog.this.failModeDto.get(position).getIdTipo());
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
            }
        });
    }

    public interface DialogClickHandler
    {
        void positiveClick(LineasProduccionAlertasDto alertDto, RegistrosFallosAlertasDto requestDto);
        void negativeClick();
    }
}