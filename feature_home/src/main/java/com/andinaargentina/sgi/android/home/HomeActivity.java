package com.andinaargentina.sgi.android.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.events.alerts.AlertsErrorEvent;
import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.events.alerts.LoadFailModesSuccessEvent;
import com.andinaargentina.core_services.events.internet.InternetConnectionUpdateReceiver;
import com.andinaargentina.core_services.events.internet.InternetUpdateEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertLineDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertService;
import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyService;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeDto;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeService;
import com.andinaargentina.core_services.providers.api.clients.graphics.GraphicsService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode.repository.EfficiencyRepository;
import com.andinaargentina.sgi.android.home.visualization.view.HomeObserverModeViewDelegate;
import com.andinaargentina.sgi.android.home.repository.AlertsRepository;
import com.andinaargentina.sgi.android.home.repository.GraphicsRepository;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.andinaargentina.sgi.core_sdk.base.times.UtilTimes;
import com.andinaargentina.sgi.core_sdk.base.workers.GoToLoginEvent;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

import static com.andinaargentina.RepositoryConst.ALERTS_DATA;
import static com.andinaargentina.RepositoryConst.GRAPHICS_DATA;
import static com.andinaargentina.RepositoryConst.MODO_DE_FALLO;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.core_services.providers.api.core.util.NetworkUtils.*;

@Route(path = ViewsNames.ROUTE_VIEW_NAME_HOME)
@RequiresApi(api = Build.VERSION_CODES.N)
public class HomeActivity extends AppCompatActivity {

    private ProgressDialog progress;
    private TextView txtRefresh;
    private TextView txtLastDateRefresh;
    private TextView txtOnlineStatus;
    private View viewStatusContainer;
    private View viewStatusOverlay;

    private HomeObserverModeViewDelegate viewDelegate;

    private GraphicsRepository graphicsRepository;
    private AlertsRepository alertsRepository;
    private EfficiencyRepository efficiencyRepository;

    private boolean isFirstOnRealTimeFetch = true;
    private PollingRequest pollingRequest = null;

    private InternetConnectionUpdateReceiver networkReceiver;

    @Override
    protected void onStart() {
        super.onStart();
        if(UtilSesion.checkSession(this)){
            return;
        }
        networkReceiver = new InternetConnectionUpdateReceiver();
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerObserver();
        if (pollingRequest!=null){
            pollingRequest.startRequest();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterObserver();
        if (networkReceiver!=null){
            unregisterReceiver(networkReceiver);
            networkReceiver = null;
        }
        if (pollingRequest!=null){
            pollingRequest.stopRequest();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        createUIComponents();
        createRepositories();
        this.viewDelegate = new HomeObserverModeViewDelegate(this);
    }

    //Handle dependencies
    private void createRepositories() {
        this.efficiencyRepository = new EfficiencyRepository(new EfficiencyService(
                ApiRestModule.providesApiService(false)),new CompositeDisposable());

        this.graphicsRepository = new GraphicsRepository(new GraphicsService(ApiRestModule.providesApiService(false)),
                new CompositeDisposable());

        this.alertsRepository = new AlertsRepository(new AlertService()
                , new FailModeService(ApiRestModule.providesApiService(false))
                ,new CompositeDisposable());
    }

    private void createUIComponents() {
        txtRefresh = findViewById(R.id.refresh_txt_time);
        txtLastDateRefresh = findViewById(R.id.refresh_txt_last_time);
        txtOnlineStatus = findViewById(R.id.refresh_txt_online);
        viewStatusContainer = findViewById(R.id.activity_home_status_container);
        viewStatusOverlay = findViewById(R.id.activity_home_status_overlay);
        setNetworkStatus();
    }

    private void setNetworkStatus(){
        if (isConnected(this)){
            txtOnlineStatus.setText("Online");
            viewStatusContainer.setBackgroundColor(getResources().getColor(R.color.color_bg_alert_green));
            viewStatusOverlay.setVisibility(View.GONE);
        }else{
            txtOnlineStatus.setText("Offline");
            viewStatusContainer.setBackgroundColor(getResources().getColor(R.color.color_txt_red_disable));
            viewStatusOverlay.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void internetConnectivityChange(InternetUpdateEvent event){
        Logger.d("Internet status changed, isConnected:" + event.isConnected());
        setNetworkStatus();
    }

    //Handle UiAndroid lifecycle

    @Override
    protected void onResume() {
        super.onResume();
        if(UtilSesion.checkSession(this)){
            return;
        }
        fetchAllData();
        viewDelegate.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //Handle Ui

    private void showProgressDialog() {
        String message = this.getResources().getString(com.andinaargentina.ui_sdk.R.string.loading);
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage(message);
        progress.show();
    }

    public void hideProgressDialog() {
        if (this.progress!=null && this.progress.isShowing()){
            progress.dismiss();
        }
    }

    //Handle observers

    private void registerObserver() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unregisterObserver() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    //Fetch data

    private void fetchAllData(){
        fetchAlertsFromBackend();
        setRefreshDate();
    }

    private void setRefreshDate(){
        DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        String dateFormatted = targetFormat.format(new Date());
        txtLastDateRefresh.setText(dateFormatted);
    }

    //Fetch to webservices

    private void fetchAlertsFromBackend() {
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                //alertsRepository.getAlerts(1, HomeActivity.this);
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessAlerts(AlertsSuccessEvent response){
        Paper.book("SGI-BOOK").write(ALERTS_DATA, response);
//        Paper.book("SGI-BOOK").delete(MODO_DE_FALLO);
//        for (AlertLineDto dto:response.getResponse()) {
//            for (AlertDto alertDto: dto.getAlertRaised()){
//                alertsRepository.getFailMode(String.valueOf(alertDto.getMachineId()),String.valueOf(dto.getProductionLineId()),this);
//            }
//        }
        Log.i("Home activity","Llegó respuesta de alertas exitoso");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorAlerts(AlertsErrorEvent response){
        Log.i("Home activity","Llegó respuesta de alertas con error");
        hideProgressDialog();
        //if (!response.getError().isVpnError())
            //onErrorCloseSession();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessFailMode(AlertsRepository.FailModeSuccessEvent response){
        Log.i("Home activity","Llegó respuesta de modo de fallos");
        Paper.book("SGI-BOOK").delete(MODO_DE_FALLO);
        List<FailModeDto> listFailMode = Paper.book("SGI-BOOK").read(MODO_DE_FALLO, new ArrayList<FailModeDto>());
        listFailMode.add(response.getFailModeDto());
        Paper.book("SGI-BOOK").write(MODO_DE_FALLO, listFailMode);
        EventBus.getDefault().post(new LoadFailModesSuccessEvent());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onErrorFailMode(AlertsRepository.FailModeErrorEvent event){
        Log.i("Home activity","Llegó respuesta de los modos de fallo con error");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGoToLogin(GoToLoginEvent event){
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(this);
        this.finish();
    }

    private void onErrorCloseSession(){
        Toast.makeText(this, "Ocurrió un error, redirigiendo al login", Toast.LENGTH_LONG).show();
        UtilSesion.clearSession();
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(this);
        this.finish();
    }

    //Polling class
    class PollingRequest {

        private CountDownTimer countDownTimer = null;

        void startRequest(){
            /*isFirstOnRealTimeFetch = false;
            countDownTimer = new CountDownTimer(120000, 1000) {
                public void onTick(long millisUntilFinished) {
                    txtRefresh.setText(String.valueOf(millisUntilFinished / 1000));
                }
                public void onFinish() {
                    User user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                    if (user != null){
                        fetchAllData();
                        countDownTimer.start();
                    }else{
                        countDownTimer.cancel();
                    }
                }
            }.start();*/
        }

        void stopRequest(){
            /*if (this.countDownTimer!=null)
                this.countDownTimer.cancel();*/
        }
    }
}
