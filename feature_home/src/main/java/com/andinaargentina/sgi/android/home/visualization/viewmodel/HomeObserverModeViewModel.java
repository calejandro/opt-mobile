package com.andinaargentina.sgi.android.home.visualization.viewmodel;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;

import org.jetbrains.annotations.NotNull;
//import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

//@Parcel
public class HomeObserverModeViewModel implements IBaseViewModel {

    //Las lineas cargadas
    @NonNull
    private List<LineasProduccionDto> listLines;

    //Que planta estaba seleccionada;
    @NonNull
    private PlantasDto plantSelected;

    //La linea actualmente seleccionada por el usuario
    private int lineSelected;

    //verifico si se está mostrando la vista de alertas
    //o gráficos
    private boolean isAlertSelected;

    @NonNull
    private String nameUser;

    @NonNull
    private String date;

    @NonNull
    private String turn;

    @NonNull
    private String workManagerDate;

    @NonNull
    private String shiftName;

    private boolean isSupervisor;
    private boolean userIsSupervisor;

    public HomeObserverModeViewModel(@NonNull List<LineasProduccionDto> listLines,
                                     @NonNull PlantasDto plantSelected,
                                     int lineSelected,
                                     boolean isAlertSelected,
                                     @NonNull String nameUser,
                                     @NonNull String date,
                                     @NonNull String turn,
                                     @NonNull String shiftName,
                                     boolean isSupervisor,
                                     boolean userIsSupervisor,
                                     @NonNull String workManagerDate) {
        this.listLines = listLines;
        this.plantSelected = plantSelected;
        this.lineSelected = lineSelected;
        this.isAlertSelected = isAlertSelected;
        this.nameUser = nameUser;
        this.date = date;
        this.turn = turn;
        this.shiftName = shiftName;
        this.isSupervisor = isSupervisor;
        this.userIsSupervisor = userIsSupervisor;
        this.workManagerDate = workManagerDate;
    }

    public boolean isSupervisor() {
        return isSupervisor;
    }

    public void setSupervisor(boolean supervisor) {
        isSupervisor = supervisor;
    }

    @NonNull
    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(@NonNull String nameUser) {
        this.nameUser = nameUser;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    @NonNull
    public String getTurn() {
        return turn;
    }

    @NonNull
    public String getWorkManagerDate(){
        return workManagerDate;
    }

    public void setTurn(@NonNull String turn) {
        this.turn = turn;
    }

    @NonNull
    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(@NonNull String shiftName) {
        this.shiftName = shiftName;
    }

    @NonNull
    public List<String> getListNames() {
        String shiftExtended = "";
        List<String> listName = new ArrayList<>();
        for (LineasProduccionDto lineDto: listLines) {
            if (lineDto.getLineasProduccionUsuarioDto() != null){
                listName.add(lineDto.getDescLinea() + " | " + lineDto.getLineasProduccionUsuarioDto().getIdOrdenSap() + " " +shiftExtended);
            }else{
                listName.add(lineDto.getDescLinea() + " " +shiftExtended);
            }
        }
        return listName;
    }


    @NonNull
    public List<LineasProduccionDto> getListLines() {
        return listLines;
    }

    public void setListLines(@NonNull List<LineasProduccionDto> listLines) {
        this.listLines = listLines;
    }

    @NonNull
    public PlantasDto getPlantSelected() {
        return plantSelected;
    }

    public void setPlantSelected(@NonNull PlantasDto plantSelected) {
        this.plantSelected = plantSelected;
    }

    public int getLineSelected() {
        return lineSelected;
    }

    public void setLineSelected(int lineSelected) {
        this.lineSelected = lineSelected;
    }

    public boolean isAlertSelected() {
        return isAlertSelected;
    }

    public void setAlertSelected(boolean alertSelected) {
        isAlertSelected = alertSelected;
    }

    @NotNull
    @Override
    public String name() {
        return "HomeObserverModeViewModel";
    }

    public boolean isUserIsSupervisor() {
        return userIsSupervisor;
    }

    public void setUserIsSupervisor(boolean userIsSupervisor) {
        this.userIsSupervisor = userIsSupervisor;
    }
}
