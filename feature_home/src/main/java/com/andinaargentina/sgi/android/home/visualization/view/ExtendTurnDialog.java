package com.andinaargentina.sgi.android.home.visualization.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.ui_sdk.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtendTurnDialog extends Dialog {

    private TextView txtCancel, txtOk,txtActualTurnLabel,txtActualTurnHours,txtExtendedTurnHours;
    private ImageView imgClose;
    private Spinner spinner;
    private DialogClickHandler clickHandler;
    private List<LineDto> listLines;
    private String turn;

    public ExtendTurnDialog(@NonNull Context context,
                            @NonNull DialogClickHandler clickHandler,
                            @NonNull String turn) {
        super(context, R.style.ThemeDialogCustom);
        this.clickHandler = clickHandler;
        this.turn = turn;
        init(context);
    }

    private void init(@NonNull Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_extend_turn, null);
        setContentView(view);
        setCancelable(false);
        initComponent();
        show();
    }

    private void initComponent() {
        txtCancel = findViewById(R.id.dialog_extend_turn_txt_cancel);
        txtOk = findViewById(R.id.dialog_extend_turn_txt_ok);
        imgClose = findViewById(R.id.dialog_extend_turn_img_close);
        spinner = findViewById(R.id.activity_registration_spn_title);
        txtActualTurnLabel = findViewById(R.id.txt_actual_turn_label);
        txtActualTurnHours = findViewById(R.id.txt_actual_hours_turn);
        txtExtendedTurnHours = findViewById(R.id.txt_extend_turn_hours);

        imgClose.setOnClickListener(onClickListener);
        txtCancel.setOnClickListener(onClickListener);
        txtOk.setOnClickListener(onClickListener);

        txtActualTurnLabel.setText("Turno Actual " +"( "+ this.turn + " )");

        String MTurn = "06:00hs a 13:59hs";
        String Aturn = "14:00hs a 21:59hs";
        String NTurn = "22:00hs a 05:59hs";
        String MTurnExtended = "06:00hs a 17:59hs";
        String AturnExtended = "14:00hs a 01:59hs";
        String NTurnExtended = "22:00hs a 09:59hs";
        switch (this.turn){
            case "Tarde":
               txtActualTurnHours.setText(Aturn);
               txtExtendedTurnHours.setText(AturnExtended);
                break;
            case "Mañana":
                txtActualTurnHours.setText(MTurn);
                txtExtendedTurnHours.setText(MTurnExtended);
                break;
            case "Noche":
                txtActualTurnHours.setText(NTurn);
                txtExtendedTurnHours.setText(NTurnExtended);
                break;
        }
    }

    public void setListLines(List<LineDto> listLines) {
        this.listLines = listLines;
        String[] lines = new String[listLines.size()];
        for (int i = 0; i < listLines.size(); i++) {
            lines[i] = listLines.get(i).getDescription();
        }
        final List<String> linesList = new ArrayList<>(Arrays.asList(lines));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
                getContext(),android.R.layout.simple_spinner_item,linesList);

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinnerArrayAdapter.notifyDataSetChanged();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == txtCancel || v == imgClose) {
                dismiss();
            } else if (v == txtOk) {
                dismiss();
                clickHandler.positiveClick(listLines.get(spinner.getSelectedItemPosition()).getId());
            }
        }
    };

    public interface DialogClickHandler
    {
        void positiveClick(int idLineSelected);
    }

}
