package com.andinaargentina.sgi.android.home.linescontainer;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftResponseDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.danielecampogiani.underlinepageindicator.UnderlinePageIndicator;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.LINES_SELECTED;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_VIEW_NAME_HOME_LINES_CONTAINER;

@Route(path = ROUTE_VIEW_NAME_HOME_LINES_CONTAINER)
public class LinesContainerFragment extends FrameworkBaseFragment implements ViewPager.OnPageChangeListener {

    public static final String KEY_IS_GRAPHICS = "IS_GRAPHICS";
    public static final String KEY_LINE_ID_SELECTED = "ID_SELECTED";

    private ViewPager mViewPager;
    private UnderlinePageIndicator mBottomNavigationTabStrip;
    private boolean isGraphics;
    private int lineSelectedId = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            isGraphics = getArguments().getBoolean(KEY_IS_GRAPHICS);
            if (getArguments().containsKey(KEY_LINE_ID_SELECTED) && getArguments().getInt(KEY_LINE_ID_SELECTED)>=0)
                lineSelectedId = getArguments().getInt(KEY_LINE_ID_SELECTED);
        }
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState, R.layout.fragment_lines_container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!UtilSesion.checkSession(this.getActivity())){
            setUI();
        }
    }

    private void initUI() {
        tabs = getView().findViewById(R.id.tabs);
        mViewPager = getView().findViewById(R.id.vp);
        mBottomNavigationTabStrip = getView().findViewById(R.id.nts_bottom);
    }

    TabLayout tabs;
    LinePageAdapter linePageAdapter;

    private void setUI() {
        List<LineasProduccionDto> filteredList = new ArrayList<>();
        List<LineasProduccionDto> listLines = Paper.book("SGI-BOOK").read(LINES_SELECTED);
        if(listLines != null){
            for (LineasProduccionDto dto: listLines) {
                if (dto.isItemChecked())
                    filteredList.add(dto);
            }
        }
        linePageAdapter = new LinePageAdapter(getChildFragmentManager(),
                filteredList,isGraphics, false);

        mViewPager.setAdapter(linePageAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.addOnPageChangeListener(this);
        // do this in a runnable to make sure the viewPager's views are already instantiated before triggering the onPageSelected call
        mViewPager.post(() -> this.onPageSelected(mViewPager.getCurrentItem()));

        if (filteredList.size()>3){
            tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        mBottomNavigationTabStrip.setTabLayoutAndViewPager(tabs,mViewPager);
        int positionPage = 0;
        for (int i = 0; i < filteredList.size(); i++) {
            if (filteredList.get(i).getIdLinea() == lineSelectedId){
                positionPage = i;
            }
        }
        mViewPager.setCurrentItem(positionPage);
    }

    @Override
    public String getTagFragment() {
        return "ALERTAS PAGE";
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        new Handler().postDelayed(() -> {
            Log.d("Graphics","onPageSelected: " + position);

            //linePageAdapter.getActiveFragment().fragmentSelected();

        }, 500);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d("Graphics","onPageScrollStateChanged");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateNameViewPager(ExtendShiftResponseDto responseShiftExtendedDto){
        linePageAdapter.notifyDataSetChanged();
    }
}
