package com.andinaargentina.sgi.android.home.linescontainer;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.ui_sdk.components.IViewPagerChildFragment;

import java.util.List;

import static com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view.AlertsContainerFragment.KEY_IS_INFORMED_ALERTS;
import static com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view.AlertsContainerFragment.KEY_LINE_DTO;

public class LinePageAdapter extends FragmentStatePagerAdapter {

    private IViewPagerChildFragment pagerChildFragment;

    private List<LineasProduccionDto> listLines;

    private boolean isGraphics;
    private boolean isInformedAlerts;

    public LinePageAdapter(FragmentManager fm,
                           @NonNull List<LineasProduccionDto> listLines,
                           boolean isGraphics,
                           boolean isInformedAlerts) {
        super(fm);
        this.isGraphics = isGraphics;
        this.listLines = listLines;
        this.isInformedAlerts = isInformedAlerts;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if (isGraphics){
            fragment = getGraphicsFragments(position);
        }else{
            fragment = getAlertsFragment(position);
        }
        if (position == 0) {
            this.pagerChildFragment = (IViewPagerChildFragment) fragment;
        }
        return fragment;
    }

    private Fragment getAlertsFragment(int position){
        Bundle params = new Bundle();
        params.putSerializable(KEY_LINE_DTO, listLines.get(position));
        params.putBoolean(KEY_IS_INFORMED_ALERTS, isInformedAlerts);
        return (Fragment) ARouter.getInstance().build(ViewsNames.ROUTE_FRAGMENT_VIEW_NAME_ALERTS_CONTAINER).with(params).navigation();
    }

    private Fragment getGraphicsFragments(int position) {
        Bundle params = new Bundle();
        params.putSerializable("KEY_LINE_DTO", listLines.get(position));
        return (Fragment) ARouter.getInstance()
                .build(ViewsNames.ROUTE_FRAGMENT_VIEW_NAME_GRAPHICS_ITEM)
                .with(params)
                .navigation();
    }

    @Override
    public int getCount() {
        return listLines.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        LineasProduccionDto lineDtoAux =  listLines.get(position);
        String pageTitle = lineDtoAux.getDescLinea();
        /*if (lineDtoAux.isExtendProduccionShift())
            pageTitle += " (Ex)";*/
        return pageTitle;
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (getActiveFragment() != object) {
            pagerChildFragment = (IViewPagerChildFragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }

    public IViewPagerChildFragment getActiveFragment() {
        return pagerChildFragment;
    }

}