package com.andinaargentina.sgi.android.home.visualization.repository;

import android.content.Context;
import android.util.Log;

import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftRequestDto;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftResponseDto;
import com.andinaargentina.core_services.providers.api.clients.extendshift.ExtendShiftService;
import com.andinaargentina.core_services.providers.api.clients.logout.LogoutService;
import com.andinaargentina.core_services.providers.api.clients.logout.UserLogoutDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderRequestDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderResponseDto;
import com.andinaargentina.core_services.providers.api.clients.orders.ChangeOrderService;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.home.visualization.presenter.IHomeObserverModeRequestHandler;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;

import org.greenrobot.eventbus.EventBus;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.andinaargentina.RepositoryConst.USER_LOGGED;

public class HomeObserverModeRepository implements IHomeObserverModeRequestHandler {

    private LogoutService logoutService;
    private ChangeOrderService changeOrderService;
    private ExtendShiftService extendShiftService;
    private CompositeDisposable compositeDisposable;

    public HomeObserverModeRepository() {
        this.logoutService = new LogoutService(ApiRestModule.providesApiService(false));
        this.changeOrderService = new ChangeOrderService(ApiRestModule.providesApiService(false));
        this.extendShiftService = new ExtendShiftService(ApiRestModule.providesApiService(false));
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void logout(Context context, boolean isShiftEnded) {
        new UtilSesion().logout(context, isShiftEnded);
    }

    @Override
    public void changeOrder(Context context, ChangeOrderRequestDto changeOrderRequestDto) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = changeOrderService
                    .postChangeOrder(changeOrderRequestDto,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getChangeOrderObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    @Override
    public void extendShift(Context context, ExtendShiftRequestDto extendShiftRequestDto) {
        if (!compositeDisposable.isDisposed()) {
            Disposable requestGetProductsDisposable = extendShiftService
                    .postExtendShift(extendShiftRequestDto,context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getExtendShiftObserver());
            compositeDisposable.add(requestGetProductsDisposable);
        }
    }

    private DisposableSingleObserver<ExtendShiftResponseDto> getExtendShiftObserver() {
        return new DisposableSingleObserver<ExtendShiftResponseDto>() {
            @Override
            public void onSuccess(ExtendShiftResponseDto changeOrderRequestDto) {
                Log.i("ExtendShift", "petición exitosa");
                EventBus.getDefault().post(changeOrderRequestDto);
            }

            @Override
            public void onError(Throwable e)
            {
                Log.i("ExtendShift", "petición con error");
            }
        };
    }

    private DisposableSingleObserver<ChangeOrderResponseDto> getChangeOrderObserver() {
        return new DisposableSingleObserver<ChangeOrderResponseDto>() {
            @Override
            public void onSuccess(ChangeOrderResponseDto changeOrderRequestDto) {
                Log.i("ChangeOrder", "petición exitosa");
            }

            @Override
            public void onError(Throwable e) {
                Log.i("ChangeOrder", "petición con error");
            }
        };
    }
}
