package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual;

import android.os.Build;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.andinaargentina.core_services.providers.apiv2.models.EquiposEstadosDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.utils.EquipoEstadosUtils;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual.EstatusActualDto.COLOR_TYPE;
import com.andinaargentina.ui_sdk.components.recyclers.BaseHolder;

import java.util.Optional;

@RequiresApi(api = Build.VERSION_CODES.N)
public class EstatusActualEquiposHolder extends BaseHolder<MachineActualStatusDto> {

    private TextView txtTitleStatusActual;
    private TextView txtCountStatusActual;
    private View container;
    private MachineActualStatusDto dto;

    public EstatusActualEquiposHolder(View itemView) {
        super(itemView);
        bindView(itemView);
    }

    @Override
    public void setDataInHolder(MachineActualStatusDto data) {
        this.dto = data;
        StringBuilder sb = new StringBuilder(data.getDescripcionEquipo());
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        txtTitleStatusActual.setText(sb.toString());
        txtCountStatusActual.setText(String.valueOf((int) data.getMinutosAcumulados()));
        container.setSelected(data.isSelected());

        Optional<EquiposEstadosDto> estado = EquipoEstadosUtils.getInstance().getEstados().stream().filter(f -> f.getIdRegistro() == this.dto.getIdEstado()).findFirst();
        if(estado.isPresent()) {
            setTypeCountColor(estado.get().getEstado());
        } else {
            setTypeCountColor(0);
        }

    }

    private void bindView(View view){
        txtTitleStatusActual = view.findViewById(R.id.item_estatus_actual_txt_title);
        txtCountStatusActual = view.findViewById(R.id.item_estatus_actual_txt_count);
        container = view.findViewById(R.id.item_performance_actual_equipos_status_container);
    }

    private EstatusActualDto.COLOR_TYPE getCorrectColor(int type){
        EstatusActualDto.COLOR_TYPE typeValue = EstatusActualDto.COLOR_TYPE.PO;
        switch (type){
            case 1:{
                typeValue = EstatusActualDto.COLOR_TYPE.PO;
                break;
            }
            case 2:{
                typeValue = EstatusActualDto.COLOR_TYPE.FP;
                break;
            }
            case 3:{
                typeValue = EstatusActualDto.COLOR_TYPE.EP;
                break;
            }
            case 7:{
                typeValue = EstatusActualDto.COLOR_TYPE.PF;
                break;
            }
            case 4:{
                typeValue = EstatusActualDto.COLOR_TYPE.PP;
                break;
            }
            case 5:{
                typeValue = EstatusActualDto.COLOR_TYPE.PA;
                break;
            }case 6:{
                typeValue = EstatusActualDto.COLOR_TYPE.PD;
                break;
            }
            case 8:{
                typeValue = EstatusActualDto.COLOR_TYPE.SV;
                break;
            }
            case 0:{
                typeValue = EstatusActualDto.COLOR_TYPE.ZERO;
            }
        }
        return typeValue;
    }


    private void setTypeCountColor(int status) {
        COLOR_TYPE valueCOLORType = COLOR_TYPE.ZERO;
        int correctColorBg = 0;
        int correctColorText = 0;
        switch (getCorrectColor(status)) {
            case FP:{
                correctColorBg = R.color.color_reference_info_performance_fuera_de_produccion;
                correctColorText = R.color.colorWhite;
                break;
            }
            case PO:{
                correctColorBg = R.color.color_reference_info_performance_parada_operacional;
                correctColorText = R.color.colorBlack;
                break;
            }
            case EP:{
                correctColorBg = R.color.color_reference_info_performance_en_produccion;
                correctColorText = R.color.colorWhite;
                break;
            }
            case PF:{
                correctColorBg = R.color.color_reference_info_performance_parada_funcional;
                correctColorText = R.color.colorBlack;
                break;
            }
            case PP:{
                correctColorBg = R.color.color_reference_info_performance_parada_propia;
                correctColorText = R.color.colorWhite;
                break;
            }
            case PA:{
                correctColorBg = R.color.color_reference_info_performance_parada_de_abastecimiento;
                correctColorText = R.color.colorWhite;
                break;
            }
            case PD:{
                correctColorBg = R.color.color_reference_info_performance_parada_de_descarga;
                correctColorText = R.color.colorWhite;
                break;
            }
            case SV:{
                correctColorBg = R.color.color_reference_info_performance_sub_velocidad;
                correctColorText = R.color.colorBlack;
                break;
            }
            case ZERO:{
                correctColorBg = R.color.color_reference_info_sin_estado;
                correctColorText = R.color.colorBlack;
            }
        }
        txtCountStatusActual.setTextColor(getThisView().getResources().getColor(correctColorText));
        txtCountStatusActual.setBackgroundColor(getThisView().getResources().getColor(correctColorBg));
    }

}
