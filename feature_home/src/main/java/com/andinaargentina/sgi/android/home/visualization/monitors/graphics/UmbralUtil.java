package com.andinaargentina.sgi.android.home.visualization.monitors.graphics;

public class UmbralUtil {

    public enum UMBRAL_TYPE {
        NORMAL,
        WARNING,
        CRITICAL
    }

    public static UMBRAL_TYPE getCOLORType(String threshold) {
        UMBRAL_TYPE colorTypeCorrect = UMBRAL_TYPE.NORMAL;
        switch (threshold){
            case "NORMAL":{
                colorTypeCorrect = UMBRAL_TYPE.NORMAL;
                break;
            }
            case "WARNING":{
                colorTypeCorrect = UMBRAL_TYPE.WARNING;
                break;
            }
            case "CRITICAL":{
                colorTypeCorrect = UMBRAL_TYPE.CRITICAL;
                break;
            }
        }
        return colorTypeCorrect;
    }
}
