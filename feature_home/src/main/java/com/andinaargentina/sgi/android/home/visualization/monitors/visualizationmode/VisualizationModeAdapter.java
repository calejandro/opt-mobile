package com.andinaargentina.sgi.android.home.visualization.monitors.visualizationmode;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.andinaargentina.core_services.providers.api.clients.efficiency.EfficiencyDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.components.recyclers.BaseAdapter;

import java.util.List;

public class VisualizationModeAdapter extends BaseAdapter<LineasProduccionDto> {

    private boolean isPlanMode;

    public VisualizationModeAdapter(boolean isPlanMode,
                                    @NonNull List<LineasProduccionDto> dataList,
                                    @NonNull IHolderItemClick clickListener){
        super(dataList, clickListener);
        this.isPlanMode = isPlanMode;
    }

    @NonNull
    @Override
    public VisualizationModeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_visualization_mode,
                parent,false);
        return new VisualizationModeHolder(v, isPlanMode);
    }
}
