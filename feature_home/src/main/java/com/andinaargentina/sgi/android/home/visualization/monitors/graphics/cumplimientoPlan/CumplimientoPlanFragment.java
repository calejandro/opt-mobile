package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.cumplimientoPlan;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment.CombinedChart_;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment.Item;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.productionfulfillment.ProductionFulfillment;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.ScaleStackMode;

import java.util.ArrayList;
import java.util.List;

public class CumplimientoPlanFragment extends FrameworkBaseFragment {

    private static final String COLOR_OBJETIVO_DESEADO = "#ECECEC";
    private static final String COLOR_SUB_PRODUCIDO = "#E63A33";
    private static final String COLOR_SOBRE_PRODUCIDO = "#28C9E6";
    private static final String COLOR_OBJETIVO_PRODUCIDO = "#63BE50";

    private static final String COLOR_CONTINUOUS_LINE = "#63BE50";
    private static final String COLOR_DASH_LINE = "#63BE50";

    private static final String NAME_VALUE_OBJETIVO = "";
    private static final String NAME_VALUE_PRODUCIDO = "";

    public static final String KEY_INPUT_DATA = "KEY_FULLFILMENT_DATA";

    private ProductionFulfillment productionFulfillmentDto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState, R.layout.layout_column_bar_combined);
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        this.productionFulfillmentDto = (ProductionFulfillment) getArguments().getSerializable(KEY_INPUT_DATA);
    }

    public void startChart() {
        CombinedChart_ combinedChart_ = this.productionFulfillmentDto.getCombinedChart();

        AnyChartView anyChartView = getView().findViewById(R.id.any_chart_view_1);
        APIlib.getInstance().setActiveAnyChartView(anyChartView);
        anyChartView.setProgressBar(getView().findViewById(R.id.progress_bar_1));

        Cartesian cartesian = AnyChart.cartesian();
        cartesian.animation(true);

        cartesian.lineMarker(0).enabled(true);
        cartesian.lineMarker(0).value(combinedChart_.getAxisYMaxValue()).stroke("{thickness: 1, dash: \"4 2\", color: \"#5E7F7E\"}");

        cartesian.yScale().minimum(0d);
        cartesian.xAxis(0).title(combinedChart_.getAxisXName());
        cartesian.xAxis(0).labels().rotation(-90);
        cartesian.yAxis(0).title(combinedChart_.getAxisYName());
        cartesian.yScale().stackMode(ScaleStackMode.VALUE);


        List<DataEntry> data = new ArrayList<>();
        for (Item item: this.productionFulfillmentDto.getCombinedChart().getItems()) {
            data.add(new CustomDataEntry(item.getTime(),
                    item.getPlanBottles()-item.getProduceBottles(),
                    item.getProduceBottles(),
                    item.getProduceBottles()));
        }

        Set set = Set.instantiate();
        set.data(data);
        Mapping column1Data = set.mapAs("{ x: 'x', value: 'value', fill: \"#5cd65c\" }");
        Mapping column2Data = set.mapAs("{ x: 'x', value: 'value2' }");

        cartesian.column(column1Data)
                .fill(COLOR_OBJETIVO_DESEADO)
                .stroke(COLOR_OBJETIVO_DESEADO)
                .tooltip().format("Envases: {%value}");

        cartesian.crosshair(false);

        cartesian.stepLine(set.mapAs("{ x: 'x', value: 'value3' }"),"").stroke("#5E7F7E");

        cartesian.column(column2Data)
                .fill(COLOR_SUB_PRODUCIDO)
                .stroke(COLOR_SUB_PRODUCIDO)
                .tooltip().format("Envases: {%value2}");

        anyChartView.setChart(cartesian);
    }

    private class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value, Number value2, Number value3) {
            super(x, value);
            setValue("value2", value2);
            setValue("value3", value3);
        }
    }

    @Override
    public String getTagFragment() {
        return null;
    }
}
