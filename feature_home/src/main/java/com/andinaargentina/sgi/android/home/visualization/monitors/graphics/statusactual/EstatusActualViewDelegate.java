package com.andinaargentina.sgi.android.home.visualization.monitors.graphics.statusactual;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andinaargentina.core_services.providers.api.clients.graphics.dto.GraphicsDto;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance.Details_;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance.EvolutionChart;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance.Historical;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance.Machine;
import com.andinaargentina.core_services.providers.api.clients.graphics.dto.machineperformance.Value;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.apiv2.models.AverageTimeFailuresProductionDto;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionAlertasParadaMaquinaDto;
import com.andinaargentina.core_services.providers.apiv2.models.ConfiguracionUmbralesTmefTmprDto;
import com.andinaargentina.core_services.providers.apiv2.models.EquiposEstadosDto;
import com.andinaargentina.core_services.providers.apiv2.models.EvolucionParadaPropiaDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.MachineActualStatusDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.TimeStopsDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.utils.ConfiguracionUmbralesUtils;
import com.andinaargentina.core_services.providers.apiv2.utils.EquipoEstadosUtils;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.GraphicsFragment;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.UmbralUtil;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual.EstatusActualDto;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual.EstatusActualEquiposAdapater;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual.EstatusActualParadaPropiaAdapater;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.adapters.estatusActual.ParadaPropiaDto;
import com.andinaargentina.sgi.android.home.visualization.monitors.graphics.dialogs.PerformaceEquiposColorsReferenceDialog;
import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.google.android.gms.common.api.Api;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@SuppressLint("SetTextI18n")
@RequiresApi(api = Build.VERSION_CODES.N)
public class EstatusActualViewDelegate {

    private LineasProduccionDto lineDto;
    private List<MachineActualStatusDto> graphicsDto;

    //Performance equipos
    private RecyclerView estatusActualEquiposRecyclerView;
    private EstatusActualEquiposAdapater estatusActualEquiposAdapater;

    //Detalle de performance de equipos
    private TextView txtTitlePerformanceEquiposDetail;
    private TextView txtSubtitlePerformanceEquiposDetail;
    private TextView txtTurnTMEF;
    private TextView txtTurnTMPR;
    private TextView txtHourTMEF;
    private TextView txtHourTMPR;
    private RecyclerView paradaPropiaRecyclerView;
    private EstatusActualParadaPropiaAdapater statusActualParadaPropiaAdapter;
    private MachineActualStatusDto machineActualStatusDto;

    //Textos
    private TextView txtHoraActual;

    private Dialog dialogPerformanceEquipos;
    private int lastPositionSelected;
    private SimpleDateFormat simpleDateFormatHour = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public EstatusActualViewDelegate(LineasProduccionDto lineDto, List<MachineActualStatusDto> graphicsDto) {
        this.lineDto = lineDto;
        this.graphicsDto = graphicsDto;
    }

    public void initView(@NonNull View view){
        this.machineActualStatusDto = this.graphicsDto.get(0);
        estatusActualEquiposRecyclerView = view.findViewById(R.id.estatus_actual_equipos_recycler_view);
        paradaPropiaRecyclerView = view.findViewById(R.id.estatus_actual_detalle_parada_propia_recycler_view);
        initChart(view);
        initDetalleEquipoTitles(view);
        initInfoButton(view);
        //initMachinesRecycler(view);
        updateDetailMachineData(view, this.machineActualStatusDto);


        //updateMachinesRecycler();
    }

    private void initChart(View view) {
    }

    private void initEvolutionChart(View view, MachineActualStatusDto evolutionChartData) {
        EventBus.getDefault().post(new SelectMachineEvent(machineActualStatusDto.getIdEquipo(), machineActualStatusDto.getIdLinea()));
    }

    private void initDetalleEquipoTitles(View view) {
        //Detalle de equipos
        txtTitlePerformanceEquiposDetail = view.findViewById(R.id.estatus_actual_detalle_txt_title);
        txtSubtitlePerformanceEquiposDetail = view.findViewById(R.id.estatus_actual_detalle_txt_subtitle);
        txtTurnTMEF = view.findViewById(R.id.estatus_actual_detalle_parada_propia_tmb_turn_tmef);
        txtTurnTMPR = view.findViewById(R.id.estatus_actual_detalle_parada_propia_tmb_turn_tmpr);
        txtHourTMEF = view.findViewById(R.id.estatus_actual_detalle_parada_propia_tmb_hour_tm_ef);
        txtHourTMPR = view.findViewById(R.id.estatus_actual_detalle_parada_propia_tmb_hour_tm_pr);
    }


    private void initParadaPropiaRecycler(@NonNull View view) {

        OptimusServices.getInstance().getPerformanceEquiposService().listTimeStops(
                lineDto.getIdLinea(),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()),
                4
                ).enqueue(new Callback<ArrayList<TimeStopsDto>>() {
            @Override
            public void onResponse(Call<ArrayList<TimeStopsDto>> call, Response<ArrayList<TimeStopsDto>> response) {
                List<TimeStopsDto> timeStopsDtos = new ArrayList<TimeStopsDto>();
                for (TimeStopsDto t:
                     response.body()) {
                    if (t.getIdEquipo() == machineActualStatusDto.getIdEquipo()){
                        timeStopsDtos.add(t);
                    }
                }

                final LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(),
                            RecyclerView.HORIZONTAL, false);
                if(paradaPropiaRecyclerView.getAdapter() != null){
                    statusActualParadaPropiaAdapter.getListData().clear();
                    statusActualParadaPropiaAdapter.setListData(timeStopsDtos);
                    statusActualParadaPropiaAdapter.notifyDataSetChanged();
                } else {
                    statusActualParadaPropiaAdapter = new EstatusActualParadaPropiaAdapater(timeStopsDtos);
                    paradaPropiaRecyclerView.setAdapter(statusActualParadaPropiaAdapter);
                    paradaPropiaRecyclerView.setNestedScrollingEnabled(false);
                    paradaPropiaRecyclerView.setLayoutManager(layoutManager);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TimeStopsDto>> call, Throwable t) {
            }
        });

    }


    private void initMachinesRecycler(@NonNull View view, MachineActualStatusDto dto) {
        this.txtHoraActual.setText(simpleDateFormatHour.format(this.machineActualStatusDto.getFechaHora()) + " hs");

        //Creo adapter para el listado de máquinas
        final LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(),
                    RecyclerView.VERTICAL, false);
        if (this.estatusActualEquiposRecyclerView.getAdapter() == null) {
            this.estatusActualEquiposAdapater = new EstatusActualEquiposAdapater(this.graphicsDto, (caller, position, bundle) -> {
                clickOnMachinesStatusActualItem(position,caller);
            });
            this.estatusActualEquiposRecyclerView.setAdapter(estatusActualEquiposAdapater);
            this.estatusActualEquiposRecyclerView.setNestedScrollingEnabled(false);
            this.estatusActualEquiposRecyclerView.setLayoutManager(layoutManager);
        } else {
            this.graphicsDto.stream().forEach(f -> f.setSelected(false));
            dto.setSelected(true);
            MachineActualStatusDto actualDto = this.graphicsDto.stream().filter(f -> f.getIdEquipo() == dto.getIdEquipo()).findFirst().get();
            this.graphicsDto.set(this.graphicsDto.indexOf(actualDto), dto);
            estatusActualEquiposAdapater.setListData(this.graphicsDto);
            estatusActualEquiposAdapater.notifyDataSetChanged();
        }
    }

/*
    private List<EstatusActualDto> getDataForMachinesStatus(){
        //Obtengo la data para las máquinas desde el dato de gráfico
        //obtengo datos para el listado de máquinas
        List<EstatusActualDto> listData = new ArrayList<>();
        EstatusActualDto estatusActualDtoAux = null;
        int i=0;
        for (Machine machine:this.graphicsDto.getMachinePerfomance().getMachines()) {
            estatusActualDtoAux = new EstatusActualDto(machine.getMachineName(), getCorrectColor(machine.getType()),machine.getMinutes());
            if (i == lastPositionSelected){
                estatusActualDtoAux.setSelected(true);
            }
            listData.add(estatusActualDtoAux);
            i++;
        }
        return listData;
    }
*/

    private void clickOnMachinesStatusActualItem(int position, View view) {
        this.lastPositionSelected = position;
        this.machineActualStatusDto =  this.graphicsDto.get(this.lastPositionSelected);

        updateDetailMachineData(view, machineActualStatusDto);
        //initEvolutionChart(view, this.graphicsDto);
        //
        /*
        EstatusActualDto dtoSelected = estatusActualEquiposAdapater.getListData().get(position);
        for (EstatusActualDto dtoAux:estatusActualEquiposAdapater.getListData()) {
            if (dtoAux != dtoSelected){
                dtoAux.setSelected(false);
            }else{
                dtoAux.setSelected(true);
            }
        }
        estatusActualEquiposAdapater.notifyDataSetChanged();
        updateDetailMachineData(view);
        updateView(this.graphicsDto,view);*/
    }

    private void initInfoButton(@NonNull View view) {
        ImageView btnInfoColorReference = view.findViewById(R.id.fragment_graphics_performance_equipos_dialog);
        btnInfoColorReference.setOnClickListener(v-> showColorReferencePerformanceEquposDialog(view.getContext()));
        //Button left
        this.txtHoraActual = view.findViewById(R.id.estatus_actual_equipos_txt_hr);
    }

    public void showColorReferencePerformanceEquposDialog(Context context){
        this.dialogPerformanceEquipos = new PerformaceEquiposColorsReferenceDialog(context);
        this.dialogPerformanceEquipos.show();
    }

    @SuppressLint("SetTextI18n")
    private void updateDetailMachineData(View view, MachineActualStatusDto machine) {

        machine.setSelected(true);
        initMachinesRecycler(view, machine);

        //Cambio nombre del label de detalle
        String titleEquipo =  view.getContext().getString(R.string.estatus_actual_detalle_title);
        txtTitlePerformanceEquiposDetail.setText( titleEquipo + " "+ machine.getDescripcionEquipo());

        //Cambio nombre de subtitulo por el estado que le corresponde a la máquina
        //txtSubtitlePerformanceEquiposDetail.setText(machine.getFullNameType());
        this.initParadaPropiaRecycler(view);
        txtSubtitlePerformanceEquiposDetail.setText("Minutos en Parada propia");

//        Details_ machineDetails = machine.getDetails();
//        for (Historical historicalDto:machineDetails.getHistorical()) {
//            listData.add(new ParadaPropiaDto(String.valueOf(historicalDto.getMinutes()),historicalDto.getTime(), historicalDto.getThreshold()));
//        }
//        this.statusActualParadaPropiaAdapter.setListData(listData);
//        this.statusActualParadaPropiaAdapter.notifyDataSetChanged();
        //LLeno datos y umbrales de TMEF TMPR
        //Hour
        OptimusServices.getInstance().getPerformanceEquiposService().listAverageTimeFailuresProduction(
                lineDto.getIdLinea(),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getInicio()),
                simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin())
                ).enqueue(new Callback<ArrayList<AverageTimeFailuresProductionDto>>() {
            @Override
            public void onResponse(Call<ArrayList<AverageTimeFailuresProductionDto>> call, Response<ArrayList<AverageTimeFailuresProductionDto>> response) {
                List<AverageTimeFailuresProductionDto> averageTimeFailuresProductionDtos = new ArrayList<AverageTimeFailuresProductionDto>();
                for (AverageTimeFailuresProductionDto t:
                        response.body()) {
                    if (t.getIdEquipo() == machineActualStatusDto.getIdEquipo()){
                        averageTimeFailuresProductionDtos.add(t);
                    }
                }
                if(averageTimeFailuresProductionDtos.size() > 0) {
                    Double tmefHora = averageTimeFailuresProductionDtos.get(0).getTmefHora();
                    Double tmprHora = averageTimeFailuresProductionDtos.get(0).getTmprHora();
                    Double tmefTurno = averageTimeFailuresProductionDtos.get(0).getTmefTurno();
                    Double tmprTurno = averageTimeFailuresProductionDtos.get(0).getTmprTurno();

                    txtHourTMEF.setText(String.valueOf(tmefHora));
                    txtHourTMPR.setText(String.valueOf(tmprHora));
                    txtTurnTMEF.setText(String.valueOf(tmefTurno));
                    txtTurnTMPR.setText(String.valueOf(tmprTurno));

                    Integer idEquipo = machine.getIdEquipo();
                    List<ConfiguracionUmbralesTmefTmprDto> estadosUmbrales = ConfiguracionUmbralesUtils.getInstance().getEstadosUmbrales(machine.getIdLinea());
                    List<ConfiguracionUmbralesTmefTmprDto> umbralEquipo = estadosUmbrales.stream().filter(
                            f -> f.getIdEquipo() == idEquipo).collect(Collectors.toList());

                    for (ConfiguracionUmbralesTmefTmprDto umbrales:
                          umbralEquipo) {
                        if (new String(umbrales.getTipo()).equals("Tmef")){
                            if(umbrales.getValorMinimo() <= tmefHora && tmefHora <= umbrales.getValorMaximo()){
                                txtHourTMEF.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
                            } else if(tmefHora < umbrales.getValorMinimo()){
                                txtHourTMEF.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
                            } else if(tmefHora > umbrales.getValorMaximo()){
                                txtHourTMEF.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
                            }

                            if(umbrales.getValorMinimo() <= tmefTurno && tmefTurno <= umbrales.getValorMaximo()){
                                txtTurnTMEF.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
                            } else if(tmefTurno < umbrales.getValorMinimo()){
                                txtTurnTMEF.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
                            } else if(tmefTurno > umbrales.getValorMaximo()){
                                txtTurnTMEF.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
                            }
                        } else {
                            if(umbrales.getValorMinimo() <= tmprHora && tmprHora <= umbrales.getValorMaximo()){
                                txtHourTMPR.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
                            } else if(tmprHora < umbrales.getValorMinimo()){
                                txtHourTMPR.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
                            } else if(tmprHora > umbrales.getValorMaximo()){
                                txtHourTMPR.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
                            }
                            if(umbrales.getValorMinimo() <= tmprTurno && tmprTurno <= umbrales.getValorMaximo()){
                                txtTurnTMPR.setBackgroundResource(R.drawable.bg_shape_line_all_sides);
                            } else if(tmprTurno < umbrales.getValorMinimo()){
                                txtTurnTMPR.setBackgroundResource(R.drawable.bg_shape_line_all_sides_yellow);
                            } else if(tmprTurno > umbrales.getValorMaximo()){
                                txtTurnTMPR.setBackgroundResource(R.drawable.bg_shape_line_all_sides_red);
                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<AverageTimeFailuresProductionDto>> call, Throwable t) {

            }
        });

        initEvolutionChart(view, machine);

    }

/*    @NotNull
    private List<ParadaPropiaDto> getParadaPropiaData() {
        List<ParadaPropiaDto> listData = new ArrayList<>();
        *//*Details_ machineDetails = graphicsDto.getMachinePerfomance().getMachines().get(0).getDetails();
        for (Historical historicalDto:machineDetails.getHistorical()) {
            listData.add(new ParadaPropiaDto(String.valueOf(historicalDto.getMinutes()),historicalDto.getTime(), historicalDto.getThreshold()));
        }*//*
        return listData;
    }*/

    public void updateView(GraphicsDto graphicsDto, View view){

        /*this.graphicsDto = graphicsDto;
        APIlib.getInstance().setActiveAnyChartView(performanceMachinesEvolutionChart);
        List<DataEntry> seriesData = new ArrayList<>();
        for (Value value: this.graphicsDto
                .getMachinePerfomance()
                .getMachines()
                .get(
                )
                .getDetails()
                .getEvolutionChart().getValues()) {
            seriesData.add(new GraphicsFragment.CustomDataEntry(value.getTime(), value.getBottles()));
        }
        performanceEvolutionDataSet.data(seriesData);
        updateMachinesRecycler();
        updateParadaPropiaRecycler();
        updateDetailMachineData(view, this.graphicsDto.getMachinePerfomance().getMachines().get(lastPositionSelected));
        */
    }


//    public void clearView() {
//        this.performanceMachinesEvolutionChart.clear();
//    }
}
