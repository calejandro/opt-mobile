package com.andinaargentina.sgi.android.home.visualization.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.work.WorkManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.ProductionOrders;
import com.andinaargentina.core_services.providers.api.clients.plants.PlantDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.PlantasDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.visualization.presenter.HomeObserverModePresenter;
import com.andinaargentina.sgi.android.home.visualization.presenter.IHomeObserverModeViewUpdateHandler;
import com.andinaargentina.sgi.android.home.visualization.repository.HomeObserverModeRepository;
import com.andinaargentina.sgi.android.home.visualization.viewmodel.HomeObserverModeViewModel;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.andinaargentina.sgi.core_sdk.base.times.UtilTimes;
import com.andinaargentina.ui_sdk.components.dialog.LogoutConfirmDialog;
import com.andinaargentina.ui_sdk.components.dialog.ProductionOrderChangeDialog;
import com.andinaargentina.ui_sdk.components.dialog.lineorder.OrderDialogModel;
import com.google.android.material.navigation.NavigationView;
import com.pixplicity.easyprefs.library.Prefs;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.paperdb.Paper;

import static com.andinaargentina.RepositoryConst.IS_OBSERVER_MODE;
import static com.andinaargentina.RepositoryConst.LINES_SELECTED;
import static com.andinaargentina.RepositoryConst.PLANT_SELECTED;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_VIEW_NAME_HOME_LINES_CONTAINER;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_VIEW_NAME_VISUALIZATION_MODE;
import static com.andinaargentina.sgi.android.home.linescontainer.LinesContainerFragment.KEY_LINE_ID_SELECTED;


@RequiresApi(api = Build.VERSION_CODES.N)
public class HomeObserverModeViewDelegate implements IHomeObserverModeViewUpdateHandler,
                                             NavigationView.OnNavigationItemSelectedListener {

    public static final int GROUP_ID_LINES = 0;
    public static final int GROUP_ID_CHANGE_LINES = 1;
    public static final int GROUP_ID_EXTEND_TURN = 2;
    public static final int GROUP_ID_ALERTS = 2;
    public static final int GROUP_ID_LOGOUT = 2;

    private WeakReference<AppCompatActivity> rootView;
    private HomeObserverModePresenter presenter;

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    //Header
    private View viewContainer;
    private TextView txtHeaderUserName;
    private TextView txtUserType;
    private TextView txtModeUser;
    private TextView txtHeaderPlantName;
    private TextView txtHeaderLastUpdate;
    private TextView txtHeaderTurn;
    private TextView txtWorkManagerDate;
    private TextView txtHeaderNameShift;
    private TextView txtHeaderTurnTimer;
    //Botton bar
    private ImageButton imgBottonBarAlert;
    private ImageView imgBottonBarGraphics;

    private Dialog logoutDialog;
    private ExtendTurnDialog extendTurnDialog;
    private Dialog changeOrderDialog;

    public HomeObserverModeViewDelegate(AppCompatActivity rootView){
        this.rootView = new WeakReference<>(rootView);
        this.presenter = new HomeObserverModePresenter(this, new HomeObserverModeRepository(), getViewModel());
        initViews();
        presenter.setViewModel(getViewModel());
        presenter.onCreatedView();
    }

    private HomeObserverModeViewModel getViewModel() {
        SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        if(user == null){
            UtilSesion.clearSessionAndExit(rootView.get());
            rootView.get().finish();
            return null;
        }
        boolean isSupervisor = Prefs.getBoolean(IS_OBSERVER_MODE, true);
        boolean userHasAnyLineWithShiftExtend = false;
        List<LineasProduccionDto> lineDtoList = Paper.book("SGI-BOOK").read(LINES_SELECTED);
        List<LineasProduccionDto> lineSelected = new ArrayList<>();
        if (lineDtoList!=null){
            for (LineasProduccionDto dto: lineDtoList) {
                if (dto.isItemChecked()){
                    lineSelected.add(dto);
                }
                if (isSupervisor && dto.getLineasProduccionUsuarioDto().getUserId() == user.getUserId())
                    userHasAnyLineWithShiftExtend = true;
            }
        }
        PlantasDto planSelected = Paper.book("SGI-BOOK").read(PLANT_SELECTED);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String turn = UtilTimes.getInitTurn() + " - " + UtilTimes.getEndTurn() + " hs" ;
        String turnName = UtilTimes.getTurnByTime();
        String workManagerDate = "";
        if (userHasAnyLineWithShiftExtend){
            workManagerDate = UtilTimes.setAlarmForChangeTurn(true);
        }else{
            workManagerDate = UtilTimes.setAlarmForChangeTurn(false);
        }
        String dateString = format.format(new Date());
        return new HomeObserverModeViewModel(lineSelected,
                planSelected,
                0,
                true,
                user.getName(),
                dateString,
                turn,
                turnName + "",
                isSupervisor,
                user.ismEsSupervisor(),
                workManagerDate);
    }

    private void initViews(){
        this.toolbar = this.rootView.get().findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.hideOverflowMenu();
        this.rootView.get().setSupportActionBar(toolbar);

        this.drawer =  this.rootView.get().findViewById(R.id.drawer_layout);
        this.navigationView = this.rootView.get().findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this.rootView.get(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //Header
        this.viewContainer = this.navigationView.getHeaderView(0).findViewById(R.id.header_view_container);
        this.txtHeaderLastUpdate = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_info_last_update);
        this.txtHeaderPlantName = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_plant_name);
        this.txtHeaderTurn = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_user_info_last_login);
        this.txtWorkManagerDate = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_alarm_configure);
        this.txtHeaderNameShift = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_user_name_shift);
        this.txtHeaderUserName = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_name_user);
        this.txtUserType = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_name_type_user);
        this.txtModeUser = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_name_mode);
        this.txtHeaderTurnTimer = this.navigationView.getHeaderView(0).findViewById(R.id.header_txt_counter_timer);
        //BottonBar
        this.imgBottonBarAlert = this.rootView.get().findViewById(R.id.gral_menu_img_alerts);
        this.imgBottonBarGraphics = this.rootView.get().findViewById(R.id.gral_menu_img_graphics);

        setOnClickListeners();
        setGraphicsOnContainer(-1);
    }

    public void onResume(){

    }

    @Override
    public void setViewOnObserverMode() {
        this.imgBottonBarAlert.setImageResource(R.drawable.i_factory_red);
        this.imgBottonBarGraphics.setImageResource(R.drawable.i_graphic_red);
    }

    @Override
    public void setViewOnSupervisorMode() {
        this.imgBottonBarAlert.setImageResource(R.drawable.i_alert_red);
        this.imgBottonBarGraphics.setImageResource(R.drawable.i_graphic_red);
    }

    private void setOnClickListeners() {
        this.imgBottonBarAlert.setOnClickListener(v -> {
            setButtomGraphicSelected(false);
            setButtomAlertSelected(true);
            presenter.onAlertIconPressed();
        });

        this.imgBottonBarGraphics.setOnClickListener(v -> {
            setButtomAlertSelected(false);
            setButtomGraphicSelected(true);
            presenter.onGraphicIconPressed();
        });
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        boolean isSupervisor = Prefs.getBoolean(IS_OBSERVER_MODE, true);

        if ( item.getGroupId() == GROUP_ID_CHANGE_LINES){
            this.presenter.onSelectedLine(item.getItemId());
            ARouter.getInstance()
                    .build(ViewsNames.ROUTE_VIEW_NAME_LINE_SELECTOR)
                    .withBoolean("withBackButton",true)
                    .withBoolean("isSupervisorMode",true)
                    .navigation(this.rootView.get());
        }
        if (item.getGroupId() == GROUP_ID_LINES){
            if (isSupervisor){
                this.presenter.onChangeOrderInLine(item.getItemId());
            }else {
                this.presenter.onSelectedLine(item.getItemId());
            }
        }
        if ( item.getGroupId() == GROUP_ID_LOGOUT){
            if (isSupervisor){
                switch (item.getItemId()){
                    case 1 : {
                        this.presenter.onExtendTurn();
                        break;
                    }
                    case 2 : {
                        this.presenter.onAlertsInformed();
                        break;
                    }
                    case 3:{
                        this.presenter.onLogout();
                    }
                }
            }else{
                this.presenter.onLogout();
            }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void setPlantNameOnTopBar(@NonNull String plantNameOnTopBar) {
        toolbar.setTitle(plantNameOnTopBar);
    }

    @Override
    public void setUserNameOnHeaderMenuDrawer(@NonNull String userName) {
        this.txtHeaderUserName.setText(userName);
    }

    @Override
    public void setTimerEndTurnInNavHeader() {
        boolean isShiftExtended =  Prefs.getBoolean("isShiftExtended",false);
        long millis = UtilTimes.getMillisecLeftByTurn(UtilTimes.getTurnByTime(),isShiftExtended);
        startRequest(millis);
    }

    @Override
    public void setUserMode(@NonNull String name) {
        this.txtModeUser.setText(name);
    }

    @Override
    public void setUserType(@NonNull String name) {
        this.txtUserType.setText(name);
    }

    @Override
    public void setHeaderModeType(boolean isSupervisor) {
        if (isSupervisor){
            this.viewContainer.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));
        }else {
            this.viewContainer.setBackgroundColor(getContext().getResources().getColor(R.color.color_bg_alert_green));
        }
    }

    @Override
    public void setPlantName(@NonNull String plantName) {
        txtHeaderPlantName.setText(plantName);
    }

    @Override
    public void setDate(@NonNull String date) {
        txtHeaderLastUpdate.setText(date);
    }

    @Override
    public void setTurn(@NonNull String turn) {
        txtHeaderTurn.setText(turn);
    }

    @Override
    public void setWorkMangerDate(@NonNull String workMangerDate){
        txtWorkManagerDate.setText(workMangerDate);
    }

    @Override
    public void setNameShift(@NonNull String shiftName) {
        txtHeaderNameShift.setText(shiftName);
    }

    @Override
    public void showLinesOnMenuDrawer(@NonNull List<String> listNamesLines, boolean isSupervisorMode) {
        final Menu menu = navigationView.getMenu();
        menu.clear();
        String groupName = "";
        if (isSupervisorMode){
            groupName = "CAMBIAR ÓRDENES DE PRODUCCIÓN";
        }else {
            groupName = "LÍNEAS VISUALIZADAS";
        }
        final SubMenu subMenu = menu.addSubMenu(groupName);
        int i = 0;
        for (String nameLine:listNamesLines) {
            MenuItem item = subMenu.add(GROUP_ID_LINES, i, i, nameLine);
            if (isSupervisorMode){
                item.setIcon(R.drawable.i_line_taked);
            }else{
                item.setIcon(R.drawable.i_line_empty);
            }
            i++;
        }
        subMenu.add(GROUP_ID_CHANGE_LINES, i, i,"Modificar líneas seleccionadas").setIcon(R.drawable.i_change_dark);
        if (isSupervisorMode){
            //Quito provisoriamente
            //menu.add(GROUP_ID_EXTEND_TURN,1,1,"Extender turno").setIcon(R.drawable.i_time_extend);
            menu.add(GROUP_ID_ALERTS,2,2,"Alertas informadas").setIcon(R.drawable.i_alert_menu);
        }
        menu.add(GROUP_ID_LOGOUT,3,3,"Cerrar sesión").setIcon(R.drawable.i_logout);
    }

    @Override
    public void setAlertVisualizationModeOnContainer() {
        Fragment fragment = (Fragment) ARouter.getInstance().build(ROUTE_VIEW_NAME_VISUALIZATION_MODE).navigation();
        //String tag = fragment.getTagFragment();
        Fragment fragment1 = this.rootView.get().getSupportFragmentManager().findFragmentByTag("visualization-frag");
        FragmentTransaction ft = this.rootView.get().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(com.andinaargentina.ui_sdk.R.anim.trans_fade_in, com.andinaargentina.ui_sdk.R.anim.trans_fade_out);
        ft.replace(R.id.drawer_container, fragment, "visualization-frag");
        ft.commitNow();
    }

    @Override
    public void setAlertSupervisorModeOnContainer(){
        Fragment fragment = (Fragment) ARouter.getInstance().build(ROUTE_VIEW_NAME_HOME_LINES_CONTAINER).navigation();
        //String tag = fragment.getTagFragment();
        Fragment fragment1 = this.rootView.get().getSupportFragmentManager().findFragmentByTag("visualization-frag");
        FragmentTransaction ft = this.rootView.get().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(com.andinaargentina.ui_sdk.R.anim.trans_fade_in, com.andinaargentina.ui_sdk.R.anim.trans_fade_out);
        ft.replace(R.id.drawer_container, fragment, "supervisor-frag");
        ft.commitNow();
    }

    @Override
    public void setGraphicsOnContainer(int lineId) {
        Fragment fragment = (Fragment) ARouter
                .getInstance()
                .build(ROUTE_VIEW_NAME_HOME_LINES_CONTAINER)
                .withBoolean("IS_GRAPHICS",true)
                .withInt(KEY_LINE_ID_SELECTED, lineId)
                .navigation();
        //String tag = fragment.getTagFragment();
        FragmentTransaction ft = this.rootView.get().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(com.andinaargentina.ui_sdk.R.anim.trans_fade_in, com.andinaargentina.ui_sdk.R.anim.trans_fade_out);
        ft.replace(R.id.drawer_container, fragment, "graphics-frag");
        ft.commitNow();
    }

    @Override
    public void setButtomAlertSelected(boolean selected) {
        this.imgBottonBarAlert.setActivated(selected);
        if (selected){
            setTintColor(imgBottonBarAlert,R.color.colorPrimary);
        }else{
            setTintColor(imgBottonBarAlert,R.color.colorWhite);
        }
    }

    @Override
    public void setButtomGraphicSelected(boolean selected) {
        this.imgBottonBarGraphics.setActivated(selected);
        if (selected){
            setTintColor(imgBottonBarGraphics,R.color.colorPrimary);
        }else{
            setTintColor(imgBottonBarGraphics,R.color.colorWhite);
        }
    }

    @Override
    public Context getContext() {
        return this.rootView.get().getApplicationContext();
    }

    public interface ILogout{
        void logout(boolean isShiftEnded);
    }

    @Override
    public void showLogoutDialog(ILogout logoutCallback) {
        if (logoutDialog == null){
           this.logoutDialog = new LogoutConfirmDialog(this.rootView.get(), new LogoutConfirmDialog.DialogClickHandler() {
                @Override public void positiveClick(boolean isChecked) {

                    logoutCallback.logout(isChecked);
                    logoutDialog.dismiss();
                    rootView.get().finish();

                    ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .navigation(getContext());
                }

                @Override
                public void negativeClick() {
                    logoutDialog.dismiss();
                }
            }, Prefs.getBoolean(IS_OBSERVER_MODE, true));
        }else{
            this.logoutDialog.show();
        }

    }

    @Override
    public void showExtendTurnDialog(@NonNull List<LineDto> listLines,
                                     @NonNull ExtendTurnDialog.DialogClickHandler callback,
                                     @NonNull String turn) {
        if (extendTurnDialog == null){
            this.extendTurnDialog = new ExtendTurnDialog(this.rootView.get(),callback,turn);
        }
        this.extendTurnDialog.setListLines(listLines);
        this.extendTurnDialog.show();
    }

    @Override
    //TODO Para esto vamos a tener que llamar a la api que nos devuelve las ordenes
    public void showOrderChangeDialog(@NonNull LineasProduccionDto lineDto,@NonNull ProductionOrderChangeDialog.DialogClickHandler callback) {

        /*List<OrderDialogModel> orderList = new ArrayList<>(lineDto.getProductionOrders().size());
        for (ProductionOrders order: lineDto.getProductionOrders()) {
            orderList.add(new OrderDialogModel(order.getShortName(),String.valueOf(order.getId()), false));
        }

        this.changeOrderDialog = new ProductionOrderChangeDialog(this.rootView.get(),
                callback,
                lineDto.getDescription(),
                orderList,
                String.valueOf(lineDto.orderIdSelected));*/
    }

    private void setTintColor(@NonNull ImageView imageView, int color){
        ColorStateList colours = imageView.getResources()
                .getColorStateList(color);
        Drawable drawable = DrawableCompat.wrap(imageView.getDrawable());
        DrawableCompat.setTintList(drawable, colours);
    }

    /**
     *
     *
     */
    private CountDownTimer countDownTimer = null;
    public long mTimeLeftInMillis = 0;


    public void startRequest( long millisLeftEndTurn) {
        countDownTimer = new CountDownTimer(millisLeftEndTurn, 1000) {
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                long h = TimeUnit.MILLISECONDS.toHours(mTimeLeftInMillis)
                        - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(mTimeLeftInMillis));
                long m = TimeUnit.MILLISECONDS.toMinutes(mTimeLeftInMillis)
                        - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mTimeLeftInMillis));
                long s = TimeUnit.MILLISECONDS.toSeconds(mTimeLeftInMillis)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mTimeLeftInMillis));
                String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d:%02d", h, m, s);
                if(txtHeaderTurnTimer != null)
                    txtHeaderTurnTimer.setText(timeLeftFormatted);
            }

            public void onFinish() {
                if (countDownTimer!=null)
                    countDownTimer.start();
            }
        }.start();
    }

    public void stopRequest() {
        if (this.countDownTimer != null)
            this.countDownTimer.cancel();
    }
}
