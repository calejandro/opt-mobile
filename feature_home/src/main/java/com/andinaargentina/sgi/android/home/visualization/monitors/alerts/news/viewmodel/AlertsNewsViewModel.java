package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.news.viewmodel;

import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel;

import org.jetbrains.annotations.NotNull;

public class AlertsNewsViewModel implements IBaseViewModel {

    @NotNull
    @Override
    public String name() {
        return "AlertNewsViewModel";
    }
}
