package com.andinaargentina.sgi.android.home.visualization.usescases;

import com.andinaargentina.sgi.android.home.visualization.repository.HomeObserverModeRepository;

public class HomeObserverModeInteractor implements IHomeObserverModeTransactionHandler {

    private HomeObserverModeRepository repository;

    public HomeObserverModeInteractor() {
        this.repository = new HomeObserverModeRepository();
    }

}
