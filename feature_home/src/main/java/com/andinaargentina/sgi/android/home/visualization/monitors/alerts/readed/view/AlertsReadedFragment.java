package com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.events.alerts.AlertsSuccessEvent;
import com.andinaargentina.core_services.events.alerts.LoadFailModesSuccessEvent;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertFailModeInformRequestDto;
import com.andinaargentina.core_services.providers.api.clients.alerts.AlertService;
import com.andinaargentina.core_services.providers.api.clients.alerts.InformAlertService;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeDto;
import com.andinaargentina.core_services.providers.api.clients.failmode.FailModeService;
import com.andinaargentina.core_services.providers.api.clients.failmode.ProductionLineAlertKind;
import com.andinaargentina.core_services.providers.api.clients.lineselection.sites.LineDto;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.LineasProduccionDto;
import com.andinaargentina.core_services.providers.apiv2.models.ModoFalloDto;
import com.andinaargentina.core_services.providers.apiv2.models.RegistrosFallosAlertasDto;
import com.andinaargentina.core_services.providers.apiv2.models.ShiftDto;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.providers.apiv2.utils.ShiftUtils;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.android.feature_home.R;
import com.andinaargentina.sgi.android.home.repository.AlertsRepository;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.container.view.AlertsAdapter;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.presenter.AlertsReadedPresenter;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.presenter.IAlertsReadedViewUpdateHandler;
import com.andinaargentina.sgi.android.home.visualization.monitors.alerts.readed.viewmodel.AlertsReadedViewModel;
import com.andinaargentina.ui_sdk.FrameworkBaseFragment;
import com.andinaargentina.ui_sdk.components.dialog.SendReportDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.andinaargentina.RepositoryConst.MODO_DE_FALLO;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;
import static com.andinaargentina.core_services.providers.api.clients.alerts.AlertDto.TYPE.FAILURE;
import static com.andinaargentina.core_services.router.ViewsNames.ROUTE_FRAGMENT_VIEW_NAME_ALERTS_READED;

@Route(path = ROUTE_FRAGMENT_VIEW_NAME_ALERTS_READED)
@RequiresApi(api = Build.VERSION_CODES.N)
public class AlertsReadedFragment extends FrameworkBaseFragment implements IAlertsReadedViewUpdateHandler {

    private AlertsAdapter adapter;
    private RecyclerView recyclerView;
    private View container;
    private View emptyState;
    private SendReportDialog dialog;
    private FailModeDialog failDialog;
    private IAlertsReadedViewEventsHandler presenter;


    private CountDownLatch finishLoadFailModes = new CountDownLatch(1);
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new AlertsReadedPresenter(new AlertsReadedViewModel(),
                this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState,
                R.layout.fragment_alerts_group);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                initView();
            }
        });

    }

    private void initView() {
        container = getView().findViewById(R.id.fragment_alerts_readed_container);
        emptyState = getView().findViewById(R.id.fragment_alerts_readed_empty_state_container);
        recyclerView = getView().findViewById(R.id.alerts_readed_recycler);
        initRecyclerView();
    }

    private void initRecyclerView() {
        ShiftUtils.getInstance().getObservable().subscribe(new Consumer<List<ShiftDto>>() {
            @Override
            public void accept(List<ShiftDto> shiftDtoList) throws Exception {
                OptimusServices.getInstance().getLineasProduccionAlertasService().list(null,
                        simpleDateFormat.format(ShiftUtils.getInstance().getShifts().get(0).getInicio()),
                        simpleDateFormat.format(ShiftUtils.getInstance().getCurrentShift().getFin()), presenter.getLineDto().getIdLinea())
                        .enqueue(new Callback<List<LineasProduccionAlertasDto>>() {
                            @Override
                            public void onResponse(Call<List<LineasProduccionAlertasDto>> call, Response<List<LineasProduccionAlertasDto>> response) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                List<LineasProduccionAlertasDto> listData = response.body();
                                                emptyState.setVisibility(View.GONE);
                                                adapter = new AlertsAdapter(listData, (caller, position, bundle) -> presenter.onClickAlert(position));
                                                final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                                                        RecyclerView.VERTICAL, false);
                                                recyclerView.setAdapter(adapter);
                                                recyclerView.setNestedScrollingEnabled(false);
                                                recyclerView.setLayoutManager(layoutManager);
                                                recyclerView.setItemAnimator(new SlideInLeftAnimator());
                                                recyclerView.setVisibility(View.VISIBLE);
                                                showComponent();
                                            }
                                        });

                                    }
                                });

                            }

                            @Override
                            public void onFailure(Call<List<LineasProduccionAlertasDto>> call, Throwable t) {
                                System.out.println(t.getMessage());
                            }
                        });
            }
        });


    }

    @Override
    public String getTagFragment() {
        return "Alerts readed fragment";
    }


    @Override
    public void addAlert(@NonNull LineasProduccionAlertasDto alertDto){
        adapter.getListData().add(0,alertDto);
        adapter.notifyItemInserted(0);
        recyclerView.scrollToPosition(0);
    }

    @Override
    public void removeAlert(int position) {
        //Removido de item, meter en adapter
        if (adapter != null && adapter.getItemCount()>0){
            adapter.getListData().remove(position);
            adapter.notifyItemRemoved(position);
            //recyclerView.scrollToPosition(0);
        }
    }

    @Override
    public void hideComponent() {
        container.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                container.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void showComponent() {
        container.setAlpha(0.0f);
        container.setVisibility(View.VISIBLE);
        container.animate().alpha(1.0f).setDuration(500);
    }

    @Override
    public void changeStatus(int position) {
        //Cambiar el estado de un item
    }

    @Override
    public void showEmptyState(){
        emptyState.setAlpha(0.0f);
        emptyState.setVisibility(View.VISIBLE);
        emptyState.animate().alpha(1.0f).setDuration(500);
    }

    private int failModeSelectedPostion = 0;

    public void showFailModeDialog(int positionInAdapter){
        LineasProduccionAlertasDto currentAlert = this.adapter.getListData().get(positionInAdapter);

        if (currentAlert.getFechaFin() == null){
            Toast.makeText(getContext(),"Esta alerta aún está activa, no puede informarla aún", Toast.LENGTH_LONG).show();
        }else{
            if (currentAlert.getTipoAlerta() == "Fallo"){
                //TODO mostrar dialogo de confirmacion
                dialog = new SendReportDialog(getContext(),
                        new SendReportDialog.DialogClickHandler() {
                            @Override
                            public void positiveClick(int position) {
                                SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                                adapter.getListData().get(position).setEstado("Informada");
                                informAlert(adapter.getListData().get(position), String.valueOf(user.getUserId()));
                                dialog.dismiss();
                                AlertsReadedFragment.this.adapter.getListData().remove(positionInAdapter);
                                AlertsReadedFragment.this.adapter.notifyItemRemoved(positionInAdapter);
                            }

                            @Override
                            public void negativeClick() {
                                dialog.dismiss();
                            }
                        }, positionInAdapter);
                dialog.show();
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        showFailModeDialogWarning(positionInAdapter);
                    }
                }).start();
            }
        }
    }

    public void showFailModeDialogWarning(int positionInAdapter){
        failModeSelectedPostion = positionInAdapter;
        LineasProduccionAlertasDto alertDtoSelected = adapter.getListData().get(failModeSelectedPostion);

        finishLoadFailModes = new CountDownLatch(1);
        new Thread(new Runnable() {
            @Override
            public void run() {
                /*AlertsReadedFragment.this.alertsRepository.getFailModeByAlertKind(String.valueOf(alertDtoSelected.getIdEquipo()),
                        String.valueOf(alertDtoSelected.getIdLinea()),
                        alertDtoSelected.getTipoAlerta() == 2 ? ProductionLineAlertKind.KpiDepletion : ProductionLineAlertKind.MachineActualStatus, getContext());
                 */
            }
        }).start();
        try {
            finishLoadFailModes.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<ModoFalloDto> listFailMode = Paper.book("SGI-BOOK").read(MODO_DE_FALLO, new ArrayList<ModoFalloDto>());
        /*ModoFalloDto failModeDtoSelected = null;
        for (ModoFalloDto failModeDto: listFailMode) {
            if (failModeDto.getIdLinea() == alertDtoSelected.getIdLinea()
                    && failModeDto.getIdEquipo() == alertDtoSelected.getIdEquipo()){
                failModeDtoSelected = failModeDto;
            }
        }
        final ModoFalloDto failModeDtoSelectedFinal = failModeDtoSelected;*/
        // TODO buscar los modo de fallos

        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                failDialog = new FailModeDialog(AlertsReadedFragment.this.getContext(),
                        AlertsReadedFragment.this.adapter.getListData().get(positionInAdapter),
                        listFailMode,
                        new FailModeDialog.DialogClickHandler() {
                            @Override
                            public void positiveClick(LineasProduccionAlertasDto alertDto, RegistrosFallosAlertasDto requestDto) {
                                SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                                //informed
                                adapter.getListData().get(failModeSelectedPostion).setEstado("Informada");
                                informFailModeAlert(requestDto, String.valueOf(user.getUserId()));
                                if (dialog!=null)
                                    dialog.dismiss();
                                AlertsReadedFragment.this.adapter.getListData().get(positionInAdapter).setEstado("Informada");
                                AlertsReadedFragment.this.adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void negativeClick() {

                            }
                        });
                failDialog.show();
            }
        });

    }

    @Override
    public void clearList() {
        if(adapter != null && adapter.getListData() != null) {
            adapter.getListData().clear();
            adapter.notifyDataSetChanged();
        }
    }

    public void setLineDto(LineasProduccionDto lineDto){
        this.presenter.setLineDto(lineDto);
    }

    @Override
    public void showConfirmDialog(int position) {
        showFailModeDialog(position);
    }

    private void informAlert(LineasProduccionAlertasDto alertToInformDto, String userId){
        /*if (!compositeDisposable.isDisposed()){
            Disposable requestInformAlertDisposable = informAlertService
                    .informAlert(alertToInformDto,userId,getContext())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(informAlertObserver());
            compositeDisposable.add(requestInformAlertDisposable);
        }*/

    }

    private void informFailModeAlert(RegistrosFallosAlertasDto alertFailMode, String userId){
        /*if (!compositeDisposable.isDisposed()){
            Disposable requestInformAlertDisposable = informAlertService
                    .informFailModeAlert(alertFailMode,userId,getContext())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(informAlertObserver());
            compositeDisposable.add(requestInformAlertDisposable);
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
