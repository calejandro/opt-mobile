package com.andinaargentina.sgi.core_sdk.base.sesion;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.providers.api.clients.logout.LogoutService;
import com.andinaargentina.core_services.providers.api.clients.logout.UserLogoutDto;
import com.andinaargentina.core_services.providers.api.core.base.ApiRestModule;
import com.andinaargentina.core_services.providers.apiv2.models.SysUsersDto;
import com.andinaargentina.core_services.providers.apiv2.services.OptimusServices;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.legacy.domain.User;
import com.andinaargentina.sgi.core_sdk.base.times.UtilTimes;
import com.microsoft.appcenter.crashes.Crashes;
import com.pixplicity.easyprefs.library.Prefs;

import io.paperdb.Paper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.test.InstrumentationRegistry.getContext;
import static com.andinaargentina.RepositoryConst.USER_LOGGED;

public class UtilSesion {
    private CompositeDisposable compositeDisposable;
    private LogoutService logoutService;

    public UtilSesion() {
        this.logoutService = new LogoutService(ApiRestModule.providesApiService(false));
        this.compositeDisposable = new CompositeDisposable();
    }

    public void logout(Context context, boolean isShiftFinalized) {
        OptimusServices.getInstance().getLineasProduccionUsuarioService().clear().enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                SysUsersDto user = Paper.book("SGI-BOOK").read(USER_LOGGED);
                clearSession();
                ComponentName cn = null;
                try {
                    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                    cn = am.getRunningTasks(1).get(0).topActivity;
                }catch (Exception e){}
                if(cn == null || !cn.getClassName().equals("com.andinaargentina.app_template.LoginActivity")){
                    ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .navigation(context);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    private DisposableSingleObserver<UserLogoutDto> getLogoutObserver(Context ctx){
        return new DisposableSingleObserver<UserLogoutDto>() {
            @Override
            public void onSuccess(UserLogoutDto response) {
               clearSession();
                ComponentName cn = null;
                try {
                    ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
                    cn = am.getRunningTasks(1).get(0).topActivity;
                }catch (Exception e){}
                if(cn == null || !cn.getClassName().equals("com.andinaargentina.app_template.LoginActivity")){
                    ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .navigation(ctx);
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(ctx, "Error al intentar cerrar sesión", Toast.LENGTH_LONG).show();
            }
        };
    }

    public static void clearSession(){
        Paper.book("SGI-BOOK").destroy();
        Prefs.clear();
        UtilTimes.cancelAlarms();
    }

    public static void clearSessionAndExit(Context context){
        clearSession();
        ARouter.getInstance().build(ViewsNames.ROUTE_VIEW_NAME_LOGIN)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .navigation(context);
    }

    public static boolean checkSession(Activity context){
        SysUsersDto user = null;
        try{
            user = Paper.book("SGI-BOOK").read(USER_LOGGED);
        }catch (Exception e){
            Crashes.trackError(e);
        }
        if(user == null){
            UtilSesion.clearSessionAndExit(context);
            context.finish();
            return true;
        }
        return false;
    }
}
