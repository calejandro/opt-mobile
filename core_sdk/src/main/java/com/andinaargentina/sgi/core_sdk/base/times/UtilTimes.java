package com.andinaargentina.sgi.core_sdk.base.times;
import android.util.Log;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.andinaargentina.sgi.core_sdk.base.workers.NotificationWorker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static androidx.work.ExistingWorkPolicy.REPLACE;

public class UtilTimes {

    public static final String MANANA = "Mañana";
    public static final String TARDE = "Tarde";
    public static final String NOCHE = "Noche";

    public static long getMillisecLeftByTurn(String turn, boolean isExtended) {
        long millisecondsLeft = 0;
        int hourEndTurn;
        switch (turn) {
            case MANANA:
                hourEndTurn = 14;
                if (isExtended) {
                    hourEndTurn = 18;
                }
                millisecondsLeft = getTimeInMilliseconds(hourEndTurn);
                break;
            case TARDE:
                hourEndTurn = 22;
                if (isExtended) {
                    hourEndTurn = 4;
                }
                millisecondsLeft = getTimeInMilliseconds(hourEndTurn);
                break;
            case NOCHE:
                hourEndTurn = 6;
                if (isExtended) {
                    hourEndTurn = 10;
                }
                millisecondsLeft = getTimeInMilliseconds(hourEndTurn);
                break;
        }
        return millisecondsLeft;
    }

    public static long getMillisecLeftByTurnFiveBefore(String turn, boolean isExtended) {
        long milliseconds = 0;
        int hourEndTurn;
        switch (turn) {
            case MANANA:
                hourEndTurn = 13;
                if (isExtended) {
                    hourEndTurn = 17;
                }
                milliseconds = getTimeInMillisecondsFiveBefore(hourEndTurn);
                break;
            case TARDE:
                hourEndTurn = 21;
                if (isExtended) {
                    hourEndTurn = 3;
                }
                milliseconds = getTimeInMillisecondsFiveBefore(hourEndTurn);
                break;
            case NOCHE:
                hourEndTurn = 5;
                if (isExtended) {
                    hourEndTurn = 9;
                }
                milliseconds = getTimeInMillisecondsFiveBefore(hourEndTurn);
                break;
        }
        return milliseconds;
    }

    public static long getTimeInMilliseconds(int hourEndTurn) {
        Date currentDay = new Date();
        Calendar calendarCurrentDay = Calendar.getInstance();
        calendarCurrentDay.setTime(currentDay);

        Date fechaActual;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendarCurrentDay.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendarCurrentDay.get(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendarCurrentDay.get(Calendar.SECOND));
        calendar.set(Calendar.DAY_OF_MONTH, calendarCurrentDay.get(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.MONTH, calendarCurrentDay.get(Calendar.MONTH));
        calendar.set(Calendar.YEAR, calendarCurrentDay.get(Calendar.YEAR));
        fechaActual = calendar.getTime();

        Date fechaFinTurno;
        calendar.set(Calendar.HOUR_OF_DAY, hourEndTurn);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.DAY_OF_MONTH, calendarCurrentDay.get(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.MONTH, calendarCurrentDay.get(Calendar.MONTH));
        calendar.set(Calendar.YEAR, calendarCurrentDay.get(Calendar.YEAR));
        fechaFinTurno = calendar.getTime();

        if (fechaFinTurno.getTime()<fechaActual.getTime()){
            calendar.add(Calendar.DATE, 1);  // number of days to add
            fechaFinTurno = calendar.getTime();
        }

        long timeLeftInMillis =  fechaFinTurno.getTime() - fechaActual.getTime();

        return timeLeftInMillis;
    }


    public static long getTimeInMillisecondsFiveBefore(int hourEndTurn) {
        Date currentDay = new Date();
        Calendar calendarCurrentDay = Calendar.getInstance();
        calendarCurrentDay.setTime(currentDay);

        Date fechaActual;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendarCurrentDay.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendarCurrentDay.get(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendarCurrentDay.get(Calendar.SECOND));
        calendar.set(Calendar.DAY_OF_MONTH, calendarCurrentDay.get(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.MONTH, calendarCurrentDay.get(Calendar.MONTH));
        calendar.set(Calendar.YEAR, calendarCurrentDay.get(Calendar.YEAR));
        fechaActual = calendar.getTime();

        Date fechaFinTurno;
        calendar.set(Calendar.HOUR_OF_DAY, hourEndTurn);
        calendar.set(Calendar.MINUTE, 55);
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.DAY_OF_MONTH, calendarCurrentDay.get(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.MONTH, calendarCurrentDay.get(Calendar.MONTH));
        calendar.set(Calendar.YEAR, calendarCurrentDay.get(Calendar.YEAR));
        fechaFinTurno = calendar.getTime();

        long mTimeLeftInMillis =  fechaFinTurno.getTime() - fechaActual.getTime();
        long h = TimeUnit.MILLISECONDS.toHours(mTimeLeftInMillis)
                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(mTimeLeftInMillis));
        long m = TimeUnit.MILLISECONDS.toMinutes(mTimeLeftInMillis)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mTimeLeftInMillis));
        long s = TimeUnit.MILLISECONDS.toSeconds(mTimeLeftInMillis)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mTimeLeftInMillis));
        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d:%02d", h, m, s);


        return fechaFinTurno.getTime() - fechaActual.getTime();
    }


    ////////////////////////////////////////////////////
    /**
     * Turno mañana -> 06:00 - 13:59
     * Turno tarde -> 14:00 - 21:59
     * Turno noche -> 22:00 - 05:59
     * @return
     */
    public static String getTurnByTime() {
        Date currentDay = new Date();
        Calendar calendarCurrentDay = Calendar.getInstance();
        calendarCurrentDay.setTime(currentDay);
        int timeOfDay = calendarCurrentDay.get(Calendar.HOUR_OF_DAY);
        String turn = "";
        if(timeOfDay >= 6 && timeOfDay < 14){
            turn = MANANA;
        }
        if(timeOfDay >= 14 && timeOfDay < 22){
            turn = TARDE;
        }
        if(timeOfDay >= 22 || timeOfDay < 6){
            turn = NOCHE;
        }
        return turn;
    }

    public static String getEndTurn(){
        String endTurn = "";
        switch (UtilTimes.getTurnByTime()) {
            case MANANA:
                endTurn = "13:59";
                break;
            case TARDE:
                endTurn = "21:59";
                break;
            case NOCHE:
                endTurn = "05:59";
                break;
        }
        return endTurn;
    }

    public static String getInitTurn(){
        String initTurn = "";
        switch (UtilTimes.getTurnByTime()) {
            case MANANA:
                initTurn = "06:00";
                break;
            case TARDE:
                initTurn = "14:00";
                break;
            case NOCHE:
                initTurn = "22:00";
                break;
        }
        return initTurn;
    }

    public static long getMillisecLeftByTurn() {
        String turn = getTurnByTime();
        boolean isExtended = false;
        long millisecondsLeft = 0;
        int hourEndTurn;
        switch (turn) {
            case MANANA:
                hourEndTurn = 14;
                if (isExtended) {
                    hourEndTurn = 18;
                }
                millisecondsLeft = getTimeInMilliseconds(hourEndTurn);
                break;
            case TARDE:
                hourEndTurn = 22;
                if (isExtended) {
                    hourEndTurn = 4;
                }
                millisecondsLeft = getTimeInMilliseconds(hourEndTurn);
                break;
            case NOCHE:
                hourEndTurn = 6;
                if (isExtended) {
                    hourEndTurn = 10;
                }
                millisecondsLeft = getTimeInMilliseconds(hourEndTurn);
                break;
        }
        return millisecondsLeft;
    }

    public static String setAlarmForChangeTurn(boolean isExtended){
        long millisecondsLeft = UtilTimes.getMillisecLeftByTurn(UtilTimes.getTurnByTime(), isExtended);
        Date currentDay = new Date();
        Calendar totalTime = Calendar.getInstance();
        totalTime.setTimeInMillis(currentDay.getTime() + millisecondsLeft);

        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String strDate =  mdformat.format(totalTime.getTime());

        Log.d("WorkManager", "WorkManager is configured : "+ strDate +", is extended: "+isExtended);

        WorkManager mWorkManager = WorkManager.getInstance();
        OneTimeWorkRequest mRequest = new OneTimeWorkRequest
                        .Builder(NotificationWorker.class)
                        .setInitialDelay(millisecondsLeft, TimeUnit.MILLISECONDS)
                        .build();
        mWorkManager.beginUniqueWork("close_session",REPLACE,mRequest).enqueue();
        return strDate;
    }

    public static void cancelAlarms(){
        Log.d("WorkManager", "WorkManager canceled");
        WorkManager mWorkManager = WorkManager.getInstance();
        mWorkManager.cancelAllWork();
    }

}
