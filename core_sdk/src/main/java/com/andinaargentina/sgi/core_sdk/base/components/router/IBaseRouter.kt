package com.andinaargentina.sgi.core_sdk.base.components.router

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment

interface IBaseRouter {

    fun standardRouterWithString(route : String)

    fun standardRouterWithString(route : String, bundle: Bundle)

    fun standardFragmentRouterWithString(route : String) : Fragment

    fun standardFragmentRouterWithString(route : String, bundle: Bundle) : Fragment


    fun standardRouterWithUri(rui : Uri)

    fun standardRouterWithUri(rui : Uri, bundle: Bundle)

    fun standardFragmentRouterWithUri(rui : Uri) : Fragment

    fun standardFragmentRouterWithUri(rui : Uri, bundle: Bundle) : Fragment

}