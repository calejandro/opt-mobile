package com.andinaargentina.sgi.core_sdk.base.components.presenter

import android.os.Bundle
import androidx.annotation.CallSuper
import org.parceler.Parcels


abstract class BasePresenter<T:IBaseViewModel> : IBaseViewEvents {

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState!=null && savedInstanceState.containsKey(getViewModelName())){
            loadViewModelState(Parcels.unwrap(savedInstanceState.getParcelable(getViewModelName())))
        }else{
           loadViewModelState(null)
        }
    }

    override fun onStart() {
        // do nth
    }

    override fun onResume() {}

    override fun onPause() {
        // do nth
    }

    @CallSuper
    override fun onStop() {
        // do nth
    }

    final override fun onDestroy() {
        doOnDestroy()
    }

    /**
     * Safe destruction of presenter
     */
    protected open fun doOnDestroy() {}

    /**
     * default impl
     */
    override fun onBackPressed(): Boolean {
        //do nth
        return true
    }

    /**
     * Save/Load presenter state
     */
    open fun saveViewModelState(outState: Bundle, viewModelStateToSave: T?){
        if (viewModelStateToSave!=null)
            outState.putParcelable(getViewModelName(), Parcels.wrap(viewModelStateToSave))
    }

    open fun loadViewModelState(viewModelStateSaved: T?){
        //do nth
    }

    abstract fun getViewModel() : T?

    abstract fun getViewModelName(): String
}