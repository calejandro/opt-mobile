package com.andinaargentina.sgi.core_sdk.base.components.view

import android.app.Application
import android.content.Context

interface IBaseView {

    companion object {
        const val NO_VIEW = 0
        const val TEXT_SEPARATOR = " "
        const val KEY_VALUE_SEPARATOR = ": "
    }

    val viewName: Int

    val tagName: String

    /**
     * @return the app context for this view
     */
    @Throws(NoContextException::class)
    fun context(): Context

    /**
     * @return application object
     */
    @Throws(NoContextException::class)
    fun application(): Application

    /**
     *
     * @param message toast
     */
    fun toast(message: String, long: Boolean = false)


    class NoContextException : Exception()
}