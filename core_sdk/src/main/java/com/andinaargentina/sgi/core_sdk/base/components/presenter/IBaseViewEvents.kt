package com.andinaargentina.sgi.core_sdk.base.components.presenter

import android.os.Bundle

interface IBaseViewEvents {


    /**
     * Determines what to do with the view once created. Generally configures views programmatically.
     */
    //fun onViewCreated()

    /**
     * Creation call back
     */
    fun onCreate(savedInstanceState: Bundle?)

    /**
     * Do resource cleanup
     */
    fun onDestroy()

    /**
     * on view resumed/paused callback
     */
    fun onResume()
    fun onPause()

    /**
     * @return true if handled, false otherwise
     */
    fun onBackPressed(): Boolean = false

    /**
     * Start/Stop pair
     */
    fun onStart()
    fun onStop()
}