package com.andinaargentina.sgi.core_sdk.base.components.presenter


interface IBaseViewModel {

    fun name() : String
}