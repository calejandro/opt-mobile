package com.andinaargentina.sgi.core_sdk.base.components.router

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.alibaba.android.arouter.launcher.ARouter

class BaseRouter: IBaseRouter {

    override fun standardRouterWithString(route: String) {
        ARouter.getInstance().build(route).navigation()
    }

    override fun standardRouterWithString(route: String, bundle: Bundle) {
        ARouter.getInstance().build(route).with(bundle).navigation()
    }

    override fun standardFragmentRouterWithString(route: String): Fragment {
        return ARouter.getInstance().build(route).navigation() as Fragment
    }

    override fun standardFragmentRouterWithString(route: String, bundle: Bundle): Fragment {
        return ARouter.getInstance().build(route).with(bundle).navigation() as Fragment
    }

    override fun standardRouterWithUri(rui: Uri) {
        ARouter.getInstance().build(rui).navigation()
    }

    override fun standardRouterWithUri(rui: Uri, bundle: Bundle) {
        ARouter.getInstance().build(rui).with(bundle).navigation()
    }

    override fun standardFragmentRouterWithUri(rui: Uri): Fragment {
        return ARouter.getInstance().build(rui).navigation() as Fragment
    }

    override fun standardFragmentRouterWithUri(rui: Uri, bundle: Bundle): Fragment {
        return ARouter.getInstance().build(rui).with(bundle).navigation() as Fragment
    }
}