package com.andinaargentina.sgi.core_sdk.base.workers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.alibaba.android.arouter.launcher.ARouter;
import com.andinaargentina.core_services.azure.notifications.UiNotificationsHelper;
import com.andinaargentina.core_services.router.ViewsNames;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import io.paperdb.Paper;

public class NotificationWorker extends Worker {
    private static final String WORK_RESULT = "work_result";
    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i("Notification Worker","se ejecuto el worker automático");
        Data taskData = getInputData();
        String taskDataString = taskData.getString("message_status");
        showAppNotification();
        closeSesion();
        Data outputData = new Data.Builder().putString(WORK_RESULT, "Jobs Finished").build();
        return Result.success(outputData);
    }

    private void closeSesion() {
       UtilSesion.clearSession();
        EventBus.getDefault().post(new GoToLoginEvent());
    }

    private void showAppNotification(){
        UiNotificationsHelper.showNormalNotificationEndTurn(getApplicationContext(),null);
    }
}
