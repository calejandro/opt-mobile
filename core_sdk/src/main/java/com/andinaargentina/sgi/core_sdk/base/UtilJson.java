package com.andinaargentina.sgi.core_sdk.base;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;

public class UtilJson {

    public static String getAssetJsonData(Context context, String nameJsonFile) {
        String json;
        try {
            InputStream is = context.getAssets().open(nameJsonFile);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Log.e("data", json);
        return json;
    }

    private static Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        return builder.create();
    }

    /**
     * Ejemplo de como usar esta método
     *         String json = UtilJson.getAssetJsonData(this,"list_lines/list_lines_cordoba.json");*
     *         LineDto[] listLines = UtilJson.fromJsonToObject(json,LineDto[].class);
     *         List<LineDto> list = Arrays.asList(listLines);
     *         Log.i("JSON - TEST", json);
     */

    /**
     * Para los listados acá un ejemplo
     *  LineDto[] listLines = UtilJson.fromJsonToObject(json,LineDto[].class);
     *         List<LineDto> list = Arrays.asList(listLines);
     */

    public static <R> R fromJsonToObject(String json, Class<R> type) {
        Gson gson = getGson();
        return gson.fromJson(json, type);
    }
}
