package com.andinaargentina.sgi.core_sdk.base.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.andinaargentina.core_services.azure.notifications.UiNotificationsHelper;
import com.andinaargentina.sgi.core_sdk.base.sesion.UtilSesion;

public class NotificationWorkerFiveBefore extends Worker {
    private static final String WORK_RESULT = "work_result";
    public NotificationWorkerFiveBefore(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }
    @NonNull
    @Override
    public Result doWork() {
        Data taskData = getInputData();
        String taskDataString = taskData.getString("message_status");
        showAppNotification();
        Data outputData = new Data.Builder().putString(WORK_RESULT, "Jobs Finished").build();
        return Result.success(outputData);
    }


    private void showAppNotification(){
        UiNotificationsHelper.showNormalNotificationFiveBefore(getApplicationContext(),null);
    }
}
