package com.andinaargentina.sgi.core_sdk.base.components.view

import android.app.Application
import android.app.KeyguardManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.andinaargentina.core_services.events.internet.InternetConnectionUpdateReceiver
import com.andinaargentina.sgi.core_sdk.R
import com.andinaargentina.sgi.core_sdk.base.components.presenter.BasePresenter
import com.andinaargentina.sgi.core_sdk.base.components.presenter.IBaseViewModel
import com.andinaargentina.sgi.core_sdk.base.components.view.IBaseView.Companion.NO_VIEW
import com.google.android.material.snackbar.Snackbar
import com.orhanobut.logger.Logger
import java.util.*

abstract class BaseActivity<T:IBaseViewModel> : AppCompatActivity(), IBaseView {

    // default is to not change viewName
    override val viewName: Int
        get() = NO_VIEW

    override val tagName: String
        get() = "VIEW WITHOUT NAME"

    private var broadcastReceivers: ArrayList<BroadcastReceiver>? = null
    private var networkReceiver: InternetConnectionUpdateReceiver? = null

    companion object {
        const val SHOW_SYSTEM_UI_FLAGS = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        //.or(View.SYSTEM_UI_FLAG_LAYOUT_STABLE) //.or(View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState)
    }

    val TAG = "SGI_SYSTEM"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Logger.t(TAG).d("Life_Cycle: onCreate")
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale)
        setColorStatusBar()
        setContentView(getLayoutResId())
        initViews()
        getBasePresenter().onCreate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        var viewModel = getBasePresenter().getViewModel()
        getBasePresenter().saveViewModelState(outState, viewModel)
        super.onSaveInstanceState(outState)
    }


    override fun onDestroy() {
        super.onDestroy()
        Logger.t(TAG).d("Life_Cycle: onDestroy")
        getBasePresenter().onDestroy()
    }


    override fun onResume() {
        super.onResume()
        Logger.t(TAG).d("Life_Cycle: onResume")
        getBasePresenter().onResume()
    }

    override fun onStart() {
        networkReceiver = InternetConnectionUpdateReceiver()
        registerReceiver(networkReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        super.onStart()
        Logger.t(TAG).d("Life_Cycle: onStart")
        getBasePresenter().onStart()
    }

    override fun onStop() {
        super.onStop()
        Logger.t(TAG).d("Life_Cycle: onStop")
        getBasePresenter().onStop()
        if (networkReceiver!=null){
            unregisterReceiver(networkReceiver)
            networkReceiver = null
        }
    }

    open fun initViews(){
        Logger.t(TAG).d("Life_Cycle: initViews")
    }

    /**
     * @return The resource id to load
     */
    abstract fun getLayoutResId(): Int

    abstract fun getBasePresenter() : BasePresenter<T>

    /*override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(this.attachCaligraphy(newBase))
    }*/

    /**
     * Links caligraphy (font-helper) to the base context
     */
    /*private fun attachCaligraphy(newBase: Context?): Context? {
        /*return try {
            //CalligraphyContextWrapper.wrap(newBase)
        } catch (e: Throwable) {
            Logger.w(javaClass.simpleName, "WARNING! Could not attach caligraphy. App will be usable but fonts wont be applied")
            newBase
        }*/
    }*/

    private fun setColorStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = this.window
            //
            //// clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            //
            //// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            //
            //// finally change the color
            //window.setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));

            getWindow().decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    /**
     * Toast the message to the user. Thread safe.
     *
     * @param message the message to toast
     */
    override fun toast(message: String, long: Boolean) {
        this.runOnUiThread {
            Toast.makeText(this@BaseActivity, message,
                if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
        }
    }

    private var snackbar: Snackbar? = null

    fun showSnackBar(title: String) {
        val parentLayout = findViewById<View>(android.R.id.content)
        snackbar = Snackbar
                .make(parentLayout, title, Snackbar.LENGTH_INDEFINITE)
        snackbar!!.setActionTextColor(Color.RED)
        snackbar!!.show()
    }

    fun showSnackBar(title: String,  titleAction:String , action: () -> Unit = {}) {
        val parentLayout = findViewById<View>(android.R.id.content)
        snackbar = Snackbar
                .make(parentLayout, title, Snackbar.LENGTH_INDEFINITE)
        snackbar!!.setActionTextColor(Color.RED)
        snackbar!!.setAction(titleAction) {
                    action()
                    snackbar?.dismiss()
            snackbar=null
                }
        snackbar!!.show()
    }

    fun hideSnackBar(){
        snackbar?.dismiss()
        snackbar = null
    }

    /**
     * @param lock if true, locks rotation, unlocks it otherwise
     */
    fun lockRotation(lock: Boolean) {
        if (lock) {
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
                this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            else
                this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        } else {
            this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
        }
    }

    fun requestPortrait() {
        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    fun isPortrait() = this.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT//DeviceUtils.isInPortraitOrientation(this)

    fun registerLocalBroadcastReceiver(broadcastReceiver: BroadcastReceiver, intentFilter: IntentFilter) {
        broadcastReceivers?.add(broadcastReceiver)
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(broadcastReceiver, intentFilter)
    }

    fun unregisterLocalBroadcastReceiver(broadcastReceiver: BroadcastReceiver) {
        broadcastReceivers?.remove(broadcastReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    fun showSystemUI() {
        window.decorView.systemUiVisibility =
            SHOW_SYSTEM_UI_FLAGS
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
    }

    /**
     *
     * @param hideNavigationBar true to hide it, false otherwise
     * @param bigInteration true to use inmersive mode, false otherwise
     * @param preventResizing true to use prevent UI from resizing, false otherwise
     */
    fun hideSystemUI(hideNavigationBar: Boolean = false, bigInteration: Boolean = true, preventResizing: Boolean = true) {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        val noFlag = 0x00000000
        window.decorView.systemUiVisibility =
            (if (preventResizing) View.SYSTEM_UI_FLAG_LAYOUT_STABLE else noFlag)
                .or(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
                .or(if (hideNavigationBar) View.SYSTEM_UI_FLAG_HIDE_NAVIGATION else noFlag) // hide nav bar
                .or(if (hideNavigationBar) View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION else noFlag) // hide nav bar and it's parent
                .or(View.SYSTEM_UI_FLAG_FULLSCREEN) // hide status bar
                .or(if (bigInteration) View.SYSTEM_UI_FLAG_IMMERSIVE else noFlag)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    fun isDeviceLocked() =
        (this.context().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager).inKeyguardRestrictedInputMode()

    override fun context(): Context {
        return this
    }

    override fun application(): Application {
        return this.application
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Logger.t(TAG).d("Life_Cycle: onBackPressed")
        finish()
    }

    override fun finish() {
        Logger.t(TAG).d("Life_Cycle: finish")
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
        super.finish()
    }

}