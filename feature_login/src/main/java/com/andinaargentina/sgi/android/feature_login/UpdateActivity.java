package com.andinaargentina.sgi.android.feature_login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.andinaargentina.core_services.router.ViewsNames;


/**
 * Created by Andina on 05/06/18.
 */
@Route(path = ViewsNames.ROUTE_VIEW_NAME_UPDATE)
public class UpdateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        setContentView(R.layout.update_layout);
        openPlaystore(null);
    }

    public void openPlaystore(@Nullable View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setData(Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
        intent.setData(Uri.parse("market://details?id=" + "com.anychart.anychart"));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        finish();
    }
}
