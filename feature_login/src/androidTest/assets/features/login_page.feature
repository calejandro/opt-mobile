Feature: Login Page
  Como usuario de la aplicación Mobile
  Quiero loguearme en la app
  Para  ser identificado en el sistema y obtener los permisos necesarios

  #Escenarios base de device
  @ignore @manual
  Scenario: Verifico la orientación del device
    Given Estoy en la vista Login
    And La orientación del dispositivo es Portrait-Mode
    When Giro el device a Landscape-Mode
    Then La vista seguirá mostrando sus componentes en Portrait-Mode

  @ignore @TODO @manual
  Scenario Outline: Verifico el reinicio de la vista
    Given Estoy en la vista Login
    And Ingreso "<email>" and "<password>"
    When Salgo de la aplicación con el botón de menu
    And Regreso seleccionando la app desde la pila de aplicaciones
    Then La app debe seguir mostrando los campos "<email>" and "<password>" ingresados
    Examples:
      | email               | password    |
      | DarwoftAdmin        | Darwoft1234 |

  #Escenarios de navegabilidad
  @ignore @automatic
  Scenario: Verifico navegabilidad hacia atrás
    Given Estoy en la vista Login
    And Ingreso "<email>" and "<password>"
    When Salgo de la aplicación con el botón back
    Then Debo visualizar la vista de menú principal de Android

  @ignore @manual @TODO @INCOMPLETE_REQUERIMENT
  Scenario: Verifico navegabilidad hacia adelante como usuario <Perfil 1>
    Given Estoy en la vista Login
    And Hay conexión a internet
    And Ingreso "perfil1@cocaandina.com" and "password_de_perfil_1"
    And Veré un dialogo de progreso
    When El login sea exitoso
    When Salgo de la aplicación con el botón back
    Then Debo visualizar la vista de menú principal de Android
    Then El dialogo de progreso estará invisible
    And Veré un dialogo de login exitoso
    And La aplicación me redirige a la pantalla <Perfil1>

  @ignore @manual @TODO @INCOMPLETE_REQUERIMENT
  Scenario: Verifico navegabilidad hacia adelante como usuario <Perfil 2>
    Given Estoy en la vista Login
    And Hay conexión a internet
    And Ingreso "perfil2@cocaandina.com" and "password_de_perfil_2"
    And Veré un dialogo de progreso
    When El login sea exitoso
    When Salgo de la aplicación con el botón back
    Then Debo visualizar la vista de menú principal de Android
    Then El dialogo de progreso estará invisible
    And Veré un dialogo de login exitoso
    And La aplicación me redirige a la pantalla <Perfil2>

  #Escenarios commons de http
  @ignore @manual @TODO
  Scenario: Verifico Login sin conexión a internet
    Given Estoy en la vista Login
    And El dispositivo no tiene conexión a Internet
    Then La notificación "Usted no posee internet" está visible

  @ignore
  @TODO @manual @TODO
  Scenario: Verifico reconexión exitosa de login cuando no halla internet
    Given Estoy en la vista Login
    And El dispositivo no tiene conexión a Internet
    And La notificación "Usted no posee internet" está visible
    And Pasaron 3 segundos de espera
    When El dispositivo recupera la conexión a internet
    Then No debo ver la notificación "Usted no posee internet"

  @ignore @TODO
  Scenario Outline: Verifico Login con problemas de respuesta del servidor
    Given Estoy en la pantalla Login
    And Hay conexión a internet
    And Me logueo usando "<email>" and "<password>"
    And Veo un dialogo de progreso
    When El servidor devuelve un error
    Then El dialogo de progreso estará invisible
    And Debo ver la notificación "Error con el servidor"

    Examples:
      | email                   | password    |
      | error404@cocaandina.com | Darwoft1234 |
      | error500@cocaandina.com | Darwoft1234 |
      | error440@cocaandina.com | Darwoft1234 |

  @ignore @manual @TODO @FALTAN_USER_TEST
  Scenario Outline: Verifico login con comunicación interrumpida
    Given Estoy en la pantalla Login
    And Hay conexión a internet
    And Me logueo usando "<email>" and "<password>"
    And Veo un dialogo de progreso
    When El dispositivo se queda sin internet
    Then El dialogo de progreso estará invisible
    And Debo ver la notificación "Error en la comunicación"

    Examples:
      | email               | password    |
      | DarwoftAdmin        | Darwoft1234 |

  #Escenarios base de feature
  @ignore @automatic
  Scenario Outline: Verifico login exitoso
    Given Estoy en la pantalla Login
    And El dispositivo tiene conexión a Internet
    And Me logueo usando "<email>" and "<password>"
    And El dialogo de progreso está visible
    When El login sea exitoso
    Then El dialogo de progreso estará invisible
    And Veré un dialogo de login exitoso

    Examples:
      | email               | password    |
      | DarwoftAdmin        | Darwoft1234 |

  @ignore @automatic
  Scenario Outline: Verifico interrupción de la carga del Login por el usuario
    Given Estoy en la pantalla de Login
    And El dispositivo tiene conexión a Internet
    And Me logueo usando "<email>" and "<password>"
    And El icono del progress está visible
    When El usuario sale de la pantalla Login
    Then Los procesos iniciales deben ser interrumpidos
    Examples:
      | email               | password    |
      | DarwoftAdmin        | Darwoft1234 |

  @ignore @automatic
  Scenario Outline: Verifico login con credenciales inválidas
    Given Estoy en la pantalla Login
    And El dispositivo tiene conexión a Internet
    And Me logueo usando "<email>" and "<password>"
    And El icono del progress está visible
    When El login finalize
    Then Debo ver la notificación "contraseña y pass inválidos"
    Examples:
      | email               | password    |
      | DarwoftAdmin        | Darwoft1234 |